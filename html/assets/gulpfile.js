var gulp = require('gulp');
var less = require('gulp-less');
var rename = require ('gulp-rename');

gulp.task('less', function(){
    gulp.src('less/bootstrap.less')
    .pipe(less())
    .pipe(rename('custom.css'))
    .pipe(gulp.dest('css'))
});

gulp.task('default', function(){
//
});