<?php

class NotificationCommand extends CConsoleCommand
{
    /**
     * @cron 1 1 * * *
     */
    public function actionIndex()
    {
//        $OneWeek = Users::model()->findAllByPk(1);
//        $TwoWeek = Users::model()->findAllByPk(1);

        $OneWeek = $this->findUsers(date('Y-m-d', strtotime('+1 week')));
        $TwoWeek = $this->findUsers(date('Y-m-d', strtotime('+2 week')));

        $this->envioCorreoNotificacion($OneWeek,1);
        $this->envioCorreoNotificacion($TwoWeek,2);

        Yii::log('prueba', CLogger::LEVEL_ERROR, 'Cron prueba');
    }

    protected function findUsers($date)
    {
        $criteria = new CDbCriteria();

	    $criteria->together=true;
        $criteria->with=['userProducts','userProducts.idProduct0'];
        $criteria->having = new CDbExpression('DATE_FORMAT(max(userProducts.dueDate),"%Y-%m-%d")').' = :duedate';
        $criteria->group='t.id,userProducts.idProduct';
        $criteria->params=[':duedate'=>$date];

//	    $userControl = Users::model()->findAllByPk([1,4006]);
//        $userControl = array_merge($userControl,Users::model()->findAll($criteria));
        $userControl = Users::model()->findAll($criteria);

        return $userControl;
    }
    protected function envioCorreoNotificacion($users,$weeks)
    {
        if(is_array($users))
        {
            foreach ($users as $user) {
//
                if(is_array($user->userProducts) and $user->userProducts!=array())
                {
                    foreach ($user->userProducts as $userProduct)
                    {
//                        $userProduct= new UserProduct();
//                        CVarDumper::dump($users,10);exit;
                        $params['to'] = $user->email;
//                        $params['id']
                        $this->sendMail($params,$weeks,$userProduct,$userProduct->idProduct0->productCode);
                    }
                }
            }
        }
    }
    protected function sendMail($params,$weeks,$userProduct,$productoCode = 'P0001')
    {
        $message = new YiiMailMessage;

        switch ($weeks)
        {
            case 1:
                $message->subject = 'Metabolic Analytics - Your software will expire in 7 days.';
                $message->view = 'notificationSupervisor'.$productoCode;
                $link = $userProduct->idProduct0->urlPayOneWeek;
                break;
            case 2:
                $message->subject = 'Metabolic Analytics - Your software will expire in 2 weeks.';
                $message->view = 'notificationSupervisor2Weeks'.$productoCode;
                $link = $userProduct->idProduct0->urlPayTwoWeeks;
                break;
        }

        $message->setBody(array('model' => '', 'topic' => "",'link'=>$link),'text/html');

        $message->addTo($params['to']);
        if(isset($params['cco']) and !empty($params['cco']))
            $message->addBcc($params['cco']);

        $message->from = Yii::app()->mail->transportOptions['username'];

        Yii::app()->mail->send($message);
    }
    public function actionTestnotifications()
    {

        $OneWeek = $this->findUsers(date('Y-m-d', strtotime('+1 week')));
        $TwoWeek = $this->findUsers(date('Y-m-d', strtotime('+2 week')));

        CVarDumper::dump($OneWeek,10);
        CVarDumper::dump($TwoWeek,10);
        exit;

        $OneWeek = Users::model()->findAllByPk(1);

        if(is_array($OneWeek))
        {
            foreach ($OneWeek as $user) {
                $params['to'] = $user->email;
                $this->sendMail($params,1,$user->userProducts[0],'P0001');
                $this->sendMail($params,2,$user->userProducts[0],'P0001');
                $this->sendMail($params,2,$user->userProducts[0],'P0002');
            }
        }
        Yii::log('prueba', CLogger::LEVEL_ERROR, 'Cron prueba modificaciones correos');
    }
}
