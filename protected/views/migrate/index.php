<?php
$script =<<<JS
    $(document).on('click','#cortina',function(){
		event.preventDefault();
		$('.bootstrap-filestyle').css('z-index','0');
		$('.main-container').addClass('grid-view-loading');
		$('#migrate-form').submit();
    });
JS;
Yii::app()->clientScript->registerScript('scriptCollapse',$script,CClientScript::POS_READY);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript(
	'myHideEffect',
	'$(".alert").animate({opacity: 1.0}, 5000).fadeOut("slow");',
	CClientScript::POS_READY
);
?>
<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
<div class="col-sm-12 main-content">
	<?php $flashMessages = Yii::app()->user->getFlashes(); ?>
	<?php if (count($flashMessages)) :?>
		<div class="alert success">
			<?php
			if ($flashMessages) {
				echo '<ul class="flashes">';
				foreach($flashMessages as $key => $message) {
					echo '<li><div class="flash-' . $key . '">' . $message . "</div></li>\n";
				}
				echo '</ul>';
			}
			?>
		</div>
	<?php endif;?>
	<?php echo CHtml::beginForm('','post',['enctype'=>'multipart/form-data','novalidate'=>'novalidate','id'=>'migrate-form']) ?>
	<div class="col-md-12">
		&nbsp;
	</div>
	<div class="col-md-12">
		<?php echo CHtml::errorSummary($model) ?>
	</div>
	<div class="form-group col-md-6">
		<?php echo CHtml::activeFileField($model,'filecsv',array('class'=>"form-control filestyle")); ?>
	</div>
	<div class="col-md-6">
		<?php echo CHtml::submitButton('Load File',array('class' => 'btn btn-success btn-block','id'=>'cortina')); ?>
	</div>
	<?php echo CHtml::endForm() ?>
</div>
