<?php
/* @var $this ExerciseController */
/* @var $model Exercise */
?>

<h1>Update Exercise <?php echo $model->description; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>