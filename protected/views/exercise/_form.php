<?php
/* @var $this ExerciseController */
/* @var $model Exercise */
/* @var $form CActiveForm */
/********************************************************* Ids Selects ****************************************/
$idMethodElement= '#'.CHtml::activeId($model,'idMethod');
$idBodyAreaElement = '#'.CHtml::activeId($model,'idBodyArea');
$idFamilyExerciseElement = '#'.CHtml::activeId($model,'idFamilyExercise');
$idOrientationElement = '#'.CHtml::activeId($model,'idOrientation');
$idImplementElement = '#'.CHtml::activeId($model,'idImplement');
$idGripElement = '#'.CHtml::activeId($model,'idGrip');

/********************************************************* URLS ****************************************/
$baseUrl = Yii::app()->getBaseUrl(true);
$urlDropsBodyArea = $this->createAbsoluteUrl('exercise/dropsBodyArea');
$urlDropsFamilyExercise = $this->createAbsoluteUrl('exercise/dropsFamilyExercise');
$urlDropsOrientation = $this->createAbsoluteUrl('exercise/dropsOrientation');
$urlDropsImplement = $this->createAbsoluteUrl('exercise/dropsImplement');
$urlDropsGrip = $this->createAbsoluteUrl('exercise/dropsGrip');

/********************************************************* Carga de los Dropdowns ****************************************/
$jsLoadSelects=<<<JS
function clearSelect(level) {
  switch(level) {
    case 1:
        $('$idBodyAreaElement').val('');
        $('$idBodyAreaElement').html('<option value="">Select.......</option>');
    case 2:
        $('$idFamilyExerciseElement').val('');
        $('$idFamilyExerciseElement').html('<option value="">Select.......</option>');
    case 3:
        $('$idOrientationElement').val('');
        $('$idOrientationElement').html('<option value="">Select.......</option>');
    case 4:
        $('$idImplementElement').val('');
        $('$idImplementElement').html('<option value="">Select.......</option>');
    case 5:
        $('$idGripElement').val('');
        $('$idGripElement').html('<option value="">Select.......</option>');
   }
}

function populationBodyArea(){
    $.ajax({
        url: '{$urlDropsBodyArea}',
        data: {id:$('$idMethodElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            clearSelect($('$idMethodElement').data('level'));
            $('$idBodyAreaElement').val('');
           
            $('$idBodyAreaElement').html(response.BodyArea);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
function populationFamilyExercise(){     
     $.ajax({
        url: '{$urlDropsFamilyExercise}',
        data: {id:$('$idBodyAreaElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            clearSelect($('$idBodyAreaElement').data('level'));
            $('$idFamilyExerciseElement').val('');
            $('$idFamilyExerciseElement').html(response.FamilyExercise);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
function populationOrientation(){
    $.ajax({
        url: '{$urlDropsOrientation}',
        data: {id:$('$idFamilyExerciseElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            clearSelect($('$idFamilyExerciseElement').data('level'));
            $('$idOrientationElement').val('');
            $('$idOrientationElement').html(response.Orientation);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
function populationImplement(){
    $.ajax({
        url: '{$urlDropsImplement}',
        data: {id:$('$idOrientationElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            clearSelect($('$idOrientationElement').data('level'));
            $('$idImplementElement').val('');
            $('$idImplementElement').html(response.Implement);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
function populationGrip(){
    $.ajax({
        url: '{$urlDropsGrip}',
        data: {id:$('$idImplementElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            clearSelect($('$idImplementElement').data('level'));
            $('$idGripElement').val('');
            $('$idGripElement').html(response.Grip);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
/*************************************************** Funciones para el evento change  ************************************/
$(document).on('change','$idMethodElement',populationBodyArea);
$(document).on('change','$idBodyAreaElement',populationFamilyExercise);
$(document).on('change','$idFamilyExerciseElement',populationOrientation);
$(document).on('change','$idOrientationElement',populationImplement);
$(document).on('change','$idImplementElement',populationGrip);

JS;
Yii::app()->clientScript->registerScript('jsLoadSelects',$jsLoadSelects, CClientScript::POS_READY);

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'exercise-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'description'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'description',array('class'=>"control-label")); ?>
        <?php echo $form->textField($model,'description',array('class'=>"form-control",'maxlength'=>255)); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'idMethod'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'idMethod',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($model,'idMethod',CHtml::listData(Method::model()->findAll(['order'=>'description']),'id','description'),array('data-level'=>'1','class'=>"form-control",'empty'=>'Select ...')); ?>
    </div>

    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'idBodyArea'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'idBodyArea',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($model,'idBodyArea',CHtml::listData(BodyArea::model()->findAll(['condition'=>'idMethod=:idMethod','order'=>'description','params'=>['idMethod'=>$model->idMethod]]),'id','description'),array('data-level'=>'2','class'=>"form-control",'empty'=>'Select ...')); ?>
    </div>

    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'idFamilyExercise'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'idFamilyExercise',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($model,'idFamilyExercise',CHtml::listData(FamilyExercise::model()->findAll(['condition'=>'bodyAreaId=:bodyAreaId','order'=>'description','params'=>['bodyAreaId'=>$model->idBodyArea]]),'id','description'),array('data-level'=>'3','class'=>"form-control",'empty'=>'Select ...')); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'idOrientation'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'idOrientation',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($model,'idOrientation',CHtml::listData(Orientation::model()->findAll(['condition'=>'idFamily_exercise=:idFamily_exercise','order'=>'description','params'=>['idFamily_exercise'=>$model->idFamilyExercise]]),'id','description'),array('data-level'=>'4','class'=>"form-control",'empty'=>'Select ...')); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'idImplement'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'idImplement',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($model,'idImplement',CHtml::listData(Implement::model()->findAll(['condition'=>'idOrientation=:idOrientation','order'=>'description','params'=>['idOrientation'=>$model->idOrientation]]),'id','description'),array('data-level'=>'5','class'=>"form-control",'empty'=>'Select ...')); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'idGrip'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'idGrip',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($model,'idGrip',CHtml::listData(Grip::model()->findAll(['condition'=>'idImplement=:idImplement','order'=>'description','params'=>['idImplement'=>$model->idImplement]]),'id','description'),array('data-level'=>'6','class'=>"form-control",'empty'=>'Select ...')); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'idRangeMotion'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'idRangeMotion',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($model,'idRangeMotion',CHtml::listData(RangeMotion::model()->findAll(['order'=>'description']),'id','description'),array('class'=>"form-control",'empty'=>'Select ...')); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'idLaterality'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'idLaterality',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($model,'idLaterality',CHtml::listData(Laterality::model()->findAll(['order'=>'description']),'id','description'),array('class'=>"form-control",'empty'=>'Select ...')); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'video'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'video',array('class'=>"control-label")); ?>
        <?php echo $form->textField($model,'video',array('class'=>"form-control",'maxlength'=>255)); ?>
    </div>
    <div class="form-group col-md-12 <?php echo !empty($form->error($model,'notes'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'notes',array('class'=>"control-label")); ?>
        <?php echo $form->textArea($model,'notes',array('class'=>"form-control")); ?>
    </div>


    <div class="row form-actions">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->