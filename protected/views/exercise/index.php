<?php
/* @var $this ExerciseController */
/* @var $model Exercise */

?>

<h1>Exercises</h1>
<div class="col-sm-12 form-container">
    <div class="text-right" style="margin-bottom: 10px">
        <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('exercise/create')?>"><i class="fa fa-plus-circle"></i> New Exercise</a>
    </div>
</div>
<div class="col-sm-12 main-content">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'exercise-grid',
        'dataProvider'=>$model->searchSql(),
        'filter'=>$model,
        'columns'=>array(
            [
                'header'=>'Exercise Name',
                'name'=>'description',
                'value'=>'!empty($data["exerciseDescription"])?$data["exerciseDescription"]:""',
                'filter'=>CHtml::activeTextField($model,'description',['class'=>"form-control",'style'=>'width: 50px;'])
            ],
            [
                'header'=>'Strength Quality',
                'name'=>'Methoddescription',
                'value'=>'!empty($data["methodDescription"])?$data["methodDescription"]:""',
                'filter'=>CHtml::activeDropDownList($model,'Methoddescription',$selects["methodSelect"] ,['class'=>"form-control",'empty'=>'Select ...'])
            ],
            [
                'header'=>'Body Area',
                'name'=>'BodyAreadescription',
                'value'=>'!empty($data["bodyAreaDescription"])?$data["bodyAreaDescription"]:""',
                'filter'=>CHtml::activeDropDownList($model,'BodyAreadescription',$selects["bodyAreaSelect"],['class'=>"form-control",'empty'=>'Select ...'])
            ],
            [
                'header'=>'Exercise Family',
                'name'=>'FamilyExercisedescription',
                'value'=>'!empty($data["familyExerciseDescription"])?$data["familyExerciseDescription"]:""',
                'filter'=>CHtml::activeDropDownList($model,'FamilyExercisedescription',$selects["familyExerciseSelect"],['class'=>"form-control",'empty'=>'Select ...'])
            ],
            [
                'header'=>'Orientation',
                'name'=>'Orientationdescription',
                'value'=>'!empty($data["orientationDescription"])?$data["orientationDescription"]:""',
                'filter'=>CHtml::activeDropDownList($model,'Orientationdescription',$selects["orientationSelect"],['class'=>"form-control",'empty'=>'Select ...'])
            ],
            [
                'header'=>'Implement',
                'name'=>'Implementdescription',
                'value'=>'!empty($data["implementDescription"])?$data["implementDescription"]:""',
                'filter'=>CHtml::activeDropDownList($model,'Implementdescription',$selects["implementSelect"],['class'=>"form-control",'empty'=>'Select ...'])
            ],
            [
                'header'=>'Grip/Stance',
                'name'=>'Gripdescription',
                'value'=>'!empty($data["gripDescription"])?$data["gripDescription"]:""',
                'filter'=>CHtml::activeDropDownList($model,'Gripdescription',$selects["gripSelect"],['class'=>"form-control",'empty'=>'Select ...'])
            ],
            [
                'header'=>'Range',
                'name'=>'RangeMotiondescription',
                'value'=>'!empty($data["rangeMotionDescription"])?$data["rangeMotionDescription"]:""',
                'filter'=>CHtml::activeDropDownList($model,'RangeMotiondescription',$selects["rangeSelect"],['class'=>"form-control",'empty'=>'Select ...'])
            ],
            [
                'header'=>'Laterality',
                'name'=>'Lateralitydescription',
                'value'=>'!empty($data["lateralityDescription"])?$data["lateralityDescription"]:""',
                'filter'=>CHtml::activeDropDownList($model,'Lateralitydescription',$selects["lateralitySelect"],['class'=>"form-control",'empty'=>'Select ...'])
            ],
            array(
                'class'=>'CButtonColumn',
                'template'=>'{xEdit}{xDelete}',
                'header'=>"  Actions  ",
                'buttons'=>[
                    'xEdit'=>array(
                        'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createUrl("exercise/update", array("id"=>$data["idExercise"]))',
                        'options'=>array('title'=>'Edit','class'=>'xEdit')
                    ),
                    'xDelete'=>array(
                        'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createUrl("exercise/delete", array("id"=>$data["idExercise"]))',
                        'options'=>array('title'=>'Delete'),
                        'click'=>"function() {
							if(!confirm('Are you sure you want to delete this item?')) return false;
							var th = this,afterDelete = function(){};
							jQuery('#exercise-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
								    if(data.length>0)
								        alert(data);
									jQuery('#exercise-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
						}"
                    ),
                ]
            ),

        ),
    )); ?>
