<?php
/* @var $this ExerciseController */
/* @var $model Exercise */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'order'); ?>
		<?php echo $form->textField($model,'order',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idMethod'); ?>
		<?php echo $form->textField($model,'idMethod'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idBodyArea'); ?>
		<?php echo $form->textField($model,'idBodyArea'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idFamilyExercise'); ?>
		<?php echo $form->textField($model,'idFamilyExercise'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idOrientation'); ?>
		<?php echo $form->textField($model,'idOrientation'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idImplement'); ?>
		<?php echo $form->textField($model,'idImplement'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idGrip'); ?>
		<?php echo $form->textField($model,'idGrip'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idRangeMotion'); ?>
		<?php echo $form->textField($model,'idRangeMotion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idLaterality'); ?>
		<?php echo $form->textField($model,'idLaterality'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'notes'); ?>
		<?php echo $form->textArea($model,'notes',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'video'); ?>
		<?php echo $form->textField($model,'video',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->