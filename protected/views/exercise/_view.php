<?php
/* @var $this ExerciseController */
/* @var $data Exercise */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order')); ?>:</b>
	<?php echo CHtml::encode($data->order); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idMethod')); ?>:</b>
	<?php echo CHtml::encode($data->idMethod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idBodyArea')); ?>:</b>
	<?php echo CHtml::encode($data->idBodyArea); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idFamilyExercise')); ?>:</b>
	<?php echo CHtml::encode($data->idFamilyExercise); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idOrientation')); ?>:</b>
	<?php echo CHtml::encode($data->idOrientation); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('idImplement')); ?>:</b>
	<?php echo CHtml::encode($data->idImplement); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idGrip')); ?>:</b>
	<?php echo CHtml::encode($data->idGrip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idRangeMotion')); ?>:</b>
	<?php echo CHtml::encode($data->idRangeMotion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idLaterality')); ?>:</b>
	<?php echo CHtml::encode($data->idLaterality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notes')); ?>:</b>
	<?php echo CHtml::encode($data->notes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('video')); ?>:</b>
	<?php echo CHtml::encode($data->video); ?>
	<br />

	*/ ?>

</div>