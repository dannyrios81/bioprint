<?php
/* @var $this BodyAreaController */
/* @var $model BodyArea */

$this->breadcrumbs=array(
	'Body Areas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List BodyArea', 'url'=>array('index')),
	array('label'=>'Create BodyArea', 'url'=>array('create')),
	array('label'=>'Update BodyArea', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BodyArea', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BodyArea', 'url'=>array('admin')),
);
?>

<h1>View BodyArea #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
	),
)); ?>
