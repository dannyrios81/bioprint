<div class="col-sm-12 main-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'country-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'name'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'name',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'name',array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-3 <?php echo !empty($form->error($model,'code'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'code',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'code',array('class'=>"form-control")); ?>
	</div>
    <div class="form-group col-md-3 <?php echo !empty(CHtml::error($model,'exclusive'))?'has-error':''?>">
        <?php echo CHtml::activeLabelEx($model,'exclusive',array('class'=>"control-label")); ?>
        <?php echo CHtml::activeCheckBox($model,'exclusive',array('class'=>"form-control",'style'=>'opacity: 1;width:100%')); ?>
    </div>

	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'notesRecipes'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'notesRecipes',array('class'=>"control-label")); ?>
		<?php echo $form->textArea($model,'notesRecipes',array('class'=>"form-control")); ?>
		<?php $this->widget('ImperaviRedactorWidget', ['selector' => '#'.CHtml::activeId($model,'notesRecipes'),'options' => ["minHeight"=> 300],]); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->