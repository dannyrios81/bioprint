<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#country-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
$scriptEnable =<<<JS
    function modalNotes(text)
    {
		$("#ajaxModal .modal-body").empty().html(text);
		$("#ajaxModal").modal();
		console.log(text);
    }
JS;
Yii::app()->clientScript->registerScript('scriptEnable',$scriptEnable,CClientScript::POS_END);
?>

<h1>Manage Zones</h1>
<hr class="separator">
<div class="col-sm-12 form-container">
	<div class="text-right">
		<a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('country/create')?>"><i class="fa fa-plus-circle"></i> New Zone</a>
	</div>
	<br>
</div>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'country-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
		'name',
		'code',
//		'notesRecipes',
		[
			'header'=>'Note',
			'type'=>'raw',
			'value'=>'
				CHtml::link(
					(CHtml::openTag("i",["class"=>"fa fa-file-text fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"]).CHtml::closeTag("i").CHtml::openTag("i",["class"=>"fa fa-file-text visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"]).CHtml::closeTag("i")),
				"#",
				["onClick"=>"modalNotes(\"$data->notesRecipes\")"]
				)'
		],
		array(
			'class'=>'CButtonColumn',
			'template'=>'{xupdate}{xdelete}',
			'header'=>"  Actions  ",
			'buttons'=>array(
				'xupdate'=>array(
					'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								<i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
					'url'=>'Yii::app()->createUrl("country/update", array("id"=>$data->id))',
					'options'=>array('title'=>'Update country')
				),
				'xdelete'=>array(
					'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>',
					'url'=>'Yii::app()->createUrl("country/delete", array("id"=>$data->id))',
					'options'=>array('title'=>'Delete zone'),
					'visible'=>'((count($data->recipes)==0 && count($data->supplements)==0 && count($data->users)==0)?true:false)',
					'click'=>"function() {
							if(!confirm('Are you sure you want to delete this item?')) return false;
							var th = this,afterDelete = function(){};
							jQuery('#country-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
									jQuery('#country-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
						}"
				),
			),
		)
	),
)); ?>
</div>
<div class="modal fade" id="ajaxModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Notes</h4>
			</div>
			<div class="modal-body" style="text-align: left">
				XXX XXXXXXXXXXXXXXX XXXXXXXXXX XXXXX
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>