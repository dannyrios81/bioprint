<?php
/* @var $this RangeMotionController */
/* @var $model RangeMotion */

$this->breadcrumbs=array(
	'Range Motions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List RangeMotion', 'url'=>array('index')),
	array('label'=>'Create RangeMotion', 'url'=>array('create')),
	array('label'=>'Update RangeMotion', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete RangeMotion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage RangeMotion', 'url'=>array('admin')),
);
?>

<h1>View RangeMotion #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
		'methodId',
	),
)); ?>
