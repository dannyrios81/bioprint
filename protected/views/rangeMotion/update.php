<?php
/* @var $this RangeMotionController */
/* @var $model RangeMotion */
?>

<h1>Update Range <?php echo $model->description; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>