<?php
/* @var $this RangeMotionController */
/* @var $model RangeMotion */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'range-motion-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="form-group col-md-12 <?php echo !empty($form->error($model,'description'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'description',array('class'=>"control-label")); ?>
        <?php echo $form->textField($model,'description',array('class'=>"form-control")); ?>
    </div>

    <div class="row form-actions">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->