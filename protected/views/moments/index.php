<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#moments-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Intakes</h1>
<hr class="separator">
<div class="col-sm-12 form-container">
	<div class="text-right">
		<a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('moments/create')?>"><i class="fa fa-plus-circle"></i> New Intake</a>
	</div>
</div>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'moments-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
		'name',
		'priority',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{xupdate}{xdelete}',
			'header'=>"  Actions  ",
			'buttons'=>array(
				'xupdate'=>array(
					'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								<i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
					'url'=>'Yii::app()->createUrl("moments/update", array("id"=>$data->id))',
					'options'=>array('title'=>'Update moments')
				),
				'xdelete'=>array(
					'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								<i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
					'url'=>'Yii::app()->createUrl("moments/delete", array("id"=>$data->id))',
					'options'=>array('title'=>'Delete moments'),
					'visible' => '(count($data->chooses)>0?false:true)',
					'click'=>"function() {
						if(!confirm('Are you sure you want to delete this item?')) return false;
						var th = this,afterDelete = function(){};
							jQuery('#moments-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
									jQuery('#moments-grid').yiiGridView('update');
									afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
					}"
				),
			),
		)
	),
)); ?>
</div>