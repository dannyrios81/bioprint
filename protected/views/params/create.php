<?php
/* @var $this ParamsController */
/* @var $model Params */

$this->breadcrumbs=array(
	'Params'=>array('index'),
	'Create',
);

?>

<h1>Create Param</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>