<?php
/* @var $this ParamsController */
/* @var $model Params */

$this->breadcrumbs=array(
	'Params'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#params-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Params</h1>
<hr class="separator">
<div class="col-sm-12 form-container">
    <div class="text-right">
        <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('params/create')?>"><i class="fa fa-plus-circle"></i> New Param</a>
    </div>
</div>

<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'params-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'paramDescription',
		'codParam',
		'valueParam',
		array(
            'class'=>'CButtonColumn',
            'template'=>'<div>{xupdate}</div>',
            'header'=>"  Actions  ",
            'buttons'=>array(
                'xupdate'=>array(
                    'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
                    'url'=>'Yii::app()->createUrl("params/update", array("id"=>$data->id))',
                    'options'=>array('title'=>'Update param')
                )
            ),
		),
	),
)); ?>
</div>