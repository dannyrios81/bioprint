<?php
/* @var $this ParamsController */
/* @var $data Params */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paramDescription')); ?>:</b>
	<?php echo CHtml::encode($data->paramDescription); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codParam')); ?>:</b>
	<?php echo CHtml::encode($data->codParam); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valueParam')); ?>:</b>
	<?php echo CHtml::encode($data->valueParam); ?>
	<br />


</div>