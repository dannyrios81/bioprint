<?php
/* @var $this ParamsController */
/* @var $model Params */
/* @var $form CActiveForm */
?>

<div class="col-sm-12 main-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'params-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'paramDescription'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'paramDescription',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'paramDescription',array('class'=>"form-control",'size'=>60,'maxlength'=>255)); ?>
	</div>

    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'codParam'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'codParam',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'codParam',array('class'=>"form-control",'size'=>20,'maxlength'=>20,'disabled'=>$model->isNewRecord ? "" : 'disabled')); ?>
	</div>

    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'valueParam'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'valueParam',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'valueParam',array('class'=>"form-control",'size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->