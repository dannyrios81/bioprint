<?php
/* @var $this RestController */
/* @var $model Rest */

$this->breadcrumbs=array(
	'Rests'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Rest', 'url'=>array('index')),
	array('label'=>'Create Rest', 'url'=>array('create')),
	array('label'=>'Update Rest', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Rest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Rest', 'url'=>array('admin')),
);
?>

<h1>View Rest #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
		'methodId',
	),
)); ?>
