<?php
/* @var $this GripController */
/* @var $model Grip */

$this->breadcrumbs=array(
	'Grips'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Grip', 'url'=>array('index')),
	array('label'=>'Create Grip', 'url'=>array('create')),
	array('label'=>'Update Grip', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Grip', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Grip', 'url'=>array('admin')),
);
?>

<h1>View Grip #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
		'familyExerciseId',
	),
)); ?>
