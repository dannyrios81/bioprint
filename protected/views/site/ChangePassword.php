<?php
Yii::app()->clientScript->registerScript(
	'myHideEffect',
	'$(".alert").animate({opacity: 1.0}, 5000).fadeOut("slow");',
	CClientScript::POS_READY
);

$this->pageTitle=Yii::app()->name;
CHtml::$errorCss = 'has-error';
?>

<div class="col-sm-12">
	<h1>Change Password</h1>
	<p>
		<?php echo CHtml::errorSummary($model,null,null,['firstError'=>false]); ?>
	</p>
	<?php $flashMessages = Yii::app()->user->getFlashes(); ?>
	<?php if (count($flashMessages)) :?>
		<div class="alert success">
			<?php
			//$flashMessages = Yii::app()->user->getFlashes();
			//					CVarDumper::dump($flashMessages,10,true);exit;
			if ($flashMessages) {
				echo '<ul class="flashes">';
				foreach($flashMessages as $key => $message) {
					echo '<li><div class="flash-' . $key . '">' . $message . "</div></li>\n";
				}
				echo '</ul>';
			}
			?>
		</div>
	<?php endif;?>
	<h3 class="text-info">
		Fill out the form below to login.
	</h3>
</div>
<hr>
<div class="col-sm-12">
	<?php echo CHtml::beginForm() ?>
		<div class="form-group <?php echo !empty(CHtml::error($model,'currentPassword'))?'has-error':''?>">
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-unlock-alt">
					</i>
				</div>
				<?php echo CHtml::activePasswordField($model,'currentPassword',['class'=>"form-control",'placeholder'=>$model->getAttributeLabel('currentPassword'), 'autocomplete'=>"off"]); ?>
			</div>
		</div>
		<div class="form-group <?php echo !empty(CHtml::error($model,'newPassword'))?'has-error':''?>">
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-unlock-alt">
					</i>
				</div>
				<?php echo CHtml::activePasswordField($model,'newPassword',['class'=>"form-control",'placeholder'=>$model->getAttributeLabel('newPassword'), 'autocomplete'=>"off"]); ?>
			</div>
		</div>
		<div class="form-group <?php echo !empty(CHtml::error($model,'repeatNewPassword'))?'has-error':''?>">
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-unlock-alt">
					</i>
				</div>
				<?php echo CHtml::activePasswordField($model,'repeatNewPassword',['class'=>"form-control",'placeholder'=>$model->getAttributeLabel('repeatNewPassword'), 'autocomplete'=>"off"]); ?>
			</div>
		</div>
		<div class="form-group ">
			<?php echo CHtml::submitButton('Save',['class'=>"btn btn-primary btn-block" ]); ?>
		</div>

	<?php echo CHtml::endForm() ?>
</div>

