<?php
Yii::app()->clientScript->registerScript(
	'myHideEffect',
	'$(".alert").animate({opacity: 1.0}, 5000).fadeOut("slow");',
	CClientScript::POS_READY
);

$this->pageTitle=Yii::app()->name . ' - Forgot Password';
CHtml::$errorCss = 'has-error';
?>

<div class="col-sm-12">
	<h1>Forgot Password</h1>
	<br>
	<p>
		<?php echo CHtml::errorSummary($model,null,null,['firstError'=>true]); ?>
	</p>
	<?php $flashMessages = Yii::app()->user->getFlashes(); ?>
	<?php if (count($flashMessages)) :?>
	<div class="alert success">
		<?php
		//$flashMessages = Yii::app()->user->getFlashes();
//					CVarDumper::dump($flashMessages,10,true);exit;
		if ($flashMessages) {
			echo '<ul class="flashes">';
			foreach($flashMessages as $key => $message) {
				echo '<li><div class="flash-' . $key . '">' . $message . "</div></li>\n";
			}
			echo '</ul>';
		}
		?>
	</div>
	<?php endif;?>
</div>
<hr>
<div class="col-sm-12">
				<?php echo CHtml::beginForm() ?>
				<div class="form-group <?php echo !empty(CHtml::error($model,'email'))?'has-error':''?>">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-at">
							</i>
						</div>
						<?php echo CHtml::activeTextField($model,'email',['class'=>"form-control",'placeholder'=>$model->getAttributeLabel('email'), 'autocomplete'=>"off"]); ?>
					</div>
					<div class="forgot-pass text-right"><a href="<?php echo $this->createUrl('site/login'); ?>"><u>Sign In</u></a></div>
				</div>
				<div class="form-group ">
					<?php echo CHtml::submitButton('Continue',['class'=>"btn btn-primary btn-block" ]); ?>
				</div>
				<?php echo CHtml::endForm() ?>
			</div>
