<?php
/**
 * @var $model LoginForm
 */
Yii::app()->clientScript->registerScript(
	'myHideEffect',
	'$(".alert").animate({opacity: 1.0}, 5000).fadeOut("slow");',
	CClientScript::POS_READY
);

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
CHtml::$errorCss = 'has-error';
?>

<div class="col-sm-12">
	<h1>Login</h1>
	<h3 class="text-info">
		Fill out the form below to login.
	</h3>
	<p>
        <?php
            if($model->getError('password')=='1'):
                ?>
                    <div class="errorSummary"><p>Please fix the following input errors:</p>
                        <ul>
			    <li>Your license has expired. Please contact <a href="mailto:info@strength-community.com" style="text-decoration: underline #0A246A;color: #0A246A">info@strength-community.com</a> to renew your license.</li>
                        </ul>
                    </div>
        <?php
            else:
                echo CHtml::errorSummary($model);
            endif;
        ?>
    </p>
	<?php $flashMessages = Yii::app()->user->getFlashes(); ?>
	<?php if (count($flashMessages)) :?>
		<div class="alert success">
			<?php
			//$flashMessages = Yii::app()->user->getFlashes();
			//					CVarDumper::dump($flashMessages,10,true);exit;
			if ($flashMessages) {
				echo '<ul class="flashes">';
				foreach($flashMessages as $key => $message) {
					echo '<li><div class="flash-' . $key . '">' . $message . "</div></li>\n";
				}
				echo '</ul>';
			}
			?>
		</div>
	<?php endif;?>
</div>
<hr>
<div class="col-sm-12">
	<?php echo CHtml::beginForm('','post',['autocomplete'=>"false"]) ?>
		<div class="form-group <?php echo !empty(CHtml::error($model,'username'))?'has-error':''?>">
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-user">
					</i>
				</div>
				<?php echo CHtml::activeTextField($model,'username',['class'=>"form-control",'placeholder'=>$model->getAttributeLabel('username'), 'autocomplete'=>"false"]); ?>

			</div>
		</div>

		<div class="form-group <?php echo !empty(CHtml::error($model,'password'))?'has-error':''?>">
			<div class="input-group">
				<div class="input-group-addon">
					<i class="fa fa-unlock-alt">
					</i>
				</div>
				<?php echo CHtml::activePasswordField($model,'password',['class'=>"form-control",'placeholder'=>$model->getAttributeLabel('password'),'autocomplete'=>"off"]); ?>
			</div>
			<div class="forgot-pass text-right"><a href="<?php echo $this->createUrl('site/ForgotPassword'); ?>"><u>Forgot your password?</u></a></div>
		</div>


		<div class="form-group ">
			<?php echo CHtml::submitButton('Login',['class'=>"btn btn-primary btn-block" ]); ?>
		</div>

	<?php echo CHtml::endForm() ?>
</div>
