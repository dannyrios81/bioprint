<?php
$criteria = new CDbCriteria();
$criteria->order='name';
$criteria->addSearchCondition('calculable','0');

$idZoneSelect = CHtml::activeId($model,'idCountry');

$urlFormChoose =  Yii::app()->createAbsoluteUrl("recipes/formChoose");
$urlFormChooseSuplement =  Yii::app()->createAbsoluteUrl("recipes/formChooseSuplement");

$scriptEnable =<<<JS
    function newChoose()
    {
    	event.preventDefault();
		//alert('buenas');
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "{$urlFormChoose}",
			success: function(datos){
				$('#choose-container').append(datos.body);
				// blockZone();
			}
		});
		blockZone();
    }
    function newChooseSuplement(idChoose)
    {
    	event.preventDefault();
    	$.ajax({
			type: "POST",
			dataType: "json",
			url: "{$urlFormChooseSuplement}",
			data: {'id':idChoose,'idZone':$('#$idZoneSelect').val()},
			success: function(datos){
				$('#'+idChoose).append(datos.body);
				blockZone();
			}
		});
    }
    function removeMoment(idChoose)
    {
    	event.preventDefault();
    	$('#'+idChoose).remove();
    	blockZone();
    }
    function removeChooseMoment(item)
    {
    	event.preventDefault();
    	$('#'+item).remove();
    	blockZone();
    }
    function blockZone()
    {
    	if($('select[name^="ChooseSuplement"]').length>0)
    		$('#$idZoneSelect').attr('disabled','disabled');
    	else 
    		$('#$idZoneSelect').removeAttr('disabled');
    }
    function unblockZone()
    {
    	$('#$idZoneSelect').removeAttr('disabled');
    }
JS;
$ready=<<<JS
	if($('select[name^="ChooseSuplement"]').length>0)
    		$('#$idZoneSelect').attr('disabled','disabled');
    	else 
    		$('#$idZoneSelect').removeAttr('disabled');
JS;

Yii::app()->clientScript->registerScript('scriptEnable',$scriptEnable,CClientScript::POS_END);
Yii::app()->clientScript->registerScript('ready',$ready,CClientScript::POS_READY);
?>
<div class="col-sm-12 main-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'recipes-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

<div class="row">
	<?php echo $form->errorSummary($model); ?>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'idHormoneFamily'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'idHormoneFamily',array('class'=>"control-label")); ?>
		<?php echo $form->dropDownList($model,'idHormoneFamily',CHtml::listData(HormoneFamily::model()->findAll(),'id','name'),array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'idMeasurmenetTypes'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'idMeasurmenetTypes',array('class'=>"control-label")); ?>
		<?php echo $form->dropDownList($model,'idMeasurmenetTypes',CHtml::listData(MeasurementTypes::model()->findAll($criteria),'id','name'),array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'idCountry'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'idCountry',array('class'=>"control-label")); ?>
		<?php echo $form->dropDownList($model,'idCountry',CHtml::listData(Country::model()->findAll(),'id','name'),array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'notes'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'notes',array('class'=>"control-label")); ?>
		<?php echo $form->TextField($model,'notes',array('class'=>"form-control")); ?>
	</div>
</div>
	<div id="choose-container">
		<?php //CVarDumper::dump($info,10,true);exit; ?>
		<?php if(isset($info['Choose'])): ?>
			<?php foreach ($info['Choose'] as $key=>$item):?>
				<?php $temp=''?>
				<?php //CVarDumper::dump($info['ChooseSuplement'],10,true);exit; ?>
				<?php if(isset($info['ChooseSuplement'])):?>
					<?php foreach ($info['ChooseSuplement'][$key] as $index=>$value):?>
						<?php $temp=$temp.$this->renderPartial('_formChooseSuplement',['model'=>$value,'itemid'=>$index,'id'=>$key,'idZone'=>$model->idCountry],true)?>
					<?php endforeach; ?>
				<?php endif; ?>
				<?php $this->renderPartial('_formChoose',['model'=>$item,'id'=>$key,'content'=>$temp]) ?>
			<?php endforeach; ?>
		<?php endif;?>
	</div>
	
	<div class="row buttons">
		<div class="col-xs-6">
			<?php echo CHtml::link('Add Intake',Yii::app()->createAbsoluteUrl(""),['class' => 'btn btn-default btn-lg btn-block','onClick'=>'newChoose()']); ?>
		</div>
		<div class="col-xs-6">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block','onClick'=>'unblockZone()')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->