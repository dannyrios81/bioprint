<?php
if(!isset($id))
$id=microtime(true)*10000;
?>
<div class="form-horizontal row moment" id="<?php echo $id?>" style='padding-top: 10px;'>

    <div style='margin-left: 0;margin-right: 0' class="form-group col-md-4 <?php echo !empty(CHtml::error($model,"[$id]idMoment"))?'has-error':''?>">
        <?php echo CHtml::activeLabelEx($model,"[$id]idMoment",array('class'=>"control-label col-xs-3 col-md-5 col-lg-4","style"=>"text-align: left;")); ?>
        <div class="col-xs-9 col-md-7 col-lg-8">
            <?php echo CHtml::activeDropDownList($model,"[$id]idMoment",CHtml::listData(Moments::model()->findAll(array('order'=>'name')),'id','name'),array('class'=>"form-control")); ?>
        </div>

    </div>

    <div style='margin-left: 0;margin-right: 0' class="form-group col-md-6 <?php echo !empty(CHtml::error($model,"[$id]note"))?'has-error':''?>">
        <?php echo CHtml::activeLabelEx($model,"[$id]note",array('class'=>"control-label col-xs-3 col-md-2")); ?>
        <div class="col-xs-9 col-md-10">
            <?php echo CHtml::activeTextField($model,"[$id]note",array('class'=>"form-control")); ?>
        </div>
    </div>

    <div style='margin-left: 0;margin-right: 0' class="form-group col-md-2 text-center hidden-xs hidden-sm">
        <div class="col-md-6 col-lg-6">
            <?php echo CHtml::link('<i class="fa fa-plus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								<i class="fa fa-plus-circle fa-2x visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
                '#',
                [
                    //'class' => 'btn btn-success',
                    'onClick' => "newChooseSuplement('$id')",
                    'title'=>'Add Supplement'
                ]); ?>
        </div>
        <div class="col-md-6 col-lg-6">
            <?php echo CHtml::link('<i class="text-danger fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								<i class="text-danger fa fa-minus-circle fa-2x visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
                '#',
                [
                    //'class' => 'btn btn-success',
                    'onClick' => "removeMoment('$id')",
                    'title'=>'Remove Moment'
                ]); ?>
        </div>
    </div>
    <div style='margin-left: 0;margin-right: 0 z-index:1000;' class="form-group col-xs-12 text-center hidden visible-xs visible-sm">
        <div class="col-xs-6 col-sm-6">
            <?php echo CHtml::link('Add Supplement',
                '#',
                [
                    'class' => 'btn btn-success btn-block ',
                    'onClick' => "newChooseSuplement('$id')",
                    'title'=>'Add Supplement'
                ]); ?>
        </div>
        <div class="col-xs-6 col-sm-6">
            <?php echo CHtml::link('Remove Moment',
                '#',
                [
                    'class' => 'btn btn-danger btn-block',
                    'onClick' => "removeMoment('$id')",
                    'title'=>'Remove Moment'
                ]); ?>
        </div>

    </div>
    <?php if(isset($content)):?>
    <?php echo $content; ?>
    <?php endif ?>
</div>