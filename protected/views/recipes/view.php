<h1>View Recipes #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'idHormoneFamily',
		'idMeasurmenetTypes',
		'idCountry',
		'notes',
	),
)); ?>
