<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#recipes-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
$scriptEnable =<<<JS
    function modalNotes(text)
    {
		$("#ajaxModal .modal-body").empty().html(text);
		$("#ajaxModal").modal();
		console.log(text);
    }
JS;


$criteria = new CDbCriteria();
$criteria->order = 'name';
$criteria->addSearchCondition('calculable','0');

Yii::app()->clientScript->registerScript('scriptEnable',$scriptEnable,CClientScript::POS_END);
//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/tooltip.js', CClientScript::POS_END);
?>

<h1>Manage Protocols</h1>
<hr class="separator">
<div class="col-sm-12 form-container">
	<div class="text-right">
		<a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('recipes/create')?>"><i class="fa fa-plus-circle"></i> New Protocol</a>
	</div>
</div>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'recipes-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
		['header'=>'Hormone Family','name'=>'idHormoneFamily','value'=>'!empty($data->idHormoneFamily)?$data->idHormoneFamily0->name:""','filter'=>CHtml::activeDropDownList($model,'idHormoneFamily',CHtml::listData(HormoneFamily::model()->findAll(),'id','name'),['empty'=>'Select ...'])],
		['header'=>'Measurement Type','name'=>'idMeasurmenetTypes','value'=>'!empty($data->idMeasurmenetTypes)?$data->idMeasurmenetTypes0->name:""','filter'=>CHtml::activeDropDownList($model,'idMeasurmenetTypes',CHtml::listData(MeasurementTypes::model()->findAll($criteria),'id','name'),['empty'=>'Select ...'])],
		['header'=>'Country','name'=>'idCountry','value'=>'!empty($data->idCountry)?$data->idCountry0->name:""','filter'=>CHtml::activeDropDownList($model,'idCountry',CHtml::listData(Country::model()->findAll(),'id','name'),['empty'=>'Select ...'])],
		[
			'header'=>'Note',
			'type'=>'raw',
			'value'=>'
				CHtml::link(
					(CHtml::openTag("i",["class"=>"fa fa-file-text fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"]).CHtml::closeTag("i").CHtml::openTag("i",["class"=>"fa fa-file-text visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"]).CHtml::closeTag("i")),
				"#",
				["onClick"=>"modalNotes(\"$data->notes\")"]
				)'
		],
//		'notes',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{xupdate}{xdelete}',
			'header'=>"  Actions  ",
			'buttons'=>array(
				'xupdate'=>array(
					'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								<i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
					'url'=>'Yii::app()->createUrl("recipes/update", array("id"=>$data->id))',
					'options'=>array('title'=>'Update recipes')
				),
				'xdelete'=>array(
					'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								<i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
					'url'=>'Yii::app()->createUrl("recipes/delete", array("id"=>$data->id))',
					'options'=>array('title'=>'Delete recipes'),
					'click'=>"function() {
						if(!confirm('Are you sure you want to delete this item?')) return false;
						var th = this,afterDelete = function(){};
							jQuery('#recipes-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
									console.log(data);
									jQuery('#recipes-grid').yiiGridView('update');
									afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
					}"
				),
			),
		)
	),
)); ?>
</div>
<div class="modal fade" id="ajaxModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Notes</h4>
			</div>
			<div class="modal-body" style="text-align: justify;">
				XXX XXXXXXXXXXXXXXX XXXXXXXXXX XXXXX
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>