<?php
if(!isset($itemid))
{
    $str = (string)microtime(true);
    $itemid=substr($str,strpos($str,'.')+1);
}
$criteria = new CDbCriteria();
$criteria->order = 'name';
if(isset($idZone))
    $criteria->addSearchCondition('idCountry',$idZone,false);


?>
        <div id="Sup-<?php echo $itemid ?>">
            <div style='margin-left: 0;margin-right: 0' class="form-group col-md-6 <?php echo !empty(CHtml::error($model,"[$id][$itemid]idSuplement"))?'has-error':''?>">
                <?php echo CHtml::activeLabelEx($model,"[$id][$itemid]idSuplement",array('class'=>"control-label col-md-3 col-xs-3 text-nowrap","style"=>"text-align: left;")); ?>
                <div class="col-md-9 col-xs-9">
                    <?php echo CHtml::activeDropDownList($model,"[$id][$itemid]idSuplement",CHtml::listData(Supplement::model()->findAll($criteria),'id','name'),array('class'=>"form-control")); ?>
                </div>

            </div>

            <div style='margin-left: 0;margin-right: 0' class="form-group col-md-4 <?php echo !empty(CHtml::error($model,"[$id][$itemid]quantity"))?'has-error':''?>">
                <?php echo CHtml::activeLabelEx($model,"[$id][$itemid]quantity",array('class'=>"control-label col-md-7 col-xs-3")); ?>
                <div class="col-md-5 col-xs-9">
                    <?php echo CHtml::activeTextField($model,"[$id][$itemid]quantity",array('class'=>"form-control")); ?>
                </div>
            </div>
            <div style='margin-left: 0;margin-right: 0' class="form-group col-md-2 text-center hidden-xs hidden-sm">
                <div class="col-md-6 col-lg-6">

                </div>
                <div class="col-md-6 col-lg-6">
                    <?php echo CHtml::link('<i class="fa text-danger fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
                                        <i class="fa text-danger fa-minus-circle fa-2x visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
                        '#',
                        [
                            //'class' => 'btn btn-danger',
                            'onClick' => "removeChooseMoment('Sup-$itemid')",
                            'title'=>'Remove Supplement'
                        ]); ?>
                </div>

            </div>
            <div style='margin-left: 0;margin-right: 0' class="form-group col-md-2 text-center visible-xs visible-sm hidden">
                <?php echo CHtml::link('Remove Suplement',
                    '#',
                    [
                        'class' => 'btn btn-danger btn-block ',
                        'onClick' => "removeChooseMoment('Sup-$itemid')",
                        'title'=>'Remove Supplement'
                    ]); ?>
            </div>
        </div>