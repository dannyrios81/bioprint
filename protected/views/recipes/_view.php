<?php
<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idHormoneFamily')); ?>:</b>
	<?php echo CHtml::encode($data->idHormoneFamily); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idMeasurmenetTypes')); ?>:</b>
	<?php echo CHtml::encode($data->idMeasurmenetTypes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCountry')); ?>:</b>
	<?php echo CHtml::encode($data->idCountry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notes')); ?>:</b>
	<?php echo CHtml::encode($data->notes); ?>
	<br />


</div>