<?php
/**
 *
 * @var FaseController $this
 * @var \NewUserPhaseExerciseForm $model
 */

$baseUrl = Yii::app()->getBaseUrl(true);

/********************************************************* URLS ****************************************/
$urlDropsRange = $this->createAbsoluteUrl('fase/dropsRange');
$urlAjaxExercises = $this->createAbsoluteUrl('fase/searchExercise');

/********************************************************* Ids Selects Con Change ****************************************/
$idExerciseElement = '#'.CHtml::activeId($model,'idExercise');

/********************************************************* Ids Selects A Modificar ****************************************/
$idRangeMotionElement = '#'.CHtml::activeId($model,'idRangeMotion');
$idNumberSeriesElement = '#'.CHtml::activeId($model,'numberSeries');
$idRepeatIntervalElement = '#'.CHtml::activeId($model,'repeatInterval');
$idRestElement = '#'.CHtml::activeId($model,'rest');

$numberSeriesElement = (!empty($model->numberSeries))?"$('$idNumberSeriesElement').editableSelect('select',$('$idNumberSeriesElement option:selected'));":"$('$idNumberSeriesElement').editableSelect();";
$repeatIntervalElement = (!empty($model->repeatInterval))?"$('$idRepeatIntervalElement').editableSelect('select',$('$idRepeatIntervalElement option:selected'));":"$('$idRepeatIntervalElement').editableSelect();";
$restElement = (!empty($model->rest))?"$('$idRestElement').editableSelect('select',$('$idRestElement option:selected'));":"$('$idRestElement').editableSelect();";

/********************************************************* Envio del formulario ****************************************/
$scriptSubmit=<<<JS
function submitFaseExerciseForm(event){  
    $(".preloader-alfa").slideDown("fast");
    console.log(this);
    // debugger;
    event.preventDefault();
    var opcion = true;
    var validate = validateForm();
    if(!validate)
    {
        alert("Please make sure all fields are completed");
        $(".preloader-alfa").slideUp("fast");
    }
    else
    {
        $.ajax({
            url: $('#NewUserPhaseExercise-form').prop('action'),
            type: 'POST',
            data: $('#NewUserPhaseExercise-form').serialize(),
            dataType:'json',
            success: function(response){
                if(response.id<=0)
                    {
                        $("#ajaxModalPhase .modal-body").empty().html(response.html);
                        $("#ajaxModalPhase .modal-footer").css('visibility','hidden');
                        $(".preloader-alfa").slideUp("fast");
                    }
                    else
                    {
                        $('#user-phase-exercise-grid').yiiGridView('update');
                        $("#ajaxModalPhase .close").click();
                        $(".preloader-alfa").slideUp("fast");
                    }
                
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location.href='$baseUrl';
                }
            }
        });
    }
};
JS;
$scriptValidateEmptyFields = <<<JS
function validateForm() {
    var valid = true;
    $('#NewUserPhaseExercise-form').find('.form-control:not([disabled^="disabled"])').each(function () {
        if ($(this).val() === '') {
            valid = false;
            return false;
        }
    });
    return valid
}
JS;

/********************************************************* Carga de los Dropdowns ****************************************/
$jsLoadSelects=<<<JS
function clearSelect(level) {
  switch(level) {
    case 6:
        $('$idExerciseElement').val('');
        $('$idExerciseElement').html('<option value="">Select.......</option>');
   }
}
function populateRange(){
    $(".preloader-alfa").slideDown("fast");
    $.ajax({
        url: '{$urlDropsRange}',
        data: {id:$('$idExerciseElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            $('$idRangeMotionElement').val('');
            /*$('$idNumberSeriesElement').editableSelect('destroy');*/
            /*$('$idRepeatIntervalElement').editableSelect('destroy');*/
            $('$idRestElement').editableSelect('destroy');
            
            $('$idRangeMotionElement').html(response.Range);
            /*$('$idNumberSeriesElement').html(response.NumberSeries);*/
            /*$('$idRepeatIntervalElement').html(response.RepeatInterval);*/
            $('$idRestElement').html(response.Rest);
            
            /*$('$idNumberSeriesElement').editableSelect();*/
            /*$('$idRepeatIntervalElement').editableSelect();*/
            $('$idRestElement').editableSelect();
            
            $(".preloader-alfa").slideUp("fast");
            
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
$(document).off('change','$idExerciseElement');
$(document).on('change','$idExerciseElement',populateRange);

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
JS;
$loadfallbacks=<<<JS
    /*$numberSeriesElement*/ 
    /*$repeatIntervalElement*/
    $restElement
    $('$idExerciseElement').select2({
        class :'form-control',
        width: '100%',
        theme: "bootstrap",
        minimumInputLength : 3,
        ajax: {
            url: '$urlAjaxExercises',
            dataType: 'json',
            delay: 250,
            quietMillis : 100,
            data: function (params) {
                console.log(params.term);
              var query = {
                search: params.term
              }
              return query;
            }
        }
    });
JS;

Yii::app()->clientScript->registerScript('scriptValidateEmptyFields',$scriptValidateEmptyFields, CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScript('scriptSubmit',$scriptSubmit, CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScript('jsLoadSelects',$jsLoadSelects, CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScript('loadfallbacks',$loadfallbacks, CClientScript::POS_END);



Yii::app()->clientScript->registerCss('tooltipSize','ul#Info{padding:15px} .tooltip-inner { max-width: 450px; width: 450px; text-align:justify} .tooltip.right .tooltip-inner {background-color:#004889;} .tooltip.right .tooltip-arrow {border-right-color: #004889;}');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/select2-bootstrap.css');

?>
<?= CHtml::beginForm('','post',['id'=>'NewUserPhaseExercise-form','onSubmit'=>'submitFaseExerciseForm(event)','autocomplete'=>'off']) ?>
<?= CHtml::errorSummary($model) ?>
<?php /*******************************************  Orden                        ***********************************/ ?>
<div class="row">
    <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'order'))?'has-error':''?>">
        <?= CHtml::activeLabelEx($model,'order',array('class'=>"control-label")); ?>
    </div>
    <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'order'))?'has-error':''?>">
        <?= CHtml::activeTextField($model,'order',array('class'=>"form-control","autocomplete"=>"off")); ?>
    </div>
</div>


<?php /*******************************************  Ejercicio NEW                    ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'idExercise'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'idExercise',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'idExercise'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model, 'idExercise',
                //CHtml::listData(Exercise::model()->findAll(['order'=>'description','group'=>'description']),'id','description'),
                [],
                array('style'=>'text-transform: capitalize;','class'=>"form-control",'empty'=>'Select .......',"autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Rango de Movimeinto          ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'idRangeMotion'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'idRangeMotion',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'idRangeMotion'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,
                'idRangeMotion',
                CHtml::listData(NewUserPhaseExerciseForm::selectRange($model),'id','description'),
                array('style'=>'text-transform: capitalize;','class'=>"form-control",'empty'=>'Select .......',"autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Numero De Series             ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'numberSeries'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'numberSeries',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'numberSeries'))?'has-error':''?>">
            <?= CHtml::activeTextField($model,'numberSeries',array('class'=>"form-control","autocomplete"=>"off")) ?>
        </div>
    </div>
<?php /*******************************************  Intervalo De Repeticion      ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'repeatInterval'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'repeatInterval',array('class'=>"control-label")); ?>
            <i class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" title='<ul id="Info">
            <li>Use "/ " as a separator when prescribing drop sets or clusters. Ex: 8/8/8</li>
            <li>Use "," as a separator when prescribing rep sequences. Ex: 4,3,2,2,3,4</li>
            <li>Use "-" as a separator when prescribing rep brackets. Ex: 8-10</li>
            </ul>' data-html="true"></i>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'repeatInterval'))?'has-error':''?>">
            <?= CHtml::activeTextField($model,'repeatInterval',array('class'=>"form-control","autocomplete"=>"off")) ?>
        </div>
    </div>
<?php /*******************************************  Tempo                        ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'tempo'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'tempo',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'tempo'))?'has-error':''?>">
            <?= CHtml::activeTextField($model,'tempo',array('class'=>"form-control","autocomplete"=>"off",'placeholder'=>'#-#-#-#')); ?>
        </div>
    </div>
<?php /*******************************************  Rest                         ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'rest'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'rest',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'rest'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,'rest',CHtml::listData(Rest::model()->findAllByAttributes(['methodId'=>$model->idMethod]),'description','description'),array('class'=>"form-control","autocomplete"=>"off")); ?>
        </div>
    </div>
    <?= CHtml::activeHiddenField($model,'idMethod') ?>
    <?= CHtml::activeHiddenField($model,'idBodyArea') ?>
    <?= CHtml::activeHiddenField($model,'idFamilyExercise') ?>
    <?= CHtml::activeHiddenField($model,'idOrientation') ?>
    <?= CHtml::activeHiddenField($model,'idImplement') ?>
    <?= CHtml::activeHiddenField($model,'idGrip') ?>
    <?= CHtml::activeHiddenField($model,'idLaterality') ?>

    <div class="row buttons">
        <div class="col-xs-12">
            <?php echo CHtml::submitButton('Add',array('class' => 'btn btn-success btn-lg btn-block')); ?>
        </div>
    </div>

<?= CHtml::endForm() ?>