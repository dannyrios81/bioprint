<?php
/* @var $practitioner Users */
/* @var $user Users */
/* @var $userPhase UsersPhase */
?>
<table style="width: 100%;">
    <tr>
        <td style="width: 16%">
            <img src="<?php echo dirname(Yii::app()->basePath) ?>/img/strengsensei.png" alt="logo" style="height: 55px">
        </td>
        <td style="width: 16%;text-align: center;vertical-align: middle">
            <img src="<?php echo dirname(Yii::app()->basePath) ?><?php echo (!empty($practitioner->idAttachment and file_exists('/var/www/html'.$practitioner->idAttachment0->path)))?$practitioner->idAttachment0->path:'/img/defaultProfileImg.jpg' ?>" alt="profile-image" style="height: 55px">
        </td>
        <td style="width: 18%">
            <table style="width: 100%">
                <tr>
                    <td><?= $practitioner->name . ' ' . $practitioner->lastName ?></td>
                </tr>
                <tr>
                    <td><?= $practitioner->email ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
        <td style="width: 50%;">
            <table style="width: 100%;" id="phaseData">
                <tr>
                    <td style="width: 50%"><?= $user->name ?></td>
                    <td style="width: 50%"><?= $user->lastName ?></td>
                </tr>
                <tr>
                    <td style="width: 50%"><?= $userPhase->Fase ?></td>
                    <td style="width: 50%"><?= $userPhase->BodyPart ?></td>
                </tr>
                <tr>
                    <td style="width: 50%"><?= date('Y-m-d') ?></td>
                    <td style="width: 50%">Phase : <?= $userPhase->numberPhase ?></td>
                </tr>
                <tr>
                    <td style="text-align: left">TUT</td>
                    <td style="text-align: left"><?= Tut::CalcTutTotalPdf($userPhase->id) ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
