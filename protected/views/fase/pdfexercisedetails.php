<?php
/* @var $userPhaseExercise UserPhaseExercise */
/* @var $userPhase UsersPhase */

//CVarDumper::dump($userPhaseExercise->relations(),10,true);exit;
?>
<table style="width: 100%;margin-top: 10px">
    <tr>
        <td><table style="width: 100%">
                <tr>
                    <td style="width: 40%;border-top:2px solid black">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 10%"><?= $userPhaseExercise->order ?></td>
                                <td style="width: 10%;text-align: center"><?= $userPhaseExercise->idRangeMotion0->description ?></td>
                                <td style="width: 80%;font-size: 9px"><?= $userPhaseExercise->idExercise0->description ?></td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 60%;font-size: 9px;border-top:2px solid black">
                        Description : <?= $userPhaseExercise->idExercise0->notes ?>
                    </td>
                </tr>
            </table>
            <?php for($i=1;$i<=$userPhase->numberOfTimes;$i++): ?>
                <table style="width: 100%;margin: 0px;padding: 0px">
                    <tr>
                        <td style="width: 40%">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 6%;text-align: center" ><?= $i ?></td>
                                    <td style="width: 24%;text-align: center"><?= $userPhaseExercise->repeatInterval ?></td>
                                    <td style="width: 6%;text-align: center"><?= $userPhaseExercise->numberSeries ?></td>
                                    <td style="width: 44%;text-align: center"><?= $userPhaseExercise->tempo ?></td>
                                    <td style="width: 10%;text-align: center"><?= $userPhaseExercise->rest ?> s</td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 60%">
                            <table style="width: 100%">
                                <tr>
                                    <?php for($j=0;$j<12;$j++): ?>
                                        <td style="width: 8%;text-align: center">
                                            <?php if($j<$userPhaseExercise->numberSeries):?>
                                                <table id="exerciseTable" style="width: 100%;">
                                                    <tr><td style="width: 50%">&nbsp;</td><td style="width: 50%">&nbsp;</td></tr>
                                                </table>
                                            <?php endif;?>
                                        </td>
                                    <?php endfor; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            <?php endfor; ?>
        </td>
    </tr>
</table>
