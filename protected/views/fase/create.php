<?php
/**
 * @var FaseCreateForm $model
 */

$baseUrl = Yii::app()->getBaseUrl(true);
$idDate = '#'.CHtml::activeId($model,'date');
$faseId = '#'.CHtml::activeId($model,'Fase');
$bodyPartId = '#'.CHtml::activeId($model,'BodyPart');

$faseElement = (!empty($model->Fase))?"$('$faseId').editableSelect('select',$('$faseId option:selected'));":"$('$faseId').editableSelect();";
$bodyPartElement = (!empty($model->BodyPart))?"$('$bodyPartId').editableSelect('select',$('$bodyPartId option:selected'));":"$('$bodyPartId').editableSelect();";



$script=<<<JS
if (!Modernizr.inputtypes.date) {
  $('$idDate').attr('type','text');
  $('$idDate').datepicker({format: "yyyy-mm-dd",autoclose: true});
}
JS;
$scriptSubmit=<<<JS
function submitFaseForm(event) 
{  
    console.log(this);
    // debugger;
    event.preventDefault();
    $(".preloader-alfa").slideDown("fast");
    $.ajax({
            url: $('#fase-form').prop('action'),
            type: 'POST',
            data: $('#fase-form').serialize(),
            dataType:'json',
            success: function(response){
                if(response.id<=0)
                    {
                        $("#ajaxModalPhase .modal-body").empty().html(response.html);
                        $("#ajaxModalPhase .modal-footer").css('visibility','hidden');
                        $(".preloader-alfa").slideUp("fast");
                    }
                    else
                    {
                        $('#user-phase-grid').yiiGridView('update');
                        $("#ajaxModalPhase .close").click();
                        $(".preloader-alfa").slideUp("fast");
                    }
                
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location.href='$baseUrl';
                }
            }
        });
	
};
JS;
$loadfallbacks=<<<JS
     $faseElement
     $bodyPartElement
JS;


//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/modernizr-custom.js', CClientScript::POS_END);
//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('Script',$script, CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScript('scriptSubmit',$scriptSubmit, CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScript('loadfallbacks',$loadfallbacks, CClientScript::POS_END);

/**
 *
 * @var FaseController $this
 * @var FaseCreateForm $model
 */
?>
<?= CHtml::beginForm('','post',['id'=>'fase-form','onSubmit'=>'submitFaseForm(event)']) ?>
<?= CHtml::errorSummary($model) ?>
<?php/** ******************************************  Fecha  *************************************** **/?>
<div class="form-group col-md-12 <?= !empty(CHtml::error($model,'date'))?'has-error':''?>">
    <?= CHtml::activeLabelEx($model,'date',array('class'=>"control-label")); ?>
    <?= CHtml::activeDateField($model,'date',array('class'=>"form-control","autocomplete"=>"off")); ?>
</div>
<?php/** ******************************************  Numero de fase  *************************************** **/?>
<div class="form-group col-md-12 <?= !empty(CHtml::error($model,'numberPhase'))?'has-error':''?>">
    <?= CHtml::activeLabelEx($model,'numberPhase',array('class'=>"control-label")); ?>
    <?= CHtml::activeNumberField($model,'numberPhase',array('class'=>"form-control","autocomplete"=>"off",'min'=>1)); ?>
</div>
<?php/** ******************************************  Fase  *************************************** **/?>
<div class="form-group col-md-12 <?= !empty(CHtml::error($model,'Fase'))?'has-error':''?>" >
    <?= CHtml::activeLabelEx($model,'Fase',array('class'=>"control-label")); ?>
    <?= CHtml::activeDropDownList($model,'Fase',FaseCreateForm::selectFase($model),array('class'=>"form-control","autocomplete"=>"off")); ?>
</div>
<?php/** ******************************************  parte del cuerpo  *************************************** **/?>
<div class="form-group col-md-12 <?= !empty(CHtml::error($model,'BodyPart'))?'has-error':''?>" >
    <?= CHtml::activeLabelEx($model,'BodyPart',array('class'=>"control-label")); ?>
    <?= CHtml::activeDropDownList($model,'BodyPart',FaseCreateForm::selectBodyPart($model),array('class'=>"form-control","autocomplete"=>"off")); ?>
</div>
<?php/** ******************************************  Numero De Veces  *************************************** **/?>
<div class="form-group col-md-12 <?= !empty(CHtml::error($model,'numberOfTimes'))?'has-error':''?>">
    <?= CHtml::activeLabelEx($model,'numberOfTimes',array('class'=>"control-label")); ?>
    <?= CHtml::activeNumberField($model,'numberOfTimes',array('class'=>"form-control","autocomplete"=>"off",'min'=>1)); ?>
</div>
<div class="row buttons">
    <div class="col-xs-12">
        <?php echo CHtml::submitButton($model->new?'Create':'Update',array('class' => 'btn btn-success btn-lg btn-block')); ?>
    </div>
</div>
<?= CHtml::endForm() ?>
