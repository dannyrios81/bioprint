<?php
/* @var $maxSets integer */
?>
<table style="width: 100%;margin: 0px">
    <tr>
        <td style="width: 40%;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 10%">Order</td>
                    <td style="width: 10%;text-align: center">Range</td>
                    <td style="width: 80%">Exercise</td>
                </tr>
            </table>
        </td>
        <td style="width: 60%;">
            &nbsp;
        </td>
    </tr>
</table>
<table style="width: 100%;margin: 0px;padding: 0px">
    <tr>
        <td style="width: 40%;">
            <table style="width: 100%">
                <tr>
                    <td style="width: 6%;text-align: center" >Week</td>
                    <td style="width: 24%;text-align: center">Reps</td>
                    <td style="width: 6%;text-align: center">Sets</td>
                    <td style="width: 44%;text-align: center">Tempo</td>
                    <td style="width: 10%;text-align: center">Rest(secs)</td>
                </tr>
            </table>
        </td>
        <td style="width: 60%;">
            <table style="width: 100%;">
                <tr>
                    <?php for($k=0;$k<12;$k++): ?>
                        <td style="width: 8%;text-align: center">
                            <?php if($k<$maxSets): ?>
                            <table style="width: 100%;">
                                <tr><td colspan="2" style="width: 100%">S<?= $k+1 ?></td></tr>
                                <tr><td style="width: 50%">W</td><td style="width: 50%">R</td></tr>
                            </table>
                            <?php else: ?>
                                <table style="width: 100%;">
                                    <tr><td colspan="2" style="width: 100%">&nbsp;</td></tr>
                                    <tr><td style="width: 50%">&nbsp;</td><td style="width: 50%">&nbsp;</td></tr>
                                </table>
                            <?php endif; ?>
                        </td>
                    <?php endfor; ?>
                </tr>
            </table>
        </td>
    </tr>
</table>