<h1><?= $user->name ." ". $user->lastName; ?></h1>

<?php
/**
 *
 * @var Params $param
 * @var UsersPhase $model
 */
/*********************** script para las cajas con date ******************/
$baseUrl = Yii::app()->getBaseUrl(true);
$idDate = '#'.CHtml::activeId($model,'date');
$this->pageTitle = $user->name ." ". $user->lastName;

$script=<<<JS
if (!Modernizr.inputtypes.date) {
  $('$idDate').attr('type','text');
  $('$idDate').datepicker({format: "yyyy-mm-dd",autoclose: true,});
}
$('#VideoModal').on('hidden.bs.modal', function () {
	/*debugger;*/
    $(this).find('iframe').attr("src", $(this).find('iframe').attr("src"));
});
$('#VideoModal').on('shown.bs.modal', function () {
    $(this).find('.modal-body div').css('min-height','480px');
    
    $(this).find('.modal-body div').width($(this).find('.modal-body').width()-50);
    $(this).find('.modal-body div').height($(this).find('.modal-body').height()-50);
	    
});
$(document).on('click','#btnajaxCopyExercises',function(event) {
    event.preventDefault();
    var info=[];
    $.each($( ".checkboxValid:checked" ),function(index,value){
        info.push($(value).val());
    });
    $.ajax({
        url: $(this).attr('href'),
        data: {ids:info},
        type: 'POST',
        dataType:'json',
        success: function(response){
            $( ":checkbox:checked" ).removeAttr('checked');
            if(response.valid>0)
                alert(response.message);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
});
$(document).on('click','#btnajaxPasteExercises',function(event) {
    event.preventDefault();
    $.ajax({
        url: $(this).attr('href'),
        type: 'POST',
        dataType:'json',
        success: function(response){
            if(response.valid>0)
                alert(response.message);
                console.log(response);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        },
        complete: function() {
            $('#user-phase-grid').yiiGridView('update');
        }
     });
});
JS;
/*********************** fin script para las cajas con date ******************/
/*********************** script para los botones que disparan un dialogbox ******************/
$scriptDialog =<<<JS
$(document).on('click','#btnajaxModalPhase,.xEditFase',function(event){     
    event.preventDefault();
     $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            dataType:'json',
            success: function(response){
                console.log(response);
                $("#ajaxModalPhase .modal-body").empty().html(response.html);
		        $("#ajaxModalPhase").modal();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location.href='$baseUrl';
                }
            }
        });
});
$(document).on('click','#btnajaxModalNewFase',function(event){   
    event.preventDefault();
    
    $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            dataType:'json',
            success: function(response){
                $("#ajaxModalNewFase .modal-body").empty().html(response.html);
                $("#ajaxModalNewFase .modal-footer").css('visibility','hidden');
		        $("#ajaxModalNewFase").modal();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location.href='$baseUrl';
                }
            }
        });
});
$(document).on('click','#btnajaxModalNewBodyPart',function(event){   
    event.preventDefault();
    $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            dataType:'json',
            success: function(response){
                console.log(response);
                // debugger;
                $("#ajaxModalNewBodyPart .modal-body").empty().html(response.html);
                $("#ajaxModalNewBodyPart .modal-footer").css('visibility','hidden');
		        $("#ajaxModalNewBodyPart").modal();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location.href='$baseUrl';
                }
            }
        });
	
});
$('#ajaxModalNewBodyPart, #ajaxModalNewFase').on('hidden.bs.modal', function (e) {
    $('body').addClass('modal-open');
});
$(document).on('click','#btnVideo',function(event){     
    event.preventDefault();
    $("#VideoModal").modal();
});
$(document).on('click','.xExecution',function(event){
    event.preventDefault();
    $.ajax({
        url: $(this).attr('href'),
        type: 'POST',
        dataType:'json',
        async: true,
        success: function(response){
            console.log(response);
            if(response.show)
            {
                if(confirm("Are you sure you want to create this execution? It will only have the current exercises on the program"))
                {
                    window.location=response.url;
                }
            }
            else
            {
                window.location=response.url;
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location='$baseUrl';
                }
            }
    });
    
});
JS;
Yii::app()->clientScript->registerScript('scriptDialog',$scriptDialog,CClientScript::POS_READY);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/modernizr-custom.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('Script',$script, CClientScript::POS_READY);
/*********************** fin script para los botones que disparan un dialogbox ******************/

/*********************** script para que los mensajes de error desaparescan en x cantidad de seg automaticamente ******************/
Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".alerta").animate({opacity: 1.0}, 10000).fadeOut("slow");',
    CClientScript::POS_READY
);
/*********************** fin script para que los mensajes de error desaparescan en x cantidad de seg automaticamente ******************/
CHtml::$errorCss = 'has-error';

$template = '{xExecution}';

?>
    <?php $flashMessages = Yii::app()->user->getFlashes(); ?>
    <?php if (count($flashMessages)) :?>
        <div class="alerta">
            <?php
            if ($flashMessages) {
                echo '<ul class="alert">';
                foreach($flashMessages as $key => $message) {
                    echo '<li><div class="alert-' . $key . '">' . $message . "</div></li>\n";
                }
                echo '</ul>';
            }
            ?>
        </div>
    <?php endif;?>
    <hr class="separator">
<?php if(Yii::app()->user->getState('userType')!='User'):?>
    <div class="text-right">
<!--        <a href="#" class="btn btn-primary" id="btnVideo" data-toggle="modal" data-target="#modal"><i class="fa fa-youtube-play"></i></a>-->
        <?= CHtml::link('<i class="fa fa-plus-circle"></i> New Phase',['fase/create','id'=>$user->id],['class'=>"btn btn-primary",'id'=>"btnajaxModalPhase"]); ?>
        <?= CHtml::link('<i class="fa fa-clone"></i> Copy Selected Phases',['fase/copyPhase'],['class'=>"btn btn-primary",'id'=>"btnajaxCopyExercises"]); ?>
        <?= CHtml::link('<i class="fa fa-clipboard"></i> Paste Phases',['fase/pastePhase','id'=>$user->id],['class'=>"btn btn-primary",'id'=>"btnajaxPasteExercises"]); ?>
    </div>
    <br>
    <?php $template = '{xAddExercise}{xExecution}{xPrintFase}{xEditFase}{xDeleteFase}'; ?>
<?php endif; ?>
    <div class="col-sm-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'user-phase-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'columns'=>array(
                [
                    'header'=>'Date',
                    'name'=>'date',
                    'value'=>'Yii::app()->dateFormatter->format("yyyy-MM-dd",$data->date)',
                    'filter'=>CHtml::activeDateField($model,'date',['class'=>"form-control"])
                ],
                [
                    'header'=>'Phase Name',
                    'name'=>'Fase',
                    'value'=>'$data->Fase',
                    'filter'=>CHtml::activeTextField($model,'Fase',['class'=>"form-control"])
                ],
                [
                    'header'=>'Phase Number',
                    'name'=>'numberPhase',
                    'value'=>'$data->numberPhase',
                    'filter'=>CHtml::activeNumberField($model,'numberPhase',['class'=>"form-control"])
                ],
                [
                    'header'=>'Body Part',
                    'value'=>'$data->BodyPart',
                    'filter'=>CHtml::activeTextField($model,'BodyPart',['class'=>"form-control"])
                ],
                [
                    'header'=>'Number of Times',
                    'name'=>'numberOfTimes',
                    'value'=>'(Calculos::lastExecutionPhase($data->id)!==array() and isset(Calculos::lastExecutionPhase($data->id)["totalExecutions"]))?((Calculos::lastExecutionPhase($data->id)["totalExecutions"]==1 and Calculos::lastExecutionPhase($data->id)["sumtotalreps"]=="0.00")?0:(string)Calculos::lastExecutionPhase($data->id)["totalExecutions"])."/".(string)$data->numberOfTimes:"0/".(string)$data->numberOfTimes',
                    'filter'=>CHtml::activeNumberField($model,'numberOfTimes',['class'=>"form-control"])
                ],
                [
                    'header'=> 'Copy',
                    'headerTemplate'=>'<div style="white-space: nowrap">Copy {item}</div>',
                    'class' => 'CCheckBoxColumn',
                    'value' => '$data->id',
                    'selectableRows' => 20,
                    'checkBoxHtmlOptions'=>['class'=>'checkboxValid']
                ],
                array
                (
                    'class'=>'ButtonColumn',
                    'template'=>"<div style='white-space: nowrap'>".$template."</div>",
                    'header'=>"  Actions  ",
                    'evaluateOptions' => true,
                    'buttons'=>array(
                        'xAddExercise'=>array(
                            'label'=>'<i class="fa icon-072-dumbbell-1 fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa icon-072-dumbbell-1 visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                            'url'=>'Yii::app()->createUrl("fase/exercise", array("id"=>$data->id))',
                            'options'=>array('title'=>'Workout','class'=>'xAddExercise')
                        ),
                        'xPrintFase'=>array(
                            'label'=>'<i class="fa fa-print fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-print visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                            'url'=>'Yii::app()->createUrl("fase/pdf", array("id"=>$data->id))',
                            'options'=>array('title'=>'Print Phase','class'=>'xPrintFase')
                        ),
                        'xEditFase'=>array(
                            'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                            'url'=>'Yii::app()->createUrl("fase/edit", array("id"=>$data->id))',
                            'options'=>array('title'=>'Edit Phase','class'=>'xEditFase')
                        ),
                        'xExecution'=>array(
                            'label'=>'<i class="fa ícono icon-clipboard-silhouette-with-arrow-pointing-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa ícono icon-clipboard-silhouette-with-arrow-pointing-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                            'url'=>'Yii::app()->createUrl("fase/validateFirstExecution", array("id"=>$data->id))',
                            'options'=>array('title'=>'Executions','class'=>'xExecution'),
                        ),
                        'xDeleteFase'=>array(
                            'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                            'url'=>'Yii::app()->createUrl("fase/delete", array("id"=>$data->id))',
                            'options'=>array('title'=>'Delete Phase'),
                            'click'=>"function() {
							if(!confirm('Are you sure you want to delete this item?')) return false;
							var th = this,afterDelete = function(){};
							jQuery('#user-phase-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
									jQuery('#user-phase-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
						}"
                        ),
                    ),
                ),
            ),
        )); ?>
    </div>

    <div class="modal fade" id="ajaxModalPhase" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="preloader-alfa"></div>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title">New User Phase</h2>
                </div>
                <div class="modal-body" style="text-align: justify;">
                    XXX XXXXXXXXXXXXXXX XXXXXXXXXX XXXXX
                    <a class="btn btn-primary" id="btnajaxModalNewFase"><i class="fa fa-plus-circle"></i> New Phase</a>
                </div>

            </div>
        </div>
    </div>
<div class="modal fade" id="ajaxModalNewFase" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="preloader-alfa"></div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">New Phase</h2>
            </div>
            <div class="modal-body" style="text-align: justify;">
                XXX XXXXXXXXXXXXXXX XXXXXXXXXX XXXXX
                <a class="btn btn-primary" id="btnajaxModalNewBodyPart"><i class="fa fa-plus-circle"></i> New Phase</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-target="#ajaxModalNewFase" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ajaxModalNewBodyPart" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="preloader-alfa"></div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">New Body Part</h2>
            </div>
            <div class="modal-body" style="text-align: justify;">
                XXX XXXXXXXXXXXXXXX XXXXXXXXXX XXXXX
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-target="#ajaxModalNewBodyPart" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="VideoModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Titulo del video</h4>
            </div>
            <div class="modal-body">
                <div class="video-measure-modal text-center" style="margin: 0 auto;">
                    <?php //$this->widget('ext.Yiitube', array('v' => $param->valueParam,'size'=>'bioprint')); ?>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>