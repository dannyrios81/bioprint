<?php
/**
 *
 * @var FaseController $this
 * @var \UserPhaseExerciseForm $model
 */

/********************************************************* URLS ****************************************/
$baseUrl = Yii::app()->getBaseUrl(true);
$urlDropsMethod = $this->createAbsoluteUrl('fase/dropsMethod');
$urlDropsFamilyExercise = $this->createAbsoluteUrl('fase/dropsFamilyExercise');

$urlDropsOrientation = $this->createAbsoluteUrl('exercise/dropsOrientation');
$urlDropsImplement = $this->createAbsoluteUrl('exercise/dropsImplement');
$urlDropsGrip = $this->createAbsoluteUrl('exercise/dropsGrip');

$urlDropsExercise = $this->createAbsoluteUrl('fase/dropsExercise');
$urlSelectlaterality = $this->createAbsoluteUrl('fase/selectLaterality');
$urlDropsRange = $this->createAbsoluteUrl('fase/dropsRange');

/********************************************************* Ids Selects Con Change ****************************************/
$idMethodElement = '#'.CHtml::activeId($model,'idMethod');
$idBodyAreaElement = '#'.CHtml::activeId($model,'idBodyArea');
$idFamilyExerciseElement = '#'.CHtml::activeId($model,'idFamilyExercise');
$idExerciseElement = '#'.CHtml::activeId($model,'idExercise');

/********************************************************* Ids Selects A Modificar ****************************************/
$idRepeatIntervalElement = '#'.CHtml::activeId($model,'repeatInterval');
$idRestElement = '#'.CHtml::activeId($model,'rest');
$idRangeMotionElement = '#'.CHtml::activeId($model,'idRangeMotion');
$idGripElement = '#'.CHtml::activeId($model,'idGrip');
$idImplementElement = '#'.CHtml::activeId($model,'idImplement');
$idOrientationElement = '#'.CHtml::activeId($model,'idOrientation');
$idNumberSeriesElement = '#'.CHtml::activeId($model,'numberSeries');
$idLateralityElement = '#'.CHtml::activeId($model,'idLaterality');

$numberSeriesElement = (!empty($model->numberSeries))?"$('$idNumberSeriesElement').editableSelect('select',$('$idNumberSeriesElement option:selected'));":"$('$idNumberSeriesElement').editableSelect();";
$repeatIntervalElement = (!empty($model->repeatInterval))?"$('$idRepeatIntervalElement').editableSelect('select',$('$idRepeatIntervalElement option:selected'));":"$('$idRepeatIntervalElement').editableSelect();";
$restElement = (!empty($model->rest))?"$('$idRestElement').editableSelect('select',$('$idRestElement option:selected'));":"$('$idRestElement').editableSelect();";

/*********************************************************** Validacion Campos Vacios **********************************/

//$('#UserPhaseExercise-form').find('.form-control:not([disabled^="disabled"])')
$scriptValidateEmptyFields = <<<JS
function validateForm() {
    var valid = true;
    $('#UserPhaseExercise-form').find('.form-control:not([disabled^="disabled"])').each(function () {
        if ($(this).val() === '') {
            valid = false;
            return false;
        }
    });
    return valid
}
JS;


/********************************************************* Envio del formulario ****************************************/
$scriptSubmit=<<<JS
function submitFaseExerciseForm(event){  
    $(".preloader-alfa").slideDown("fast");
    console.log(this);
    // debugger;
    event.preventDefault();
    var opcion = true;
    var validate = validateForm();
    if(!validate)
    {
        alert("Please make sure all fields are completed");
        $(".preloader-alfa").slideUp("fast");
    }
    else
    {
        $.ajax({
            url: $('#UserPhaseExercise-form').prop('action'),
            type: 'POST',
            data: $('#UserPhaseExercise-form').serialize(),
            dataType:'json',
            success: function(response){
                if(response.id<=0)
                    {
                        $("#ajaxModalPhase .modal-body").empty().html(response.html);
                        $("#ajaxModalPhase .modal-footer").css('visibility','hidden');
                        $(".preloader-alfa").slideUp("fast");
                    }
                    else
                    {
                        $('#user-phase-exercise-grid').yiiGridView('update');
                        $("#ajaxModalPhase .close").click();
                        $(".preloader-alfa").slideUp("fast");
                    }
                
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location.href='$baseUrl';
                }
            }
        });
    }
};
JS;

/********************************************************* Carga de los Dropdowns ****************************************/
$jsLoadSelects=<<<JS
function clearSelect(level) {
  switch(level) {
    case 1:
        $('$idBodyAreaElement').val('');
        $('$idBodyAreaElement').html('<option value="">Select.......</option>');
    case 2:
        $('$idFamilyExerciseElement').val('');
        $('$idFamilyExerciseElement').html('<option value="">Select.......</option>');
    case 3:
        $('$idOrientationElement').val('');
        $('$idOrientationElement').html('<option value="">Select.......</option>');
    case 4:
        $('$idImplementElement').val('');
        $('$idImplementElement').html('<option value="">Select.......</option>');
    case 5:
        $('$idGripElement').val('');
        $('$idGripElement').html('<option value="">Select.......</option>');
    case 6:
        $('$idExerciseElement').val('');
        $('$idLateralityElement').val('');
        $('$idExerciseElement').html('<option value="">Select.......</option>');
        // $('$idLateralityElement').html('<option value="">Select.......</option>');
   }
}
function populateMethod() {
    $(".preloader-alfa").slideDown("fast");
    $.ajax({
        url: '{$urlDropsMethod}',
        data: {id:$('$idMethodElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            $('$idRepeatIntervalElement').editableSelect('destroy');
            $('$idRestElement').editableSelect('destroy');
            $('$idRangeMotionElement').val('');
            $('$idNumberSeriesElement').editableSelect('destroy');
            
            clearSelect($('$idMethodElement').data('level'));
            $('$idBodyAreaElement').val('');
           
            $('$idBodyAreaElement').html(response.BodyArea);
            $('$idRepeatIntervalElement').html(response.RepeatInterval);
            $('$idRestElement').html(response.Rest);
            $('$idRangeMotionElement').html(response.RangeMotion);
            $('$idNumberSeriesElement').html(response.NumberSeries);
            
            $('$idNumberSeriesElement').editableSelect();
            $('$idRepeatIntervalElement').editableSelect();
            $('$idRestElement').editableSelect();
            
            $(".preloader-alfa").slideUp("fast");
            
            // populateExercise();           
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
function populationFamilyExercise(){   
     $(".preloader-alfa").slideDown("fast");
     $.ajax({
        url: '{$urlDropsFamilyExercise}',
        data: {id:$('$idBodyAreaElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            clearSelect($('$idBodyAreaElement').data('level'));
            $('$idFamilyExerciseElement').val('');
            $('$idFamilyExerciseElement').html(response.FamilyExercise);
            $(".preloader-alfa").slideUp("fast");
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
function populationOrientation(){
    $(".preloader-alfa").slideDown("fast");
    $.ajax({
        url: '{$urlDropsOrientation}',
        data: {id:$('$idFamilyExerciseElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            clearSelect($('$idFamilyExerciseElement').data('level'));
            $('$idOrientationElement').val('');
            $('$idOrientationElement').html(response.Orientation);
            $(".preloader-alfa").slideUp("fast");
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
function populationImplement(){
    $(".preloader-alfa").slideDown("fast");
    $.ajax({
        url: '{$urlDropsImplement}',
        data: {id:$('$idOrientationElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            clearSelect($('$idOrientationElement').data('level'));
            $('$idImplementElement').val('');
            $('$idImplementElement').html(response.Implement);
            $(".preloader-alfa").slideUp("fast");
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
function populationGrip(){
    $(".preloader-alfa").slideDown("fast");
    $.ajax({
        url: '{$urlDropsGrip}',
        data: {id:$('$idImplementElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            clearSelect($('$idImplementElement').data('level'));
            $('$idGripElement').val('');
            $('$idGripElement').html(response.Grip);
            $(".preloader-alfa").slideUp("fast");
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
function populateExercise(){
    /* *************************************  validar ejercicio   *************************** */
    $(".preloader-alfa").slideDown("fast");
     $.ajax({
        url: '{$urlDropsExercise}',
        data: $('#UserPhaseExercise-form').serialize(),
        type: 'POST',
        dataType:'json',
        success: function(response){
            //debugger;
            if(response.valid>0)
            {
                $('$idExerciseElement').html(response.Exercise);
                $('$idLateralityElement').val(response.idLaterality);
                $(".preloader-alfa").slideUp("fast");
            }
            else 
            {
                $('#user-phase-exercise-grid').yiiGridView('update');
                $("#ajaxModalPhase .close").click();
                $(".preloader-alfa").slideUp("fast");
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
function selectLaterality() {
    $(".preloader-alfa").slideDown("fast");
    $.ajax({
        url: '{$urlSelectlaterality}',
        data: {id:$('$idExerciseElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            $('$idLateralityElement').val(response.idLaterality);
            $(".preloader-alfa").slideUp("fast");
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}
function populateRange(){
    $(".preloader-alfa").slideDown("fast");
    $.ajax({
        url: '{$urlDropsRange}',
        data: {id:$('$idExerciseElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            $('$idRangeMotionElement').val('');
            $('$idRangeMotionElement').html(response.Range);
            $(".preloader-alfa").slideUp("fast");
            
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}

$(document).off('change','$idMethodElement');
$(document).on('change','$idMethodElement',populateMethod);
$(document).off('change','$idBodyAreaElement');
$(document).on('change','$idBodyAreaElement',populationFamilyExercise);

$(document).off('change','$idFamilyExerciseElement');
$(document).on('change','$idFamilyExerciseElement',populationOrientation);

$(document).off('change','$idOrientationElement');
$(document).on('change','$idOrientationElement',populationImplement);
$(document).off('change','$idImplementElement');
$(document).on('change','$idImplementElement',populationGrip);

$(document).off('change','$idGripElement');
$(document).on('change','$idGripElement',populateExercise);

$(document).off('change','$idExerciseElement');
$(document).on('change','$idExerciseElement',selectLaterality);
$(document).on('change','$idExerciseElement',populateRange);

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
JS;
$loadfallbacks=<<<JS
    // $('$idNumberSeriesElement').editableSelect();
    // $('$idRepeatIntervalElement').editableSelect();
    // $('$idRestElement').editableSelect();
    
    $numberSeriesElement
    $repeatIntervalElement
    $restElement
JS;

Yii::app()->clientScript->registerScript('scriptValidateEmptyFields',$scriptValidateEmptyFields, CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScript('scriptSubmit',$scriptSubmit, CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScript('jsLoadSelects',$jsLoadSelects, CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScript('loadfallbacks',$loadfallbacks, CClientScript::POS_END);

Yii::app()->clientScript->registerCss('tooltipSize','ul#Info{padding:15px} .tooltip-inner { max-width: 450px; width: 450px; text-align:justify} .tooltip.right .tooltip-inner {background-color:#004889;} .tooltip.right .tooltip-arrow {border-right-color: #004889;}');

?>
<?= CHtml::beginForm('','post',['id'=>'UserPhaseExercise-form','onSubmit'=>'submitFaseExerciseForm(event)','autocomplete'=>'off']) ?>
<?= CHtml::errorSummary($model) ?>

<?php /*******************************************  Orden                        ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'order'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'order',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'order'))?'has-error':''?>">
            <?= CHtml::activeTextField($model,'order',array('class'=>"form-control","autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Metodo                       ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'idMethod'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'idMethod',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'idMethod'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,'idMethod',CHtml::listData(Method::model()->findAll(['order'=>'description']),'id','description'),array('data-level'=>'1','style'=>'text-transform: capitalize;','class'=>"form-control",'empty'=>'Select .......',"autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Area Del Cuerpo              ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'idBodyArea'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'idBodyArea',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'idBodyArea'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,'idBodyArea',CHtml::listData(BodyArea::model()->findAll(['condition'=>'idMethod=:idMethod','order'=>'description','params'=>['idMethod'=>$model->idMethod]]),'id','description'),array('data-level'=>'2','style'=>'text-transform: capitalize;','class'=>"form-control",'empty'=>'Select .......',"autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Familia De Ejercicios        ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'idFamilyExercise'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'idFamilyExercise',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'idFamilyExercise'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,'idFamilyExercise',CHtml::listData(FamilyExercise::model()->findAll(['condition'=>'bodyAreaId=:bodyAreaId','order'=>'description','params'=>['bodyAreaId'=>$model->idBodyArea]]),'id','description'),array('data-level'=>'3','style'=>'text-transform: capitalize;','class'=>"form-control",'empty'=>'Select .......',"autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Orientacion                  ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'idOrientation'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'idOrientation',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'idOrientation'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,'idOrientation',CHtml::listData(Orientation::model()->findAll(['condition'=>'idFamily_exercise=:idFamily_exercise','order'=>'description','params'=>['idFamily_exercise'=>$model->idFamilyExercise]]),'id','description'),array('data-level'=>'4','style'=>'text-transform: capitalize;','class'=>"form-control",'empty'=>'Select .......',"autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Implemento                   ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'idImplement'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'idImplement',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'idImplement'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,'idImplement',CHtml::listData(Implement::model()->findAll(['condition'=>'idOrientation=:idOrientation','order'=>'description','params'=>['idOrientation'=>$model->idOrientation]]),'id','description'),array('data-level'=>'5','style'=>'text-transform: capitalize;','class'=>"form-control",'empty'=>'Select .......',"autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Grip                         ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'idGrip'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'idGrip',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'idGrip'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,'idGrip',CHtml::listData(Grip::model()->findAll(['condition'=>'idImplement=:idImplement','order'=>'description','params'=>['idImplement'=>$model->idImplement]]),'id','description'),array('data-level'=>'6','style'=>'text-transform: capitalize;','class'=>"form-control",'empty'=>'Select .......',"autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Ejercicio                    ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'idExercise'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'idExercise',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'idExercise'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,
                'idExercise',
                CHtml::listData(Exercise::model()->findAll(
                    [
                        'condition'=>'idMethod=:idMethod AND 
                                                              idBodyArea=:idBodyArea AND 
                                                              idFamilyExercise=:idFamilyExercise AND 
                                                              idOrientation=:idOrientation AND 
                                                              idImplement=:idImplement AND 
                                                              idGrip=:idGrip',
                        'order'=>'description',
                        'group'=>'idBodyArea,idFamilyExercise,idOrientation,idImplement,idGrip,description',
                        'params'=>
                            [
                                'idMethod'=>$model->idMethod,
                                'idBodyArea'=>$model->idBodyArea,
                                'idFamilyExercise'=>$model->idFamilyExercise,
                                'idOrientation'=>$model->idOrientation,
                                'idImplement'=>$model->idImplement,
                                'idGrip'=>$model->idGrip,
                            ]
                    ]),'id','description'),
                array('style'=>'text-transform: capitalize;','class'=>"form-control",'empty'=>'Select .......',"autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Rango de Movimeinto          ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'idRangeMotion'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'idRangeMotion',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'idRangeMotion'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,
                'idRangeMotion',
                CHtml::listData(UserPhaseExerciseForm::selectRange($model),'id','description'),
                array('style'=>'text-transform: capitalize;','class'=>"form-control",'empty'=>'Select .......',"autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Lateralidad                  ***********************************/ ?>
    <div class="row hidden">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'idLaterality'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'idLaterality',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'idLaterality'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,'idLaterality',CHtml::listData(Laterality::model()->findAll(),'id','description'),array('disabled'=>'disabled','style'=>'text-transform: capitalize;','class'=>"form-control",'empty'=>'Select .......',"autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Numero De Series             ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'numberSeries'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'numberSeries',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'numberSeries'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,'numberSeries',CHtml::listData(NumberSeries::model()->findAllByAttributes(['methodId'=>$model->idMethod]),'description','description'),array('class'=>"form-control","autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Intervalo De Repeticion      ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'repeatInterval'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'repeatInterval',array('class'=>"control-label")); ?>
            <i class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="right" title='<ul id="Info">
            <li>Use "/ " as a separator when prescribing drop sets or clusters. Ex: 8/8/8</li>
            <li>Use "," as a separator when prescribing rep sequences. Ex: 4,3,2,2,3,4</li>
            <li>Use "-" as a separator when prescribing rep brackets. Ex: 8-10</li>
            </ul>' data-html="true"></i>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'repeatInterval'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,'repeatInterval',CHtml::listData(RepeatInterval::model()->findAllByAttributes(['methodId'=>$model->idMethod]),'description','description'),array('class'=>"form-control","autocomplete"=>"off")); ?>
        </div>
    </div>
<?php /*******************************************  Tempo                        ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'tempo'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'tempo',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'tempo'))?'has-error':''?>">
            <?= CHtml::activeTextField($model,'tempo',array('class'=>"form-control","autocomplete"=>"off",'placeholder'=>'#-#-#-#')); ?>
        </div>
    </div>
<?php /*******************************************  Rest                         ***********************************/ ?>
    <div class="row">
        <div class="form-group col-md-4 <?= !empty(CHtml::error($model,'rest'))?'has-error':''?>">
            <?= CHtml::activeLabelEx($model,'rest',array('class'=>"control-label")); ?>
        </div>
        <div class="form-group col-md-8 <?= !empty(CHtml::error($model,'rest'))?'has-error':''?>">
            <?= CHtml::activeDropDownList($model,'rest',CHtml::listData(Rest::model()->findAllByAttributes(['methodId'=>$model->idMethod]),'description','description'),array('class'=>"form-control","autocomplete"=>"off")); ?>
        </div>
    </div>

    <div class="row buttons">
        <div class="col-xs-12">
            <?php echo CHtml::submitButton('Add',array('class' => 'btn btn-success btn-lg btn-block')); ?>
        </div>
    </div>

<?= CHtml::endForm() ?>