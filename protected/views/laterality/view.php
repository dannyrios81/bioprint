<?php
/* @var $this LateralityController */
/* @var $model Laterality */

$this->breadcrumbs=array(
	'Lateralities'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Laterality', 'url'=>array('index')),
	array('label'=>'Create Laterality', 'url'=>array('create')),
	array('label'=>'Update Laterality', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Laterality', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Laterality', 'url'=>array('admin')),
);
?>

<h1>View Laterality #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
	),
)); ?>
