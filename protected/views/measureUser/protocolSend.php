<?php
$standardSupplements = Supplement::model()->findAllByAttributes(['independent'=>1,'idCountry'=>$recipe->idCountry],['order'=>'name']);

$scriptEnable =<<<JS
    $(document).on('click','.addSuplement',function(){
        //alert("xoxoxoxox");
        event.preventDefault();
        var html ='';
        if($( "p:contains('You should also use:')").length<=0)
        {
            //agregar titulo de los suplementos 
            html = '<p><strong data-redactor-tag="strong">You should also use:</strong></p>';
        }
        html = html + '<p>' + $( this ).data('info') + '</p>';
        //alert($( this ).text());
        //alert($( this ).data('info'));
        
        var editor = $('#ProtocoloForm_description').redactor('core.getObject');
        html = editor.code.get() + html;
        editor.insert.set(html);
    });
JS;

Yii::app()->clientScript->registerScript('scriptEnable',$scriptEnable,CClientScript::POS_READY);

?>
<div class="col-xs-12">
    <h1>Send Protocol</h1>
    <h3>&nbsp;</h3>
</div>
<hr class="separator">
<div class="col-xs-12 form-container">
    <?php echo CHtml::form() ?>
        <div class="form-group col-md-12 <?php echo !empty(CHtml::error($model,'description'))?'has-error':''?>">

            <?php echo CHtml::activeLabelEx($model,'description',array('class'=>"control-label")); ?>
            <?php echo CHtml::activeTextArea($model,'description',array('class'=>"form-control","cols"=>"20","rows"=>"50")); ?>
            <?php $this->widget('ImperaviRedactorWidget', ['selector' => '#'.CHtml::activeId($model,'description'),'options' => ["minHeight"=> 300,'focusEnd'=> true],]); ?>
        </div>
        <?php if(count($standardSupplements)>0 ):?>
        <h4>Add Standard Supplements</h4>
        <?php $buttons = CHtml::listData($standardSupplements,'id','name'); ?>
            <?php foreach ($buttons as $button): ?>
                <div class="col-sm-6 col-xs-12">
                <?php echo CHtml::link('<i class="fa fa-medkit"></i>','#',['class'=>"btn btn-primary col-xs-1 addSuplement",'data-info'=>$button]) ?>
                <?php echo CHtml::label($button,'',['class'=>"col-xs-11"]) ?>
                <?php //echo CHtml::button($button) ?>
                </div>
            <?php endforeach; ?>
        <?php //echo CHtml::activeCheckBoxList($model,'suplementsArray',CHtml::listData($standardSupplements,'id','name'),array('separator'=>'','template'=>"<div class=\"checkbox\">{input} {label}</div>")); ?>
        <?php endif; ?>
        <div class="text-center col-sm-12 col-xs-12" style="margin-top: 20px">
            <?php if(empty($recipe)):?>
                <p>this hormone family and measure selected don't have a defined protocol</p>
            <?php else:?>
                <?php echo CHtml::submitButton('Send Protocol',['class'=>"btn btn-primary"]) ?>
            <?php endif; ?>
        </div>

    <?php echo CHtml::endForm()?>
</div>