<div class="col-sm-6">
<?php
$this->Widget('vendor.miloschuman.yii-highcharts.highcharts.HighchartsWidget', array(
    'options' => array(
        'title' => false,
//        'title' => ['text'=>$title],
        'xAxis' => array(
            'type' => 'category'
        ),
        'yAxis' => array(
            'title' => array('text' => 'Score')
        ),
        'credits' => array('enabled' => false),
        'exporting' => array('enabled' => false),
        'chart' => array(
            'plotBackgroundColor' => '#ffffff',
            'plotBorderWidth' => null,
            'plotShadow' => false,
            'height' => 200,
            'type'=>'column'
        ),
        'plotOptions' => array(
            'column'=>array(
                'pointPadding'=>0.2,'borderWidth'=>0
            ),

        ),
        'tooltip' => array(
            // 'pointFormat' => '{series.name}: <b>{point.percentage}%</b>',
            // 'percentageDecimals' => 1,
            'formatter'=> 'js:function() { return "<b>"+this.point.name+"</b>"; }',
            //the reason it didnt work before was because you need to use javascript functions to round and refrence the JSON as this.<array>.<index> ~jeffrey
        ),
        'series' => array(
            $serie
        ),
    )
));
?>
</div>
