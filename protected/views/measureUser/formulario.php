<?php
$validaNumber = <<<JS
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46 || (charCode >= 48 && charCode <= 57)) {
        return true;
    }
    return false;
}
JS;
$actionsModals=<<<JS
$('.modal').on('hidden.bs.modal', function () {
	/*debugger;*/
    $(this).find('iframe').attr("src", $(this).find('iframe').attr("src"));
});
$('.modal').on('shown.bs.modal', function () {
	debugger;
	if($(this).attr('id')=='youtube')
	    {
	        $(this).find('.modal-body div').width($(this).find('.modal-body').width()-50);
            $(this).find('.modal-body div').height($(this).find('.modal-body').height()-50);
	    }
	    else
        {
            $(this).find('.imagen-measure-modal').show();
            $(this).find('.video-measure-modal').hide();
            $(this).find('.video-measure-modal').width($(this).find('.imagen-measure-modal').width());
            $(this).find('.video-measure-modal').height($(this).find('.imagen-measure-modal').height());
        }
    var self = this;
    $(this).on('click touch','.fa-camera',function () {
		$(self).find('.imagen-measure-modal').show();
		$(self).find('.video-measure-modal').hide();
		$(self).find('iframe').attr("src", $(self).find('iframe').attr("src"));
    });
    $(this).on('click touch','.fa-youtube-play',function () {
		$(self).find('.imagen-measure-modal').hide();
		$(self).find('.video-measure-modal').show();
    })
});
JS;


Yii::app()->clientScript->registerScript('validaNumber',$validaNumber, CClientScript::POS_END);
Yii::app()->clientScript->registerScript('actionsModals',$actionsModals, CClientScript::POS_READY);
?>
<h1>Enter Measurement Set</h1>

<br>
<div class="col-sm-12 main-content">
    <h4 style="color: #9c9b9c;font-size: 16px;white-space: nowrap"></h4>
    <h3>Use the dot as decimal separator. Key Points of Skinfold Measurements <!--<i class="fa fa-youtube-play fa-2x" style="cursor: pointer" data-toggle="modal" data-target="#youtube"></i>--></h3>
    <?php echo CHtml::beginForm('','post',['class'=>'form-horizontal','id'=>'measurementForm']); ?>
    <?php echo CHtml::errorSummary($model); ?>
    <div class="form-group col-sm-6 <?php echo !empty(CHtml::error($modelWH,'weight'))?'has-error':''?>">
        <?php echo CHtml::activelabelEx($modelWH,'weight',array('class'=>"control-label col-sm-4 col-xs-4",'style'=>'text-align: left;')); ?>
        <div class="col-sm-8 col-xs-8 "><?php echo CHtml::activeNumberField($modelWH,'weight',array('class'=>"form-control","step"=>"0.01")); ?></div>
    </div>

    <div class="form-group col-sm-6 <?php echo !empty(CHtml::error($modelWH,'weightUnit'))?'has-error':''?>">
        <?php echo CHtml::activelabelEx($modelWH,'weightUnit',array('class'=>"control-label col-sm-4 col-xs-4",'style'=>'text-align: left;')); ?>
        <div class="col-sm-8 col-xs-8 "><?php echo CHtml::activeDropDownList($modelWH,'weightUnit',array('Kg'=>'Kg','Lbs'=>'Lbs'),array('class'=>"form-control","empty"=>'Select...')); ?></div>
    </div>
    <div class="form-group col-sm-6 <?php echo !empty(CHtml::error($modelWH,'height'))?'has-error':''?>">
        <?php echo CHtml::activeLabelEx($modelWH,'height',array('class'=>"control-label col-sm-4 col-xs-4",'style'=>'text-align: left;')); ?>
        <div class="col-sm-8 col-xs-8 "><?php echo CHtml::activeNumberField($modelWH,'height',array('class'=>"form-control","step"=>"0.01")); ?></div>
    </div>
    <div class="form-group col-sm-6 <?php echo !empty(CHtml::error($modelWH,'heightUnit'))?'has-error':''?>">
        <?php echo CHtml::activeLabelEx($modelWH,'heightUnit',array('class'=>"control-label col-sm-4 col-xs-4",'style'=>'text-align: left;')); ?>
        <div class="col-sm-8 col-xs-8 "><?php echo CHtml::activeDropDownList($modelWH,'heightUnit',array('Cm'=>'Cm','In'=>'In'),array('class'=>"form-control","empty"=>'Select...')); ?></div>
    </div>

    <?php foreach($measures as $i=>$item): ?>
        <?php //CVarDumper::dump($item->MeasurementType->idAttachment0->path,10,true);exit; ?>
    <div class="form-group <?php echo !empty(CHtml::error($item,"[$i]val"))?'has-error':''?> col-sm-4">
        <?php echo CHtml::activeLabel($item,"[$i]val",array('class'=>"control-label col-sm-4 col-xs-4",'style'=>'text-align: left;')); ?>
        <div class="col-sm-4 col-xs-6 ">
            <?php echo CHtml::activeNumberField($item,"[$i]val",array('class'=>"form-control ","step"=>"0.01","onKeypress"=>"return isNumber(event)")); ?>
        </div>
        <div class="col-sm-4 col-xs-2">
<!--            <button type="button" class="btn" data-toggle="modal" data-target="#--><?php //echo $item->getAttributeLabel('val') ?><!--"><i class="fa fa-arrow-circle-up fa-2x"></i></button>-->
            <i class="fa fa-camera fa-2x" style="color:black;cursor: pointer" data-toggle="modal" data-target="#<?php echo $item->getAttributeLabel('val') ?>"></i>
        </div>
    </div>
    <div class="modal fade" id="<?php echo $item->getAttributeLabel('val') ?>" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php echo $item->getAttributeLabel('val') ?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="imagen-measure-modal" style="display: block">
                            <img src="<?php echo  Yii::app()->baseUrl.$item->MeasurementType->idAttachment0->path ?>" style="margin: 0 auto">
                        </div>
                        <div class="video-measure-modal" style="display: none">
                            <?php if(!empty($item->MeasurementType->youtubeId)): ?>
                                <?php $this->widget('ext.Yiitube', array('v' => $item->MeasurementType->youtubeId,'size'=>'bioprint')); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-xs-5 text-left clear">
                                <i class="fa fa-camera fa-2x" style="color:black;cursor: pointer" ></i>
                                <?php if(!empty($item->MeasurementType->youtubeId)): ?>
<!--                                    <i class="fa fa-youtube-play fa-2x" style="cursor: pointer" ></i>-->
                                <?php endif; ?>
                            </div>
                            <div class="col-xs-7 text-right">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <?php endforeach; ?>
    <div class="modal fade" id="youtube" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Key Points of Skinfold Measurements For S&M Software</h4>
                </div>
                <div class="modal-body">
                    <div style="width: 450px;height: 300px;text-align: center;margin: 0 auto;">
                        <?php $this->widget('ext.Yiitube', array('v' => 'hYFOVWINKvw','size'=>'bioprint')); ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <?php echo CHtml::submitButton('Save',array('id'=>'btnSave','class' => 'btn btn-success btn-lg btn-block')); ?>
    <?php echo CHtml::endForm(); ?>
</div><!-- form -->