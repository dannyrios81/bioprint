<div class="col-xs-12">
    <h1>Analysis</h1>
    <h3>&nbsp;</h3>
</div>
<hr class="separator">
<div class="col-sm-12 main-content analysis-table">

        <?php //CVarDumper::dump($homoneUserDate,10,true);exit; ?>
        <?php
         $header = true;
         $idProtocol = 0;
//         $title = '';
         foreach ($homoneUserDate as $index => $item) {
             $serie = [];
//             CVarDumper::dump($item,10,true);exit;
//             $title = $item->idHormoneFamily0->name.'  ('. $item->deviation.')';
             if($item->idHormoneFamily0->id == Yii::app()->params['HormoneFamiliesWithPriority']['HormoneFamilyIdTyroid'])
                $serie['name'] = $item->idHormoneFamily0->name;
             else
                 $serie['name'] = $item->idHormoneFamily0->name.'  ('. $item->deviation.')';
             $serie['color'] = '#8E9399';
             $serie['data'] = [];
             $idProtocol = ($idProtocol == 0 ? $item->id : $idProtocol);
             $i=0;
             foreach ($measurementsOrder[$item->id] as  $value) {
                 if($value['visible'])
                 {
                     $serie['data'][$i]['name'] = $value['measurementType']->name;
                     $serie['data'][$i]['y'] = (double)$value['score'];
                 }
                 else
                 {
                     $serie['data'][$i]['name'] = "";
                     $serie['data'][$i]['y'] = 0;
                 }

                 if (isset($value['color'])) {
                     $serie['data'][$i]['color'] = $value['color'];
                 }
                 $i++;
             }

             $this->renderPartial('graph', ['serie' => $serie]);
//             CVarDumper::dump($serie,10,true);
         }
//        exit;
        ?>
    <div class="clearfix"></div>
    <div class="text-center" style="margin-top: 20px">
        <?php echo CHtml::link('Back',['measureUser/index','id'=>$idUser],['class'=>"btn btn-default"]) ?>
        <?php echo CHtml::link('Protocol',['measureUser/protocol','id'=>$idProtocol],['class'=>"btn btn-primary"]) ?>
    </div>
</div>