<?php
$urlAddMeasurements =  Yii::app()->createAbsoluteUrl("measureUser/Addmeasurement",["id"=>$user->id]);
$urlDelMeasurementsSet =  Yii::app()->createAbsoluteUrl("measureUser/DelMeasurementsSet",["id"=>$user->id]);

$scriptEnable =<<<JS
    $(document).on('click','.plus-symbol',function(){
        window.location.href = "$urlAddMeasurements"

    });
    $(document).on('click','.delete',function(){
        console.log($(this).prop('id').replace('md-','').replace('dd-',''));
        var dateMeasurement = $(this).prop('id').replace('md-','').replace('dd-','');
        if(!confirm('Are you sure you want to delete this item?')) return false;

        $.ajax({
            url: '{$urlDelMeasurementsSet}',
            type: 'POST',
            data: {'date':dateMeasurement},
            async: false,
            success: function(response){
                window.location.reload();
                console.log(response);
            },
            error: function(response){
                if(response.hasOwnProperty('readyState')){
                    alert(response.statusText + ' ' + response.responseText)
                }
                console.log(response);
            }
        });


    });
    
JS;
$scriptColapse =<<<JS
    $(document).on('click','.colapsar',function(){
        event.preventDefault();
        var divName = '#'+$(this).data('info');
        $(divName).slideToggle();

    });
JS;
$style = <<<CSS
td {
    min-width: 26px;
}
CSS;

Yii::app()->clientScript->registerScript('scriptCollapse',$scriptColapse,CClientScript::POS_READY);
Yii::app()->clientScript->registerCss('style',$style);
if(Yii::app()->user->getState('userType')=='Trainer' or Yii::app()->user->getState('userType')=='Practitioner1')
    Yii::app()->clientScript->registerScript('scriptEnable',$scriptEnable,CClientScript::POS_READY);
?>
<div class="col-xs-12">
    <div class="col-xs-12 container-avatar">

        <div class="col-xs-12 col-sm-4 col-sm-push-8">
            <img src="<?php echo Yii::app()->baseUrl.$photo ?>" class="img-circle" alt="User Image" src="User-Image">
            <?php if(!empty($user->name) and !empty($user->lastName) and !empty($user->email)): ?>
                <div class="col-sm-12 userInfoImg">
                    <div class="col-sm-12 text-center"><?php echo ucwords(strtolower($user->name.' '.$user->lastName)); ?> </div>
                    <div class="col-sm-12 text-center"><?php echo $user->email; ?></div>
                </div>
            <?php endif;?>
        </div>
        <div class="col-xs-12 col-sm-8 col-sm-pull-4 ">
            <h1>Measurements</h1>
        </div>

    </div>
</div>
<div class="col-sm-12 main-content measures-table">
    <ul id="switch-btn">
        <li class="selected" data-info="mobile-normal">Normal</li>
        <li class="" data-info="mobile-diff">Difference</li>
        <li class="" data-info="mobile-percent">Percentage</li>
    </ul>
    <span class="left-arrow close fa fa-chevron-left"> </span>
    <span class="right-arrow open fa fa-chevron-right"></span>
<!--Tablas para moviles-->
    <!--Tabla De Medidas-->
    <table class="table-responsive hidden-lg hidden-md hidden-sm visible-xs" id="mobile-normal">
        <tr>
            <th colspan="2" class="plus-symbol">+</th>
            <?php $headers=array_keys($mobile) ?>
            <?php foreach ($headers as $header):?>
            <th><span><?php echo date('d/m/Y',strtotime($header)) ?></span></th>
            <?php endforeach;?>
        </tr>
        <tr>
            <th colspan="2">Age</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$header); ?></td>
            <?php endforeach;?>
        </tr>
        <tr>
            <th rowspan="2">Height</th>
            <th>Cm</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->heightUnit=='Cm'?number_format($WH[$header]->height,1):'&nbsp;'):'&nbsp;' ?></td>
            <?php endforeach; ?>
        </tr>
        <tr>
            <th>In</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->heightUnit=='In'?number_format($WH[$header]->height,1):'&nbsp;'):'&nbsp;'  ?></td>
            <?php endforeach; ?>
        </tr>
        <tr>
            <th rowspan="2">Weight</th>
            <th>Kg</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->weightUnit=='Kg'?number_format($WH[$header]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
            <?php endforeach; ?>
        </tr>
        <tr>
            <th>Lb</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->weightUnit=='Lbs'?number_format($WH[$header]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
            <?php endforeach; ?>
        </tr>

        <!--filas de las medidas -->
            <?php foreach ($desktop as $key=>$measure):?>
                <?php //CVarDumper::dump($desktop,10,true);exit; ?>
                <?php if($key != 'Lean Mass' and $key != 'Fat Mass'): ?>
                    <tr><th colspan="2"><?php echo $key ?></th><td><?php echo implode('</td><td>',$measure) ?></td></tr>
                <?php else: ?>
                    <tr>
                        <th rowspan="2"><?php echo $key ?></th>
                        <th>Kg</th>
                        <?php foreach ($measure as $date => $item):?>
                            <td><?php echo isset($WH[$date])?($WH[$date]->weightUnit=='Kg'?$item:'&nbsp;'):'&nbsp;' ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <th>Lb</th>
                        <?php foreach ($measure as $date => $item):?>
                            <td><?php echo isset($WH[$date])?($WH[$date]->weightUnit=='Lbs'?$item:'&nbsp;'):'&nbsp;' ?></td>
                        <?php endforeach; ?>
                    </tr>
                <?php endif; ?>
            <?php endforeach;?>
        <!--fin filas medidas-->
        <?php if(Yii::app()->user->getState('userType')=='Trainer' or Yii::app()->user->getState('userType')=='Practitioner1'): ?>
        <tr><th colspan="2">Analysis</th>
            <?php foreach ($headers as $header):?>
                <?php echo "<td class='analysis' id='md-$header'>".CHtml::link('<i class=\'fa fa-bar-chart fa-2x\'></i>',['MeasureUser/analisisMeasurement','id'=>strtotime($header),'idUser'=>$idUser])."</td>";?>
            <?php endforeach;?>
        </tr>
        <tr><th colspan="2">Edit</th>
            <?php foreach ($headers as $header):?>
                <?php echo "<td class='edit' id='me-$header'>".CHtml::link('<i class=\'fa fa-pencil-square-o fa-2x\'></i>',['MeasureUser/Editmeasurement','id'=>strtotime($header),'idUser'=>$idUser])."</td>";?>
            <?php endforeach;?>
        </tr>
        <tr><th colspan="2">Delete</th><?php foreach ($headers as $header) {echo "<td class='delete' id='md-$header'><i class='fa fa-trash fa-2x'></i></td>";}?></tr>
        <?php endif;?>
    </table>
    <!--Tabla de diferencias-->
    <?php $dateRemove = $headers[0] ?>
    <?php unset($headers[0]); ?>
    <table class="table-responsive hidden hidden-lg hidden-md hidden-sm " id="mobile-diff">
        <tr>
            <th colspan="2" class="plus-symbol">+</th>
            <?php foreach ($headers as $header):?>
                <th><span><?php echo date('d/m/Y',strtotime($header)) ?></span></th>
            <?php endforeach;?>
        </tr>
        <tr>
            <th colspan="2">Age</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$header); ?></td>
            <?php endforeach;?>
        </tr>
        <tr>
            <th rowspan="2">Height</th>
            <th>Cm</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->heightUnit=='Cm'?number_format($WH[$header]->height,1):'&nbsp;'):'&nbsp;' ?></td>
            <?php endforeach; ?>
        </tr>
        <tr>
            <th>In</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->heightUnit=='In'?number_format($WH[$header]->height,1):'&nbsp;'):'&nbsp;'  ?></td>
            <?php endforeach; ?>
        </tr>
        <tr>
            <th rowspan="2">Weight</th>
            <th>Kg</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->weightUnit=='Kg'?number_format($WH[$header]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
            <?php endforeach; ?>
        </tr>
        <tr>
            <th>Lb</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->weightUnit=='Lbs'?number_format($WH[$header]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
            <?php endforeach; ?>
        </tr>
        <!--filas de las medidas -->
        <?php foreach ($desktopDiff as $key=>$measure):?>
            <?php unset($measure[$dateRemove]);?>
            <?php //CVarDumper::dump($measure,10,true);exit; ?>
            <?php if($key != 'Lean Mass' and $key != 'Fat Mass'): ?>
                <tr><th colspan="2"><?php echo $key ?></th><td><?php echo implode('</td><td>',$measure) ?></td></tr>
            <?php endif; ?>
        <?php endforeach;?>
        <!--fin filas medidas-->
    </table>
    <!--Tabla de Porcentaje-->
    <table class="table-responsive hidden hidden-lg hidden-md hidden-sm " id="mobile-percent">
        <tr>
            <th colspan="2" class="plus-symbol">+</th>
            <?php foreach ($headers as $header):?>
                <th><span><?php echo date('d/m/Y',strtotime($header)) ?></span></th>
            <?php endforeach;?>
        </tr>
        <tr>
            <th colspan="2">Age</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$header); ?></td>
            <?php endforeach;?>
        </tr>
        <tr>
            <th rowspan="2">Height</th>
            <th>Cm</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->heightUnit=='Cm'?number_format($WH[$header]->height,1):'&nbsp;'):'&nbsp;' ?></td>
            <?php endforeach; ?>
        </tr>
        <tr>
            <th>In</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->heightUnit=='In'?number_format($WH[$header]->height,1):'&nbsp;'):'&nbsp;'  ?></td>
            <?php endforeach; ?>
        </tr>
        <tr>
            <th rowspan="2">Weight</th>
            <th>Kg</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->weightUnit=='Kg'?number_format($WH[$header]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
            <?php endforeach; ?>
        </tr>
        <tr>
            <th>Lb</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo isset($WH[$header])?($WH[$header]->weightUnit=='Lbs'?number_format($WH[$header]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
            <?php endforeach; ?>
        </tr>
        <!--filas de las medidas -->
        <?php foreach ($desktopPercent as $key=>$measure):?>
            <?php unset($measure[$dateRemove]);?>
            <?php //CVarDumper::dump($desktop,10,true);exit; ?>
            <?php if($key != 'Lean Mass' and $key != 'Fat Mass'): ?>
                <tr><th colspan="2"><?php echo $key ?></th><td><?php echo implode('</td><td>',$measure) ?></td></tr>
            <?php endif; ?>
        <?php endforeach;?>
        <!--fin filas medidas-->
    </table>
<!--Tablas para desktop-->
    <!--Tabla De Medidas-->
    <h2><?php echo CHtml::link('Measurements','#',['class'=>'colapsar visible-lg visible-md visible-sm hidden-xs','data-info'=>'desktop-normal-div']) ?></h2>
    <div id="desktop-normal-div" >
        <table  class="visible-lg visible-md visible-sm hidden-xs" id="desktop-normal">
            <tr rowspan="2">
                <th rowspan="2" class="plus-symbol">+</th>
                <th rowspan="2"><span>Age</span></th>
                <th colspan="2">Height</th>
                <th colspan="2">Weight</th>
                <th rowspan="2"><span>Chin</span></th>
                <th rowspan="2"><span>Cheek</span></th>
                <th rowspan="2"><span>Pec</span></th>
                <th rowspan="2"><span>Biceps</span></th>
                <th rowspan="2"><span>Triceps</span></th>
                <th rowspan="2"><span>S.Scap</span></th>
                <th rowspan="2"><span>Austr</span></th>
                <th rowspan="2"><span>Midax</span></th>
                <th rowspan="2"><span>Supra</span></th>
                <th rowspan="2"><span>Umbil</span></th>
                <th rowspan="2"><span>Knee</span></th>
                <th rowspan="2"><span>Calves</span></th>
                <th rowspan="2"><span>Quadric</span></th>
                <th rowspan="2"><span>Hamst</span></th>
                <th rowspan="2"><span>Sum</span></th>
                <th rowspan="2"><span>%Fat</span></th>
                <th colspan="2">Fat Mass</th>
                <th colspan="2">Lean Mass</th>
                <?php if(Yii::app()->user->getState('userType')=='Trainer' or Yii::app()->user->getState('userType')=='Practitioner1'): ?>
                    <th rowspan="2"><span>Analysis<span></th>
                    <th rowspan="2"><span>Edit<span></th>
                    <th rowspan="2"><span>Delete<span></th>
                <?php endif;?>
            </tr>
            <tr>
                <th>cm</th>
                <th>In</th>
                <th>Kg</th>
                <th>Lb</th>
                <th>Kg</th>
                <th>Lb</th>
                <th>Kg</th>
                <th>Lb</th>
            </tr>
            <?php foreach ($mobile as $key=>$item): ?>
                <tr>
                    <th><?php echo  date('d/m/Y',strtotime($key)) ?></th>
                    <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$key); ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->heightUnit=='Cm'?number_format($WH[$key]->height,1):'&nbsp;'):'&nbsp;'; ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->heightUnit=='In'?number_format($WH[$key]->height,1):'&nbsp;'):'&nbsp;'; ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->weightUnit=='Kg'?number_format($WH[$key]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->weightUnit=='Lbs'?number_format($WH[$key]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
                    <?php foreach($item as $measureName=>$value): ?>
                        <?php if(strpos($measureName, 'Mass') === false): ?>
                            <td><?php echo $value ?></td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td><?php echo isset($WH[$key])?($WH[$key]->weightUnit=='Kg'?$item['Fat Mass']:'&nbsp;'):'&nbsp;' ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->weightUnit=='Lbs'?$item['Fat Mass']:'&nbsp;'):'&nbsp;' ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->weightUnit=='Kg'?$item['Lean Mass']:'&nbsp;'):'&nbsp;' ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->weightUnit=='Lbs'?$item['Lean Mass']:'&nbsp;'):'&nbsp;' ?></td>
                    <?php if(Yii::app()->user->getState('userType')=='Trainer' or Yii::app()->user->getState('userType')=='Practitioner1'): ?>
                        <td class='analysis' id="da-<?php echo $key ?>"><?php echo CHtml::link('<i class=\'fa fa-bar-chart\'></i>',['MeasureUser/analisisMeasurement','id'=>strtotime($key),'idUser'=>$idUser]) ?></td>
                        <td class='edit' id="de-<?php echo $key ?>"><?php echo CHtml::link('<i class=\'fa fa-pencil-square-o\'></i>',['MeasureUser/Editmeasurement','id'=>strtotime($key),'idUser'=>$idUser]) ?></td>
                        <td class='delete' id="dd-<?php echo $key ?>"><i class="fa fa-trash"></i></td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </table>
        <br class="visible-lg visible-md visible-sm hidden-xs" /><br class="visible-lg visible-md visible-sm hidden-xs" />
    </div>
    <!--Tabla de diferencias-->
    <h2><?php echo CHtml::link('Difference','#',['class'=>'colapsar visible-lg visible-md visible-sm hidden-xs','data-info'=>'desktop-diff-div']) ?></h2>
    <div id="desktop-diff-div" style="display: none">
        <table  class="visible-lg visible-md visible-sm hidden-xs" id="desktop-diff">
            <tr rowspan="2">
                <th rowspan="2" class="plus-symbol">+</th>
                <th rowspan="2"><span>Age</span></th>
                <th colspan="2">Height</th>
                <th colspan="2">Weight</th>
                <th rowspan="2"><span>Chin</span></th>
                <th rowspan="2"><span>Cheek</span></th>
                <th rowspan="2"><span>Pec</span></th>
                <th rowspan="2"><span>Biceps</span></th>
                <th rowspan="2"><span>Triceps</span></th>
                <th rowspan="2"><span>S.Scap</span></th>
                <th rowspan="2"><span>Austr</span></th>
                <th rowspan="2"><span>Midax</span></th>
                <th rowspan="2"><span>Supra</span></th>
                <th rowspan="2"><span>Umbil</span></th>
                <th rowspan="2"><span>Knee</span></th>
                <th rowspan="2"><span>Calves</span></th>
                <th rowspan="2"><span>Quadric</span></th>
                <th rowspan="2"><span>Hamst</span></th>
                <th rowspan="2"><span>Sum</span></th>
                <th rowspan="2"><span>%Fat</span></th>
            </tr>
            <tr>
                <th>cm</th>
                <th>In</th>
                <th>Kg</th>
                <th>Lb</th>
            </tr>
            <?php if(isset($mobileDiff[$dateRemove])):?>
            <?php unset($mobileDiff[$dateRemove]);?>
            <?php endif;?>
            <?php foreach ($mobileDiff as $key=>$item): ?>
                <tr>
                    <th><?php echo  date('d/m/Y',strtotime($key)) ?></th>
                    <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$key); ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->heightUnit=='Cm'?number_format($WH[$key]->height,1):'&nbsp;'):'&nbsp;'; ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->heightUnit=='In'?number_format($WH[$key]->height,1):'&nbsp;'):'&nbsp;'; ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->weightUnit=='Kg'?number_format($WH[$key]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->weightUnit=='Lbs'?number_format($WH[$key]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
                    <td><?php echo implode('</td><td>',array_slice($item, 0,count($item)-3)) ?></td>
                    <td><?php echo $item['% Fat'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
        <br /><br />
    </div>
    <!--Tabla de Porcentaje-->
    <h2><?php echo CHtml::link('Changes (%)','#',['class'=>'colapsar visible-lg visible-md visible-sm hidden-xs','data-info'=>'desktop-percent-div']) ?></h2>
    <div id="desktop-percent-div" style="display: none">
        <table  class="visible-lg visible-md visible-sm hidden-xs" style="display: none" id="desktop-percent">
            <tr rowspan="2">
                <th rowspan="2" class="plus-symbol">+</th>
                <th rowspan="2"><span>Age</span></th>
                <th colspan="2">Height</th>
                <th colspan="2">Weight</th>
                <th rowspan="2"><span>Chin</span></th>
                <th rowspan="2"><span>Cheek</span></th>
                <th rowspan="2"><span>Pec</span></th>
                <th rowspan="2"><span>Biceps</span></th>
                <th rowspan="2"><span>Triceps</span></th>
                <th rowspan="2"><span>S.Scap</span></th>
                <th rowspan="2"><span>Austr</span></th>
                <th rowspan="2"><span>Midax</span></th>
                <th rowspan="2"><span>Supra</span></th>
                <th rowspan="2"><span>Umbil</span></th>
                <th rowspan="2"><span>Knee</span></th>
                <th rowspan="2"><span>Calves</span></th>
                <th rowspan="2"><span>Quadric</span></th>
                <th rowspan="2"><span>Hamst</span></th>
                <th rowspan="2"><span>Sum</span></th>
                <th rowspan="2"><span>%Fat</span></th>
            </tr>
            <tr>
                <th>cm</th>
                <th>In</th>
                <th>Kg</th>
                <th>Lb</th>
            </tr>
            <?php if(isset($mobilePercent[$dateRemove])):?>
                <?php unset($mobilePercent[$dateRemove]);?>
            <?php endif;?>
            <?php foreach ($mobilePercent as $key=>$item): ?>
                <tr>
                    <th><?php echo  date('d/m/Y',strtotime($key)) ?></th>
                    <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$key); ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->heightUnit=='Cm'?number_format($WH[$key]->height,1):'&nbsp;'):'&nbsp;'; ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->heightUnit=='In'?number_format($WH[$key]->height,1):'&nbsp;'):'&nbsp;'; ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->weightUnit=='Kg'?number_format($WH[$key]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
                    <td><?php echo isset($WH[$key])?($WH[$key]->weightUnit=='Lbs'?number_format($WH[$key]->weight,1):'&nbsp;'):'&nbsp;' ?></td>
                    <td><?php echo implode('</td><td>',array_slice($item, 0,count($item)-3)) ?></td>
                    <td><?php echo $item['% Fat'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>