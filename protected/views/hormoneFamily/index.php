<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#hormone-family-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
//CVarDumper::dump($model,10,true);exit;
$criteria = new CDbCriteria();
$criteria->order = 'name';
$criteria->addSearchCondition('calculable','0');

?>

<h1>Manage Hormone Families</h1>
<hr class="separator">
<div class="col-sm-12 form-container">
	<div class="text-right">
		<a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('hormoneFamily/create')?>"><i class="fa fa-plus-circle"></i> New hormone family</a>
	</div>
</div>
<br>
<br>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'hormone-family-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
		'name',
		['header'=>'Associated Site 1','name'=>'idAssociatedMeasurementType1','value'=>'!empty($data->idAssociatedMeasurementType1)?$data->idAssociatedMeasurementType10->name:""','filter'=>CHtml::activeDropDownList($model,'idAssociatedMeasurementType1',CHtml::listData(MeasurementTypes::model()->findAll($criteria),'id','name'),['empty'=>'Select ...'])],
		['header'=>'Associated Site 2','name'=>'idAssociatedMeasurementType2','value'=>'!empty($data->idAssociatedMeasurementType2)?$data->idAssociatedMeasurementType20->name:""','filter'=>CHtml::activeDropDownList($model,'idAssociatedMeasurementType2',CHtml::listData(MeasurementTypes::model()->findAll($criteria),'id','name'),['empty'=>'Select ...'])],
		['header'=>'Associated Site 3','name'=>'idAssociatedMeasurementType3','value'=>'!empty($data->idAssociatedMeasurementType3)?$data->idAssociatedMeasurementType30->name:""','filter'=>CHtml::activeDropDownList($model,'idAssociatedMeasurementType3',CHtml::listData(MeasurementTypes::model()->findAll($criteria),'id','name'),['empty'=>'Select ...'])],
		array(
			'class'=>'CButtonColumn',
			'template'=>'{xupdate}{xdelete}',
			'header'=>"  Actions  ",
			'buttons'=>array(
				'xupdate'=>array(
					'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>',
					'url'=>'Yii::app()->createUrl("hormoneFamily/update", array("id"=>$data->id))',
					'options'=>array('title'=>'Update hormone-family')
				),
				'xdelete'=>array(
					'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>',
					'url'=>'Yii::app()->createUrl("hormoneFamily/delete", array("id"=>$data->id))',
					'visible'=>'$data->is_deletable()',
					'options'=>array('title'=>'Delete hormone-family'),
					'click'=>"function() {
						if(!confirm('Are you sure you want to delete this item?')) return false;
						var th = this,afterDelete = function(){};
							jQuery('#hormone-family-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
									jQuery('#hormone-family-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
					}"
				),
			),
		)
	),
)); ?>
</div>