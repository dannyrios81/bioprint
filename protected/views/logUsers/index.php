<?php
/* @var $this LogUsersController */
/* @var $dataProvider CActiveDataProvider */

?>

<h1>Users Deletes</h1>
<hr class="separator">
<div class="col-sm-12">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'users-delete-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'columns'=>array(
            'email',
            [
                'name'=>'deleted_date',
                'value'=>'Yii::app()->dateFormatter->format("yyyy-MM-dd",$data->deleted_date)',
                'filter'=>CHtml::activeDateField($model,'deleted_date',['class'=>"form-control"])
            ],
            [
                'header'=>'User Delete',
                'name'=>'idUserDelete',
                'value'=>'$data->idUsers0->name." ".$data->idUsers0->lastName'
            ],
            [
                'header'=>'Email Admin Delete',
                'name'=>'emailAdminDelete'
            ],
            [
                'name'=>'userType',
                'value'=>'($data->userType=="Trainer")?"Practitioner":($data->userType=="Practitioner1")?"New Practitioner":$data->userType'
            ],
        ),
    )); ?>
</div>
