<?php
/**
 *
 * @var BodyPartController $this
 * @var \BodyPart $model
 */
$baseUrl = Yii::app()->getBaseUrl(true);
$loadUrl = Yii::app()->createAbsoluteUrl('fase/ListBodyPart');
$script=<<<JS
function submitNewBodyPartForm(event){  
    // debugger;
    event.preventDefault();
    $.ajax({
            url: $('#bodyPart-form').prop('action'),
            type: 'POST',
            data: $('#bodyPart-form').serialize(),
            dataType:'json',
            success: function(response){
                console.log(response);
                // debugger;
                if(response.id<=0)
                    {
                        $("#ajaxModalNewBodyPart .modal-body").empty().html(response.html);
                        $("#ajaxModalNewBodyPart .modal-footer").css('visibility','hidden');
                    }
                    else
                    {
                        $('#containerSelectIdBodyPart').load('$loadUrl',{id:response.id});
                        //$('#selectIdBodyPart').val(response.id);
                        $("#ajaxModalNewBodyPart .close").click();
                    }
                
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location.href='$baseUrl';
                }
            }
        });
	
};
JS;
Yii::app()->clientScript->registerScript('Script',$script, CClientScript::POS_END);
?>
<div class="form">
    <?= CHtml::beginForm('','post',['id'=>'bodyPart-form','onSubmit'=>'submitNewBodyPartForm(event)']) ?>
    <div class="col-xs-12">
        <p class="note">Fields with <span class="required">*</span> are required.</p>
    </div>

    <?= CHtml::errorSummary($model,null,null, ['class'=>'form-group col-md-12']); ?>

    <div class="form-group col-md-12 <?= !empty(CHtml::error($model,'description'))?'has-error':''?>">
        <?= CHtml::activeLabelEx($model,'description',array('class'=>"control-label")); ?>
        <?= CHtml::activeTextField($model,'description',array('class'=>"form-control",'maxlength'=>255)); ?>
    </div>

    <div class=" buttons">
        <div class="col-xs-12">
            <?= CHtml::submitButton('Create',['class' => 'btn btn-success btn-lg btn-block']); ?>
        </div>
    </div>

    <?= CHtml::endForm() ?>
</div>
