<?php
/* @var $this BodyPartController */
/* @var $model BodyPart */

$this->breadcrumbs=array(
	'Body Parts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List BodyPart', 'url'=>array('index')),
	array('label'=>'Create BodyPart', 'url'=>array('create')),
	array('label'=>'Update BodyPart', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BodyPart', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BodyPart', 'url'=>array('admin')),
);
?>

<h1>View BodyPart #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
	),
)); ?>
