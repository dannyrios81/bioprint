<?php
/* @var $this BodyPartController */
/* @var $model BodyPart */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'body-part-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <?php echo $form->errorSummary($model); ?>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'description'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'description',array('class'=>"control-label",'maxlength'=>255)); ?>
        <?php echo $form->textField($model,'description',array('class'=>"form-control")); ?>
    </div>

    <div class="row form-actions">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->