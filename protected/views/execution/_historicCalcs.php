<?php
/**
 * @var ExecutionExerciseCalc $executionExerciseCalc
 */
//CVarDumper::dump($executionExerciseCalc,10,true);
?>

<table class="table table-condensed table-responsive table-bordered">
    <tr>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Avg Weight</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Willks</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Avg Reps</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Rel Str</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Tot Sets</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Avg Int</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Goal</th>
    </tr>
    <tr>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->averageWeight ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->willks ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->averageRepetitions ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->relStrength ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->totalSets ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->averageIntensity ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->goal ?></td>
    </tr>
    <tr>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Tot Tonnage</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Total Reps</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Avg Lift</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Max Lift</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">D-0%</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">%&#916;</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">%</th>
    </tr>
    <tr>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->totalTonnage ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->totalReps ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->averageLift ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->maximunLift ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->dropoff ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExerciseCalc[0]->delta ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><strong><?= number_format($executionExerciseCalc[0]->percent,2) ?></strong></td>
    </tr>
</table>

