<?php
/**
 *
 * @var ExecutionController $this
 * @var Execution $executions
 */
//CVarDumper::dump ($executions,10,true);exit;
$actionsModals=<<<JS
$('.modal').on('hidden.bs.modal', function () {
	/*debugger;*/
    $(this).find('iframe').attr("src", $(this).find('iframe').attr("src"));
});
$('.modal').on('shown.bs.modal', function () {
    $(this).find('.modal-body div').css('min-height','480px');
    
    $(this).find('.modal-body div').width($(this).find('.modal-body').width()-50);
    $(this).find('.modal-body div').height($(this).find('.modal-body').height()-50);
	    
});
JS;


Yii::app()->clientScript->registerScript('actionsModals',$actionsModals, CClientScript::POS_READY);
//CVarDumper::dump($executions,10,true);exit;
$renderPopups='';
$this->pageTitle = $executions->idUsers0->name ." ". $executions->idUsers0->lastName;
$this->renderPartial('_js');
$this->renderPartial('_titlePage');
$this->renderPartial('_executionData',['executions'=>$executions]);
foreach ($executions->executionExerciseCalcs as $executionExerciseCalc) {
    $this->renderPartial('_exercises',['executionExerciseCalc'=>$executionExerciseCalc]);
    $renderPopups=$renderPopups . $this->renderPartial('popupYoutube',['namePopup'=>"exercice".$executionExerciseCalc->id."-popup",'exercise'=>$executionExerciseCalc->idUserPhaseExercice0->idExercise0],true);
}
echo $renderPopups;
?>
<div class="text-right">
    <?php if(Yii::app()->user->getState('_user')->userType=='Admin' or Yii::app()->user->getState('_user')->userType=='Trainer' or Yii::app()->user->getState('_user')->userType=='Practitioner1'): ?>
    <a class="btn btn-primary deletePhase" style="margin-top: 20px;" data-userphaseid="<?= $executions->idUserPhase ?>" data-iduser="<?= $executions->idUsers ?>" href="<?php echo Yii::app()->createAbsoluteUrl('execution/deleteExecution',['id'=>$executions->id])?>">Delete Execution</a>
    <?php endif; ?>
    <a class="btn btn-primary" style="margin-top: 20px;" href="<?php echo Yii::app()->createAbsoluteUrl('Fase/index',['id'=>$executions->idUsers])?>">Return to Phases</a>

</div>

