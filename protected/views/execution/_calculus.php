<?php
/**
 *
 * @var ExecutionExerciseCalc $executionExerciseCalc
 */
//CVarDumper::dump ($executionExerciseCalc,10,true);
?>
<table class="table table-condensed table-responsive table-bordered" style="margin-bottom: 0px">
    <tr>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Avg Weight</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Willks</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Avg Reps</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Rel Str</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Tot Sets</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Avg Int</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Goal</th>
    </tr>
    <tr>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= number_format($executionExerciseCalc->averageWeight,2);  ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= number_format($executionExerciseCalc->willks,2) ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= number_format($executionExerciseCalc->averageRepetitions,2) ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= number_format($executionExerciseCalc->relStrength,2) ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= number_format($executionExerciseCalc->totalSets,2) ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= number_format($executionExerciseCalc->averageIntensity,2) ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= CHtml::activeTextField($executionExerciseCalc,'goal',['style'=>'width: 30px;']) ?></td>
    </tr>
    <tr>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Tot Tonnage</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Total Reps</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Avg Lift</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">Max Lift</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">D-0%</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">%&#916;</th>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">%</th>
    </tr>
    <tr>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= number_format($executionExerciseCalc->totalTonnage,2) ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= number_format($executionExerciseCalc->totalReps,2) ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= number_format($executionExerciseCalc->averageLift,2) ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= number_format($executionExerciseCalc->maximunLift,2) ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= is_null($executionExerciseCalc->dropoff)?"":number_format($executionExerciseCalc->dropoff,2); ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= number_format($executionExerciseCalc->delta,2) ?></td>
        <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><strong><?= number_format($executionExerciseCalc->percent,2) ?></strong><?= CHtml::activeHiddenField($executionExerciseCalc,'id') ?>&nbsp;</td>
    </tr>
</table>
