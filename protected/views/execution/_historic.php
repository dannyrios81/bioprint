<?php
/**
 * @var ExecutionController $this
 * @var array Execution $executions
 * @var string $nameExercise
 * @var Execution $execution
 */
//CVarDumper::dump($executions,10,true);exit;
$render = '';
?>
<div class="row" style="margin-top: 10px">
    <ul class="nav nav-tabs" style="border-bottom: 0px solid #ddd;">
        <?php foreach ($executions as $index => $execution): ?>
            <?php //CVarDumper::dump($execution,10,true);exit; ?>
            <?php if($index>5) break;?>
            <?php if($index==0): ?>
                <li class="active">
                    <a data-toggle="tab" href="#<?= Utilities::slugify(date('Y-m-d',strtotime($execution->date)).'-'.$nameExercise.''.$execution->executionExerciseCalcs[0]->id) ?>">
                        <?= date('Y-m-d',strtotime($execution->date)) ?>
                    </a>
                </li>
            <?php else:?>
                <li>
                    <a data-toggle="tab" href="#<?= Utilities::slugify(date('Y-m-d',strtotime($execution->date)).'-'.$nameExercise.''.$execution->executionExerciseCalcs[0]->id) ?>">
                        <?= date('Y-m-d',strtotime($execution->date)) ?>
                    </a>
                </li>
            <?php endif; ?>
            <?php $render = $render.$this->renderPartial('_historicPart',['execution'=>$execution,'nameExercise'=>$nameExercise,'index'=>$index],true)  ?>
        <?php endforeach; ?>
    </ul>
    <div class="tab-content row">
        <?= $render ?>
    </div>
</div>
