<?php
/**
 *
 */

$aviso=Yii::app()->user->getFlash('new');


$scriptColapse =<<<JS
    function autosave(element){
        var url=$(element).prop('href');
        var form = $(element).parents("form");
        var data = $.merge($('#executionForm').serializeArray(),form.serializeArray());
        
        $.ajax(
            {
                url: url, 
                method: "POST",
                dataType: "json",
                context: form,
                forms: form, 
                data: data,
                success: function(result){
                    console.log(result);
                    $(result.selectorCalculus).html(result.calculus);
                    $(result.selectorDetails).html(result.details);
                }
            }
         );
    }
    $(document).on('click','.title-link',function(event){
        event.preventDefault();
        var divName = '#'+$(this).data('toggle');
        // $('.slideContent').slideUp('fast');
        $(divName).find('.slideContent').slideToggle();
        if($(this).find('.icon-change>i').hasClass("fa-plus-square"))
            $(this).find('.icon-change>i').removeClass("fa-plus-square").addClass("fa-minus-square");
        else
            $(this).find('.icon-change>i').removeClass("fa-minus-square").addClass("fa-plus-square");
    });
    $(document).on('click','.btn-save',function(event){
        event.preventDefault();
        autosave(this);
        
    });
    /*$(document).on('keypress','.txt_weigth_valid',function(event){
        //event.preventDefault();
        var value = parseFloat($(this).val() + event.key);
        
        if (isNaN(value) || value < 0 || value > 300)
        {
            alert("invalid value, must be a positive number and less than or equal to 300");
            return false;
        }
    });
    $(document).on('keypress','.txt_repetitions_valid',function(event){
        // event.preventDefault();
        var value = parseFloat($(this).val() + event.key);
        
        if (isNaN(value) || value < 0 || value > 30)
        {
            alert("invalid value, must be a positive number and less than or equal to 30");
            return false;
        }
        // return true;
    });*/
    
    $(document).on('click','.deletePhase',function(event){
        event.preventDefault();
        var r = confirm("Are you sure you want to delete this execution?");
        if (r)
        {
            var id=$(this).data('userphaseid');
            var idUser=$(this).data('iduser');
            // console.log($(this).attr('href'));
            // console.log(id);
            $.ajax({
                url:$(this).attr('href'),
                method: "POST",
                data: {idUserPhase:id,idUser:idUser},
                async:false,
                dataType:'json',
                success:function(response) {
                    history.replaceState( {} , 'foo', response.url );
                    window.location=response.url;                      
                }
            });
        }
        
    });
    $(document).on('change','input[name="Execution[unitType]"]',function(event){
        var value = $('input[name="Execution[unitType]"]:checked').val();
        var weight = $('input[name="Execution[weigth]"]').val(); 
        
        if(value == 'Kg')
        {
            // dividir el valor del peso en 2.2046  
            weight = Math.round((weight/2.2046) * 100) / 100;  
            $('input[name="Execution[weigth]"]').val(weight);
        }
        else
        {
            weight = Math.round((weight*2.2046) * 100) / 100;
            $('input[name="Execution[weigth]"]').val(weight);
        }
        $('.btn-save').each(function(index,element){
                autosave(element);
         });
        
    });
    $(document).on('change','.unitTypeEx',function(event){
		var form = $(this).parents('form');
        var value = $(form).find('.unitTypeEx:checked').val();
	    var weight = 0;
	    var goal = 0;
        if(value == 'Kg')
        {	
			$(form).find('input[name$="[weigth]"]').each(function(index,element){
				weight = $(element).val();
                weight = Math.round((weight/2.2046) * 100) / 100;
				$(element).val(weight);
            });
			$(form).find('input[name$="[goal]"]').each(function(index,element){
				goal = $(element).val();
                goal = Math.round((goal/2.2046) * 100) / 100;
				$(element).val(goal);
            });
        }
        else
        {
			$(form).find('input[name$="[weigth]"]').each(function(index,element){
				weight = $(element).val();
                weight = Math.round((weight*2.2046) * 100) / 100;
				$(element).val(weight);
            });
			$(form).find('input[name$="[goal]"]').each(function(index,element){
				goal = $(element).val();
                goal = Math.round((goal*2.2046) * 100) / 100;
				$(element).val(goal);
            });
        }
		autosave($(form).find('.btn-save'));
       
    });

    var interval = setInterval(
        function()
        { 
            $('.btn-save:visible').each(function(index,element){
                autosave(element);
            });
        }, 60000
        );
JS;
$execution = <<<JS
    (function(){
        alert('$aviso');
    })();
JS;


Yii::app()->clientScript->registerScript('scriptCollapse',$scriptColapse,CClientScript::POS_READY);
if(!empty($aviso))
    Yii::app()->clientScript->registerScript('execution',$execution,CClientScript::POS_LOAD);

?>