<?php
/**
 *
 * @var string $namePopup
 * @var Exercise $exercise
 */
?>
<div class="modal fade" id="<?= $namePopup ?>" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?= $exercise->description ?></h4>
            </div>
            <div class="modal-body">
                <div class="video-measure-modal text-center" style="margin: 0 auto;">
                    <?php $this->widget('ext.Yiitube', array('v' => $exercise->video,'size'=>'bioprint')); ?>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
