<?php
/**
 * @var ExecutionController $this
 * @var ExecutionExerciseCalc $executionExerciseCalc
 */
?>
<div class="exercice" id="exercice<?= $executionExerciseCalc->id?>">
    <div class="row">
        <h2>
            <div class="col-xs-12">
                <a class="title-link" href="#" data-toggle="exercice<?= $executionExerciseCalc->id?>">
                    <span class="icon-change"><i class="fa fa-plus-square" aria-hidden="true"></i></span>
                    <?= $executionExerciseCalc->idUserPhaseExercice0->order ?> : <?= $executionExerciseCalc->idUserPhaseExercice0->idExercise0->description ?>
                </a>
                <a href="#" data-toggle="modal" data-target="#<?= "exercice".$executionExerciseCalc->id."-popup" ?>"><!--<i class="fa fa-youtube-play" style="font-size: 40px"></i>--></a>
            </div>
        </h2>
    </div>
    <div class="slideContent" style="display: none;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;padding: 10px;font-size: 10px ">
        <?= CHtml::beginForm() ?>
        <div class="row">
            <div class="col-sm-12" id="Information<?= $executionExerciseCalc->id?>">
                <?php $this->renderPartial('_information',['exerciceInfo'=>$executionExerciseCalc]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7 col-xs-12" id="Details<?= $executionExerciseCalc->id?>" style="padding-left: 0px;padding-right: 0px">
                <?php $this->renderPartial('_executionDetails',['executionExercises'=>$executionExerciseCalc->executionExercises]) ?>
            </div>
            <div class="col-md-5 col-xs-12" id="Calculations<?= $executionExerciseCalc->id?>" >
                <?php $this->renderPartial('_calculus',['executionExerciseCalc'=>$executionExerciseCalc]) ?>
            </div>
        </div>
        <?php $this->renderPartial('_historic',['executions'=>$this->getHistoric($executionExerciseCalc->idUserPhaseExercice,$executionExerciseCalc->idExecution),
                                                      'nameExercise'=>$executionExerciseCalc->idUserPhaseExercice0->idExercise0->description,
                                                      'userPhaseExercise'=>$executionExerciseCalc->idUserPhaseExercice]); ?>
        <div class="text-right">
            <a class="btn btn-primary btn-save btn-lg btn-block" style="margin-top: 20px;" href="<?php echo Yii::app()->createAbsoluteUrl('execution/update')?>">Save</a>
        </div>
        <?= CHtml::endForm()?>
    </div>
</div>

