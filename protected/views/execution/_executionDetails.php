<?php
/**
 *
 * @var ExecutionExercise $executionExercises
 */
?>
<div class="col-md-1">
    <div class="col-md-1 col-xs-4 text-center col-sm-4" style="padding-left: 0px;line-height: 18px"><strong style="line-height: 10px">W</strong></div>
    <div class="col-md-1 col-xs-4 text-center col-sm-4" style="padding-left: 0px;line-height: 18px"><strong>R</strong></div>
    <div class="col-md-1 col-xs-4 text-center col-sm-4" style="padding-left: 0px;line-height: 18px"><strong>PM</strong></div>
</div>
<div class="col-md-11">
    <?php for($i=0;$i<12;$i++): ?>
        <div class="col-md-1 text-center" style="padding-left: 0px;padding-right: 0px">
            <?php if(isset($executionExercises[$i])):?>
                <?= CHtml::activeTextField($executionExercises[$i],"[$i]weigth",['value'=>(isset($executionExercises[$i]) and !is_null($executionExercises[$i]->weigth))?number_format($executionExercises[$i]->weigth,2):'','class'=>'col-md-12 col-xs-4 textfieldChange txt_weigth_valid','style'=>'padding: 0px;']) ?>
                <?= CHtml::activeTextField($executionExercises[$i],"[$i]repetitions",['class'=>'col-md-12 col-xs-4 textfieldChange txt_repetitions_valid','style'=>'padding: 0px;']) ?>
                <?= CHtml::activeTextField($executionExercises[$i],"[$i]weigthModificate",['value'=>(isset($executionExercises[$i]) and !empty((double)$executionExercises[$i]->weigthModificate))?number_format($executionExercises[$i]->weigthModificate,2):'','class'=>'col-md-12 col-xs-4 textfieldChange','style'=>'padding: 0px;','disabled'=>'disabled']) ?>
                <?= CHtml::activeHiddenField($executionExercises[$i],"[$i]id",['class'=>'col-md-12 col-xs-4 textfieldChange','style'=>'padding: 0px;']) ?>
            <?php endif; ?>
        </div>
    <?php endfor; ?>
</div>
