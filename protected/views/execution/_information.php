<?php
/**
 * @var ExecutionExerciseCalc $exerciceInfo
 */
?>
<table class="table table-condensed table-responsive table-bordered">
    <tr>
        <th style="padding: 2px;text-align: center;vertical-align: middle">Order</th>
        <th style="padding: 2px;text-align: center;vertical-align: middle">Range</th>
        <th style="padding: 2px;text-align: center;vertical-align: middle">Sets</th>
        <th style="padding: 2px;text-align: center;vertical-align: middle">Rep Int</th>
        <th style="padding: 2px;text-align: center;vertical-align: middle">Tempo</th>
        <th style="padding: 2px;text-align: center;vertical-align: middle">Rest(secs.)</th>
        <th style="padding: 2px;text-align: center;vertical-align: middle">Units</th>
        <th style="padding: 2px;text-align: center;vertical-align: middle"><?= CHtml::label('Notes', 'notas') ?></th>
    </tr>
    <tr>
        <td style="padding: 2px;text-align: center;vertical-align: middle"><?= $exerciceInfo->idUserPhaseExercice0->order ?></td>
        <td style="padding: 2px;text-align: center;vertical-align: middle"><?= (!empty($exerciceInfo->idUserPhaseExercice0->rangeMotion) and isset($exerciceInfo->idUserPhaseExercice0->idRangeMotion0->description))?$exerciceInfo->idUserPhaseExercice0->idRangeMotion0->description:'' ?></td>
        <td style="padding: 2px;text-align: center;vertical-align: middle"><?= $exerciceInfo->idUserPhaseExercice0->numberSeries ?></td>
        <td style="padding: 2px;text-align: center;vertical-align: middle"><?= $exerciceInfo->idUserPhaseExercice0->repeatInterval ?></td>
        <td style="padding: 2px;text-align: center;vertical-align: middle"><?= $exerciceInfo->idUserPhaseExercice0->tempo ?></td>
        <td style="padding: 2px;text-align: center;vertical-align: middle"><?= $exerciceInfo->idUserPhaseExercice0->rest ?></td>
        <td style="padding: 2px;text-align: center;vertical-align: middle"><?= CHtml::activeRadioButtonList($exerciceInfo->idUserPhaseExercice0,'['.$exerciceInfo->idUserPhaseExercice.']unitType',['Kg'=>'Kg','Lbs'=>'Lbs'],['class'=>'unitTypeEx','container'=>'','separator'=>'','labelOptions'=>['style'=>'margin-top: 0px;margin-bottom: 0px;height: 20px;']])  ?></td>
        <td colspan="6" style="padding: 2px;text-align: center;vertical-align: middle">
            <?= CHtml::activeTextField($exerciceInfo->idUserPhaseExercice0,'['.$exerciceInfo->idUserPhaseExercice.']notes',['class'=>'form-control','style'=>'padding: 0px;']);?>
            <?= CHtml::activeHiddenField($exerciceInfo->idUserPhaseExercice0,'['.$exerciceInfo->idUserPhaseExercice.']id');?>
        </td>
    </tr>
</table>