<?php
/**
 * @var Execution $execution
 * @var string $nameExercise
 * @var integer $index
 */
//CVarDumper::dump($execution->executionExerciseCalcs,10,true);exit;
?>
<?php if($index==0): ?>
<div id="<?= Utilities::slugify(date('Y-m-d',strtotime($execution->date)).'-'.$nameExercise.''.$execution->executionExerciseCalcs[0]->id) ?>" class="tab-pane fade in active">
    <?php else:?>
    <div id="<?= Utilities::slugify(date('Y-m-d',strtotime($execution->date)).'-'.$nameExercise.''.$execution->executionExerciseCalcs[0]->id) ?>" class="tab-pane fade">
        <?php endif; ?>
        <div class="col-md-7"  style="font-size: 10px">
            <?php $this->renderPartial('_historicDetails',['executionExercise'=>$execution->executionExerciseCalcs[0]->executionExercises]); ?>
        </div>
        <div class="col-md-5" style="font-size: 10px">
            <?php $this->renderPartial('_historicCalcs',['executionExerciseCalc'=>$execution->executionExerciseCalcs]); ?>
        </div>
    </div>
