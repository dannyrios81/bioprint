<?php
/**
 * @var array $executionExercise
 */
?>
<table class="table table-condensed table-responsive table-bordered">
    <tr>
        <th class="text-center" style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle;width: 7.6%"></th>
        <?php for($i=0;$i<12;$i++): ?>
            <?php if(isset($executionExercise[$i])): ?>
                <th class="text-center" style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle;width: 7.7%"><?= $i+1; ?></th>
            <?php else: ?>
                <th class="text-center" style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle;width: 7.7%">&nbsp;</th>
            <?php endif; ?>
        <?php endfor; ?>
    </tr>
    <tr>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">W</th>
        <?php for($i=0;$i<12;$i++): ?>
            <?php if(isset($executionExercise[$i])): ?>
                <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExercise[$i]->weigth ?></td>
            <?php else: ?>
                <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">&nbsp;</td>
            <?php endif; ?>
        <?php endfor; ?>
    </tr>
    <tr>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">R</th>
        <?php for($i=0;$i<12;$i++): ?>
            <?php if(isset($executionExercise[$i])): ?>
                <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExercise[$i]->repetitions ?></td>
            <?php else: ?>
                <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">&nbsp;</td>
            <?php endif; ?>
        <?php endfor; ?>
    </tr>
    <tr>
        <th style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">PM</th>
        <?php for($i=0;$i<12;$i++): ?>
            <?php if(isset($executionExercise[$i])): ?>
                <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle"><?= $executionExercise[$i]->weigthModificate ?></td>
            <?php else: ?>
                <td style="padding-top: 0px;padding-bottom: 0px;height: 20px;vertical-align: middle">&nbsp;</td>
            <?php endif; ?>
        <?php endfor; ?>
    </tr>
</table>