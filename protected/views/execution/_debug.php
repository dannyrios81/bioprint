<?php
/**
 *
 * @var \Execution $executions
 */
?>
<div class="row">
    <input class="btn btn-success btn-lg btn-block" type="button" value="Mostrar Codigo" onclick="$('.codigo').slideToggle('slow')">
</div>
<div class="codigo" style="display: none">
    <?php CVarDumper::dump($executions,10,true); ?>
</div>
