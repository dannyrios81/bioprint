<?php
/**
 *
 * @var \Execution $executions
 */
$urlvalidator=$this->createAbsoluteUrl('execution/validate');
$jsurladd=<<<JS
function addurl(ref)
{
    event.preventDefault();
    
    $.ajax({
        url: '$urlvalidator',
        type: 'POST',
        dataType : 'json',
        data: $('#executionForm').serialize(),
        success: function(response) {
            if(response.result)
            {
                window.location.href = response.url;
            }
            else
            {
                var texto = "";
                $.each(response.errors,function(index,element){
                texto = texto + element + "\\n";
                });
                alert(texto);
            }
        },
        error: function(response) {
            console.log(response);
        }
    });

    // var url = $(ref).attr('href') +'?variationweigth=' + $('#variationweigth').val() + '&variationreps=' + $('#variationreps').val()
    // window.location.href = url;
}
JS;
Yii::app()->clientScript->registerScript('scriptCollapse',$jsurladd,CClientScript::POS_HEAD);
?>
<?= CHtml::beginForm('','post',['id'=>'executionForm','class'=>'form-inline']) ?>
<div class="row">
    <div class="col-md-2 col-xs-4" style="line-height: 34px">
        <strong>Date</strong>
    </div>
    <div class="col-md-2 col-xs-8" style="line-height: 34px">
        <?= Yii::app()->dateFormatter->format('yyyy-MM-dd',$executions->date); ?>
    </div>
    <div class="col-md-2 col-xs-6" style="text-align: right;padding-right: 0px;">
        <?= CHtml::activeLabelEx($executions, 'weigth') ?>
    </div>
    <div class="col-md-2 col-xs-6">
        <?= CHtml::activeTextField($executions,'weigth',['class'=>'form-control','style'=>'padding: 0px;']); ?>
    </div>
    <div class="col-md-4 col-xs-12 text-center">
        <?= CHtml::activeLabelEx($executions, 'unitType') ?>
        <?= CHtml::activeRadioButtonList($executions,'unitType',['Kg'=>'Kg','Lbs'=>'Lbs'],['container'=>'','separator'=>''])  ?>
        <?= CHtml::activeHiddenField($executions,'id') ?>
        <?= CHtml::activeHiddenField($executions,'idUserPhase') ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        &nbsp;
    </div>
    <div class="col-md-6" style="border: 1px solid gainsboro;padding: 10px;margin: 10px 0;border-radius: 5px;">
        <div class="col-md-2 col-xs-6" style="padding: 0 0 0 10px;text-align: right;line-height: 28px;">
            <?= CHtml::activeLabelEx($executions, 'variationweigth') ?>
        </div>
        <div class="col-md-2 col-xs-6" style="padding: 0;">
            <?= CHtml::activeNumberField($executions,'variationweigth',['class'=>'form-control','style'=>'width:80%;padding: 0px;','id'=>'variationweigth']); ?><span>%</span>
        </div>
        <div class="col-md-2 col-xs-6" style="padding: 0 0 0 10px;text-align: right;line-height: 28px;">
            <?= CHtml::activeLabelEx($executions, 'variationreps') ?>
        </div>
        <div class="col-md-2 col-xs-6" style="padding: 0;">
            <?= CHtml::activeNumberField($executions,'variationreps',['class'=>'form-control','style'=>'width:100%;padding: 0px;','id'=>'variationreps']); ?>
        </div>
        <div class="hidden-xs hidden-sm col-md-4 text-right">
            <?= CHtml::link('New Execution',['execution/createExecution','id'=>$executions->idUserPhase],['class'=>'btn btn-primary btn-block','onClick'=>'addurl(this)']) ?>
        </div>
        <div class="row hidden-md hidden-lg">
            <div class="col-xs-12"><?= CHtml::link('New Execution',['execution/createExecution','id'=>$executions->idUserPhase],['class'=>'btn btn-primary btn-block','onClick'=>'addurl(this)']) ?></div>
        </div>
    </div>
</div>

<?= CHtml::endForm()?>
