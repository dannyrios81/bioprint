<?php
Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".alert").animate({opacity: 1.0}, 5000).fadeOut("slow");',
    CClientScript::POS_READY
);
?>
<h1>Seminars Client</h1><br>
<?php $flashMessages = Yii::app()->user->getFlashes(); ?>
<?php if (count($flashMessages)) :?>
    <div class="alert success">
        <?php
        //$flashMessages = Yii::app()->user->getFlashes();
        //					CVarDumper::dump($flashMessages,10,true);exit;
        if ($flashMessages) {
            echo '<ul class="flashes">';
            foreach($flashMessages as $key => $message) {
                echo '<li><div class="flash-' . $key . '">' . $message . "</div></li>\n";
            }
            echo '</ul>';
        }
        ?>
    </div>
<?php endif;?>
<div class="col-sm-12">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'users_seminars-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'htmlOptions'=>['enctype'=>'multipart/form-data',]
    )); ?>


    <?php echo $form->errorSummary($model); ?>

    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'calculable'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'idCourse',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($model,'idCourse',$model->loadCourses(),array('empty'=>'Select ...','class'=>"form-control")); ?>
    </div>


    <div class="col-md-6">
        <?php echo CHtml::label('&nbsp;','',array('class'=>"control-label")); ?>
        <?php echo CHtml::submitButton('Add Seminar',array('class' => 'btn btn-success btn-block')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
<div class="col-sm-12">
    <hr>
</div>
<div class="col-sm-12">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'users_courses-grid',
        'dataProvider'=>$model->search(),
        'columns'=>array(
            ['name'=>'Seminar ID','value'=>'$data->idCourse0->courseIdentification'],
            ['name'=>'Name','value'=>'$data->idCourse0->name'],
            ['name'=>'Date','value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->idCourse0->startDate);'],
            ['name'=>'Place','value'=>'$data->idCourse0->location'],
        ),
    )); ?>
</div>