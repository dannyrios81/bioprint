<?php
$idactivateDate = '#'.CHtml::activeId($model,'activateDate');
$iddueDate = '#'.CHtml::activeId($model,'dueDate');
$script=<<<JS
if (!Modernizr.inputtypes.date) {
  $('$idactivateDate').attr('type','text');
  $('$iddueDate').attr('type','text');
  $('$idactivateDate').datepicker({format: "yyyy-mm-dd",autoclose: true,orientation:'auto bottom'});
  $('$iddueDate').datepicker({format: "yyyy-mm-dd",autoclose: true,orientation:'auto bottom'});
}
JS;

Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap-datepicker3.min.css');

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/modernizr-custom.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('Script',$script, CClientScript::POS_READY);
?>
<div class="col-sm-12 main-content">
<h1>Create Practitioner Access</h1>
    <?php echo CHtml::beginForm(); ?>

    <?php echo CHtml::errorSummary($model); ?>

    <div class="form-group col-md-6 <?php echo !empty(CHtml::error($model,'idProduct'))?'has-error':''?>">
        <?php echo CHtml::activeLabelEx($model,'idProduct',array('class'=>"control-label")); ?>
        <?php echo CHtml::activeDropDownList($model,'idProduct',$model->loadProducts(),array('class'=>"form-control")); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty(CHtml::error($model,'activateDate'))?'has-error':''?>">
        <?php echo CHtml::activeLabelEx($model,'activateDate',array('class'=>"control-label")); ?>
        <?php echo CHtml::activeDateField($model,'activateDate',array('class'=>"form-control")); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty(CHtml::error($model,'dueDate'))?'has-error':''?>">
        <?php echo CHtml::activeLabelEx($model,'dueDate',array('class'=>"control-label")); ?>
        <?php echo CHtml::activeDateField($model,'dueDate',array('class'=>"form-control")); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty(CHtml::error($model,'licence'))?'has-error':''?>">
        <?php echo CHtml::activeLabelEx($model,'licence',array('class'=>"control-label")); ?>
        <?php echo CHtml::activeCheckBox($model,'licence',array('class'=>"form-control",'style'=>'opacity: 1;width:100%')); ?>
    </div>
    <div class="row buttons">
        <div class="col-xs-6">
            <?php echo CHtml::link('Back',Yii::app()->createAbsoluteUrl("users/productsList",['id'=>$model->idUsers]),array('class' => 'btn btn-default btn-lg btn-block')); ?>
        </div>
        <div class="col-xs-6">
            <?php echo CHtml::submitButton('Create',array('class' => 'btn btn-success btn-lg btn-block')); ?>
        </div>
    </div>

    <?php echo CHtml::endForm(); ?>

</div><!-- form -->