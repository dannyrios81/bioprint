<?php
$profiles = ['Administrator'=>'Administrator','Trainer'=>'Practitioner','User'=>'Client'];

Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".alert").animate({opacity: 1.0}, 5000).fadeOut("slow");',
    CClientScript::POS_READY
);
CHtml::$errorCss = 'has-error';

?>
<?php $flashMessages = Yii::app()->user->getFlashes(); ?>
<?php if (count($flashMessages)) :?>
    <div class="alert success">
        <?php
        //$flashMessages = Yii::app()->user->getFlashes();
        //					CVarDumper::dump($flashMessages,10,true);exit;
        if ($flashMessages) {
            echo '<ul class="flashes">';
            foreach($flashMessages as $key => $message) {
                echo '<li><div class="flash-' . $key . '">' . $message . "</div></li>\n";
            }
            echo '</ul>';
        }
        ?>
    </div>
<?php endif;?>
    <h1>Practitioner Products </h1>
    <hr class="separator">
    <div class="text-right">
        <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('users/createAccessPractitioner',['id'=>$user->id])?>"><i class="fa fa-plus-circle"></i> New Practitioner Access</a>
    </div>
    <br>
    <div class="col-sm-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'users-product-grid',
            'dataProvider'=>$model,
            'columns'=>array(
                [
                    'header'=>'Product',
                    'value'=>'$data->idProduct0->name'],
                [
                    'header'=>'Activate Date',
                    'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->activateDate)'
                ],
                [
                    'header'=>'Due Date',
                    'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->dueDate)'
                ],
                array
                (
                    'class'=>'CButtonColumn',
                    'template'=>'{xupdate}',
                    'header'=>"  Actions  ",
                    'buttons'=>array(
                        'xupdate'=>array(
                            'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>',
                            'url'=>'Yii::app()->createUrl("users/updateAccessPractitioner", array("id"=>$data->id))',
                            'options'=>array('title'=>'Update Client')
                        ),

                    ),
                ),
            ),
        )); ?>
    </div>