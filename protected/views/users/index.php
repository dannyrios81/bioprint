<?php
//CVarDumper::dump($user,10,true);exit;
$baseUrl = Yii::app()->getBaseUrl(true);
$scriptColapse =<<<JS
    $(document).on('click','.products',function(){
        event.preventDefault();
        console.log($(this).attr('href'));
        $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            success: function(response){
                console.log(response);
                $("#ajaxModal .modal-title").empty().html('Client Products');
                $("#ajaxModal .modal-body").empty().html(response);
		        $("#ajaxModal").modal();
            },
            error: function(response){
                console.log(response);
            }
        });

    });
$(document).on('click','.changepractitioner',function(event){     
    event.preventDefault();
     $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            dataType:'json',
            success: function(response){
                console.log(response);
                $("#ajaxModal .modal-title").empty().html('Change Practitioner');
                $("#ajaxModal .modal-body").empty().html(response.html);
		        $("#ajaxModal").modal();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location.href='$baseUrl';
                }
            }
        });
});
JS;
Yii::app()->clientScript->registerScript('scriptCollapse',$scriptColapse,CClientScript::POS_READY);
$profiles = ['Administrator'=>'Administrator','Trainer'=>'Practitioner','User'=>'Client'];

/*$urlProducts =  Yii::app()->createAbsoluteUrl("productsUser/index",["id"=>$user->id]);
$urlDetails =  Yii::app()->createAbsoluteUrl("users/Update",["id"=>$user->id]);
$urlMeasure =  Yii::app()->createAbsoluteUrl("measureUser/index",["id"=>$user->id]);
$urlPassword =  Yii::app()->createAbsoluteUrl("site/ChangePassword",["change"=>Utilities::encriptar($user->email)]);

$scriptEnable =<<<JS
    $(document).on('click','#btn-Products',function(){
        window.location.href = "$urlProducts"

    });
    $(document).on('click','#btn-Update',function(){
        window.location.href = "$urlDetails"

    });
    $(document).on('click','#btn-Measures',function(){
        window.location.href = "$urlMeasure"

    });
    $(document).on('click','#btn-Password',function(){
        window.location.href = "$urlPassword"

    });
JS;*/
Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".alert").animate({opacity: 1.0}, 5000).fadeOut("slow");',
    CClientScript::POS_READY
);

//Yii::app()->clientScript->registerScript('scriptEnable',$scriptEnable,CClientScript::POS_READY);
CHtml::$errorCss = 'has-error';

?>

<!--<div class="col-xs-12 col-sm-4 col-sm-push-8 " >-->
<!--    <img src="--><?php //echo Yii::app()->baseUrl.$photo ?><!--" class="img-circle" alt="User Image" src="User-Image">-->
<!--</div>-->
<!--<div class="col-xs-12 col-sm-8 col-sm-pull-4 ">-->
<!--    --><?php //$this->renderPartial('title'); ?>
<!--</div>-->
<!--<hr class="col-sm-12 hidden-xs form-separator">-->
<!--<div class="col-sm-12 form-container">-->

<!--    --><?php //$form=$this->beginWidget('CActiveForm', array(
//        'id'=>'user-form',
//        // Please note: When you enable ajax validation, make sure the corresponding
//        // controller action is handling ajax validation correctly.
//        // There is a call to performAjaxValidation() commented in generated controller code.
//        // See class documentation of CActiveForm for details on this.
//        'enableAjaxValidation'=>false,
//    )); ?>
<!--    <div class="form-group col-md-6 --><?php //echo !empty($form->error($user,'name'))?'has-error':''?><!--">-->
<!--        --><?php //echo $form->labelEx($user,'name',array('class'=>"control-label")); ?>
<!--        --><?php //echo $form->textField($user,'name',array('class'=>"form-control","disabled"=>"disabled")); ?>
<!--    </div>-->
<!--    <div class="form-group col-md-6 --><?php //echo !empty($form->error($user,'lastName'))?'has-error':''?><!--">-->
<!--        --><?php //echo $form->labelEx($user,'lastName',array('class'=>"control-label")); ?>
<!--        --><?php //echo $form->textField($user,'lastName',array('class'=>"form-control","disabled"=>"disabled")); ?>
<!--    </div>-->
<!--    <div class="form-group col-md-6 --><?php //echo !empty($form->error($user,'email'))?'has-error':''?><!--">-->
<!--        --><?php //echo $form->labelEx($user,'email',array('class'=>"control-label")); ?>
<!--        --><?php //echo $form->textField($user,'email',array('class'=>"form-control","disabled"=>"disabled")); ?>
<!--    </div>-->
<!--    <div class="form-group col-md-6 --><?php //echo !empty($form->error($user,'gender'))?'has-error':''?><!--">-->
<!--        --><?php //echo $form->labelEx($user,'gender',array('class'=>"control-label")); ?>
<!--        --><?php //echo $form->dropDownList($user,'gender',array('Male'=>'Male','Female'=>'Female'),array('class'=>"form-control","disabled"=>"disabled")); ?>
<!--    </div>-->
<!--    <div class="form-group col-md-6 --><?php //echo !empty($form->error($user,'bornDate'))?'has-error':''?><!--">-->
<!--        --><?php //echo $form->labelEx($user,'bornDate',array('class'=>"control-label")); ?>
<!--        <div class="input-group">-->
<!--            --><?php //echo $form->textField($user,'bornDate',array('class'=>"form-control","disabled"=>"disabled")); ?>
<!--            <div class="input-group-addon"><i class="fa fa-birthday-cake"></i></div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="form-group col-md-6">-->
<!--        <label class="hidden visible-md visible-lg">&nbsp;</label>-->
<!--        <div class="btn-group btn-group-justified">-->
<!--            --><?php //foreach($buttons as $button): ?>
<!--            --><?php ////CVarDumper::dump($button,10,true); ?>
<!--                <div class="btn-group">-->
<!--                    <button class="btn btn-primary" name="btn---><?php //echo $button['name'] ?><!--" type="button" id="btn---><?php //echo $button['name'] ?><!--">-->
<!--                        --><?php //echo $button['label'] ?>
<!--                    </button>-->
<!--                </div>-->
<!--            --><?php //endforeach; ?>
<!--        </div>-->
<!--    </div>-->
<!--    <div class="form-group col-md-12 --><?php //echo !empty($form->error($user,'objectivesToBeAchieved'))?'has-error':''?><!--">-->
<!--        --><?php //echo $form->labelEx($user,'objectivesToBeAchieved',array('class'=>"control-label")); ?>
<!--        --><?php //echo $form->textArea($user,'objectivesToBeAchieved',array('class'=>"form-control","cols"=>"20","rows"=>"10","disabled"=>"disabled")); ?>
<!--    </div>-->
<!--    --><?php //$this->endWidget(); ?>
<!--</div>-->
<?php //if(Yii::app()->user->getState('userType')=='Trainer'): ?>
<!--<div class="col-sm-12 form-container">-->
    <h1>Clients</h1>
    <hr class="separator">
    <?php $flashMessages = Yii::app()->user->getFlashes(); ?>
    <?php if (count($flashMessages)) :?>
        <div class="alert success">
            <?php
            //$flashMessages = Yii::app()->user->getFlashes();
            //					CVarDumper::dump($flashMessages,10,true);exit;
            if ($flashMessages) {
                echo '<ul class="flashes">';
                foreach($flashMessages as $key => $message) {
                    echo '<li><div class="flash-' . $key . '">' . $message . "</div></li>\n";
                }
                echo '</ul>';
            }
            ?>
        </div>
    <?php endif;?>
    <div class="text-right">
        <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('users/create')?>"><i class="fa fa-plus-circle"></i> New Client</a>
    </div>
<!--</div>-->
<br>
<?php //endif; ?>
<?php //if(Yii::app()->user->getState('userType')=='Admin' or Yii::app()->user->getState('userType')=='Trainer'): ?>
<?php //CVarDumper::dump($user->id,10,true);exit; ?>

<div class="col-sm-12">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'users-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'columns'=>array(
            [
                'name'=>'name',
                'value'=>'$data->name'
            ],
            [
                'name'=>'lastName',
                'value'=>'$data->lastName'
            ],
            [
                'name'=>'email',
                'value'=>'$data->email',
                'filterHtmlOptions'=>['class'=>'hidden-xs'],
                'headerHtmlOptions'=>['class'=>'hidden-xs'],
                'htmlOptions'=>['class'=>'hidden-xs']
            ],
            [
                'name'=>'userType',
                'value'=>'($data->userType)=="Trainer"?"Practitioner":(($data->userType)=="User"?"Client": (($data->userType)=="Admin"?"Administrator":(($data->userType)=="Practitioner1"?"New Practitioner":"")))',
                'filter'=>CHtml::activeDropDownList($model,'userType',['Practitioner1'=>'New Practitioner','Trainer'=>'Practitioner','User'=>'Client'],['empty'=>'Select ...','class'=>"form-control"])
            ],
            array
            (
                'class'=>'EButtonColumnWithClearFilters',
                'imageUrl'=>false,
                'clearHtmlOptions'=>array('title'=>'Clear Filters','style'=>'text-align:left;display:block;'),
                'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                'template'=>$this->templateButtonsGrilla(),
                'header'=>"  Actions  ",
                'buttons'=>array(
                    'xupdate'=>array(
                        'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createUrl("users/update", array("id"=>$data->id))',
                        'options'=>array('title'=>'Update')
                    ),
                    'xseminar'=>array(
                        'label'=>'<i class="fa fa-calendar fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-calendar visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createUrl("users/seminars", array("id"=>$data->id))',
                        'visible'=>'("Admin"=="'.$user->userType.'" and ($data->userType=="Trainer" or $data->userType=="Practitioner1")?true:false)',
                        'options'=>array('title'=>'Seminars')
                    ),
                    'xproducts'=>array(
                        'label'=>'<i class="fa fa-shopping-bag fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-shopping-bag visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createAbsoluteUrl("product/userProducts", array("id"=>$data->id))',
                        'options'=>array('title'=>'Products','class'=>'products'),
                        'visible'=>'("Admin"=="'.$user->userType.'" and ($data->userType=="Trainer" or $data->userType=="Practitioner1")?true:false)',
                    ),
                    'xchangepractitioner'=>array(
                        'label'=>'<i class="fa fa-users fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-users visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createAbsoluteUrl("users/changepractitioner", array("id"=>$data->id))',
                        'options'=>array('title'=>'Change Practitioner','class'=>'changepractitioner'),
                        'visible'=>'(($data->userType=="Trainer" or $data->userType=="Practitioner1")?true:false)',
                    ),
                    'xprofile'=>array(
                        'label'=>'<i class="fa fa-user fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-user visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createUrl("users/profile")',
                        'options'=>array('title'=>'Profile'),
                        'visible'=>'($data->id=='.$user->id.'?true:false)',
                    ),
                    'xuserProduct'=>array(
                        'label'=>'<i class="fa fa-book fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-book visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createUrl("users/productsList", array("id"=>$data->id))',
                        'options'=>array('title'=>'Access'),
                        'visible'=>'("Admin"=="'.$user->userType.'" and ($data->userType=="Trainer" or $data->userType=="Practitioner1")?true:false)',
                    ),
                    'xmeasure'=>array(
                        'label'=>'<i class="fa fa-sliders fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-sliders visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createUrl("MeasureUser/index", array("id"=>$data->id))',
                        'options'=>array('title'=>'Measures')
                    ),
                    'xfase'=>array(
                        'label'=>'<i class="fa icon-dumbbell-variant-outline fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa icon-dumbbell-variant-outline visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createUrl("Fase/index", array("id"=>$data->id))',
                        'options'=>array('title'=>'Workout','target'=>'_blank')
                    ),
                    'xdelete'=>array(
                        'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createUrl("users/delete", array("id"=>$data->id))',
                        'options'=>array('title'=>'Delete'),
                        'visible'=>'(("Admin"=="'.$user->userType.'") or ((count($data->hormoneUserDates)==0 && count($data->measurements)==0 && count($data->purchases)==0 && count($data->userProducts)==0 && count($data->weightHeights)==0))?true:false)',
                        'click'=>"function() {
							if(!confirm('Are you sure you want to delete this item?')) return false;
							var th = this,afterDelete = function(){};
							jQuery('#users-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
								    if(data.length>0)
                                            alert(data);
									jQuery('#users-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
						}"
                    ),
                ),
            ),
        ),
    )); ?>
</div>
<div class="modal fade" id="ajaxModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Client Products</h4>
            </div>
            <div class="modal-body" style="text-align: justify;">
                XXX XXXXXXXXXXXXXXX XXXXXXXXXX XXXXX
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php //endif; ?>