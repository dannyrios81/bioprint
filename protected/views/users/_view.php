<?php
/* @var $this UsersController */
/* @var $data Users */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_record')); ?>:</b>
	<?php echo CHtml::encode($data->create_record); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastName')); ?>:</b>
	<?php echo CHtml::encode($data->lastName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('genre')); ?>:</b>
	<?php echo CHtml::encode($data->genre); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('bornDate')); ?>:</b>
	<?php echo CHtml::encode($data->bornDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weight')); ?>:</b>
	<?php echo CHtml::encode($data->weight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weightUnit')); ?>:</b>
	<?php echo CHtml::encode($data->weightUnit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('height')); ?>:</b>
	<?php echo CHtml::encode($data->height); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('heightUnit')); ?>:</b>
	<?php echo CHtml::encode($data->heightUnit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('historyHealthCondition')); ?>:</b>
	<?php echo CHtml::encode($data->historyHealthCondition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('familyHistory')); ?>:</b>
	<?php echo CHtml::encode($data->familyHistory); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currentHealthCondition')); ?>:</b>
	<?php echo CHtml::encode($data->currentHealthCondition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('objectivesToBeAchieved')); ?>:</b>
	<?php echo CHtml::encode($data->objectivesToBeAchieved); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imcGoal')); ?>:</b>
	<?php echo CHtml::encode($data->imcGoal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sportsPractice')); ?>:</b>
	<?php echo CHtml::encode($data->sportsPractice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insuranceCompany')); ?>:</b>
	<?php echo CHtml::encode($data->insuranceCompany); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prescriptionDrugs')); ?>:</b>
	<?php echo CHtml::encode($data->prescriptionDrugs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('allergies')); ?>:</b>
	<?php echo CHtml::encode($data->allergies); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('medicalRestrictions')); ?>:</b>
	<?php echo CHtml::encode($data->medicalRestrictions); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idAttachment')); ?>:</b>
	<?php echo CHtml::encode($data->idAttachment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parentEmail')); ?>:</b>
	<?php echo CHtml::encode($data->parentEmail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('forgotPass')); ?>:</b>
	<?php echo CHtml::encode($data->forgotPass); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userType')); ?>:</b>
	<?php echo CHtml::encode($data->userType); ?>
	<br />

	*/ ?>

</div>