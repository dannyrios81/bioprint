<?php
/**
 * @var $model Users
 * @var $user Users
 */
$idDate = '#'.CHtml::activeId($model,'bornDate');
$script=<<<JS
if (!Modernizr.inputtypes.date) {
  $('$idDate').attr('type','text');
  $('$idDate').datepicker({format: "yyyy-mm-dd",autoclose: true,});
}
JS;
$validaNumber = <<<JS
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46 || (charCode >= 48 && charCode <= 57)) {
        return true;
    }
    return false;
}
JS;

Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap-datepicker3.min.css');

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/modernizr-custom.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('Script',$script, CClientScript::POS_READY);
Yii::app()->clientScript->registerScript('validaNumber',$validaNumber, CClientScript::POS_END);

if(!$model->isNewRecord)
{
	$profiles =['Trainer'=>'Practitioner','User'=>'Client'];
}
else
{
	if($user->userType == 'Admin')
		$profiles =['Trainer'=>'Practitioner','Practitioner1'=>'New Practitioner'];
	else
		$profiles =['User'=>'Client'];
}

$parent = Users::model()->findByAttributes(['email'=>$model->parentEmail]);


if(($user->userType == 'Trainer' or $user->userType == 'Practitioner1') and $user->idCountry0->exclusive)
{
    $model->idCountry=$user->idCountry;
}
elseif ($user->userType == 'User' and !$model->isNewRecord and $parent->idCountry0->exclusive)
{
    $model->idCountry=$parent->idCountry;
}

?>
<div class="col-sm-12 main-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>['enctype'=>'multipart/form-data','novalidate'=>'novalidate']

)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'name'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'name',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'name',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'lastName'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'lastName',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'lastName',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'gender'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'gender',array('class'=>"control-label")); ?>
		<?php echo $form->dropDownList($model,'gender',array('Male'=>'Male','Female'=>'Female'),array('class'=>"form-control","empty"=>'Select...')); ?>
	</div>
	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'email'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'email',array('class'=>"control-label")); ?>
		<?php echo $form->emailField($model,'email',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'bornDate'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'bornDate',array('class'=>"control-label")); ?>
		<?php echo $form->dateField($model,'bornDate',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'imcGoal'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'imcGoal',array('class'=>"control-label")); ?>
		<?php echo $form->numberField($model,'imcGoal',array('class'=>"form-control","step"=>"0.01","onKeypress"=>"return isNumber(event)")); ?>
	</div>
	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'contactInformation'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'contactInformation',array('class'=>"control-label")); ?>
		<?php echo $form->textArea($model,'contactInformation',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'idCountry'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'idCountry',array('class'=>"control-label")); ?>
        <?php if(($user->userType == 'Trainer' or $user->userType == 'Practitioner1') and $user->idCountry0->exclusive): //creando o actualizando nuevo usuario ?>
		    <?php echo $form->dropDownList($model,'idCountry',CHtml::listData(Country::model()->findAll(['condition' => 'id = :id','params'=> array(':id' => $user->idCountry)]),'id','name'),array('class'=>"form-control","empty"=>'Select...')); ?>
        <?php elseif ($user->userType == 'User' and !$model->isNewRecord and $parent->idCountry0->exclusive)://usuario actualizando infromacion?>
            <?php echo $form->dropDownList($model,'idCountry',CHtml::listData(Country::model()->findAll(['condition' => 'id = :id','params'=> array(':id' => $parent->idCountry)]),'id','name'),array('class'=>"form-control","empty"=>'Select...')); ?>
        <?php elseif ($user->userType == 'Admin')://administrador creando trainer?>
            <?php echo $form->dropDownList($model,'idCountry',CHtml::listData(Country::model()->findAll(),'id','name'),array('class'=>"form-control","empty"=>'Select...')); ?>
        <?php else: ?>
            <?php echo $form->dropDownList($model,'idCountry',CHtml::listData(Country::model()->findAll(['condition' => 'exclusive = 0']),'id','name'),array('class'=>"form-control","empty"=>'Select...')); ?>
        <?php endif ?>
	</div>
	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'userType'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'userType',array('class'=>"control-label")); ?>
		<?php echo $form->dropDownList($model,'userType',$profiles,array('class'=>"form-control","empty"=>'Select...',"disabled"=>(!$model->isNewRecord)?"disabled":"")); ?>
	</div>
	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'historyHealthCondition'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'historyHealthCondition',array('class'=>"control-label")); ?>
		<?php echo $form->textArea($model,'historyHealthCondition',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'familyHistory'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'familyHistory',array('class'=>"control-label")); ?>
		<?php echo $form->textArea($model,'familyHistory',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'currentHealthCondition'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'currentHealthCondition',array('class'=>"control-label")); ?>
		<?php echo $form->textArea($model,'currentHealthCondition',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'objectivesToBeAchieved'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'objectivesToBeAchieved',array('class'=>"control-label")); ?>
		<?php echo $form->textArea($model,'objectivesToBeAchieved',array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'sportsPractice'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'sportsPractice',array('class'=>"control-label")); ?>
		<?php echo $form->textArea($model,'sportsPractice',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'insuranceCompany'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'insuranceCompany',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'insuranceCompany',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'prescriptionDrugs'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'prescriptionDrugs',array('class'=>"control-label")); ?>
		<?php echo $form->textArea($model,'prescriptionDrugs',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'allergies'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'allergies',array('class'=>"control-label")); ?>
		<?php echo $form->textArea($model,'allergies',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'medicalRestrictions'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'medicalRestrictions',array('class'=>"control-label")); ?>
		<?php echo $form->textArea($model,'medicalRestrictions',array('class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-12 <?php echo !empty($form->error($model,'idAttachment'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'idAttachment',array('class'=>"control-label")); ?>
		<?php echo $form->fileField($model,'idAttachment',array('class'=>"form-control filestyle")); ?>
	</div>

	<div class="row buttons">
		<div class="col-xs-6">
			<?php echo CHtml::link('Back',Yii::app()->createAbsoluteUrl("users/index"),array('class' => 'btn btn-default btn-lg btn-block')); ?>
		</div>
		<div class="col-xs-6">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->