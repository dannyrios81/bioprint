<div class="col-xs-12 container-avatar">
    <div class="col-xs-12 col-sm-4 col-sm-push-8">
        <img src="<?php echo Yii::app()->baseUrl.$photo ?>" class="img-circle" alt="User Image" src="User-Image">
        <?php if(!empty($model->name) and !empty($model->lastName) and !empty($model->email)): ?>
        <div class="col-sm-12 userInfoImg">
            <div class="col-sm-12 text-center"><?php echo ucwords(strtolower($model->name.' '.$model->lastName)); ?> </div>
            <div class="col-sm-12 text-center"><?php echo $model->email; ?></div>
        </div>
        <?php endif;?>
    </div>

    <div class="col-xs-12 col-sm-8 col-sm-pull-4 ">
        <h1>Create Client</h1>
    </div>
</div>

<?php $this->renderPartial('_form', array('model'=>$model,'user'=>$user)); ?>