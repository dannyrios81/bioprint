<?php
/**
 * @var ChangePractitionerForm $model
 */

$baseUrl = Yii::app()->getBaseUrl(true);

$scriptSubmit=<<<JS
function submitForm(event) 
{  
    console.log(this);
    // debugger;
    event.preventDefault();
    $(".preloader-alfa").slideDown("fast");
    $.ajax({
            url: $('#changePractitioner-form').prop('action'),
            type: 'POST',
            data: $('#changePractitioner-form').serialize(),
            dataType:'json',
            success: function(response){
                console.log(response);
                if(response.id<=0)
                    {
                        $("#ajaxModal .modal-body").empty().html(response.html);
                        $("#ajaxModal .modal-footer").css('visibility','hidden');
                        $(".preloader-alfa").slideUp("fast");
                    }
                    else
                    {
                        $('#user-grid').yiiGridView('update');
                        $("#ajaxModal .close").click();
                        $(".preloader-alfa").slideUp("fast");
                        location.reload();
                    }
                
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location.href='$baseUrl';
                }
            }
        });
	
};
JS;


Yii::app()->clientScript->registerScript('scriptSubmit',$scriptSubmit, CClientScript::POS_BEGIN);

?>
<?= CHtml::beginForm('','post',['id'=>'changePractitioner-form','onSubmit'=>'submitForm(event)']) ?>
<?= CHtml::errorSummary($model) ?>

<?php/** ******************************************  Fase  *************************************** **/?>
<div class="form-group col-md-12 <?= !empty(CHtml::error($model,'Fase'))?'has-error':''?>" >
    <?= CHtml::activeLabelEx($model,'idPractitionerTo',array('class'=>"control-label")); ?>
    <?= CHtml::activeDropDownList($model,'idPractitionerTo',$model->practitionersList(),array('class'=>"form-control","autocomplete"=>"off")); ?>
</div>

<div class="row buttons">
    <div class="col-xs-12">
        <?php echo CHtml::submitButton('Change',array('class' => 'btn btn-success btn-lg btn-block')); ?>
    </div>
</div>
<?= CHtml::endForm() ?>
