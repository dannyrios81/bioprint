<?php
$urlProducts =  Yii::app()->createAbsoluteUrl("productsUser/index",["id"=>$user->id]);
$urlDetails =  Yii::app()->createAbsoluteUrl("users/Update",["id"=>$user->id]);
$urlWorkout =  Yii::app()->createAbsoluteUrl("Fase/index",["id"=>$user->id]);
$urlMeasure =  Yii::app()->createAbsoluteUrl("measureUser/index",["id"=>$user->id]);
$urlPassword =  Yii::app()->createAbsoluteUrl("site/ChangePassword",["change"=>Utilities::encriptar($user->email)]);
$countMeasuresUser = count($user->measurements);
$userProfile = $user->userType;

$scriptEnable =<<<JS
    $(document).on('click','#btn-Products',function(){
        window.location.href = "$urlProducts"

    });
    $(document).on('click','#btn-Update',function(){
        window.location.href = "$urlDetails"

    });
    $(document).on('click','#btn-Workout',function(){
        window.location.href = "$urlWorkout"

    });
    $(document).on('click','#btn-Measures',function(){
        if("$userProfile"=="User" && $countMeasuresUser<=0)
        {
            alert("You do not have entered measures");
        }
        else {
            window.location.href = "$urlMeasure"
        }
        

    });
    $(document).on('click','#btn-Password',function(){
        window.location.href = "$urlPassword"

    });
JS;
/*Yii::app()->clientScript->registerScript(
    'myHideEffect',
    '$(".alert").animate({opacity: 1.0}, 5000).fadeOut("slow");',
    CClientScript::POS_READY
);*/

Yii::app()->clientScript->registerScript('scriptEnable',$scriptEnable,CClientScript::POS_READY);
CHtml::$errorCss = 'has-error';

?>

<div class="col-xs-12 col-sm-4 col-sm-push-8 " >
    <img src="<?php echo Yii::app()->baseUrl.$photo ?>" class="img-circle" alt="User Image" src="User-Image">
</div>
<div class="col-xs-12 col-sm-8 col-sm-pull-4 ">
    <?php $this->renderPartial('title'); ?>
</div>
<div class="col-sm-12 form-container">
    <?php $flashMessages = Yii::app()->user->getFlashes(); ?>
    <?php //CVarDumper::dump($flashMessages,10,true);exit;?>
    <?php if (count($flashMessages)>0) :?>
    <div class="alert success">
        <?php
        //$flashMessages = Yii::app()->user->getFlashes();
        if ($flashMessages) {
            echo '<ul class="flashes">';
            foreach($flashMessages as $key => $message) {
                echo '<li><div class="flash-' . $key . '">' . $message . "</div></li>\n";
            }
            echo '</ul>';
        }
//        CVarDumper::dump($flashMessages,10,true);exit;
        ?>
    </div>
    <?php endif;?>
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'user-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>
    <div class="form-group col-md-6 <?php echo !empty($form->error($user,'name'))?'has-error':''?>">
        <?php echo $form->labelEx($user,'name',array('class'=>"control-label")); ?>
        <?php echo $form->textField($user,'name',array('class'=>"form-control","disabled"=>"disabled")); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($user,'lastName'))?'has-error':''?>">
        <?php echo $form->labelEx($user,'lastName',array('class'=>"control-label")); ?>
        <?php echo $form->textField($user,'lastName',array('class'=>"form-control","disabled"=>"disabled")); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($user,'email'))?'has-error':''?>">
        <?php echo $form->labelEx($user,'email',array('class'=>"control-label")); ?>
        <?php echo $form->textField($user,'email',array('class'=>"form-control","disabled"=>"disabled")); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($user,'gender'))?'has-error':''?>">
        <?php echo $form->labelEx($user,'gender',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($user,'gender',array('Male'=>'Male','Female'=>'Female'),array('class'=>"form-control","disabled"=>"disabled")); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($user,'bornDate'))?'has-error':''?>">
        <?php echo $form->labelEx($user,'bornDate',array('class'=>"control-label")); ?>
        <div class="input-group">
            <?php echo $form->textField($user,'bornDate',array('class'=>"form-control","disabled"=>"disabled")); ?>
            <div class="input-group-addon"><i class="fa fa-birthday-cake"></i></div>
        </div>
    </div>
    <div class="form-group col-md-6">
        <label class="hidden visible-md visible-lg">&nbsp;</label>
        <div class="btn-group btn-group-justified">
            <?php foreach($buttons as $button): ?>
            <?php //CVarDumper::dump($button,10,true); ?>
                <div class="btn-group">
                    <button class="btn btn-primary" name="btn-<?php echo $button['name'] ?>" type="button" id="btn-<?php echo $button['name'] ?>">
                        <?php echo $button['label'] ?>
                    </button>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="form-group col-md-12 <?php echo !empty($form->error($user,'objectivesToBeAchieved'))?'has-error':''?>">
        <?php echo $form->labelEx($user,'objectivesToBeAchieved',array('class'=>"control-label")); ?>
        <?php echo $form->textArea($user,'objectivesToBeAchieved',array('class'=>"form-control","cols"=>"20","rows"=>"10","disabled"=>"disabled")); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>