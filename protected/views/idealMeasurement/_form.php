<?php
$criteria = new CDbCriteria();
$criteria->order='name';
$criteria->addSearchCondition('calculable','0');

?>

<div class="col-sm-12 main-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ideal-measurement-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array("novalidate"=>"novalidate")
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'idealMeasure'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'idealMeasure',array('class'=>"control-label")); ?>
		<?php echo $form->numberField($model,'idealMeasure',array('class'=>"form-control","step"=>"0.01")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'idMeasuremetType'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'idMeasuremetType',array('class'=>"control-label")); ?>
		<?php echo $form->dropDownList($model,'idMeasuremetType',CHtml::listData(MeasurementTypes::model()->findAll($criteria),'id','name'),array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'gender'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'gender',array('class'=>"control-label")); ?>
		<?php echo $form->dropDownList($model,'gender',array('Male'=>'Male','Female'=>'Female'),array('class'=>"form-control")); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->