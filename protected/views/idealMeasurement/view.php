<h1>View IdealMeasurement #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'idealMeasure',
		'idMeasuremetType',
		'gender',
	),
)); ?>
