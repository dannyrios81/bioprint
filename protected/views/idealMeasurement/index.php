<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ideal-measurement-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

//CVarDumper::dump($model,10,true);exit;
$criteria = new CDbCriteria();
$criteria->order = 'name';
$criteria->addSearchCondition('calculable','0');

?>

<h1>Manage Norms</h1>
<hr class="separator">
<div class="col-sm-12 form-container">
	<div class="text-right">
		<a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('idealMeasurement/create',['id'=>(!empty($model->idMeasuremetType)?$model->idMeasuremetType:0)])?>"><i class="fa fa-plus-circle"></i> New norm</a>
	</div>
</div>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ideal-measurement-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
		['header'=>'Norm','name'=>'idealMeasure'],
		['header'=>'Site','name'=>'idMeasuremetType','value'=>'$data->idMeasuremetType0->name','filter'=>CHtml::activeDropDownList($model,'idMeasuremetType',CHtml::listData(MeasurementTypes::model()->findAll($criteria),'id','name'),['empty'=>'Select ...'])],
		['name'=>'Gender','value'=>'$data->gender','filter'=>CHtml::activeDropDownList($model,'gender',['Female'=>'Female','Male'=>'Male'],['empty'=>'Select ...'])],
		array(
			'class'=>'CButtonColumn',
			'template'=>'{xupdate}{xdelete}',
			'header'=>"  Actions  ",
			'buttons'=>array(
				'xupdate'=>array(
					'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								<i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
					'url'=>'Yii::app()->createUrl("idealMeasurement/update", array("id"=>$data->id))',
					'options'=>array('title'=>'Update ideal-measurement')
				),
				'xdelete'=>array(
					'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								<i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
					'url'=>'Yii::app()->createUrl("idealMeasurement/delete", array("id"=>$data->id))',
					'options'=>array('title'=>'Delete ideal-measurement'),
					'click'=>"function() {
						if(!confirm('Are you sure you want to delete this item?')) return false;
						var th = this,afterDelete = function(){};
							jQuery('#ideal-measurement-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
									jQuery('#ideal-measurement-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
					}"
				),
			),
		)
	),
)); ?>
</div>