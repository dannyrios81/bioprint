<?php
<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idealMeasure')); ?>:</b>
	<?php echo CHtml::encode($data->idealMeasure); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idMeasuremetType')); ?>:</b>
	<?php echo CHtml::encode($data->idMeasuremetType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gender')); ?>:</b>
	<?php echo CHtml::encode($data->gender); ?>
	<br />


</div>