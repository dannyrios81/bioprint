<table class="table-responsive" id="table-withoutuse">
    <tr>
        <th colspan="3">Without Use Licenses </th>
    </tr>
    <tr>
        <th width="30%">Name</th>
        <th width="30%">Last Name</th>
        <th width="40%">EMail</th>
    </tr>
    <?php foreach ($withoutusers as $reg): ?>
            <tr>
                <td><?php echo $reg['name'] ?></td>
                <td><?php echo $reg['lastName'] ?></td>
                <td><?php echo $reg['email'] ?></td>
            </tr>
    <?php endforeach; ?>
</table>
<div class="text-center">
    <?php echo CHtml::link('Back','',['class'=>"btn btn-default","onClick"=>'withuseHidden()']) ?>
</div>