<h1><?php echo $title ?></h1>

<br>
<div class="col-sm-12 main-content">
    <?php
    $this->Widget('vendor.miloschuman.yii-highcharts.highcharts.HighchartsWidget', array(
        'options' => array(
            //'chart'=>['type'=>'pie'],
            'title' => array('text' => ''),
            'subtitle'=> array('text'=>'All data is represented in Kg'),
            'plotOptions' => array(
                'pie' => array(
                    'allowPointSelect' => true,
                    'cursor' => 'pointer',
                    'dataLabels' => array(
                        'enabled' => true,
                        'format'=>'<b>{point.name}</b> ({point.y}) : {point.percentage:.1f} %',
                        'style'=>array('color'=> "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'")
                    )
                )
            ),

            'gradient' => array('enabled'=> true),
            'credits' => array('enabled' => false),
            'exporting' => array('enabled' => false),
            'chart' => array(
                'plotBackgroundColor' => '#ffffff',
                'plotBorderWidth' => null,
                'plotShadow' => false,
            ),
            'tooltip' => array(
                // 'pointFormat' => '{series.name}: <b>{point.percentage}%</b>',
                // 'percentageDecimals' => 1,
                'formatter'=> 'js:function() { return this.point.name+":  <b>"+Math.round(this.point.percentage*10)/10+"</b>%"; }',
                //the reason it didnt work before was because you need to use javascript functions to round and refrence the JSON as this.<array>.<index> ~jeffrey
            ),
            'series' => array(
                array(
                    'type' => 'pie',
                    'name' => 'Percentage',
                    'data' => $dataGraph,
                ),
            )
        ),
        'htmlOptions'=>array('id'=>'grafico')
    ));
    ?>
    <div class="col-sm-12"><?php echo CHtml::link('Back',['reports/trainerReports'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
</div><!-- form -->