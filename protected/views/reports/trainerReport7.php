<h1>Clients</h1>
<hr class="separator">
<!--</div>-->
<br>
<div class="col-sm-12">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'users-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'columns'=>array(
            ['name'=>'name','value'=>'$data->name'],
            ['name'=>'lastName','value'=>'$data->lastName'],
            ['name'=>'email','value'=>'$data->email'],
            array
            (
                'class'=>'CButtonColumn',
                'template'=>'{xupdate}{xpercent}{xcalculus}{xprotocols}',
                'header'=>"  Actions  ",
                'buttons'=>array(
                    'xupdate'=>array(
                        'label'=>'<i class="fa fa-line-chart fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
                              <i class="fa fa-line-chart visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>',
                        'url'=>'Yii::app()->createUrl("reports/treport7a", array("id"=>$data->id))',
                        'visible'=>'(count($data->weightHeights)>0?true:false)',
                        'options'=>array('title'=>'History Client Measure')
                    ),
                    'xpercent'=>array(
                        'label'=>'<i class="fa fa-percent fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
                              <i class="fa fa-percent visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>',
                        'url'=>'Yii::app()->createUrl("reports/treport7b", array("id"=>$data->id))',
                        'visible'=>'(count($data->weightHeights)>0?true:false)',
                        'options'=>array('title'=>'History Fat Percent')
                    ),
                    'xcalculus'=>array(
                        'label'=>'<i class="fa fa-calculator fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
                              <i class="fa fa-calculator visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>',
                        'url'=>'Yii::app()->createUrl("reports/treport7c", array("id"=>$data->id))',
                        'visible'=>'(count($data->weightHeights)>0?true:false)',
                        'options'=>array('title'=>'History calculation Measure')
                    ),
                    'xprotocols'=>array(
                        'label'=>'<i class="fa fa-bullseye fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
                              <i class="fa fa-bullseye visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>',
                        'url'=>'Yii::app()->createUrl("reports/treport7d", array("id"=>$data->id))',
                        'visible'=>'(count($data->weightHeights)>0?true:false)',
                        'options'=>array('title'=>'Hormonal Family History')
                    ),
                ),
            ),
        ),
    )); ?>

    <div class="col-sm-12"><?php echo CHtml::link('Back',['reports/trainerReports'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
</div>