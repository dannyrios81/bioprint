<?php
$urlFilter=Yii::app()->createAbsoluteUrl("reports/areport3");
$urlRedirect=Yii::app()->createAbsoluteUrl("reports/areport3a");

$script=<<<JS
if (!Modernizr.inputtypes.date) {
  $('#beginDate').attr('type','text');
  $('#beginDate').datepicker({format: "yyyy-mm-dd",autoclose: true,});
  $('#endDate').attr('type','text');
  $('#endDate').datepicker({format: "yyyy-mm-dd",autoclose: true,});
}
$('#filterBtn').click(function (event) {
        event.preventDefault();
        $("#withoutuse").slideUp();
        $("#inuse").slideUp();
        $("#div-grafica").slideDown();
    	$.ajax({
			type: "POST",
			dataType: "json",
			url: "{$urlFilter}",
			data: $('#filterForm').serialize(),
			success: function(datos){
			    // debugger;
			    var chart = $('#grafico').highcharts();
			    chart.series[0].setData(datos.data);
			    
			    $("#inuse").empty().html(datos.inusetable);
			    $("#withoutuse").empty().html(datos.withoutusetable);
			}
		});
    });
JS;
$botones=<<<JS
    function inuseHidden()
    {
        event.preventDefault();
        $("#div-grafica").slideDown();
        $("#inuse").slideUp();
    }
    function withuseHidden() {
      event.preventDefault();
      $("#div-grafica").slideDown();
      $("#withoutuse").slideUp();
    }
JS;

/*
 * CSS
 */
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap-datepicker3.min.css');
/*
 * JAVASCRIPT
 */
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/modernizr-custom.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('Script',$script, CClientScript::POS_READY);
Yii::app()->clientScript->registerScript('botones',$botones, CClientScript::POS_END);
?>
<h1><?php echo $title ?></h1>
<br>
<div class="col-sm-12 main-content analysis-table">
    <div class="row text-center">
        <?php echo CHtml::form("",'post',['id'=>'filterForm']) ?>
        <?php echo CHtml::dateField('beginDate','',['id'=>'beginDate','style'=>'display: inline-block;vertical-align: middle;float: none;'])?>
        &nbsp;To&nbsp;
        <?php echo CHtml::dateField('endDate','',['id'=>'endDate','style'=>'display: inline-block;vertical-align: middle;float: none;'])?>
        <?php echo CHtml::link('filter','#',['id'=>'filterBtn','class'=>'btn btn-primary btn-sm']) ?>
        <?php echo CHtml::endForm() ?>
    </div>
    <br>
    <div id="div-grafica">
        <?php
        $this->Widget('vendor.miloschuman.yii-highcharts.highcharts.HighchartsWidget', array(
            'options' => array(
                //'chart'=>['type'=>'pie'],
                'title' => array('text' => ''),
                'plotOptions' => array(
                    'pie' => array(
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'dataLabels' => array(
                            'enabled' => true,
                            'format'=>'<b>{point.name}</b> ({point.y}) : {point.percentage:.1f} %',
                            'style'=>array('color'=> "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'")
                        )
                    )
                ),

                'gradient' => array('enabled'=> true),
                'credits' => array('enabled' => false),
                'exporting' => array('enabled' => false),
                'chart' => array(
                    'plotBackgroundColor' => '#ffffff',
                    'plotBorderWidth' => null,
                    'plotShadow' => false,
                ),
                'tooltip' => array(
                    // 'pointFormat' => '{series.name}: <b>{point.percentage}%</b>',
                    // 'percentageDecimals' => 1,
                    'formatter'=> 'js:function() { return this.point.name+":  <b>"+Math.round(this.point.percentage)+"</b>%"; }',
                    //the reason it didnt work before was because you need to use javascript functions to round and refrence the JSON as this.<array>.<index> ~jeffrey
                ),
                'series' => array(
                    array(
                        'type' => 'pie',
                        'name' => 'Percentage',
                        'data' => $dataGraph,
                        'animation'=> false,
                        'point'=> [
                            'events'=>[
                                'click'=>'js:function (event) {$("#"+this.id).slideDown();$("#div-grafica").slideUp();console.log(this);}'
                            ]
                        ]
                    ),
                )
            ),
            'htmlOptions'=>array('id'=>'grafico')
        ));
        ?>
    </div>
    <div id="inuse" style="display: none">
        <?php echo $inusetable ?>
    </div>
    <div id="withoutuse" style="display: none">
        <?php echo $withoutusetable ?>
    </div>
</div><!-- form -->