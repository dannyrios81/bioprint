<h1><?php echo $title ?></h1>
<br>
<div class="col-sm-12 main-content analysis-table">
    <table class="table-responsive">
        <tr>
            <th width="60%">Product Name</th>
            <th width="20%" colspan="2">Active</th>
            <th width="20%" colspan="2">Inactive</th>
        </tr>
        <tr>
            <th width="60%">&nbsp;</th>
            <th width="10%">#</th>
            <th width="10%">%</th>
            <th width="10%">#</th>
            <th width="10%">%</th>
        </tr>
        <?php foreach ($data as $reg): ?>
            <tr>
                <td style="font-weight: bold"><?php echo $reg['name'] ?></td>
                <td style="font-weight: bold"><?php echo $reg['usersActives'] ?></td>
                <td style="font-weight: bold"><?php echo round((($reg['usersActives']*100)/$reg['totalUsers']),2) ?>%</td>
                <td style="font-weight: bold"><?php echo $reg['totalUsers']-$reg['usersActives'] ?></td>
                <td style="font-weight: bold"><?php echo 100-round((($reg['usersActives']*100)/$reg['totalUsers']),2) ?>%</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <br>
    <br>
    <div class="text-center">
        <?php echo CHtml::link('Back',['reports/adminReports'],['class'=>"btn btn-default"]) ?>
    </div>
</div><!-- form -->