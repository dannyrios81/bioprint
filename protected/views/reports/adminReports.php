<h1>Administrator Reports</h1>

<br>
<div class="col-sm-12 main-content">
	<div class="col-sm-6"><?php echo CHtml::link('Seminars Attendees',['reports/areport1'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Licenses Report',['reports/areport2'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Active Practitioners',['reports/areport3'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Products',['reports/areport4'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Measure Sets By Practitioner',['reports/areport5'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
</div><!-- form -->