<h1>Practitioner Reports</h1>

<br>
<div class="col-sm-12 main-content">
	<div class="col-sm-6"><?php echo CHtml::link('Gender Distribution',['reports/treport1'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Age Distribution',['reports/treport2'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Body Fat Percentage Distribution',['reports/treport3'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Fat Mass Distribution',['reports/treport4'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Lean Mass Distribution',['reports/treport5'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Clients Measurements',['reports/treport6'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Measurement History',['reports/treport7'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
</div><!-- form -->