<?php
$urlFilter=Yii::app()->createAbsoluteUrl("reports/treport7c",['id'=>$id]);

$script=<<<JS
if (!Modernizr.inputtypes.date) {
  $('#beginDate').attr('type','text');
  $('#beginDate').datepicker({format: "yyyy-mm-dd",autoclose: true,});
  $('#endDate').attr('type','text');
  $('#endDate').datepicker({format: "yyyy-mm-dd",autoclose: true,});
}
$('#filterBtn').click(function (event) {
        event.preventDefault();
    	$.ajax({
			type: "POST",
			dataType: "json",
			url: "{$urlFilter}",
			data: $('#filterForm').serialize(),
			success: function(datos){
			    // debugger;
			    var chart = $('#grafico').highcharts();
			    var seriesLength = chart.series.length;
                for(var i = seriesLength - 1; i > -1; i--)
                {
                    chart.series[i].remove();
                }
                chart.xAxis[0].setCategories(datos.serieGraph);
                $(datos.dataGraph).each(function(index,element){
                    chart.addSeries(element);
                });
			}
		});
    });
JS;
/*
 * CSS
 */
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap-datepicker3.min.css');
/*
 * JAVASCRIPT
 */
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/modernizr-custom.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('Script',$script, CClientScript::POS_READY);
?>
<h1><?php echo $title ?></h1>
<br>
<div class="col-sm-12 main-content">
    <div class="row text-center">
    <?php echo CHtml::form("",'post',['id'=>'filterForm']) ?>
        <?php echo CHtml::dateField('beginDate','',['id'=>'beginDate','style'=>'display: inline-block;vertical-align: middle;float: none;'])?>
        &nbsp;To&nbsp;
        <?php echo CHtml::dateField('endDate','',['id'=>'endDate','style'=>'display: inline-block;vertical-align: middle;float: none;'])?>
        <?php echo CHtml::link('filter','#',['id'=>'filterBtn','class'=>'btn btn-primary btn-sm']) ?>
    <?php echo CHtml::endForm() ?>
    </div>
    <?php
    $this->Widget('vendor.miloschuman.yii-highcharts.highcharts.HighchartsWidget', array(
        'options'=>array(
            'title' => array('text' => ''),
            'tooltip'=> array(
                'headerFormat'=> '<b>{point.key}</b><br/>',
                'pointFormat'=> '{point.y:.1f}'
            ),
            'xAxis' => array(
                'categories' => $serieGraph
            ),
            'yAxis' => array(
                'title' => array('text' => '')
            ),
            'legend'=> ['layout'=>'vertical','align'=>'right','verticalAlign'=>'middle','borderWidth'=>0],
            'credits' => array('enabled' => false),
            'series' => $dataGraph
        ),
        'htmlOptions'=>array('id'=>'grafico')
    ));
    ?>
    <div class="col-sm-12"><?php echo CHtml::link('Back',['reports/treport7'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
</div><!-- form -->