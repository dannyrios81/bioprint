<h1><?php echo $title ?></h1>
<div class="col-sm-12 main-content analysis-table">
    <table class="table-responsive">
        <tr>
            <th>Seminar ID</th>
            <th>Name Seminar</th>
            <th>Date</th>
            <th>Location</th>
        </tr>
        <tr>
            <td><?php echo $seminar->courseIdentification ?></td>
            <td><?php echo $seminar->name ?></td>
            <td><?php echo $seminar->startDate ?></td>
            <td><?php echo $seminar->location ?></td>
        </tr>
    </table>
    <table class="table-responsive">
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
        </tr>
        <?php foreach ($users as $user): ?>
        <tr>
            <td><?php echo $user->idUsers0->name ?></td>
            <td><?php echo $user->idUsers0->lastName ?></td>
            <td><?php echo $user->idUsers0->email ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
    <br>
    <div class="text-center">
        <?php echo CHtml::link('Back',['reports/areport1'],['class'=>"btn btn-default"]) ?>
    </div>
</div>