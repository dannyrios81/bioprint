<?php
$urlFilter=Yii::app()->createAbsoluteUrl("reports/areport2");

$script=<<<JS
if (!Modernizr.inputtypes.date) {
  $('#beginDate').attr('type','text');
  $('#beginDate').datepicker({format: "yyyy-mm-dd",autoclose: true,});
  $('#endDate').attr('type','text');
  $('#endDate').datepicker({format: "yyyy-mm-dd",autoclose: true,});
}
$('#filterBtn').click(function (event) {
        event.preventDefault();
    	$.ajax({
			type: "POST",
			dataType: "json",
			url: "{$urlFilter}",
			data: $('#filterForm').serialize(),
			success: function(datos){
			    // debugger;
			    var chart = $('#grafico').highcharts();
			    var chart2 = $('#grafico2').highcharts();
			    var seriesLength1 = chart.series.length;
			    var seriesLength2 = chart2.series.length;
                for(var i = seriesLength1 - 1; i > -1; i--)
                {
                    chart.series[i].remove();
                }
                for(var i = seriesLength2 - 1; i > -1; i--)
                {
                    chart2.series[i].remove();
                }
                chart.xAxis[0].setCategories(datos.serie);
                chart2.xAxis[0].setCategories(datos.serie);

                $(datos.dataLicencia).each(function(index,element){
                    chart.addSeries(element);
                });
                $(datos.dataFacturado).each(function(index,element){
                    chart2.addSeries(element);
                });
                // ingeresando los datos a la tabla actualizados
                $('#table-newlicences tr:gt(1)').remove();
                $('#table-renewlicences tr:gt(1)').remove();
                
                $(datos.tabla).each(function(index,element){
                    if(element.renewal == 1)
                    {
                        $('#table-renewlicences').append("<tr><td>Licenses renewed "+element.name+"</td><td>"+element.count+"</td><td>$ "+element.total+"</td></tr>");
                    }
                    else 
                    {
                        $('#table-newlicences').append("<tr><td>Licenses sold "+element.name+"</td><td>"+element.count+"</td><td>$ "+element.total+"</td></tr>");
                    }
                    console.log(element);
                });

			}
		});
    });
JS;
/*
 * CSS
 */
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap-datepicker3.min.css');
/*
 * JAVASCRIPT
 */
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/modernizr-custom.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('Script',$script, CClientScript::POS_READY);
?>
<h1><?php echo $title ?></h1>
<br>
<div class="col-sm-12 main-content analysis-table">
    <div class="row text-center">
        <?php echo CHtml::form("",'post',['id'=>'filterForm']) ?>
        <?php echo CHtml::dateField('beginDate','',['id'=>'beginDate','style'=>'display: inline-block;vertical-align: middle;float: none;'])?>
        &nbsp;To&nbsp;
        <?php echo CHtml::dateField('endDate','',['id'=>'endDate','style'=>'display: inline-block;vertical-align: middle;float: none;'])?>
        <?php echo CHtml::link('filter','#',['id'=>'filterBtn','class'=>'btn btn-primary btn-sm']) ?>
        <?php echo CHtml::endForm() ?>
    </div>
    <br>
    <br>
        <table class="table-responsive" id="table-newlicences" style="font-family: 'Oswald', sans-serif;">
        <tr>
            <th colspan="3">New Licenses</th>
        </tr>
        <tr>
            <th width="50%">&nbsp;</th>
            <th width="25%">#</th>
            <th width="25%">$</th>
        </tr>
        <?php foreach ($tabla as $reg): ?>
            <?php if($reg['renewal']==0): ?>
            <tr>
                <td>Licenses sold <?php echo $reg['name'] ?></td>
                <td><?php echo $reg['count'] ?></td>
                <td>$ <?php echo $reg['total'] ?></td>
            </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </table>
    <table class="table-responsive" id="table-renewlicences" style="font-family: 'Oswald', sans-serif;">
        <tr>
            <th colspan="3">Licenses Renewed</th>
        </tr>
        <tr>
            <th width="50%">&nbsp;</th>
            <th width="25%">#</th>
            <th width="25%">$</th>
        </tr>
        <?php foreach ($tabla as $reg): ?>
            <?php if($reg['renewal']==1): ?>
            <tr>
                <td>Licenses renewed <?php echo $reg['name'] ?></td>
                <td><?php echo $reg['count'] ?></td>
                <td>$ <?php echo $reg['total'] ?></td>
            </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </table>
    <div class="col-md-6">
        <h2 style="font-weight: bold;text-align: center;font-family: 'Oswald', sans-serif;">Licenses Sold per Month</h2>
        <?php
        $this->Widget('vendor.miloschuman.yii-highcharts.highcharts.HighchartsWidget', array(
            'options'=>array(
                'title' => array('text' => ''),
                'xAxis' => array(
                    'categories' => $serie
                ),
                'yAxis' => array(
                    'title' => array('text' => '')
                ),
                'legend'=> ['layout'=>'vertical','align'=>'right','verticalAlign'=>'middle','borderWidth'=>0],
                'credits' => array('enabled' => false),
                'series' => $dataLicencia
            ),
            'htmlOptions'=>array('id'=>'grafico')
        ));
        ?>
    </div>
    <div class="col-md-6">
        <h2 style="font-weight: bold;text-align: center;font-family: 'Oswald', sans-serif;">Income per Month</h2>
        <?php
        $this->Widget('vendor.miloschuman.yii-highcharts.highcharts.HighchartsWidget', array(
            'options'=>array(
                'title' => array('text' => ''),
                'xAxis' => array(
                    'categories' => $serie
                ),
                'yAxis' => array(
                    'title' => array('text' => '')
                ),
                'legend'=> ['layout'=>'vertical','align'=>'right','verticalAlign'=>'middle','borderWidth'=>0],
                'credits' => array('enabled' => false),
                'series' => $dataFacturado
            ),
            'htmlOptions'=>array('id'=>'grafico2')
        ));
        ?>
    </div>
</div><!-- form -->