<table class="table-responsive" id="table-newlicences">
    <tr>
        <th width="40%">Name</th>
        <th width="40%">Last Name</th>
        <th width="20%">#</th>
    </tr>
    <?php foreach ($tabla as $reg): ?>
            <tr>
                <td style="font-weight: bold"><?php echo $reg['name'] ?></td>
                <td style="font-weight: bold"><?php echo $reg['lastName'] ?></td>
                <td style="font-weight: bold"><?php echo $reg['measures'] ?></td>
            </tr>
    <?php endforeach; ?>
</table>