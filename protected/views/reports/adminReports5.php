<?php
$urlFilter=Yii::app()->createAbsoluteUrl("reports/areport5");

$script=<<<JS
if (!Modernizr.inputtypes.date) {
  $('#beginDate').attr('type','text');
  $('#beginDate').datepicker({format: "yyyy-mm-dd",autoclose: true,});
  $('#endDate').attr('type','text');
  $('#endDate').datepicker({format: "yyyy-mm-dd",autoclose: true,});
}
$('#filterBtn').click(function (event) {
        event.preventDefault();
    	$.ajax({
			type: "POST",
			dataType: "json",
			url: "{$urlFilter}",
			data: $('#filterForm').serialize(),
			success: function(datos){
			    // debugger;
			    console.log(datos);
			    $('#mod-tabla').empty().html(datos.tabla);

			}
		});
    });
JS;
/*
 * CSS
 */
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap-datepicker3.min.css');
/*
 * JAVASCRIPT
 */
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/modernizr-custom.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-datepicker.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('Script',$script, CClientScript::POS_READY);
?>
<h1><?php echo $title ?></h1>
<br>
<div class="col-sm-12 main-content analysis-table">
    <div class="row text-center">
        <?php echo CHtml::form("",'post',['id'=>'filterForm']) ?>
        <?php echo CHtml::dateField('beginDate','',['id'=>'beginDate','style'=>'display: inline-block;vertical-align: middle;float: none;'])?>
        &nbsp;To&nbsp;
        <?php echo CHtml::dateField('endDate','',['id'=>'endDate','style'=>'display: inline-block;vertical-align: middle;float: none;'])?>
        <?php echo CHtml::link('filter','#',['id'=>'filterBtn','class'=>'btn btn-primary btn-sm']) ?>
        <?php echo CHtml::endForm() ?>
    </div>
    <br>
    <br>
    <div id="mod-tabla">
        <?php echo $tabla ?>
    </div>
</div><!-- form -->