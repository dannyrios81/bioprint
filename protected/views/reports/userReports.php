<h1>Client Reports</h1>

<br>
<div class="col-sm-12 main-content">
	<div class="col-sm-6"><?php echo CHtml::link('Body Fat Percentage History',['reports/ureport1'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Fat Mass History',['reports/ureport2'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Lean Mass History',['reports/ureport3'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
	<div class="col-sm-6"><?php echo CHtml::link('Sent Protocols',['reports/ureport4'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
</div><!-- form -->