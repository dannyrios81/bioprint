<h1><?php echo $title ?></h1>
<div class="col-sm-12 main-content">
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'course-grid',
        'dataProvider'=>$model->search(),
        'ajaxUpdate'=>true,
        'filter'=>$model,
        'columns'=>array(
            [
                'header'=>'Seminars ID',
                'name'=>'courseIdentification'
            ],
            'name',
            [
                'header'=>'Date',
                'name'=>'startDate',
                'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",$data->startDate);'
            ],
            [
                'header'=>'Place',
                'name'=>'location',
            ],
            array
            (
                'class'=>'CButtonColumn',
                'template'=>'{users}',
                'header'=>"  Actions  ",
                'buttons'=>array(
                    'users'=>array(
                        'label'=>'<i class="fa fa-user fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-user visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>',
                        'url'=>'Yii::app()->createUrl("reports/areport1a", array("id"=>$data->id))',
                        'options'=>array('title'=>'Update Seminars')
                    )
                ),
            )
        ),
    ));
    ?>
</div>	<!--End white-border-radius-->