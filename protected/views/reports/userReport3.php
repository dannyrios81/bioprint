<h1><?php echo $title ?></h1>

<br>
<div class="col-sm-12 main-content">
    <?php
    $this->Widget('vendor.miloschuman.yii-highcharts.highcharts.HighchartsWidget', array(
        'options'=>array(
            'title' => array('text' => ''),
            'subtitle'=> array('text'=>'All data are represented in Kg'),
            'tooltip'=> array(
                'headerFormat'=> '<b>{point.key}</b><br/>',
                'pointFormat'=> '{point.y:.1f}'
            ),
            'xAxis' => array(
                'categories' => $serieGraph
            ),
            'yAxis' => array(
                'title' => array('text' => '')
//            ,
//                'tickInterval'=> 25,
//                'minRange'=> 1,
//                'allowDecimals'=> false,
//                'startOnTick'=> true,
//                'endOnTick'=> true
            ),
            'legend'=> ['layout'=>'vertical','align'=>'right','verticalAlign'=>'middle','borderWidth'=>0],
            'credits' => array('enabled' => false),
            'series' => [['data'=>$dataGraph,'name'=>'Lean Mass']]
        ),
        'htmlOptions'=>array('id'=>'grafico')
    ));
    ?>
    <div class="col-sm-12"><?php echo CHtml::link('Back',['reports/userReports'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
</div><!-- form -->