<h1><?php echo $title ?></h1>
<hr class="separator">
<div class="col-sm-12 main-content">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'sent-protocols-grid',
        'dataProvider'=>$model->search(),
        'columns'=>array(
//            ['name'=>'Users','value'=>'(!empty($data->idUsers))?$data->idUsers0->name." ".$data->idUsers0->lastName:""'],
            'hormoneFamily',
            'measurementType',
            ['name'=>'Sent Date','value'=>'(!empty($data->sentDate))?Yii::app()->dateFormatter->format(\'yyyy-MM-dd\',$data->sentDate):""'],
            array(
                'class'=>'CButtonColumn',
                'template'=>'{xview}',
                'header'=>"  Actions  ",
                'buttons'=>array(
                    'xview'=>array(
                        'label'=>'<i class="fa fa-eye fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
                                <i class="fa fa-eye visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
                        'url'=>'Yii::app()->createUrl("reports/ureport4a", array("id"=>$data->id))',
                        'options'=>array('title'=>'View Sent Protocol')
                    ),
                ),
            )
        ),
    )); ?>
    <div class="col-sm-12"><?php echo CHtml::link('Back',['reports/userReports'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
</div>