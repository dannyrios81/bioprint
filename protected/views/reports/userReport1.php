<h1><?php echo $title ?></h1>

<br>
<div class="col-sm-12 main-content">
    <?php
    $this->Widget('vendor.miloschuman.yii-highcharts.highcharts.HighchartsWidget', array(
        'options'=>array(
            'title' => array('text' => ''),
            'tooltip'=> array(
                'headerFormat'=> '<b>{point.key}</b><br/>',
                'pointFormat'=> '{point.y:.1f}'
            ),
            'xAxis' => array(
                'categories' => $serieGraph
            ),
            'yAxis' => array(
                'title' => array('text' => '')
            ),
            'legend'=> ['layout'=>'vertical','align'=>'right','verticalAlign'=>'middle','borderWidth'=>0],
            'credits' => array('enabled' => false),
            'series' => [['data'=>$dataGraph,'name'=>'% Fat']]
        ),
        'htmlOptions'=>array('id'=>'grafico')
    ));
    ?>
    <div class="col-sm-12"><?php echo CHtml::link('Back',['reports/userReports'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
</div><!-- form -->