<h1><?php echo $title ?></h1>

<br>
<div class="col-sm-12 main-content">
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th width="40%">Name</th>
                <th width="40%">Last Name</th>
                <th width="20%"># Measurement Sets</th>
            </tr>
            <?php $sumMeasurement = 0; ?>
            <?php foreach ($dataGraph as $item): ?>
            <tr>
                <td><?php echo ucfirst(strtolower($item['name'])) ?></td>
                <td><?php echo ucfirst(strtolower($item['lastName'])) ?></td>
                <td><?php echo (integer)$item['qty_measures'] ?></td>
            </tr>
            <?php $sumMeasurement = $sumMeasurement + (integer)$item['qty_measures']; ?>
            <?php endforeach; ?>
            <tr>
                <td class="text-right" colspan="2"><b>Total Measurements </b></td>
                <td><b><?php echo $sumMeasurement ?></b></td>
            </tr>
        </table>
    </div>
    <div class="col-sm-12"><?php echo CHtml::link('Back',['reports/trainerReports'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
</div><!-- form -->