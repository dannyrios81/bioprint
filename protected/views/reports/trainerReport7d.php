<h1><?php echo $title ?></h1>
<br>
<div class="col-sm-12 main-content">
    <?php
    $this->Widget('vendor.miloschuman.yii-highcharts.highcharts.HighchartsWidget', array(
        'options'=>array(
            'title' => array('text' => ''),
            'tooltip'=> array(
                'headerFormat'=> '<b>{point.series.name}</b><br/>',
                'pointFormat'=> '{point.name} : {point.y:.1f}',
//                'formatter'=> 'js:function() { console.log(this); }',
//                'crosshairs'=> true,
//                'shared'=> true
            ),
            'xAxis' => array(
                'categories' => $serie
            ),
            'yAxis' => array(
                'title' => array('text' => '')
            ),
            'legend'=> ['layout'=>'vertical','align'=>'right','verticalAlign'=>'middle','borderWidth'=>0],

            'plotOptions' => array(
                'line' => array(
                    'allowPointSelect' => true,
                    'cursor' => 'pointer',
                    'dataLabels' => array(
                        'enabled' => true,
                        'format'=>'<b>{point.name}</b>'
                    )
                )
            ),
            'credits' => array('enabled' => false),
            'series' => $dataJson
        ),
        'htmlOptions'=>array('id'=>'grafico')
    ));
    ?>
    <div class="col-sm-12"><?php echo CHtml::link('Back',['reports/treport7'],['class'=>'btn btn-default btn-lg btn-block'])?></div>
</div><!-- form -->