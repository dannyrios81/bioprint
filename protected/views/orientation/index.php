<?php
/* @var $this OrientationController */
/* @var $model Orientation */
?>

<h1>Orientations</h1>
<div class="col-sm-12 form-container">
    <div class="text-right" style="margin-bottom: 10px">
        <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('orientation/create')?>"><i class="fa fa-plus-circle"></i> New Orientation</a>
    </div>
</div>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orientation-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'columns'=>array(
        'description',
        [
            'header'=>'Strength Quality',
            'name'=>'idMethod',
            'value'=>'$data->idFamilyExercise->bodyArea->idMethod0->description',
            'filter'=>CHtml::activeDropDownList($model,'idMethod',CHtml::listData(Method::model()->findAll(['order'=>'description']),'id','description'),['class'=>"form-control",'empty'=>'Select ...'])
        ],
        [
            'header'=>'Body Area',
            'name'=>'idBodyArea',
            'value'=>'$data->idFamilyExercise->bodyArea->description',
            'filter'=>CHtml::activeDropDownList($model,'idBodyArea',CHtml::listData(BodyArea::model()->findAll(['order'=>'description']),'id','description'),['class'=>"form-control",'empty'=>'Select ...'])
        ],
        [
            'header'=>'Exercise Family',
            'name'=>'idFamily_exercise',
            'value'=>'$data->idFamilyExercise->description',
            'filter'=>CHtml::activeDropDownList($model,'idFamily_exercise',Utilities::bodyAreaWithFamilyExerciceListData(),['class'=>"form-control",'empty'=>'Select ...'])
        ],
        array(
            'class'=>'CButtonColumn',
            'template'=>'{xEdit}{xDelete}',
            'header'=>"  Actions  ",
            'buttons'=>[
                'xEdit'=>array(
                    'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                    'url'=>'Yii::app()->createUrl("orientation/update", array("id"=>$data->id))',
                    'options'=>array('title'=>'Edit','class'=>'xEdit')
                ),
                'xDelete'=>array(
                    'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                    'url'=>'Yii::app()->createUrl("orientation/delete", array("id"=>$data->id))',
                    'options'=>array('title'=>'Delete'),
                    'click'=>"function() {
							if(!confirm('Are you sure you want to delete this item?')) return false;
							var th = this,afterDelete = function(){};
							jQuery('#orientation-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
								    if(data.length>0)
								        alert(data);
									jQuery('#orientation-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
						}"
                ),
            ]
        ),
    ),
)); ?>
</div>
