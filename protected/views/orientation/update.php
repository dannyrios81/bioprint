<?php
/* @var $this OrientationController */
/* @var $model Orientation */
?>

<h1>Update Orientation <?php echo $model->description; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>