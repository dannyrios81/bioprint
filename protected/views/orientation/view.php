<?php
/* @var $this OrientationController */
/* @var $model Orientation */

$this->breadcrumbs=array(
	'Orientations'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Orientation', 'url'=>array('index')),
	array('label'=>'Create Orientation', 'url'=>array('create')),
	array('label'=>'Update Orientation', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Orientation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Orientation', 'url'=>array('admin')),
);
?>

<h1>View Orientation #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
		'familyExerciseId',
	),
)); ?>
