<h1>View Supplement #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'independent',
		'linkToBuy',
		'idCountry',
	),
)); ?>
