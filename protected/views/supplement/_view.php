<?php
<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('independent')); ?>:</b>
	<?php echo CHtml::encode($data->independent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('linkToBuy')); ?>:</b>
	<?php echo CHtml::encode($data->linkToBuy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCountry')); ?>:</b>
	<?php echo CHtml::encode($data->idCountry); ?>
	<br />


</div>