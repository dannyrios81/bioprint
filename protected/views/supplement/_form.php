<div class="col-sm-12 main-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'supplement-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'name'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'name',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'name',array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'independent'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'independent',array('class'=>"control-label")); ?>
		<?php echo $form->dropDownList($model,'independent',array('0'=>'No','1'=>'Yes'),array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'linkToBuy'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'linkToBuy',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'linkToBuy',array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'idCountry'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'idCountry',array('class'=>"control-label")); ?>
		<?php echo $form->dropDownList($model,'idCountry',CHtml::listData(Country::model()->findAll(),'id','name'),array('class'=>"form-control")); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->