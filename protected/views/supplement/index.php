<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#supplement-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Supplements</h1>
<hr class="separator">
<div class="col-sm-12 form-container">
	<div class="text-right" style="margin-bottom: 10px">
		<a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('supplement/create')?>"><i class="fa fa-plus-circle"></i> New supplement</a>
	</div>
</div>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplement-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
		'name',
		['header'=>'Independent','name'=>'independent','value'=>'(empty($data->independent)?"No":"Yes")','filter'=>CHtml::activeDropDownList($model,'independent',['0'=>'No','1'=>'Yes'],['empty'=>'Select ...'])],
		'linkToBuy',
		['header'=>'Zone','name'=>'idCountry','value'=>'$data->idCountry0->name','filter'=>CHtml::activeDropDownList($model,'idCountry',CHtml::listData(Country::model()->findAll(),'id','name'),['empty'=>'Select ...'])],
		array(
			'class'=>'CButtonColumn',
			'template'=>'{xupdate}{xdelete}',
			'header'=>"  Actions  ",
			'buttons'=>array(
				'xupdate'=>array(
					'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								<i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
					'url'=>'Yii::app()->createUrl("supplement/update", array("id"=>$data->id))',
					'options'=>array('title'=>'Update supplement')
				),
				'xdelete'=>array(
					'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								<i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
					'url'=>'Yii::app()->createUrl("supplement/delete", array("id"=>$data->id))',
					'options'=>array('title'=>'Delete supplement'),
					'visible'=>'((count($data->chooseSuplements)==0)?true:false)',
					'click'=>"function() {
						if(!confirm('Are you sure you want to delete this item?')) return false;
						var th = this,afterDelete = function(){};
							jQuery('#supplement-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
									jQuery('#supplement-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
					}"
				),
			),
		)
	),
)); ?>
</div>
