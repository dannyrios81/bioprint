<?php
$urlAddMeasurements =  Yii::app()->createAbsoluteUrl("prueba/formulario",["id"=>$user->id]);

$scriptEnable =<<<JS
    $(document).on('click','.plus-symbol',function(){
        window.location.href = "$urlAddMeasurements"

    });
JS;

Yii::app()->clientScript->registerScript('scriptEnable',$scriptEnable,CClientScript::POS_READY);

?>
<div class="col-xs-12">
    <h1>Measures</h1>
</div>
<hr class="separator">
<div class="col-sm-12 main-content measures-table">
    <span class="left-arrow close fa fa-chevron-left"> </span>
    <span class="right-arrow open fa fa-chevron-right"></span>
    <!--Tablas para moviles-->
    <!--Tabla De Medidas-->
    <table class="table-responsive hidden-lg hidden-md hidden-sm visible-xs" id="mobile-normal">
        <tr>
            <th colspan="2" class="plus-symbol">+</th>
            <?php $headers=array_keys($mobile) ?>
            <?php foreach ($headers as $header):?>
            <th><span><?php echo date('d/m/Y',strtotime($header)) ?></span></th>
            <?php endforeach;?>
        </tr>
        <tr>
            <th colspan="2">Age</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$header); ?></td>
            <?php endforeach;?>
        </tr>
        <tr><th rowspan="2">Height</th><th>cm</th><td><?php echo implode('</td><td>',($user->heightUnit=='Cm'?array_fill(0,count($headers),$user->height):array_fill(0,count($headers),'x'))) ?></td></tr>
        <tr><th>inches</th><td><?php echo implode('</td><td>',($user->heightUnit=='In'?array_fill(0,count($headers),$user->height):array_fill(0,count($headers),'x'))) ?></td></tr>
        <tr><th rowspan="2">Weight</th><th>Kg</th><td><?php echo implode('</td><td>',($user->weightUnit=='Kg'?array_fill(0,count($headers),$user->weight):array_fill(0,count($headers),'x'))) ?></td></tr>
        <tr><th>Lbs</th><td><?php echo implode('</td><td>',($user->weightUnit=='Lbs'?array_fill(0,count($headers),$user->weight):array_fill(0,count($headers),'x'))) ?></td></tr>
        <!--filas de las medidas -->
            <?php foreach ($desktop as $key=>$measure):?>
                <?php //CVarDumper::dump($desktop,10,true);exit; ?>
                <?php if($key != 'Lean Mass' and $key != 'Fat Mass'): ?>
        <tr><th colspan="2"><?php echo $key ?></th><td><?php echo implode('</td><td>',$measure) ?></td></tr>
                <?php else: ?>
        <tr><th rowspan="2"><?php echo $key ?></th><th>Kg</th><td><?php echo implode('</td><td>',($user->weightUnit=='Kg'?$measure:array_fill(0,count($measure),'x'))) ?></td></tr>
        <tr><th>Lbs</th><td><?php echo implode('</td><td>',($user->weightUnit=='Lbs'?$measure:array_fill(0,count($measure),'x'))) ?></td></tr>

                <?php endif; ?>
            <?php endforeach;?>
        <!--fin filas medidas-->
        <tr><th colspan="2">Analysis</th><?php foreach ($headers as $header) {echo "<td id='ma-$header'>&gt;</td>";}?></tr>
        <tr><th colspan="2">Delete</th><?php foreach ($headers as $header) {echo "<td id='md-$header'>X</td>";}?></tr>
    </table>
    <!--Tabla de diferencias-->
    <table class="table-responsive hidden hidden-lg hidden-md hidden-sm " id="mobile-diff">
        <tr>
            <th colspan="2" class="plus-symbol">+</th>
            <?php foreach ($headers as $header):?>
                <th><span><?php echo date('d/m/Y',strtotime($header)) ?></span></th>
            <?php endforeach;?>
        </tr>
        <tr>
            <th colspan="2">Age</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$header); ?></td>
            <?php endforeach;?>
        </tr>
        <tr><th rowspan="2">Height</th><th>cm</th><td><?php echo implode('</td><td>',($user->heightUnit=='Cm'?array_fill(0,count($headers),$user->height):array_fill(0,count($headers),'x'))) ?></td></tr>
        <tr><th>inches</th><td><?php echo implode('</td><td>',($user->heightUnit=='In'?array_fill(0,count($headers),$user->height):array_fill(0,count($headers),'x'))) ?></td></tr>
        <tr><th rowspan="2">Weight</th><th>Kg</th><td><?php echo implode('</td><td>',($user->weightUnit=='Kg'?array_fill(0,count($headers),$user->weight):array_fill(0,count($headers),'x'))) ?></td></tr>
        <tr><th>Lbs</th><td><?php echo implode('</td><td>',($user->weightUnit=='Lbs'?array_fill(0,count($headers),$user->weight):array_fill(0,count($headers),'x'))) ?></td></tr>
        <!--filas de las medidas -->
        <?php foreach ($desktopDiff as $key=>$measure):?>
            <?php //CVarDumper::dump($desktop,10,true);exit; ?>
            <?php if($key != 'Lean Mass' and $key != 'Fat Mass'): ?>
                <tr><th colspan="2"><?php echo $key ?></th><td><?php echo implode('</td><td>',$measure) ?></td></tr>
            <?php else: ?>
                <tr><th rowspan="2"><?php echo $key ?></th><th>Kg</th><td><?php echo implode('</td><td>',($user->weightUnit=='Kg'?$measure:array_fill(0,count($measure),'x'))) ?></td></tr>
                <tr><th>Lbs</th><td><?php echo implode('</td><td>',($user->weightUnit=='Lbs'?$measure:array_fill(0,count($measure),'x'))) ?></td></tr>

            <?php endif; ?>
        <?php endforeach;?>
        <!--fin filas medidas-->
        <tr><th colspan="2">Analysis</th><?php foreach ($headers as $header) {echo "<td id='ma-$header'>&gt;</td>";}?></tr>
        <tr><th colspan="2">Delete</th><?php foreach ($headers as $header) {echo "<td id='md-$header'>X</td>";}?></tr>
    </table>
    <!--Tabla de Porcentaje-->
    <table class="table-responsive hidden hidden-lg hidden-md hidden-sm " id="mobile-percent">
        <tr>
            <th colspan="2" class="plus-symbol">+</th>
            <?php foreach ($headers as $header):?>
                <th><span><?php echo date('d/m/Y',strtotime($header)) ?></span></th>
            <?php endforeach;?>
        </tr>
        <tr>
            <th colspan="2">Age</th>
            <?php foreach ($headers as $header):?>
                <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$header); ?></td>
            <?php endforeach;?>
        </tr>
        <tr><th rowspan="2">Height</th><th>cm</th><td><?php echo implode('</td><td>',($user->heightUnit=='Cm'?array_fill(0,count($headers),$user->height):array_fill(0,count($headers),'x'))) ?></td></tr>
        <tr><th>inches</th><td><?php echo implode('</td><td>',($user->heightUnit=='In'?array_fill(0,count($headers),$user->height):array_fill(0,count($headers),'x'))) ?></td></tr>
        <tr><th rowspan="2">Weight</th><th>Kg</th><td><?php echo implode('</td><td>',($user->weightUnit=='Kg'?array_fill(0,count($headers),$user->weight):array_fill(0,count($headers),'x'))) ?></td></tr>
        <tr><th>Lbs</th><td><?php echo implode('</td><td>',($user->weightUnit=='Lbs'?array_fill(0,count($headers),$user->weight):array_fill(0,count($headers),'x'))) ?></td></tr>
        <!--filas de las medidas -->
        <?php foreach ($desktopPercent as $key=>$measure):?>
            <?php //CVarDumper::dump($desktop,10,true);exit; ?>
            <?php if($key != 'Lean Mass' and $key != 'Fat Mass'): ?>
                <tr><th colspan="2"><?php echo $key ?></th><td><?php echo implode('</td><td>',$measure) ?></td></tr>
            <?php else: ?>
                <tr><th rowspan="2"><?php echo $key ?></th><th>Kg</th><td><?php echo implode('</td><td>',($user->weightUnit=='Kg'?$measure:array_fill(0,count($measure),'x'))) ?></td></tr>
                <tr><th>Lbs</th><td><?php echo implode('</td><td>',($user->weightUnit=='Lbs'?$measure:array_fill(0,count($measure),'x'))) ?></td></tr>

            <?php endif; ?>
        <?php endforeach;?>
        <!--fin filas medidas-->
        <tr><th colspan="2">Analysis</th><?php foreach ($headers as $header) {echo "<td id='ma-$header'>&gt;</td>";}?></tr>
        <tr><th colspan="2">Delete</th><?php foreach ($headers as $header) {echo "<td id='md-$header'>X</td>";}?></tr>
    </table>
    <!--Tablas para desktop-->
    <!--Tabla De Medidas-->
    <table  class="visible-lg visible-md visible-sm hidden-xs" id="desktop-normal">
        <tr rowspan="2">
            <th rowspan="2" class="plus-symbol">+</th>
            <th rowspan="2"><span>Age</span></th>
            <th colspan="2">Height</th>
            <th colspan="2">Weight</th>
            <th rowspan="2"><span>Chin</span></th>
            <th rowspan="2"><span>Cheek</span></th>
            <th rowspan="2"><span>Pec</span></th>
            <th rowspan="2"><span>Biceps</span></th>
            <th rowspan="2"><span>Triceps</span></th>
            <th rowspan="2"><span>S.Scap</span></th>
            <th rowspan="2"><span>Midax</span></th>
            <th rowspan="2"><span>Supra</span></th>
            <th rowspan="2"><span>Austr</span></th>
            <th rowspan="2"><span>Umbil</span></th>
            <th rowspan="2"><span>Knee</span></th>
            <th rowspan="2"><span>Calves</span></th>
            <th rowspan="2"><span>Quadric</span></th>
            <th rowspan="2"><span>Hamst</span></th>
            <th rowspan="2"><span>Sum</span></th>
            <th rowspan="2"><span>%Fat</span></th>
            <th colspan="2">Lean Mass</th>
            <th colspan="2">Fat Mass</th>
            <th rowspan="2">Analysis</th>
            <th rowspan="2">Delete</th>
        </tr>
        <tr>
            <th>cm</th>
            <th>inches</th>
            <th>Kg</th>
            <th>Lbs</th>
            <th>Kg</th>
            <th>Lbs</th>
            <th>Kg</th>
            <th>Lbs</th>
        </tr>
        <?php foreach ($mobile as $key=>$item): ?>
        <tr>
            <th><?php echo  date('d/m/Y',strtotime($key)) ?></th>
            <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$key); ?></td>
            <td><?php echo $user->heightUnit=='Cm'?$user->height:'X' ?></td>
            <td><?php echo $user->heightUnit=='In'?$user->height:'X' ?></td>
            <td><?php echo $user->weightUnit=='Kg'?$user->weight:'X' ?></td>
            <td><?php echo $user->weightUnit=='Lbs'?$user->weight:'X' ?></td>
            <?php foreach($item as $measureName=>$value): ?>
                <?php if(strpos($measureName, 'Mass') !== false): ?>
                    <td><?php echo $user->weightUnit=='Kg'?$value:'X' ?></td>
                    <td><?php echo $user->weightUnit=='Lbs'?$value:'X' ?></td>
                <?php else: ?>
                    <td><?php echo $value ?></td>
                <?php endif; ?>


            <?php endforeach; ?>
            <td id="da-<?php echo $key ?>">&gt;</td>
            <td id="dd-<?php echo $key ?>">X</td>
        </tr>
        <?php endforeach; ?>
    </table>
    <br /><br />
    <!--Tabla de diferencias-->
    <table  class="visible-lg visible-md visible-sm hidden-xs" id="desktop-diff">
        <tr rowspan="2">
            <th rowspan="2" class="plus-symbol">+</th>
            <th rowspan="2"><span>Age</span></th>
            <th colspan="2">Height</th>
            <th colspan="2">Weight</th>
            <th rowspan="2"><span>Chin</span></th>
            <th rowspan="2"><span>Cheek</span></th>
            <th rowspan="2"><span>Pec</span></th>
            <th rowspan="2"><span>Biceps</span></th>
            <th rowspan="2"><span>Triceps</span></th>
            <th rowspan="2"><span>S.Scap</span></th>
            <th rowspan="2"><span>Midax</span></th>
            <th rowspan="2"><span>Supra</span></th>
            <th rowspan="2"><span>Austr</span></th>
            <th rowspan="2"><span>Umbil</span></th>
            <th rowspan="2"><span>Knee</span></th>
            <th rowspan="2"><span>Calves</span></th>
            <th rowspan="2"><span>Quadric</span></th>
            <th rowspan="2"><span>Hamst</span></th>
            <th rowspan="2"><span>Sum</span></th>
        </tr>
        <tr>
            <th>cm</th>
            <th>inches</th>
            <th>Kg</th>
            <th>Lbs</th>
        </tr>
        <?php foreach ($mobileDiff as $key=>$item): ?>
            <tr>
                <th><?php echo  date('d/m/Y',strtotime($key)) ?></th>
                <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$key); ?></td>
                <td><?php echo $user->heightUnit=='Cm'?$user->height:'X' ?></td>
                <td><?php echo $user->heightUnit=='In'?$user->height:'X' ?></td>
                <td><?php echo $user->weightUnit=='Kg'?$user->weight:'X' ?></td>
                <td><?php echo $user->weightUnit=='Lbs'?$user->weight:'X' ?></td>
                <td><?php echo implode('</td><td>',array_slice($item, 0,count($item)-3)) ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <br /><br />
    <!--Tabla de Porcentaje-->
    <table  class="visible-lg visible-md visible-sm hidden-xs" id="desktop-percent">
        <tr rowspan="2">
            <th rowspan="2" class="plus-symbol">+</th>
            <th rowspan="2"><span>Age</span></th>
            <th colspan="2">Height</th>
            <th colspan="2">Weight</th>
            <th rowspan="2"><span>Chin</span></th>
            <th rowspan="2"><span>Cheek</span></th>
            <th rowspan="2"><span>Pec</span></th>
            <th rowspan="2"><span>Biceps</span></th>
            <th rowspan="2"><span>Triceps</span></th>
            <th rowspan="2"><span>S.Scap</span></th>
            <th rowspan="2"><span>Midax</span></th>
            <th rowspan="2"><span>Supra</span></th>
            <th rowspan="2"><span>Austr</span></th>
            <th rowspan="2"><span>Umbil</span></th>
            <th rowspan="2"><span>Knee</span></th>
            <th rowspan="2"><span>Calves</span></th>
            <th rowspan="2"><span>Quadric</span></th>
            <th rowspan="2"><span>Hamst</span></th>
            <th rowspan="2"><span>Sum</span></th>
        </tr>
        <tr>
            <th>cm</th>
            <th>inches</th>
            <th>Kg</th>
            <th>Lbs</th>
        </tr>
        <?php foreach ($mobilePercent as $key=>$item): ?>
            <tr>
                <th><?php echo  date('d/m/Y',strtotime($key)) ?></th>
                <td><?php echo Utilities::userAgeSpecificDate($user->bornDate,$key); ?></td>
                <td><?php echo $user->heightUnit=='Cm'?$user->height:'X' ?></td>
                <td><?php echo $user->heightUnit=='In'?$user->height:'X' ?></td>
                <td><?php echo $user->weightUnit=='Kg'?$user->weight:'X' ?></td>
                <td><?php echo $user->weightUnit=='Lbs'?$user->weight:'X' ?></td>
                <td><?php echo implode('</td><td>',array_slice($item, 0,count($item)-3)) ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>