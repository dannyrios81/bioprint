<h1>Prueba de la grilla</h1>
<br>
<?= CHtml::beginForm() ?>
<div class="row">
    <div class="col-md-4 col-xs-4"><?= CHtml::label('fecha', 'fecha') ?></div><div class="col-md-2 col-xs-8"><?= CHtml::textField('fecha',date('Y-m-d'),['class'=>'form-control','style'=>'padding: 0px;'])  ?></div>
    <div class="hidden-xs hidden-sm col-md-6 text-right"><?= CHtml::link('Nueva Ejecucion','#',['class'=>'btn btn-primary']) ?></div>
</div>
<div class="row">
    <div class="col-md-4 col-xs-4"><?= CHtml::label('peso', 'peso') ?></div><div class="col-md-2 col-xs-8"><?= CHtml::textField('peso','0',['class'=>'form-control','style'=>'padding: 0px;'])  ?></div>
    <div class="col-md-4 col-xs-4"><?= CHtml::label('unidad Peso', 'unipeso') ?></div><div class="col-md-2 col-xs-8 text-center"><?= CHtml::radioButtonList('unipeso','Kg',['Kg'=>'Kg','Lbs'=>'Lbs'],['container'=>'','separator'=>''])  ?></div>
</div>
<div class="row hidden-md hidden-lg">
    <div class="col-xs-12"><?= CHtml::link('Nueva Ejecucion','#',['class'=>'btn btn-primary btn-block']) ?></div>
</div>
<?= CHtml::endForm()?>