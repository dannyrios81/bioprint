<?php
/* @var $userPhaseExercise UserPhaseExercise */
/* @var $userPhase UsersPhase */

//CVarDumper::dump($userPhaseExercise->relations(),10,true);exit;
?>
<table style="width: 100%;margin: 0px;padding: 0px">
    <tr>
        <td style="width: 40%;">
            <table style="width: 100%">
                <tr>
                    <td style="width: 10%;background-color: #31b0d5"><?= $userPhaseExercise->order ?></td>
                    <td style="width: 10%;background-color: #b2dba1"><?= $userPhaseExercise->idRangeMotion0->description ?></td>
                    <td style="width: 80%;background-color: #BCE774;font-size: 9px"><?= $userPhaseExercise->idExercise0->description ?></td>
                </tr>
            </table>
        </td>
        <td style="width: 60%;font-size: 9px">
            Description : <?= $userPhaseExercise->idExercise0->notes ?>
        </td>
    </tr>
</table>
<?php for($i=1;$i<=$userPhase->numberOfTimes;$i++): ?>
<table style="width: 100%;margin: 0px;padding: 0px">
    <tr>
        <td style="width: 40%">
            <table style="width: 100%">
                <tr>
                    <td style="width: 6%;background-color: #edde34" ><?= $i ?></td>
                    <td style="width: 24%;background-color: #761c19"><?= $userPhaseExercise->repeatInterval ?></td>
                    <td style="width: 6%;background-color: #7a43b6"><?= $userPhaseExercise->numberSeries ?></td>
                    <td style="width: 44%;background-color: #00b3ee"><?= $userPhaseExercise->tempo ?></td>
                    <td style="width: 10%;background-color: #03F614"><?= $userPhaseExercise->rest ?> s</td>
                </tr>
            </table>
        </td>
        <td style="width: 60%">
            <table style="width: 100%">
                <tr>
                    <?php for($j=0;$j<12;$j++): ?>
                        <td style="width: 8%;text-align: center">
                        <?php if($j<$userPhaseExercise->numberSeries):?>
                            <table id="exerciseTable" style="width: 100%;">
                                <tr><td style="width: 50%">&nbsp;</td><td style="width: 50%">&nbsp;</td></tr>
                            </table>
                        <?php endif;?>
                    </td>
                    <?php endfor; ?>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php endfor; ?>
<hr style="border:1px solid #808080;">
