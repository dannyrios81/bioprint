<h1>Enter Measurement Set</h1>
<div class="col-sm-12 main-content">
    <?php echo CHtml::beginForm('','post',['class'=>'form-horizontal']); ?>
    <?php echo CHtml::errorSummary($model); ?>
    <?php foreach($measures as $i=>$item): ?>
        <?php //CVarDumper::dump($item->MeasurementType->idAttachment0->path,10,true);exit; ?>
    <div class="form-group <?php echo !empty(CHtml::error($item,"[$i]val"))?'has-error':''?>">
        <?php echo CHtml::activeLabel($item,"[$i]val",array('class'=>"control-label col-sm-2 col-xs-4",'style'=>'text-align: left;')); ?>
        <div class="col-sm-9 col-xs-6 ">
            <?php echo CHtml::activeNumberField($item,"[$i]val",array('class'=>"form-control ","step"=>"0.01")); ?>
        </div>
        <div class="col-sm-1 col-xs-2">
<!--            <button type="button" class="btn" data-toggle="modal" data-target="#--><?php //echo $item->getAttributeLabel('val') ?><!--"><i class="fa fa-arrow-circle-up fa-2x"></i></button>-->
            <i class="fa fa-arrow-circle-up fa-2x" style="cursor: pointer" data-toggle="modal" data-target="#<?php echo $item->getAttributeLabel('val') ?>"></i>
        </div>
    </div>
    <div class="modal fade" id="<?php echo $item->getAttributeLabel('val') ?>" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php echo $item->getAttributeLabel('val') ?></h4>
                    </div>
                    <div class="modal-body">
                        <img src="<?php echo  Yii::app()->baseUrl.$item->MeasurementType->idAttachment0->path ?>" style="margin: 0 auto">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
    </div>
    <?php endforeach; ?>
    <?php echo CHtml::submitButton('Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
    <?php echo CHtml::endForm(); ?>
</div><!-- form -->