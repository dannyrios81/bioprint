<?php
/**
 *
 * @var PruebaController $this
 */
$scriptColapse =<<<JS
    $(document).on('click','.title-link',function(event){
        event.preventDefault();
        var divName = '#'+$(this).data('toggle');
        // $('.slideContent').slideUp('fast');
        $(divName).find('.slideContent').slideToggle();
        if($(this).find('.icon-change>i').hasClass("fa-plus-square"))
            $(this).find('.icon-change>i').removeClass("fa-plus-square").addClass("fa-minus-square");
        else
            $(this).find('.icon-change>i').removeClass("fa-minus-square").addClass("fa-plus-square");
    });
    $(document).on('click','.btn-save',function(event){
        event.preventDefault();
        // alert("hola Mundo");
        var url=$(this).prop('href');
        var form = $(this).parents("form");
        console.log($(this).parents("form"),url);
        $.ajax(
            {
                url: url, 
                dataType:'json',
                data:$(form).serialize(),
                success: function(result){
                    console.log(result);
                }
            });
        // console.log(url);
        
    });
JS;
Yii::app()->clientScript->registerScript('scriptCollapse',$scriptColapse,CClientScript::POS_READY);

?>
<?php $this->renderPartial('_formExecution') ?>
<?php for($j=0;$j<12;$j++): ?>
<?php $this->renderPartial('_exercices',['j'=>$j]) ?>
<?php endfor; ?>