<?php
/* @var $practitioner Users */
/* @var $user Users */
/* @var $userPhase UsersPhase */

?>
<table style="width: 100%;height: 10px">
    <tr>
        <td style="width: 16%"><img src="<?php echo dirname(Yii::app()->basePath) ?>/img/streng.png" alt="logo"></td>
        <td style="width: 16%;text-align: center;vertical-align: middle"><img src="<?php echo dirname(Yii::app()->basePath) ?><?php echo (!empty($practitioner->idAttachment))?$practitioner->idAttachment0->path:'/img/defaultProfileImg.jpg' ?>" alt="profile-image" style="height: 110px"></td>
        <td style="width: 18%">
            <table style="width: 100%">
                <tr>
                    <td><?= $practitioner->name . ' ' . $practitioner->lastName ?></td>
                </tr>
                <tr>
                    <td>strength club</td>
                </tr>
                <tr>
                    <td>{direccion (no DB)}</td>
                </tr>
                <tr>
                    <td>{ciudad, post code (no DB)}</td>
                </tr>
                <tr>
                    <td>{pais (no DB)}</td>
                </tr>
                <tr>
                    <td><?= $practitioner->email ?></td>
                </tr>
            </table>
        </td>
        <td style="width: 50%;">
            <table style="width: 100%;" id="phaseData">
                <tr>
                    <td colspan="2" style="width: 50%"><?= $user->name ?></td>
                    <td colspan="2" style="width: 50%"><?= $user->lastName ?></td>
                </tr>
                <tr>
                    <td colspan="2" style="width: 50%"><?= $userPhase->Fase ?></td>
                    <td colspan="2" style="width: 50%"><?= $userPhase->BodyPart ?></td>
                </tr>
                <tr>
                    <td style="width: 25%"><?= $userPhase->numberOfTimes ?></td>
                    <td style="width: 25%"><?= $userPhase->date ?></td>
                    <td style="width: 25%"><?= $userPhase->numberPhase ?></td>
                    <td style="width: 25%"></td>
                </tr>
                <tr>
                    <td>TUT</td>
                    <td colspan="3" style="text-align: center"><?= Tut::CalcTutTotalPdf($userPhase->id) ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>