<?php
$this->Widget('vendor.miloschuman.yii-highcharts.highcharts.HighchartsWidget', array(
    'options' => array(
        //'chart'=>['type'=>'pie'],
        'title' => array('text' => 'Gender Distribution'),
        'plotOptions' => array(
            'pie' => array(
                'allowPointSelect' => true,
                'cursor' => 'pointer',
                'dataLabels' => array(
                    'enabled' => true,
                    'format'=>'<b>{point.name}</b>: {point.percentage:.1f} %',
                    'style'=>array('color'=> "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'")
                )
            )
        ),
        
        'gradient' => array('enabled'=> true),
        'credits' => array('enabled' => false),
        'exporting' => array('enabled' => false),
        'chart' => array(
            'plotBackgroundColor' => '#ffffff',
            'plotBorderWidth' => null,
            'plotShadow' => false,
        ),
        'tooltip' => array(
            // 'pointFormat' => '{series.name}: <b>{point.percentage}%</b>',
            // 'percentageDecimals' => 1,
            'formatter'=> 'js:function() { return this.point.name+":  <b>"+Math.round(this.point.percentage)+"</b>%"; }',
            //the reason it didnt work before was because you need to use javascript functions to round and refrence the JSON as this.<array>.<index> ~jeffrey
        ),
        'series' => array(
            array(
                'type' => 'pie',
                'name' => 'Percentage',
                'data' => array(
                    // array('Ready / Deployable', 15),
                    array('Male', 4),
                    array('Female', 1)
                ),
            ),
        )
    )
));