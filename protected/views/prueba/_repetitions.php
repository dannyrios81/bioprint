<?php
/**
 *
 * @var integer $j
 */
?>
<div class="row">
    <div class="col-md-1">
        <?= CHtml::label('W','campo01',['class'=>"col-md-1 col-xs-4 text-center col-sm-4"]) ?>
        <?= CHtml::label('R','campo02',['class'=>"col-md-1 col-xs-4 text-center col-sm-4"]) ?>
        <?= CHtml::label('MW','campo03',['class'=>"col-md-1 col-xs-4 text-center col-sm-4"]) ?>
    </div>
    <div class="col-md-11">
        <?php for($i=0;$i<12;$i++): ?>
            <div class="col-md-1 text-center" style="padding-left: 0px;padding-right: 0px">
                <?= CHtml::textField('campo01[]','1',['class'=>'col-md-12 col-xs-4 textfieldChange','style'=>'padding: 0px;background-color:transparent']) ?>
                <?= CHtml::textField('campo02[]','2',['class'=>'col-md-12 col-xs-4 textfieldChange','style'=>'padding: 0px;background-color:transparent']) ?>
                <?= CHtml::textField('campo03[]','3',['class'=>'col-md-12 col-xs-4 textfieldChange','style'=>'padding: 0px;background-color:transparent']) ?>
            </div>
        <?php endfor; ?>
    </div>
</div>
