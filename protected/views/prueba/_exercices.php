<?php
/**
 *
 * @var PruebaController $this
 */
?>
<div class="exercice" id="exercice<?= $j?>">
    <div class="row">
        <h2>
            <div class="col-xs-12">
                <a class="title-link" href="#" data-toggle="exercice<?= $j?>"><span class="icon-change"><i class="fa fa-plus-square" aria-hidden="true"></i></span> Ejercicio <?=$j+1 ?></a>
                <!--<a href="https://www.youtube.com/watch?v=H9es_ERUuVg" target="_blank"><i class="fa fa-youtube-play" style="font-size: 20px"></i></a>-->
            </div>
        </h2>
    </div>
    <div class="slideContent" style="display: none;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;padding: 10px;">
        <?= CHtml::beginForm() ?>
        <div class="row">
            <div class="col-sm-12">
                <?php $this->renderPartial('_information',['j'=>$j]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php $this->renderPartial('_calculus',['j'=>$j]); ?>
            </div>
        </div>
        <?php $this->renderPartial('_repetitions',['j'=>$j]); ?>
        <div class="text-right">
            <a class="btn btn-primary btn-save btn-lg btn-block" style="margin-top: 20px;" href="<?php echo Yii::app()->createAbsoluteUrl('prueba/update')?>">Save</a>
        </div>
        <?= CHtml::endForm()?>
        <?php $this->renderPartial('_historic',['j'=>$j]); ?>
    </div>
</div>