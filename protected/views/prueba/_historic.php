<?php
/**
 *
 * @var integer $j
 */
$i =0;
?>
<div class="row" style="margin-top: 10px">
    <ul class="nav nav-tabs">
        <?php if($i==0): ?>
            <li class="active"><a data-toggle="tab" href="#<?= 'hola' ?>">2017-08-28</a></li>
        <?php else:?>
            <li><a data-toggle="tab" href="#<?= 'hola' ?>">2017-08-25</a></li>
        <?php endif; ?>
    </ul>
    <div class="tab-content" style="margin-top: 10px">
        <?php if($i==0): ?>
        <div id="<?= 'hola' ?>" class="tab-pane fade in active">
        <?php else:?>
        <div id="<?= 'hola' ?>" class="tab-pane fade">
        <?php endif; ?>
            <div class="col-md-7"  style="font-size: 10px">
                <?php $this->renderPartial('_historicDetails'); ?>
            </div>
            <div class="col-md-5" style="font-size: 10px">
                <?php $this->renderPartial('_historicCalcs'); ?>
            </div>
        </div>
    </div>
</div>
