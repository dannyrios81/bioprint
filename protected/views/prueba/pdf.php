<?php
/* @var $userPhaseExercises UserPhaseExercise */
/* @var $userPhase UsersPhase */
/* @var $practitioner Users */
/* @var $user Users */
/* @var $maxSets integer */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        #phaseData, #phaseData th, #phaseData td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        #exerciseTable, #exerciseTable th, #exerciseTable td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
        }
        body, table, th, td {
            font-size: 10px;
            margin: 0px;
            border-collapse:collapse;
            padding-top: 0px;
            padding-right: 1px;
            padding-bottom: 0px;
            padding-left: 0px;
        }

    </style>
</head>
<body style="background-color: #b4b900">
<!-- cabecera principal del documento -->

<?php $this->renderPartial('pdfheader', array('practitioner'=>$practitioner,'user'=>$user,'userPhase'=>$userPhase)); ?>

<hr style="border:1px solid #808080">
<?php $this->renderPartial('pdfexerciseheader', array('maxSets'=>$maxSets)); ?>
<hr style="border:1px solid #808080;margin: 0px;padding: 0px">
<?php foreach($userPhaseExercises as $userPhaseExercise): ?>
<?php $this->renderPartial('pdfexercisedetails', array('userPhaseExercise'=>$userPhaseExercise,'userPhase'=>$userPhase)); ?>
<?php endforeach; ?>
</body>
</html>