<?php
/* @var $maxSets integer */
?>
<table style="width: 100%;margin: 0px">
    <tr>
        <td style="width: 40%;">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 10%;background-color: #31b0d5">Order</td>
                    <td style="width: 10%;background-color: #b2dba1">Range</td>
                    <td style="width: 80%;background-color: #BCE774">Exercise</td>
                </tr>
            </table>
        </td>
        <td style="width: 60%;">
            &nbsp;
        </td>
    </tr>
</table>
<table style="width: 100%;margin: 0px">
    <tr>
        <td style="width: 40%;">
            <table style="width: 100%">
                <tr>
                    <td style="width: 6%;background-color: #edde34" >Week</td>
                    <td style="width: 24%;background-color: #761c19">Reps</td>
                    <td style="width: 6%;background-color: #7a43b6">Sets</td>
                    <td style="width: 44%;background-color: #00b3ee">Tempo</td>
                    <td style="width: 10%;background-color: #03F614">Rest(segs)</td>
                </tr>
            </table>
        </td>
        <td style="width: 60%;">
            <table style="width: 100%;">
                <tr>
                    <?php for($k=0;$k<12;$k++): ?>
                        <td style="width: 8%;text-align: center">
                            <?php if($k<$maxSets): ?>
                            <table style="width: 100%;">
                                <tr><td colspan="2" style="width: 100%">S<?= $k+1 ?></td></tr>
                                <tr><td style="width: 50%">W</td><td style="width: 50%">R</td></tr>
                            </table>
                            <?php else: ?>
                                <table style="width: 100%;">
                                    <tr><td colspan="2" style="width: 100%">&nbsp;</td></tr>
                                    <tr><td style="width: 50%">&nbsp;</td><td style="width: 50%">&nbsp;</td></tr>
                                </table>
                            <?php endif; ?>
                        </td>
                    <?php endfor; ?>
                </tr>
            </table>
        </td>
    </tr>
</table>