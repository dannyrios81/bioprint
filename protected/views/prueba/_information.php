<?php
/**
 *
 * @var integer $j
 */
?>
<table class="table table-condensed table-responsive table-bordered" style="margin-bottom: 10px">
        <tr>
            <td colspan="7"><h3 class="text-center" style="margin: 0px;padding: 0px">Exercise Information</h3></td>
        </tr>
        <tr>
            <th style="padding: 2px;text-align: center;vertical-align: middle">orden</th>
            <th style="padding: 2px;text-align: center;vertical-align: middle">rango movimiento</th>
            <th style="padding: 2px;text-align: center;vertical-align: middle">Intervalo De Repeticion</th>
            <th style="padding: 2px;text-align: center;vertical-align: middle">Numero De series</th>
            <th style="padding: 2px;text-align: center;vertical-align: middle">Tempo</th>
            <th style="padding: 2px;text-align: center;vertical-align: middle">Rest</th>
            <th style="padding: 2px;text-align: center;vertical-align: middle">Unidades</th>
        </tr>
        <tr>
            <td style="padding: 2px;text-align: center;vertical-align: middle">0</td>
            <td style="padding: 2px;text-align: center;vertical-align: middle">0</td>
            <td style="padding: 2px;text-align: center;vertical-align: middle">0</td>
            <td style="padding: 2px;text-align: center;vertical-align: middle">0</td>
            <td style="padding: 2px;text-align: center;vertical-align: middle">0</td>
            <td style="padding: 2px;text-align: center;vertical-align: middle">0</td>
            <td style="padding: 2px;text-align: center;vertical-align: middle"><?= CHtml::radioButtonList('unipeso'.$j,'Kg',['Kg'=>'Kg','Lbs'=>'Lbs'],['container'=>'','separator'=>''])  ?></td>
        </tr>
        <tr>
            <th style="padding: 2px;text-align: center;vertical-align: middle"><?= CHtml::label('Notas', 'notas') ?></th>
            <td colspan="6" style="padding: 2px;text-align: center;vertical-align: middle"><?= CHtml::textArea('notas','0',['class'=>'form-control','style'=>'padding: 0px;'])  ?></td>
        </tr>
</table>
