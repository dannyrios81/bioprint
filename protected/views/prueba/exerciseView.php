<?php
/**
 *
 * @var FaseController $this
 * @var \UserPhaseExercise $model
 * @var \identificador $idUserPhase
 */

$baseUrl = Yii::app()->getBaseUrl(true);

$scriptDialog =<<<JS
$(document).on('click','#btnajaxCloneExercises',function(event) {
    event.preventDefault();
    var info=[];
    $.each($( ".checkboxValid:checked" ),function(index,value){
        info.push($(value).val());
    });
    $.ajax({
        url: $(this).attr('href'),
        data: {ids:info},
        type: 'POST',
        dataType:'json',
        success: function(response){
            if(response.valid>0)
                alert(response.message);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        },
        complete: function() {
            $('#user-phase-exercise-grid').yiiGridView('update');
        }
     });
});
$(document).on('click','#btnajaxModalPhase,.xEditFaseExercise',function(event){     
    event.preventDefault();
     $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            dataType:'json',
            success: function(response){
                console.log(response);
                $("#ajaxModalPhase .modal-body").empty().html(response.html);
		        $("#ajaxModalPhase").modal();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location.href='$baseUrl';
                }
            }
        });
});
JS;

Yii::app()->clientScript->registerCssFile('https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css');
Yii::app()->clientScript->registerScriptFile('https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js', CClientScript::POS_END);

Yii::app()->clientScript->registerScript('scriptDialog',$scriptDialog,CClientScript::POS_READY);
?>
<h1>Exercises by Phase</h1>
<hr class="separator">
<div class="text-right">
    <?= CHtml::link('<i class="fa fa-clone"></i> Clone Exercises Selected',['fase/cloneExercise','id'=>$idUserPhase],['class'=>"btn btn-primary",'id'=>"btnajaxCloneExercises"]); ?>
    <?php if(Yii::app()->user->getState('userType')=='Practitioner1'):?>
        <?= CHtml::link('<i class="fa fa-plus-circle"></i> New Exercise',['fase/newAddExercise','id'=>$idUserPhase],['class'=>"btn btn-primary",'id'=>"btnajaxModalPhase"]); ?>
    <?php else: ?>
        <?= CHtml::link('<i class="fa fa-plus-circle"></i> New Exercise',['fase/addExercise','id'=>$idUserPhase],['class'=>"btn btn-primary",'id'=>"btnajaxModalPhase"]); ?>
    <?php endif; ?>

</div>
<br>
<div class="col-sm-12">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'user-phase-exercise-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'columns'=>array(
//            idExercise0
            [
                'header'=>'Order',
                'name'=>'order',
                'value'=>'!empty($data->order)?$data->order:""',
                'filter'=>CHtml::activeTextField($model,'order',['class'=>"form-control"])
            ],
            [
                'header'=>'Exercise Name',
                'name'=>'idExercise0.description',
                'value'=>'!empty($data->idExercise)?$data->idExercise0->description:""',
//                'value'=>'Yii::app()->dateFormatter->format("yyyy-MM-dd",$data->date)',
                'filter'=>CHtml::activeTextField($model,'exerciseDescription',['class'=>"form-control"])
            ],
            [
                'header'=>'Range',
//                'name'=>'idExercise0.idMethod0.description',
                'name'=>'idExercise0.idRangeMotion0.description',
                'value'=>'!empty($data->idRangeMotion0)?$data->idExercise0->idRangeMotion0->description:""',
//                'value'=>'Yii::app()->dateFormatter->format("yyyy-MM-dd",$data->date)',
                'filter'=>CHtml::activeTextField($model,'methodDescription',['class'=>"form-control"])
            ],
            [
                'header'=>'Sets',
                'name'=>'numberSeries',
                'value'=>'$data->numberSeries',
                'filter'=>CHtml::activeTextField($model,'numberSeries',['class'=>"form-control"])
            ],
            [
                'header'=>'Rep Int',
                'value'=>'$data->repeatInterval',
                'filter'=>CHtml::activeTextField($model,'repeatInterval',['class'=>"form-control"])
            ],
            [
                'header'=>'Tempo',
                'value'=>'$data->tempo',
                'filter'=>CHtml::activeTextField($model,'tempo',['class'=>"form-control"])
            ],
            [
                'header'=>'Rest(secs)',
                'value'=>'$data->rest',
                'filter'=>CHtml::activeTextField($model,'rest',['class'=>"form-control"])
            ],
            [
                'header'=>'TUT',
                'value'=>'$data->tut',
                'filter'=>CHtml::activeTextField($model,'tut',['class'=>"form-control"]),
                'footer'=>Tut::CalcTutTotalFooter($idUserPhase)
            ],
            /*[
                'header'=>'TUT Total',
                'value'=>'$data->tutTotal',
                'filter'=>CHtml::activeTextField($model,'tutTotal',['class'=>"form-control"]),
                'footer'=>Tut::CalcTutTotalFooter($idUserPhase)
            ],*/
            [
                'header'=> 'Clone',
                'headerTemplate'=>'<div style="white-space: nowrap">Clone {item}</div>',
                'class' => 'CCheckBoxColumn',
                'value' => '$data->id',
                'selectableRows' => 20,
                'checkBoxHtmlOptions'=>['class'=>'checkboxValid']
            ],
            array
            (
                'class'=>'CButtonColumn',
                'template'=>'<div style="white-space: nowrap">{xEditFaseExercise}{xDeleteFaseExercise}</div>',
                'header'=>"  Actions  ",
                'buttons'=>array(
                    'xEditFaseExercise'=>array(
                        'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>(Yii::app()->user->getState('userType')=='Practitioner1')?
                            'Yii::app()->createUrl("fase/newEditFaseExercise", array("id"=>$data->id))':
                            'Yii::app()->createUrl("fase/editFaseExercise", array("id"=>$data->id))',
                        'options'=>array('title'=>'Edit Exercise','class'=>'xEditFaseExercise')
                    ),
                    'xDeleteFaseExercise'=>array(
                        'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                        'url'=>'Yii::app()->createUrl("fase/DeleteExercise", array("id"=>$data->id))',
                        'options'=>array('title'=>'Delete Exercise'),
                        'click'=>"function() {
							if(!confirm('Are you sure you want to delete this item?')) return false;
							var th = this,afterDelete = function(){};
							jQuery('#user-phase-exercise-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
									jQuery('#user-phase-exercise-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
						}"
                    ),
                ),
            ),
        ),
    )); ?>
</div>
<div class="text-right">
    <?= CHtml::link('<i class="fa fa-arrow-circle-left"></i> Back',['fase/index','id'=>$userId],['class'=>"btn btn-primary"]); ?>
</div>
<div class="modal fade" id="ajaxModalPhase" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="preloader-alfa"></div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Exercise in Phase</h2>
            </div>
            <div class="modal-body" style="text-align: justify;">
                XXX XXXXXXXXXXXXXXX XXXXXXXXXX XXXXX
                <a class="btn btn-primary" id="btnajaxModalNewFase"><i class="fa fa-plus-circle"></i> New Phase</a>
            </div>

        </div>
    </div>
</div>
