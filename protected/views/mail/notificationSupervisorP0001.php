<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es" style="background:url(<?php //echo Yii::app()->createAbsoluteUrl('/'); ?>/img/bgMail.jpg) repeat-x top #004889">
<head>
    <title>S&M Software</title>
    <style type="text/css">
        #outlook a {padding:0;}
        body{font-family:Sans-serif;width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
        .ExternalClass {width:100%;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
        .ExternalClass * {line-height: 100%}

        img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
        a img {border:none;}
        .image_fix {display:block; overflow:hidden;}

        p {margin: 1em 0;}
        h1, h2, h4, h5, h6 {color: #004889 !important;}
        h3{color: #6F6F6F!important;}
        h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
        h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
            color: red !important;
        }
        h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
            color: purple !important;
        }
        table td {border-collapse: collapse;}
        table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
        a {color: #004889; font-size: 14px; font-weight: bold; padding: 7px!important; }

        .tableContent{
            display:block; overflow:hidden;
            width:600px;
        }

        /*Main Table*/
        #backgroundTable {margin:0; padding:0; width:100% !important; }

        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {

            /*Text color for telephone and address*/
            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: #ffffff; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }
            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: #ffffff !important;
                pointer-events: auto;
                cursor: default;
            }
        }
        @media only screen and (max-width: 768px){



            table[id="maintable"] .tableContent{
                max-width:600px !important;
                width:100% !important;
            }

            table[id="maintable"]{
                max-width:600px !important;
                width:100% !important;
            }

            table[id="footer"]{
                width:100% !important;
            }
            table[id="footer"] .col1{
                width:6.11% !important;
            }
            table[id="footer"] .col2{
                width:93.88% !important;
            }
            .sideCol{
                width: 9.375% !important;
            }
            /*.middleCol{
                width: 81.25% !important;
            }*/

        }
        @media only screen and (max-device-width: 640px) {
            table[id="maintable"] .text001 {
                font-size:10px !important;
                line-height:10px !important;
            }
            table[id="maintable"] .text002 {
                font-size:8px !important;
            }
            table[id="maintable"] .text003 {
                font-size:10px !important;
                line-height:12px !important;
            }
            table[id="maintable"] .text004 {
                font-size:10px !important;
                line-height:12px !important;
            }

            table[id="maintable"] .text004 br { display:none;}
        }
        @media only screen and (max-device-width: 480px) {

            table[id="maintable"] .tableContent{
                max-width:600px !important;
                width:100% !important;
            }

            table[id="maintable"]{
                max-width:600px !important;
                width:100% !important;
            }

            /*Text color for telephone and address*/
            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: #ffffff; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }
            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: #ffffff !important; /* or whatever your want */
                pointer-events: auto;
                cursor: default;
            }
        }
    </style>
</head>
<body >
<!--table shell -->
<table cellpadding="0" cellspacing="0" border="0"  id="backgroundTable" style="background:url(<?php //echo Yii::app()->createAbsoluteUrl('/'); ?>/img/bgMail.jpg) repeat-x top #004889">
    <tr>
        <td align="center" valign="top">
            <table width="600" border="0" align="center"  cellpadding="0" cellspacing="0" style="margin:0 auto" id="maintable">
                <tr style="border:2px solid #004889">
                    <td>
                        <!-- Table Content -->
                        <table width="100%" border="0" align="center" cellpadding="0"  cellspacing="0" style="margin:0 auto;">
                            <tr>
                                <td align="center" valign="top" >
                                    <img src="http://strengthandmetabolic.com/img/logo.png" alt="S&M Software" width="65%" />
                                </td>
                            </tr>
                            <tr>
                                <td  valign="top" bgcolor="#FFFFFF" >
                                    <table width="90%" border="0" align="center" cellpadding="0"  cellspacing="0" style="margin:0 auto;">
                                        <tr>
                                            <td>
                                                <h1>S&M Software - Your S&M Software will expire in 7 days</h1>
                                                <br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <p>
                                                    We have switched to a subscription payment to make it easier for you to keep your access going. If you have already made payment via the new subscription then you do not need to make payment again, it will come out automatically. If you have not, then please click the correct link below to make your first subscription payment. If you are unsure if you are on the subscription plan please email us at info@strength-community.com to find out.
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><p>
                                                    <?= CHtml::link($link,$link) ?>
                                                </p></td>
                                        </tr>
					                    <tr>
                                            <td colspan="3"><p>Do not reply to this e-mail. Please contact us at <a href="mailto:info@strength-community.com">info@strength-community.com</a></p></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><p>Thank you, </p></td>
                                            <br>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><p>S&M Software</p></td>
                                            <br>
                                        </tr>
                                    </table>
                                    <!-- END.Table Content -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
</body>
</html>
