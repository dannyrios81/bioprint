<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
		<title><?php echo CHtml::encode(!empty($this->pageTitle)?$this->pageTitle:Yii::app()->name); ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.DIRECTORY_SEPARATOR ?>css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.DIRECTORY_SEPARATOR ?>css/custom.css">
		<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.DIRECTORY_SEPARATOR ?>css/bioprint.css">
		<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.DIRECTORY_SEPARATOR ?>css/cortina.css">
		<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl.DIRECTORY_SEPARATOR ?>css/jquery-editable-select.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
	<?php $this->renderPartial('//layouts/menu') ?>
	<div class="main-container">
		<div class="container">
			<div class="row">
				<div class="white-border-radius container">
					<?php echo $content; ?>
				</div>	<!--End white-border-radius-->
			</div>
		</div>
	</div>
    <footer class="col-xs-12 ">
        <div class="container">
            <a class="col-xs-6 text-left" href="https://members.strength-community.com/terms-conditions/">Privacy Policy</a>
            <a class="col-xs-6 text-right" href="http://www.iguazoft.com">Copyright Iguazoft</a>
        </div>
    </footer>
	<script src="<?php echo Yii::app()->baseUrl.DIRECTORY_SEPARATOR ?>js/bootstrap-datepicker.min.js"></script>
	<script src="<?php echo Yii::app()->baseUrl.DIRECTORY_SEPARATOR ?>js/scripts.js"></script>
	<script src="<?php echo Yii::app()->baseUrl.DIRECTORY_SEPARATOR ?>js/bootstrap.js"></script>
	<script src="<?php echo Yii::app()->baseUrl.DIRECTORY_SEPARATOR ?>js/jquery-editable-select.js"></script>
</body>
</html>
