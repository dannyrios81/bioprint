<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo CHtml::encode(Yii::app()->name); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/css/custom.css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<header>
	<div class="header-logo">
		<img src="<?php echo Yii::app()->baseUrl ?>/img/logo.png" alt="Strenght Sensei" class="img-responsive">
	</div>
	<nav></nav>
</header>

<div class="main-container login">
	<div class="container">
		<div class="row">
			<div class="white-border-radius container">
				<?php echo $content; ?>
			</div>
		</div>
	</div>
</div>

<footer class="col-xs-12 ">
    <div class="container">
        <a class="col-xs-6 text-left" href="https://members.strength-community.com/terms-conditions/">Privacy Policy</a>
        <a class="col-xs-6 text-right" href="http://www.iguazoft.com">Copyright Iguazoft</a>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

</body>
</html>