<?php
$user = Yii::app()->user->getState('_user');
//$user = new Users();
//CVarDumper::dump($user,10,true);exit;
?>
<header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <!-- El logotipo y el icono que despliega el menú se agrupan
        para mostrarlos mejor en los dispositivos móviles -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target=".navbar-ex1-collapse">
                <span class="sr-only">Desplegar navegación</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Agrupar los enlaces de navegación, los formularios y cualquier
        otro elemento que se pueda ocultar al minimizar la barra -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li class="hidden-xs"><img src="<?php echo Yii::app()->baseUrl.DIRECTORY_SEPARATOR ?>img/logo-Mobile.png"></li>
                <?php if(@$user->userType == 'Admin'): ?>
                    <li <?php Utilities::ActiveClass('/course/')?>><a href="<?php echo Yii::app()->createUrl('course/index') ?>">Seminars</a></li>
                    <li <?php Utilities::ActiveClass('/product/')?>><a href="<?php echo Yii::app()->createUrl('product/index') ?>">Products</a></li>
                    <li <?php Utilities::ActiveClass(['/users/','/measureUser/'])?>><a href="<?php echo Yii::app()->createUrl('users/index') ?>">Clients</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">MA Parameters <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li <?php Utilities::ActiveClass('/supplement/')?>><a href="<?php echo Yii::app()->createUrl('supplement/index') ?>">Supplements</a></li>
                            <li <?php Utilities::ActiveClass(['/measurementTypes/','/idealMeasurement/'])?>><a href="<?php echo Yii::app()->createUrl('measurementTypes/index') ?>">Sites</a></li>
                            <li <?php Utilities::ActiveClass('/hormoneFamily/')?>><a href="<?php echo Yii::app()->createUrl('hormoneFamily/index') ?>">Hormone Families</a></li>
                            <li <?php Utilities::ActiveClass('/moments/')?>><a href="<?php echo Yii::app()->createUrl('moments/index') ?>">Intakes</a></li>
                            <li <?php Utilities::ActiveClass('/params/')?>><a href="<?php echo Yii::app()->createUrl('params/index') ?>">Params</a></li>
                            <li <?php Utilities::ActiveClass('/recipes/')?>><a href="<?php echo Yii::app()->createUrl('recipes/index') ?>">Protocols</a></li>
                            <li <?php Utilities::ActiveClass('/country/')?>><a href="<?php echo Yii::app()->createUrl('country/index') ?>">Zones</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">PD Parameters <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li <?php Utilities::ActiveClass('/bodyArea/')?>><a href="<?php echo Yii::app()->createUrl('bodyArea/index') ?>">Body Areas</a></li>
                            <li <?php Utilities::ActiveClass('/bodyPart/')?>><a href="<?php echo Yii::app()->createUrl('BodyPart/index') ?>">Body Parts</a></li>
                            <li <?php Utilities::ActiveClass('/exercise/')?>><a href="<?php echo Yii::app()->createUrl('exercise/index') ?>">Exercises</a></li>
                            <li <?php Utilities::ActiveClass('/familyExercise/')?>><a href="<?php echo Yii::app()->createUrl('familyExercise/index') ?>">Exercise Families</a></li>
                            <li <?php Utilities::ActiveClass('/grip/')?>><a href="<?php echo Yii::app()->createUrl('grip/index') ?>">Grip/Stances</a></li>
                            <li <?php Utilities::ActiveClass('/implement/')?>><a href="<?php echo Yii::app()->createUrl('implement/index') ?>">Implements</a></li>
                            <li <?php Utilities::ActiveClass('/method/')?>><a href="<?php echo Yii::app()->createUrl('method/index') ?>">Strength Qualities</a></li>
                            <li <?php Utilities::ActiveClass('/numberSeries/')?>><a href="<?php echo Yii::app()->createUrl('NumberSeries/index') ?>">Sets</a></li>
                            <li <?php Utilities::ActiveClass('/orientation/')?>><a href="<?php echo Yii::app()->createUrl('orientation/index') ?>">Orientations</a></li>
                            <li <?php Utilities::ActiveClass('/rangeMotion/')?>><a href="<?php echo Yii::app()->createUrl('rangeMotion/index') ?>">Ranges</a></li>
                            <li <?php Utilities::ActiveClass('/repeatInterval/')?>><a href="<?php echo Yii::app()->createUrl('repeatInterval/index') ?>">Rep Intervals</a></li>
                            <li <?php Utilities::ActiveClass('/phase/')?>><a href="<?php echo Yii::app()->createUrl('phase/index') ?>">Phases</a></li>
                            <li <?php Utilities::ActiveClass('/rest/')?>><a href="<?php echo Yii::app()->createUrl('rest/index') ?>">Rest Intervals</a></li>
                        </ul>
                    </li>
                    <li <?php Utilities::ActiveClass('/reports/')?>><a href="<?php echo Yii::app()->createUrl('reports/index') ?>">Reports</a></li>
                    <li <?php Utilities::ActiveClass('/migrate/')?>><a href="<?php echo Yii::app()->createUrl('migrate/index') ?>">Migrate</a></li>
                    <li <?php Utilities::ActiveClass('/logUsers/')?>><a href="<?php echo Yii::app()->createUrl('logUsers/index') ?>">Log</a></li>
<!--                    <li><a href="https://www.facebook.com/groups/bioprint/" target="_blank">Community</a></li>-->
                <?php elseif(@$user->userType == 'Trainer' or @$user->userType == 'Practitioner1'): ?>
                    <li <?php Utilities::ActiveClass(['/users/','/measureUser/'])?>><a href="<?php echo Yii::app()->createUrl('users/index') ?>">Clients</a></li>
                    <li class="dropdown">
                    <li <?php Utilities::ActiveClass('/reports/')?>><a href="<?php echo Yii::app()->createUrl('reports/index') ?>">Reports</a></li>
<!--                    <li><a href="--><?php //echo Yii::app()->baseUrl."/manuals/Manual_Practitioner.pdf" ?><!--">Manual</a></li>-->
<!--                    <li><a href="https://www.facebook.com/groups/bioprint/" target="_blank">Community</a></li>-->
                <?php elseif(@$user->userType == 'User'): ?>
                    <li <?php Utilities::ActiveClass(['/users/','/measureUser/'])?>><a href="<?php echo Yii::app()->createUrl('users/index') ?>">Clients</a></li>
                    <li <?php Utilities::ActiveClass('/reports/')?>><a href="<?php echo Yii::app()->createUrl('reports/index') ?>">Reports</a></li>
                    <li <?php Utilities::ActiveClass(['/Fase/','/execution/'])?>><a href="<?php echo Yii::app()->createUrl('Fase/index',['id'=>$user->id]) ?>">Workouts</a></li>
<!--                    <li><a href="--><?php //echo Yii::app()->baseUrl."/manuals/Manual_Client.pdf" ?><!--">Manual</a></li>-->
                <?php endif; ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo Yii::app()->createUrl('site/logout') ?>">Logout</a></li>
            </ul>
        </div>
    </nav>
</header>
