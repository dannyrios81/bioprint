<?php
/* @var $this PhaseController */
/* @var $model Fase */
?>

<h1>Update Phase <?php echo $model->description; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>