<?php
/**
 *
 * @var PhaseController $this
 * @var \Fase $model
 */
$baseUrl = Yii::app()->getBaseUrl(true);
$loadUrl = Yii::app()->createAbsoluteUrl('fase/ListFase');
$script=<<<JS
function submitNewFaseForm(){  
    //debugger;
    event.preventDefault();
    $.ajax({
            url: $('#newFase-form').prop('action'),
            type: 'POST',
            data: $('#newFase-form').serialize(),
            dataType:'json',
            success: function(response){
                console.log(response);
                // debugger;
                if(response.id<=0)
                    {
                        $("#ajaxModalNewFase .modal-body").empty().html(response.html);
                        $("#ajaxModalNewFase .modal-footer").css('visibility','hidden');
                    }
                    else
                    {
                        $('#containerSelectIdFase').load('$loadUrl',{id:response.id});
                        //$('#selectIdBodyPart').val(response.id);
                        $("#ajaxModalNewFase .close").click();
                    }
                
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if(XMLHttpRequest.status == 403)
                {
                    window.location.href='$baseUrl';
                }
            }
        });
	
};
JS;
Yii::app()->clientScript->registerScript('Script',$script, CClientScript::POS_END);
?>
<div class="form">
    <?= CHtml::beginForm('','post',['id'=>'newFase-form','onSubmit'=>'submitNewFaseForm()']) ?>
    <div class="col-xs-12">
        <p class="note">Fields with <span class="required">*</span> are required.</p>
    </div>

    <?= CHtml::errorSummary($model,null,null, ['class'=>'form-group col-md-12']); ?>

    <div class="form-group col-md-12 <?= !empty(CHtml::error($model,'description'))?'has-error':''?>">
        <?= CHtml::activeLabelEx($model,'description',array('class'=>"control-label")); ?>
        <?= CHtml::activeTextField($model,'description',array('class'=>"form-control",'maxlength'=>255,"autocomplete"=>"off")); ?>
    </div>

    <div class=" buttons">
        <div class="col-xs-12">
            <?= CHtml::submitButton('Create',['class' => 'btn btn-success btn-lg btn-block']); ?>
        </div>
    </div>

    <?= CHtml::endForm() ?>
</div>