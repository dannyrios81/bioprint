<?php
/* @var $this PhaseController */
/* @var $model Fase */

$this->breadcrumbs=array(
	'Fases'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Fase', 'url'=>array('index')),
	array('label'=>'Create Fase', 'url'=>array('create')),
	array('label'=>'Update Fase', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Fase', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Fase', 'url'=>array('admin')),
);
?>

<h1>View Fase #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
	),
)); ?>
