<?php
/* @var $this PhaseController */
/* @var $model Fase */
?>

<h1>Phases</h1>
<div class="col-sm-12 form-container">
    <div class="text-right" style="margin-bottom: 10px">
        <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('phase/create')?>"><i class="fa fa-plus-circle"></i> New Phase</a>
    </div>
</div>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fase-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'description',
        array(
            'class'=>'CButtonColumn',
            'template'=>'{xEdit}{xDelete}',
            'header'=>"  Actions  ",
            'buttons'=>[
                'xEdit'=>array(
                    'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                    'url'=>'Yii::app()->createUrl("phase/update", array("id"=>$data->id))',
                    'options'=>array('title'=>'Edit','class'=>'xEdit')
                ),
                'xDelete'=>array(
                    'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                    'url'=>'Yii::app()->createUrl("phase/delete", array("id"=>$data->id))',
                    'options'=>array('title'=>'Delete'),
                    'click'=>"function() {
							if(!confirm('Are you sure you want to delete this item?')) return false;
							var th = this,afterDelete = function(){};
							jQuery('#fase-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
									jQuery('#fase-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
						}"
                ),
            ]
        ),
	),
)); ?>
</div>