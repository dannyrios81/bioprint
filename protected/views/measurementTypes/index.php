<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#measurement-types-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Sites</h1>
<hr class="separator">
<div class="col-sm-12 form-container">
	<div class="text-right">
		<a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('measurementTypes/create')?>"><i class="fa fa-plus-circle"></i> New Sites</a>
	</div>
</div>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'measurement-types-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'name',
		['header'=>'Calculable','name'=>'calculable','value'=>'(empty($data->calculable)?"No":"Yes")','filter'=>CHtml::activeDropDownList($model,'calculable',['0'=>'No','1'=>'Yes'],['empty'=>'Select ...'])],
//		['header'=>'Gender','name'=>'gender','value'=>'$data->gender','filter'=>CHtml::activeDropDownList($model,'gender',['Female'=>'Female','Male'=>'Male'],['empty'=>'Select ...'])],
//		'idalMeasurement',
		'youtubeId',
		array(
			'class'=>'CButtonColumn',
			'template'=>'<div>{xupdate}{xdelete}{xidealMeasurement}</div>',
			'header'=>"  Actions  ",
			'buttons'=>array(
				'xupdate'=>array(
					'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
					'url'=>'Yii::app()->createUrl("measurementTypes/update", array("id"=>$data->id))',
					'options'=>array('title'=>'Update measurement-types')
				),
				'xidealMeasurement'=>array(
					'label'=>'<i class="fa fa-sliders fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-sliders visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>',
					'url'=>'Yii::app()->createUrl("idealMeasurement/index", array("id"=>$data->id))',
					'options'=>array('title'=>'Ideal Measurement Config'),
					'visible'=>'empty($data->calculable)?true:false'
				),
				'xdelete'=>array(
					'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
					'url'=>'Yii::app()->createUrl("measurementTypes/delete", array("id"=>$data->id))',
					'options'=>array('title'=>'Delete measurement-types'),
					'visible'=>'$data->is_deletable()',
					'click'=>"function() {
						if(!confirm('Are you sure you want to delete this item?')) return false;
						var th = this,afterDelete = function(){};
							jQuery('#measurement-types-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
									jQuery('#measurement-types-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
					}"
				),
			),
		)
	),
)); ?>
</div>