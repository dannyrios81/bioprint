<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
?>
<div class="col-sm-12 main-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'measurement-types-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>['enctype'=>'multipart/form-data',]
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'name'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'name',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'name',array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'idAttachment'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'idAttachment',array('class'=>"control-label")); ?>
		<?php echo $form->fileField($model,'idAttachment',array('class'=>"form-control filestyle")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'calculable'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'calculable',array('class'=>"control-label")); ?>
		<?php echo $form->dropDownList($model,'calculable',array('0'=>'No','1'=>'Yes'),array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'youtubeId'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'youtubeId',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'youtubeId',array('class'=>"form-control")); ?>
	</div>
	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->