<h1>View MeasurementTypes #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'idAttachment',
		'youtubeId',
	),
)); ?>
