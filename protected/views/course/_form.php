<?php
$idInput = CHtml::activeId($model,'startDate');
$js = <<<JS
	$('#$idInput').attr('value',Date.parse($('#$idInput').attr('value')).toString('yyyy-MM-dd'))
JS;

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/Datejs/build/date.js',CClientScript::POS_END);
Yii::app()->clientScript->registerScript('scriptDate',$js,CClientScript::POS_READY);


?>
<div class="col-sm-12 main-content">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'course-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'name'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'name',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'name',array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'startDate'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'startDate',array('class'=>"control-label")); ?>
		<?php echo $form->dateField($model,'startDate',array('size'=>60,'maxlength'=>255,'class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'location'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'location',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'location',array('size'=>60,'maxlength'=>255,'class'=>"form-control")); ?>
	</div>
	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'courseIdentification'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'courseIdentification',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'courseIdentification',array('size'=>60,'maxlength'=>255,'class'=>"form-control")); ?>
	</div>
    <div class="form-group col-md-12 <?php echo !empty($form->error($model,'courseIdentification'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'courseHasProductsCheckBoxSelect',array('class'=>"control-label")); ?>
<!--        --><?php //echo $form->checkBoxList($model,'courseHasProducts',array('size'=>60,'maxlength'=>255,'class'=>"form-control")); ?>

        <?php echo $form->checkBoxList($model,'courseHasProductsCheckBoxSelect',CHtml::listData(Product::model()->findAll(),'id','name'),['class'=>'row','template'=>'<div class="checkbox">{input} {label}</div>','separator'=>'']); ?>
    </div>
	<div class="row form-actions">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->