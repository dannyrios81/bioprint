<table width="90%" border="0" align="center" cellpadding="0"  cellspacing="0" style="margin:0 auto;">
    <tr>
        <td>
            <h1>Metabolic Balance - Protocol</h1>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <p>Follow these guidelines</p>
<!--            inicio de un ciclo for para sacar todos los momentos -->
            <?php $tempIdMoment = 0?>
            <?php foreach ($recipe->chooses as $choose):?>
                <?php //$choose=new Choose();?>

                <?php if($tempIdMoment!=$choose->idMoment):?>
                    <p style="font-size: 14px;font-weight: bold">
                        <?php echo $choose->idMoment0->name;?>
                    </p>
                <?php endif;?>
                <p>
                    <?php $orcount = count($choose->chooseSuplements)-1; ?>
                    <?php if(($orcount+1)>1):?>
                    1 choice between: <?php echo !empty($choose->note)?'('.$choose->note.')':'';?>
                    <?php endif; ?>
                    <ul>
                        <?php //$orcount = count($choose->chooseSuplements)-1; ?>
                        <?php foreach ($choose->chooseSuplements as $suplement):?>
                            <li><?php echo $suplement->idSuplement0->name .', '. $suplement->quantity.($orcount>0?'; or':'') ?></li>
                            <?php $orcount--; ?>
                        <?php endforeach;?>
                    </ul>
                </p>
                <?php $tempIdMoment=$choose->idMoment;?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <td colspan="3"><p><?php echo !empty($recipe->notes)?'<p><b>Note</b></p><p>'.htmlentities(nl2br($recipe->notes)).'</p>':'' ?></p></td>
    </tr>
    <tr>
        <td colspan="3"><?php echo !empty($model->description)?'<p><b>Note</b></p><p>'.$model->description.'</p>':'' ?></td>
    </tr>

</table>
<!-- END.Table Content -->
