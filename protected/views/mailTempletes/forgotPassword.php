<table width="90%" border="0" align="center" cellpadding="0"  cellspacing="0" style="margin:0 auto;">
    <tr>
        <td>
            <h1>Password Recovery</h1>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <p>Please click on the link below It will reset your forgotten password and let you create a new one.</p>
        </td>
    </tr>
    <tr>
        <td colspan="3"><p><a style="font-size:12px!important;font-weight: normal!important;" href="<?php echo $url ?>">Click Here</a></p></td>
    </tr>
    <tr>
        <td colspan="2"><p>Use the following new password&nbsp;:&nbsp;&nbsp;<strong><?php echo $clave ?></strong></p></td>
        <br>
    </tr>
</table>
<!-- END.Table Content -->
