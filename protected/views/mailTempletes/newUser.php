<table width="90%" border="0" align="center" cellpadding="0"  cellspacing="0" style="margin:0 auto;">
    <tr>
        <td>
            <br>
            <h1>New Account</h1>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <p><a style="font-size:12px!important;font-weight: normal!important;" href="<?php echo $url ?>">Click Here to access</a></p>
        </td>
    </tr>
    <tr style="margin-bottom: 10px"><td colspan="2">Use the following User&nbsp;:&nbsp;&nbsp;<strong><?php echo $email ?></strong></td></tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr style="margin-bottom: 10px"><td colspan="2">Use the following Password&nbsp;:&nbsp;&nbsp;<strong><?php echo $password ?></strong></td></tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr style="margin-bottom: 10px"><td colspan="2">You <strong>MUST</strong> update your profile information.</td></tr>
    <tr style="margin-bottom: 10px"><td colspan="2"><p>Do not reply to this e-mail. Please contact us at <a href="mailto:info@strength-community.com"></a>info@strength-community.com</p></td></tr>
</table>
<!-- END.Table Content -->