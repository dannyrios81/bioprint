<table width="90%" border="0" align="center" cellpadding="0"  cellspacing="0" style="margin:0 auto;">
    <tr>
        <td>
            <br>
            <h1>S&M Software - Course Not Found</h1>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <p>Welcome to S&M Software</p>
            <br>
            <p>this message been automatically submitted</p>
            <br>
            <br>
            <p>Please notice that practitioner <?php echo $user ?> paid for the seminar with code <?php echo $courseCode ?> - <?php echo $course ?>, and this seminar is not created on the Software. In this moment, practitioner <?php echo $user ?> does not have access to the Software.</p>
            <p>Please proceed to create seminar with code <?php echo $courseCode ?> - <?php echo $course ?> in the Software, and associate it to the practitioner, keeping in mind the starting date of the seminar.</p>
            <p>Thank you,</p>
            <p>S&M Software</p>
        </td>
    </tr>
</table>
<!-- END.Table Content -->