<?php
/* @var $this NumberSeriesController */
/* @var $model NumberSeries */

$this->breadcrumbs=array(
	'Number Series'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List NumberSeries', 'url'=>array('index')),
	array('label'=>'Create NumberSeries', 'url'=>array('create')),
	array('label'=>'Update NumberSeries', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete NumberSeries', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage NumberSeries', 'url'=>array('admin')),
);
?>

<h1>View NumberSeries #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
		'methodId',
	),
)); ?>
