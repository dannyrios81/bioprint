<?php
/* @var $this FamilyExerciseController */
/* @var $model FamilyExercise */

$this->breadcrumbs=array(
	'Family Exercises'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FamilyExercise', 'url'=>array('index')),
	array('label'=>'Create FamilyExercise', 'url'=>array('create')),
	array('label'=>'Update FamilyExercise', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FamilyExercise', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FamilyExercise', 'url'=>array('admin')),
);
?>

<h1>View FamilyExercise #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
		'bodyAreaId',
	),
)); ?>
