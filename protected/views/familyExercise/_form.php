<?php
/* @var $this FamilyExerciseController */
/* @var $model FamilyExercise */
/* @var $form CActiveForm */
/********************************************************* Ids Selects ****************************************/
$idMethodElement= '#'.CHtml::activeId($model,'idMethod');
$idBodyAreaElement = '#'.CHtml::activeId($model,'bodyAreaId');

/********************************************************* URLS ****************************************/
$baseUrl = Yii::app()->getBaseUrl(true);
$urlDropsBodyArea = $this->createAbsoluteUrl('exercise/dropsBodyArea');

/********************************************************* Carga de los Dropdowns ****************************************/
$jsLoadSelects=<<<JS
function clearSelect(level) {
  switch(level) {
    case 1:
        $('$idBodyAreaElement').val('');
        $('$idBodyAreaElement').html('<option value="">Select.......</option>');
   }
}

function populationBodyArea(){
    $.ajax({
        url: '{$urlDropsBodyArea}',
        data: {id:$('$idMethodElement').val()},
        type: 'POST',
        dataType:'json',
        success: function(response){
            // debugger;
            console.log(response);
            clearSelect($('$idMethodElement').data('level'));
            $('$idBodyAreaElement').val('');
           
            $('$idBodyAreaElement').html(response.BodyArea);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.status == 403)
            {
                window.location.href='$baseUrl';
            }
        }
     });
}

/*************************************************** Funciones para el evento change  ************************************/
$(document).on('change','$idMethodElement',populationBodyArea);

JS;
Yii::app()->clientScript->registerScript('jsLoadSelects',$jsLoadSelects, CClientScript::POS_READY);
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'family-exercise-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'description'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'description',array('class'=>"control-label")); ?>
        <?php echo $form->textField($model,'description',array('class'=>"form-control",'maxlength'=>255)); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'idMethod'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'idMethod',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($model,'idMethod',CHtml::listData(Method::model()->findAll(['order'=>'description']),'id','description'),array('data-level'=>'1','class'=>"form-control",'empty'=>'Select ...')); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'bodyAreaId'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'bodyAreaId',array('class'=>"control-label")); ?>
        <?php echo $form->dropDownList($model,'bodyAreaId',CHtml::listData(BodyArea::model()->findAll(['condition'=>'idMethod=:idMethod','order'=>'description','params'=>['idMethod'=>$model->idMethod]]),'id','description'),array('data-level'=>'2','class'=>"form-control",'empty'=>'Select ...')); ?>
    </div>

    <div class="row form-actions">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->