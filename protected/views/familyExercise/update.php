<?php
/* @var $this FamilyExerciseController */
/* @var $model FamilyExercise */

?>

<h1>Update Exercise Family <?php echo $model->description; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>