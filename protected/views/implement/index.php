<?php
/* @var $this ImplementController */
/* @var $model Implement */

?>

<h1>Implements</h1>
<div class="col-sm-12 form-container">
    <div class="text-right" style="margin-bottom: 10px">
        <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('implement/create')?>"><i class="fa fa-plus-circle"></i> New implement</a>
    </div>
</div>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'implement-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'columns'=>array(
        'description',
        [
            'header'=>'Strength Quality',
            'name'=>'idMethod',
            'value'=>'$data->idOrientation0->idFamilyExercise->bodyArea->idMethod0->description',
            'filter'=>CHtml::activeDropDownList($model,'idMethod',CHtml::listData(Method::model()->findAll(['order'=>'description']),'id','description'),['class'=>"form-control",'empty'=>'Select ...'])
        ],
        [
            'header'=>'Body Area',
            'name'=>'idBodyArea',
            'value'=>'$data->idOrientation0->idFamilyExercise->bodyArea->description',
            'filter'=>CHtml::activeDropDownList($model,'idBodyArea',CHtml::listData(BodyArea::model()->findAll(['order'=>'description']),'id','description'),['class'=>"form-control",'empty'=>'Select ...'])
        ],
        [
            'header'=>'Exercise Family',
            'name'=>'idFamilyExercise',
            'value'=>'$data->idOrientation0->idFamilyExercise->description',
            'filter'=>CHtml::activeDropDownList($model,'idFamilyExercise',CHtml::listData(FamilyExercise::model()->findAll(['order'=>'description']),'id','description'),['class'=>"form-control",'empty'=>'Select ...'])
        ],
        [
            'header'=>'Orientation',
            'name'=>'idOrientation',
            'value'=>'$data->idOrientation0->description ',
            'filter'=>CHtml::activeDropDownList($model,'idOrientation',CHtml::listData(Orientation::model()->findAll(['order'=>'description']),'id','description'),['class'=>"form-control",'empty'=>'Select ...'])
        ],
        array(
            'class'=>'CButtonColumn',
            'template'=>'{xEdit}{xDelete}',
            'header'=>"  Actions  ",
            'buttons'=>[
                'xEdit'=>array(
                    'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                    'url'=>'Yii::app()->createUrl("implement/update", array("id"=>$data->id))',
                    'options'=>array('title'=>'Edit','class'=>'xEdit')
                ),
                'xDelete'=>array(
                    'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                    'url'=>'Yii::app()->createUrl("implement/delete", array("id"=>$data->id))',
                    'options'=>array('title'=>'Delete'),
                    'click'=>"function() {
							if(!confirm('Are you sure you want to delete this item?')) return false;
							var th = this,afterDelete = function(){};
							jQuery('#implement-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
								    if(data.length>0)
								        alert(data);
									jQuery('#implement-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
						}"
                ),
            ]
        ),
    ),
)); ?>
</div>
