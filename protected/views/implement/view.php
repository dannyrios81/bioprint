<?php
/* @var $this ImplementController */
/* @var $model Implement */

$this->breadcrumbs=array(
	'Implements'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Implement', 'url'=>array('index')),
	array('label'=>'Create Implement', 'url'=>array('create')),
	array('label'=>'Update Implement', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Implement', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Implement', 'url'=>array('admin')),
);
?>

<h1>View Implement #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
		'familyExerciseId',
	),
)); ?>
