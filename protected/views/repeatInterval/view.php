<?php
/* @var $this RepeatIntervalController */
/* @var $model RepeatInterval */

$this->breadcrumbs=array(
	'Repeat Intervals'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List RepeatInterval', 'url'=>array('index')),
	array('label'=>'Create RepeatInterval', 'url'=>array('create')),
	array('label'=>'Update RepeatInterval', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete RepeatInterval', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage RepeatInterval', 'url'=>array('admin')),
);
?>

<h1>View RepeatInterval #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
		'methodId',
		'numberSeriesId',
	),
)); ?>
