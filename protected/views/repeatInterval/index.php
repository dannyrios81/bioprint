<?php
/* @var $this RepeatIntervalController */
/* @var $model RepeatInterval */
?>

<h1>Rep Interval</h1>
<div class="col-sm-12 form-container">
    <div class="text-right" style="margin-bottom: 10px">
        <a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('repeatInterval/create')?>"><i class="fa fa-plus-circle"></i> New Rep Interval</a>
    </div>
</div>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'repeat-interval-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'columns'=>array(
        'description',
        [
            'header'=>'Strength Quality',
            'name'=>'methodId',
            'value'=>'$data->method->description',
            'filter'=>CHtml::activeDropDownList($model,'methodId',CHtml::listData(Method::model()->findAll(['order'=>'description']),'id','description'),['class'=>"form-control",'empty'=>'Select ...'])
        ],
        array(
            'class'=>'CButtonColumn',
            'template'=>'{xEdit}{xDelete}',
            'header'=>"  Actions  ",
            'buttons'=>[
                'xEdit'=>array(
                    'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                    'url'=>'Yii::app()->createUrl("repeatInterval/update", array("id"=>$data->id))',
                    'options'=>array('title'=>'Edit','class'=>'xEdit')
                ),
                'xDelete'=>array(
                    'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block"></i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs"></i>&nbsp;&nbsp;',
                    'url'=>'Yii::app()->createUrl("repeatInterval/delete", array("id"=>$data->id))',
                    'options'=>array('title'=>'Delete'),
                    'click'=>"function() {
							if(!confirm('Are you sure you want to delete this item?')) return false;
							var th = this,afterDelete = function(){};
							jQuery('#repeat-interval-grid').yiiGridView('update', {
								type: 'POST',
								url: jQuery(this).attr('href'),
								success: function(data) {
								    if(data.length>0)
								        alert(data);
									jQuery('#repeat-interval-grid').yiiGridView('update');
								afterDelete(th, true, data);
								},
								error: function(XHR) {
									return afterDelete(th, false, XHR);
								}
							});
							return false;
						}"
                ),
            ]
        ),
    ),
)); ?>
</div>