<?php
/* @var $this RepeatIntervalController */
/* @var $data RepeatInterval */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('methodId')); ?>:</b>
	<?php echo CHtml::encode($data->methodId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numberSeriesId')); ?>:</b>
	<?php echo CHtml::encode($data->numberSeriesId); ?>
	<br />


</div>