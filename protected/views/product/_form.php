<div class="col-sm-12 main-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>
	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'name'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'name',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'name',array('class'=>"form-control")); ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'renewal'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'renewal',array('class'=>"control-label")); ?>
		<?php echo $form->dropDownList($model,'renewal',array('0'=>'Disabled','1'=>'Enabled'),array('class'=>"form-control")); ?>
	</div>
    <div class="form-group col-md-3 <?php echo !empty($form->error($model,'cost'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'cost',array('class'=>"control-label")); ?>
        <?php echo $form->textField($model,'cost',array('class'=>"form-control")); ?>
    </div>
	<div class="form-group col-md-3 <?php echo !empty($form->error($model,'productCode'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'productCode',array('class'=>"control-label")); ?>
        <?php if($model->isNewRecord ): ?>
		    <?php echo $form->textField($model,'productCode',array('class'=>"form-control")); ?>
        <?php else: ?>
		    <?php echo $form->textField($model,'productCode',array('class'=>"form-control","disabled"=>"disabled")); ?>
        <?php endif; ?>
	</div>

	<div class="form-group col-md-6 <?php echo !empty($form->error($model,'linkToUserManual'))?'has-error':''?>">
		<?php echo $form->labelEx($model,'linkToUserManual',array('class'=>"control-label")); ?>
		<?php echo $form->textField($model,'linkToUserManual',array('class'=>"form-control")); ?>
	</div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'urlPayOneWeek'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'urlPayOneWeek',array('class'=>"control-label")); ?>
        <?php echo $form->textField($model,'urlPayOneWeek',array('class'=>"form-control")); ?>
    </div>
    <div class="form-group col-md-6 <?php echo !empty($form->error($model,'urlPayTwoWeeks'))?'has-error':''?>">
        <?php echo $form->labelEx($model,'urlPayTwoWeeks',array('class'=>"control-label")); ?>
        <?php echo $form->textField($model,'urlPayTwoWeeks',array('class'=>"form-control")); ?>
    </div>

	<div class="row form-actions">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-success btn-lg btn-block')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->