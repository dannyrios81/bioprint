<?php $this->renderPartial('title'); ?>
<hr class="separator">
<div class="col-sm-12 form-container">
	<div class="text-right"  style="margin-bottom: 10px">
		<a class="btn btn-primary" href="<?php echo Yii::app()->createAbsoluteUrl('product/create')?>"><i class="fa fa-plus-circle"></i> New Product</a>
	</div>
</div>
<div class="col-sm-12 main-content">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-grid',
	'dataProvider'=>$model->search(),
    'ajaxUpdate'=>true,
	'filter'=>$model,
	'columns'=>array(
		'name',
		[
		    'header'=>'Renewal',
            'name'=>'renewal',
            'value'=>'(empty($data->renewal)?"No":"Yes")',
            'filter'=>CHtml::activeDropDownList($model,'renewal',['0'=>'No','1'=>'Yes'],['empty'=>'Select ...'])
        ],
        [
            'header'=>'Cost',
            'name'=>'cost',
            'value'=>'Yii::app()->NumberFormatter->formatCurrency( $data->cost,"USD");',
            'filterHtmlOptions'=>['class'=>'hidden-xs'],
            'headerHtmlOptions'=>['class'=>'hidden-xs'],
            'htmlOptions'=>['class'=>'hidden-xs']
        ],
		[
            'header'=>'Link to User Manual',
            'name'=>'linkToUserManual',
            'value'=>'$data->linkToUserManual',
            'filterHtmlOptions'=>['class'=>'hidden-xs hidden-sd'],
            'headerHtmlOptions'=>['class'=>'hidden-xs  hidden-sd'],
            'htmlOptions'=>['class'=>'hidden-xs  hidden-sd']
        ],
		array(
            'class'=>'CButtonColumn',
            'template'=>'{xupdate}{xdelete}',
            'header'=>"  Actions  ",
            'buttons'=>array(
                'xupdate'=>array(
					'label'=>'<i class="fa fa-arrow-circle-up fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								  <i class="fa fa-arrow-circle-up visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
                    'url'=>'Yii::app()->createUrl("product/update", array("id"=>$data->id))',
                    'options'=>array('title'=>'Update Product')
                ),
                'xdelete'=>array(
					'label'=>'<i class="fa fa-minus-circle fa-2x hidden-lg hidden-md visible-sm-inline-block visible-xs-inline-block">&nbsp;</i>
								  <i class="fa fa-minus-circle visible-lg-inline-block visible-md-inline-block hidden-sm hidden-xs">&nbsp;</i>',
                    'url'=>'Yii::app()->createUrl("product/delete", array("id"=>$data->id))',
                    'options'=>array('title'=>'Delete Product'),
                    'click'=>"function() {
								if(!confirm('Are you sure you want to delete this item?')) return false;
								var th = this,afterDelete = function(){};
								jQuery('#product-grid').yiiGridView('update', {
									type: 'POST',
									url: jQuery(this).attr('href'),
									success: function(data) {
                                        if(data.length>0)
                                            alert(data);
										jQuery('#product-grid').yiiGridView('update');
									afterDelete(th, true, data);
									},
									error: function(XHR) {
										return afterDelete(th, false, XHR);
									}
								});
								return false;
							}"
                ),
            ),
		),
	),
)); ?>
</div>