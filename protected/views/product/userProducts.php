<div class="container-fluid">
    <table>
        <?php foreach ($product as $prod): ?>
            <?php //CVarDumper::dump($product->idProduct0,10,true);exit; ?>
        <tr>
            <td><h2><?php echo $prod->name ?></h2></td>
        </tr>
        <tr>
            <td><a href="<?php echo $prod->linkToUserManual?>" target="_blank" class="descargaManual">Download User Manual</a></td>
        </tr>
        <?php endforeach; ?>
    </table>
    <br>
    <br>
    <table width="100%" class="table-responsive">
        <thead style="border-bottom-style: solid;border-bottom-color: darkgray">
        <tr style="text-align: center">
            <td width="50%">Product</td>
            <td width="25%">From</td>
            <td width="25%">Until</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($products as $product): ?>
        <tr>
            <td><strong><?php echo $product->idProduct0->name ?></strong></td>
            <td style="text-align: center"><?php echo Yii::app()->dateFormatter->format('yyyy-MM-dd',$product->activateDate) ?></td>
            <td style="text-align: center"><?php echo Yii::app()->dateFormatter->format('yyyy-MM-dd',$product->dueDate) ?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
