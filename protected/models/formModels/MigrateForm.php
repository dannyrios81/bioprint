<?php 
class MigrateForm extends CFormModel
{
    public $idAttachmentType = 3;
    public $filecsv;
    public $path = '/migrations/';
    public $dirname;
    public $realRoute;

    public function rules()
	{
		return array(
			array('path,idAttachmentType', 'required'),
            array('filecsv','file','types'=>'csv','allowEmpty'=>false,'maxSize'=>2000000),
            array('path','validateFolderDestination'),
		);
	}
    public function attributeLabels()
	{
		return array(
						'filecsv'=>'file',
						'path'=>'path',
						'dirname'=>'dirname',
						'idattachmentType'=>'idattachmentType',
					);
	}
	public function init()
    {
        parent::init();
    }
    public function __construct()
    {
        parent::__construct();
    }
    public function validateFolderDestination()
    {
        $dirName 	= 	pathinfo(Yii::app()->request->scriptFile);
        $this->dirname = $dirName['dirname'];

        if(!is_dir($this->dirname.$this->path))
        {
            mkdir($this->dirname.$this->path);
        }
    }
    public function save()
    {
        if($this->validate())
        {
            $filename = uniqid().'.'.CFileHelper::getExtension($this->filecsv->name);
            $this->realRoute = $this->dirname.$this->path.$filename;
            if($this->filecsv->saveAs($this->dirname.$this->path.$filename)){
                //$this->path = $this->path.$filename;

                $att = new Attachment();
                $att->idAttachmentType = $this->idAttachmentType;
                $att->path = $this->path.$filename;
                if($att->save(false))
                    return $att->id;
                else
                    return 0;
            }
        }

    }
}
