<?php 
class ValidatorForm extends CFormModel
{
	    public $email;
        public $number;
        
    public function rules()
	{
		return array(
			array('email,number', 'required'),
		);
	}
    public function attributeLabels()
	{
		return array(
						'email'=>'email',
						'number'=>'number',
					);
	}
	public function init()
    {
        parent::init();
    }
    public function __construct()
    {
        parent::__construct();
    }
	
}
