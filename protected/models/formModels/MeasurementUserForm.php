<?php

class MeasurementUserForm extends CFormModel
{
    public $MeasurementType;// object
    public $Measurement;//object
    public $name;
    public $val;
    public $date;
    public $idUser;
    public $migration;

    public function rules()
    {
        return array(
            array('val', 'required'),
            array('val', 'numerical','min'=>0,'max'=>250,'tooSmall'=>"The {$this->name} measure must be greater than zero",'tooBig'=>"The {$this->name} measure can not exceed 250"),

        );
    }
    public function attributeLabels()
    {
        return array(
            'val'=>$this->name,
        );
    }
    public function save()
    {
        $Measurement = new Measurement();
        $Measurement->date = $this->date;
        $Measurement->idUsers = $this->idUser;
        $Measurement->measurement = $this->val;
        $Measurement->idMeasurementType = $this->MeasurementType->id;
        $Measurement->migrate = $this->migration;

        $retorno =  $Measurement->save(false);
        //CVarDumper::dump($retorno,10,true);
        return $retorno;

    }

    public function edit()
    {
//        $this->Measurement = new Measurement();
        $this->Measurement->measurement = $this->val;

        return $this->Measurement->save(false);
    }
}