<?php 
class AccessPractitionerForm extends CFormModel
{
	    public $idUsers;
	    public $idProduct;
	    public $lastDatePurchase;
	    public $activateDate;
	    public $dueDate;
	    public $id;
		public $licence;

    public function rules()
	{
		return array(
			array('lastDatePurchase,activateDate, dueDate,idProduct', 'required'),
			array('activateDate','compare','operator'=>'<=','compareAttribute'=>'dueDate','message'=>'The {attribute} can not be greater than due date', ),
			array('idProduct','validateExist','on'=>'new'),
			array('idUsers, idProduct', 'numerical', 'integerOnly'=>true),
			array('activateDate, dueDate,licence,id', 'safe'),
		);
	}
    public function attributeLabels()
	{
		return array(
						'id'=>'id',
						'idUsers'=>'idUsers',
						'idProduct'=>'Product',
						'lastDatePurchase'=>'lastDatePurchase',
						'activateDate'=>'Activate Date',
						'dueDate'=>'Due Date',
						'licence'=>'License',
					);
	}
	public function init()
    {
        parent::init();
    }
    public function __construct($id=0)
    {
		$scenary='new';
		if(!empty($id))
		{
			$tmp = UserProduct::model()->findByPk($id);
			if(!empty($tmp))
			{
				$this->attributes = $tmp->attributes;
				$this->activateDate=Yii::app()->dateFormatter->format("yyyy-MM-dd",$this->activateDate);
				$this->dueDate=Yii::app()->dateFormatter->format("yyyy-MM-dd",$this->dueDate);
				$this->licence = true;
				$scenary='edit';
			}
		}
		else
		{
			$this->lastDatePurchase = date('Y-m-d H:i:s',time());

		}
        parent::__construct($scenary);
    }
	public function loadProducts()
	{
		return CHtml::listData(Product::model()->findAll(),'id','name');
	}

	public function save()
	{
		$trans = Yii::app()->db->beginTransaction();
		$return = false;
//		CVarDumper::dump($this->getScenario(),10,true);exit;
		try
		{
			if($this->getScenario()=='new')
			{
				$tmp = new UserProduct();
			}
			else
			{
				$tmp=UserProduct::model()->findByPk($this->id)	;
			}

			$tmp->attributes = $this->attributes;
			$return = $tmp->save();

			if($return and $this->licence == 1)
			{
				$lctmp = new Licences();
				$lctmp->purchaseDate = !empty($this->lastDatePurchase)?$this->lastDatePurchase:date('Y-m-d H:i:s',time());;
				$lctmp->idUsers = $this->idUsers;
				$lctmp->idProduct = $this->idProduct;
				$lctmp->renewal = 0;

				$return = $lctmp->save();
			}
			if($return)
			{
				$trans->commit();
			}
			else
			{
				$trans->rollback();
			}
			return $return;
		}
		catch (Exception $e)
		{
			$this->addError('id','An error occurs in the system, contact the administrator');
			$trans->rollback();
			return false;
		}
	}

	public function validateExist()
	{
		$temp = UserProduct::model()->findByAttributes(['idUsers'=>$this->idUsers,'idProduct'=>$this->idProduct]);
		if(!empty($temp))
		{
			$this->addError('idProduct','The practitioner already has the product assigned');
		}
	}
}
