<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ProtocoloForm extends CFormModel
{
    public $description;
    public $suplementsArray=[];
    public $arrayObjSupplements=[];

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // email has to be a valid email address
            array('description,suplementsArray', 'safe'),
            array('arrayObjSupplements','unsafe'),
            array('arrayObjSupplements','load'),

        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'description'=>'Description',
            'suplementsArray'=>'Add Standard Supplements',
        );
    }
    public function load()
    {
//        CVarDumper::dump($this->suplementsArray,10,true);exit;

        if(count($this->suplementsArray)>0 and is_array($this->suplementsArray))
        {
            foreach ($this->suplementsArray as $value) {
                $this->arrayObjSupplements[]=Supplement::model()->findByPk($value);
            }
        }
    }
    
}