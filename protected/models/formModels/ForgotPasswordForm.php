<?php 
class ForgotPasswordForm extends CFormModel
{
	public $email;
	public $pass;
	public $_users;
        
    public function rules()
	{
		return array(
			array('email', 'required'),
			array('email', 'email','allowEmpty'=>false),
			array('email', 'exist','allowEmpty'=>false,'attributeName'=>'email','className'=>'Users','message'=>'{value} account doesn\'t exist .'),
		);
	}
    public function attributeLabels()
	{
		return array(
						'email'=>'Email',
					);
	}
	public function init()
    {
        parent::init();
    }
    public function __construct()
    {
        parent::__construct();
    }
	public function afterValidate()
	{
		parent::afterValidate();
		if(!$this->hasErrors())
		{
			$this->_users=Users::model()->findByAttributes(['email'=>$this->email]);
//			$this->_users=new Users();
			$this->pass = Yii::app()->getSecurityManager()->generateRandomString(10);
			$this->_users->password=CPasswordHelper::hashPassword($this->pass);
			$this->_users->forgotPass=1;
			$this->_users->save(false);

			Yii::app()->user->setFlash('success', "S&M Software has sent you a message with a temporary password. Please check your email.");
		}

	}
}
