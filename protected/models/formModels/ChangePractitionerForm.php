<?php
/**
 * Created by PhpStorm.
 * User: iguazo
 * Date: 4/07/18
 * Time: 11:51 PM
 */

class ChangePractitionerForm extends CFormModel
{
    public $idPractitionerFrom;
    public $idPractitionerTo;

    public function rules()
    {
        return array(
            array('idPractitionerFrom,idPractitionerTo', 'exist','attributeName'=>'id','className'=>'Users'),
        );
    }
    public function attributeLabels()
    {
        return array(
            'idPractitionerFrom'=>'Practitioner From',
            'idPractitionerTo'=>'Practitioner To',
        );
    }
    public function practitionersList()
    {
        $criteria = new CDbCriteria();
        if(!empty($this->idPractitionerFrom))
        {
            $criteria->addCondition('id <> :idPractitionerFrom');
            $criteria->params = [':idPractitionerFrom'=>$this->idPractitionerFrom];
        }
        $criteria->addInCondition('userType',['Trainer','Practitioner1']);
        $criteria->order='trim(name),trim(lastName)';

        return CHtml::listData(Users::model()->findAll($criteria),'id',function($users) { return $users->name. ' ' . $users->lastName; });
    }
    public function save()
    {
        $practitionerTo = Users::model()->findByPk($this->idPractitionerTo);
        $practitionerFrom = Users::model()->findByPk($this->idPractitionerFrom);

        try
        {
            Users::model()->updateAll(['parentEmail'=>$practitionerTo->email],'parentEmail = :emailPractitionerFrom AND id <> :idPractitionerFrom',[':emailPractitionerFrom'=>$practitionerFrom->email,':idPractitionerFrom'=>$practitionerFrom->id]);
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }


    }

}