<?php 
class ChangePasswordForm extends CFormModel
{
	public $currentPassword;
    public $newPassword;
    public $repeatNewPassword;
	public $email;
	public $_user;
        
    public function rules()
	{
		return array(
			array('currentPassword,newPassword,repeatNewPassword', 'required'),
			array('repeatNewPassword', 'compare','strict'=>true,'compareAttribute'=>'newPassword'),
			array('newPassword', 'compare','strict'=>true,'compareAttribute'=>'currentPassword','operator'=>'!=','message'=>'The new password must be different to the current'),
			//array('newPassword','match','allowEmpty'=>'false','pattern'=>'/^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,20}$/','message'=>'El password nuevo debe tener por lo menos una letra mayuscula, un numero, un caracter especial y tener minimo 8 carcateres'),
			array('newPassword', 'length','min'=>8,'tooShort'=>'New password must have 8 characters at least'),

		);
	}
    public function attributeLabels()
	{
		return array(
						'currentPassword'=>'Current Password',
						'newPassword'=>'New Password',
						'repeatNewPassword'=>'Repeat New Password',
					);
	}
	public function init()
    {
        parent::init();
    }
    public function __construct()
    {
        parent::__construct();
    }
	public function afterValidate()
	{
		parent::afterValidate();
		if($this->hasErrors())
		{
			$this->currentPassword = '';
			$this->newPassword='';
			$this->repeatNewPassword='';

			return false;
		}
		//CVarDumper::dump($this,10,true);exit;
		$this->_user = Users::model()->findByAttributes(['email'=>$this->email]);

		if(count($this->_user)<=0)
		{
			$this->addError('currentPassword','Link invalid recovery');
			return false;
		}
		elseif(!CPasswordHelper::verifyPassword($this->currentPassword, $this->_user->password))
		{
			$this->addError('currentPassword','Check the current password');
			return false;
		}
		else
		{
			//$this->_user = new Users();
			$this->_user->password=CPasswordHelper::hashPassword($this->newPassword);
			$this->_user->forgotPass = 0;
			$this->_user->save(false);
			Yii::app()->user->setFlash('success', "Password change successful.");
		}

	}
	
}
