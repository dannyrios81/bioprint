<?php 
class ImageForm extends CFormModel
{
	public $image;
	public $path = '/img/imgUsers/';
	public $dirname;
	public $idAttachmentType = 1;
        
    public function rules()
	{
		return array(
				array('image','file','types'=>['jpeg','jpg','png'],'allowEmpty'=>true,'maxSize'=>6291456,'on'=>'Crear'),
				array('image','file','types'=>['jpeg','jpg','png'],'allowEmpty'=>true,'maxSize'=>6291456,'on'=>'Actualizar'),
				array('path','validateFolderDestination'),
		);
	}
    public function attributeLabels()
	{
		return array(
						'image'=>'image',
					);
	}
	public function validateFolderDestination()
	{
		$dirName 	= 	pathinfo(Yii::app()->request->scriptFile);
		$this->dirname = $dirName['dirname'];

		if(!is_dir($this->dirname.$this->path))
		{
			mkdir($this->dirname.$this->path);
		}
	}

	public function save()
	{
		$this->validate();
		$filename = uniqid().'.'.CFileHelper::getExtension($this->image->name);
		//$this->path = $this->path.$filename;
//		CVarDumper::dump($this->image,10,true);exit;

		if($this->image->saveAs($this->dirname.$this->path.$filename)){
			//$this->path = $this->path.$filename;

			$att = new Attachment();
			$att->idAttachmentType = $this->idAttachmentType;
			$att->path = $this->path.$filename;
			if($att->save(false))
				return $att->id;
			else
				return 0;
		}

	}
	
}
