<?php 
class UsersSeminarsForm extends CFormModel
{
	    public $idCourse;
        public $idUsers;
        
    public function rules()
	{
		return array(
			array('idCourse,idUsers', 'required'),
			array('idCourse', 'validaDuplicados'),
		);
	}
    public function attributeLabels()
	{
		return array(
						'idCourse'=>'Seminar ID',
						'idUsers'=>'idUsers',
					);
	}
	public function init()
    {
        parent::init();
    }
    public function __construct($id=0)
    {
		$this->idUsers = $id;
        parent::__construct();
    }

	public function loadCourses()
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition('startDate >= now()');

		return CHtml::listData(Course::model()->findAll($criteria),'id',function($course) { return $course->courseIdentification. ' - ' . $course->name; });
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		//$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('idUsers',$this->idUsers);

		return new CActiveDataProvider('UsersCourses', array(
			'criteria'=>$criteria,
		));
	}

	public function validaduplicados()
	{
		$criteria = new CDbCriteria();
		$criteria->addColumnCondition(['idCourse'=>$this->idCourse,'idUsers'=>$this->idUsers]);

		$count = UsersCourses::model()->count($criteria);

		if($count>0)
			$this->addError('idCourse','This client already enrolled in this course');
	}
	public function save()
	{
		try
		{
			$temp = new UsersCourses();
			$temp->idCourse = $this->idCourse;
			$temp->idUsers = $this->idUsers;

			return $temp->save();
		}
		catch (Exception $e)
		{
			$this->addError('idCourse','The system present a problem if it persists contact the administrator');
		}
	}
	
}
