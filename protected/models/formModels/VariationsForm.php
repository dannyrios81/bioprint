<?php
/**
 * Created by PhpStorm.
 * User: iguazo
 * Date: 27/08/18
 * Time: 07:43 PM
 */

class VariationsForm extends CFormModel
{
    public $variationweigth;
    public $variationreps;

    public function rules()
    {
        return [
            ['variationweigth', 'numerical', 'integerOnly'=>false,'max'=>'100','min'=>'-100','tooBig'=>'The {attribute} is to big','tooSmall'=>'The maximum negative variation is 100%'],
            ['variationreps', 'numerical', 'integerOnly'=>false,'max'=>'100','min'=>'-100','tooBig'=>'The {attribute} is to big','tooSmall'=>'The {attribute} is to small'],
            ['variationweigth, variationreps', 'safe'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'variationweigth' => 'Variation Weigth',
            'variationreps' => 'Variation Reps',
        ];
    }
}
