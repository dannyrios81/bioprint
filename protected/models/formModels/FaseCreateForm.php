<?php

class FaseCreateForm extends CFormModel
{
    public $id;
    public $date;
    public $BodyPart;
    public $numberPhase;
    public $Fase;
    public $numberOfTimes;
//    public $numberOfTimesDone;
    public $idUser;
    public $new;
    private $_userPhase;

    public function __construct($id=0)
    {
        if(is_numeric($id))
        {
            if(!empty($id))
            {
                $this->_userPhase = UsersPhase::model()->findByPk($id);
                $this->attributes = $this->_userPhase->attributes;
                $this->date=Yii::app()->dateFormatter->format("yyyy-MM-dd",$this->_userPhase->date);
                $scenario = 'update';
                $this->new = false;
            }
            else
            {
                $this->_userPhase = new UsersPhase();
                $this->new = true;
                $scenario = 'new';
            }
        }
        else
        {
            throw new CException('id User Phase not valid');
        }
        parent::__construct($scenario);
    }
    public function rules()
    {
        return array(
            array('idUser,date, numberPhase,numberOfTimes', 'required'),
//            array('idUser,date, numberPhase,numberOfTimesDone,numberOfTimes', 'required'),
            array('date', 'date', 'format'=>'yyyy-M-d'),
//            array('idUser,numberPhase,numberOfTimes', 'numerical','integerOnly'=>true),
//            array('numberOfTimes', 'numerical','integerOnly'=>true,'min'=>$this->_executions,'tooSmall'=>'{attribute} can not be less than the executions already entered','on'=>'update'),
            array('numberOfTimes','validateExecutions','on'=>'update'),
            array('Fase, BodyPart', 'length', 'max'=>255),
            array('Fase,idUser,date,BodyPart, numberPhase,numberOfTimes', 'safe'),
//            array('Fase,idUser,date,BodyPart, numberPhase,numberOfTimes,numberOfTimesDone', 'safe'),
        );
    }
    public function attributeLabels()
    {
        return array(
            'date'=>'Date',
            'BodyPart'=>'Body Part',
            'Fase'=>'Phase',
            'numberPhase'=>'Phase Number',
            'numberOfTimes'=>'Number Of Times',
//            'numberOfTimesDone'=>'Number Of Times Done',
        );
    }
    public static function bodyParts()
    {
        return CHtml::listData(BodyPart::model()->findAll(array('order'=>'description')),'id','description');
    }
    public static function fases()
    {
        return CHtml::listData(Fase::model()->findAll(array('order'=>'description')),'id','description');
    }
    public function validateExecutions($attribute,$params)
    {
        if($this->$attribute<count($this->_userPhase->executions))
        {
            $this->addError($attribute,'Number Of Times can not be less than the executions already entered');
        }
    }
    public function save()
    {
//        $this->_userPhase->attributes = $this->attributes;
//        $retorno =  $this->_userPhase->update();


        $this->_userPhase->date = $this->date ;
        $this->_userPhase->BodyPart = $this->BodyPart ;
        $this->_userPhase->numberPhase = $this->numberPhase ;
        $this->_userPhase->Fase = $this->Fase ;
        $this->_userPhase->numberOfTimes = $this->numberOfTimes ;
//        $this->_userPhase->numberOfTimesDone = $this->numberOfTimesDone ;
        $this->_userPhase->idUser = $this->idUser ;

        $retorno =  $this->_userPhase->save(false);
        $this->id = $this->_userPhase->id;
        //CVarDumper::dump($retorno,10,true);
        return $retorno;

    }
    public static function selectFase(FaseCreateForm $model)
    {
        $options=CHtml::listData(Fase::model()->findAll(['order'=>'description']),'description','description');
        $Fases = Fase::model()->findByAttributes(['description'=>$model->Fase]);

        if(empty($Fases))
        {
            $options[$model->Fase]=$model->Fase;
        }

        return $options;
    }
    public static function selectBodyPart(FaseCreateForm $model)
    {
        $options=CHtml::listData(BodyPart::model()->findAll(['order'=>'description']),'description','description');
        $BodyParts = BodyPart::model()->findByAttributes(['description'=>$model->BodyPart]);

        if(empty($BodyParts))
        {
            $options[$model->BodyPart]=$model->BodyPart;
        }

        return $options;
    }

}