<?php
class SeminarForm extends CFormModel
{
    public $id;
    public $name;
    public $startDate;
    public $location;
    public $courseIdentification;
    public $courseHasProducts; //array con ids de los productos

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('startDate, name, courseIdentification', 'required'),
            array('name, location, courseIdentification', 'length', 'max'=>255),
        );
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'startDate' => 'Start Date',
            'location' => 'Location',
            'courseIdentification' => 'Seminar ID',
            'courseHasProducts' => 'Products',
        );
    }
    public function save()
    {
        $Measurement = new Measurement();
        $Measurement->date = $this->date;
        $Measurement->idUsers = $this->idUser;
        $Measurement->measurement = $this->val;
        $Measurement->idMeasurementType = $this->MeasurementType->id;
        $Measurement->migrate = $this->migration;

        $retorno =  $Measurement->save(false);
        //CVarDumper::dump($retorno,10,true);
        return $retorno;

    }

    public function edit()
    {
//        $this->Measurement = new Measurement();
        $this->Measurement->measurement = $this->val;

        return $this->Measurement->save(false);
    }
}