<?php

/**
 * This is the model class for table "execution_exercise_calc".
 *
 * The followings are the available columns in table 'execution_exercise_calc':
 * @property integer $id
 * @property integer $idExecution
 * @property integer $idUserPhaseExercice
 * @property string $averageWeight
 * @property string $willks
 * @property string $averageRepetitions
 * @property string $relStrength
 * @property integer $totalSets
 * @property string $goal
 * @property string $totalTonnage
 * @property string $totalReps
 * @property string $averageLift
 * @property string $maximunLift
 * @property string $percent
 * @property string $delta
 * @property string $averageIntensity
 * @property integer $dropoff
 *
 * The followings are the available model relations:
 * @property ExecutionExercise[] $executionExercises
 * @property UserPhaseExercise $idUserPhaseExercice0
 * @property Execution $idExecution0
 */
class ExecutionExerciseCalc extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'execution_exercise_calc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idExecution, idUserPhaseExercice, totalSets, averageIntensity', 'numerical', 'integerOnly'=>true),
			array('averageWeight, willks, averageRepetitions, relStrength, goal, totalTonnage, totalReps, averageLift, maximunLift, percent', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idExecution, idUserPhaseExercice, averageWeight, willks, averageRepetitions, relStrength, totalSets, goal, totalTonnage, totalReps, averageLift, maximunLift, percent, delta, averageIntensity,dropoff', 'safe'),
			array('id, idExecution, idUserPhaseExercice, averageWeight, willks, averageRepetitions, relStrength, totalSets, goal, totalTonnage, totalReps, averageLift, maximunLift, percent, delta, averageIntensity,dropoff', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'executionExercises' => array(self::HAS_MANY, 'ExecutionExercise', 'idExecutionExerciseCalc'),
			'idUserPhaseExercice0' => array(self::BELONGS_TO, 'UserPhaseExercise', 'idUserPhaseExercice'),
			'idExecution0' => array(self::BELONGS_TO, 'Execution', 'idExecution'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idExecution' => 'Id Execution',
			'idUserPhaseExercice' => 'Id User Phase Exercice',
			'averageWeight' => 'Average Weight',
			'willks' => 'Willks',
			'averageRepetitions' => 'Average Repetitions',
			'relStrength' => 'Rel Strength',
			'totalSets' => 'Total Sets',
			'goal' => 'Goal',
			'totalTonnage' => 'Total Tonnage',
			'totalReps' => 'Total Reps',
			'averageLift' => 'Average Lift',
			'maximunLift' => 'Maximun Lift',
			'percent' => 'Percent',
            'delta' => 'Delta',
            'averageIntensity' => 'Average Intensity',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idExecution',$this->idExecution);
		$criteria->compare('idUserPhaseExercice',$this->idUserPhaseExercice);
		$criteria->compare('averageWeight',$this->averageWeight,true);
		$criteria->compare('willks',$this->willks,true);
		$criteria->compare('averageRepetitions',$this->averageRepetitions,true);
		$criteria->compare('relStrength',$this->relStrength,true);
		$criteria->compare('totalSets',$this->totalSets);
		$criteria->compare('goal',$this->goal,true);
		$criteria->compare('totalTonnage',$this->totalTonnage,true);
		$criteria->compare('totalReps',$this->totalReps,true);
		$criteria->compare('averageLift',$this->averageLift,true);
		$criteria->compare('maximunLift',$this->maximunLift,true);
		$criteria->compare('percent',$this->percent,true);
        $criteria->compare('delta',$this->delta,true);
        $criteria->compare('averageIntensity',$this->averageIntensity);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ExecutionExerciseCalc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
