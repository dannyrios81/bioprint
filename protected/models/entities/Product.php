<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $id
 * @property string $name
 * @property integer $renewal
 * @property string $cost
 * @property string $linkToUserManual
 * @property string $productCode
 * @property string $urlPayOneWeek
 * @property string $urlPayTwoWeeks
 *
 * The followings are the available model relations:
 * @property CourseHasProducts[] $courseHasProducts
 * @property Licences[] $licences
 * @property UserProduct[] $userProducts
 */
class Product extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('renewal', 'numerical', 'integerOnly'=>true),
			array('name, linkToUserManual,productCode,urlPayOneWeek,urlPayTwoWeeks', 'length', 'max'=>255),
			array('cost', 'length', 'max'=>19),
            array('productCode', 'match','pattern' => '/^[[:alnum:]]*$/', 'message' => '{attribute} only accepts alphanumeric characters'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, renewal, cost, linkToUserManual, productCode,urlPayOneWeek,urlPayTwoWeeks', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'courseHasProducts' => array(self::HAS_MANY, 'CourseHasProducts', 'idproduct'),
			'licences' => array(self::HAS_MANY, 'Licences', 'idProduct'),
			'userProducts' => array(self::HAS_MANY, 'UserProduct', 'idProduct'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'renewal' => 'Renewal',
			'cost' => 'Cost',
			'linkToUserManual' => 'Link to User Manual',
            'productCode' => 'Product Code',
            'urlPayOneWeek'=>'Url Pay One Week',
            'urlPayTwoWeeks'=>'Url Pay two Weeks'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('renewal',$this->renewal);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('linkToUserManual',$this->linkToUserManual,true);
        $criteria->compare('productCode',$this->productCode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    function beforeDelete(){
        if( $this->courseHasProducts !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The product cannot be deleted because it\'s linked to a seminar');
            return false;
        }
        if( $this->licences !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The product cannot be deleted because it\'s linked to a license');
            return false;
        }
        if( $this->userProducts !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The product cannot be deleted because it\'s linked to a client');
            return false;
        }
        return parent::beforeDelete();
    }
}
