<?php

/**
 * This is the model class for table "user_product".
 *
 * The followings are the available columns in table 'user_product':
 * @property integer $idUsers
 * @property integer $idProduct
 * @property string $lastDatePurchase
 * @property string $activateDate
 * @property string $dueDate
 * @property integer $id
 *
 * The followings are the available model relations:
 * @property Product $idProduct0
 * @property Users $idUsers0
 */
class UserProduct extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lastDatePurchase', 'required'),
			array('idUsers, idProduct', 'numerical', 'integerOnly'=>true),
			array('activateDate, dueDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idUsers, idProduct, lastDatePurchase, activateDate, dueDate, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProduct0' => array(self::BELONGS_TO, 'Product', 'idProduct'),
			'idUsers0' => array(self::BELONGS_TO, 'Users', 'idUsers'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idUsers' => 'Id Users',
			'idProduct' => 'Id Product',
			'lastDatePurchase' => 'Last Date Purchase',
			'activateDate' => 'Activate Date',
			'dueDate' => 'Due Date',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUsers',$this->idUsers);
		$criteria->compare('idProduct',$this->idProduct);
		$criteria->compare('lastDatePurchase',$this->lastDatePurchase,true);
		$criteria->compare('activateDate',$this->activateDate,true);
		$criteria->compare('dueDate',$this->dueDate,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserProduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
