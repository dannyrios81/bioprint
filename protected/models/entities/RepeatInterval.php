<?php

/**
 * This is the model class for table "repeat_interval".
 *
 * The followings are the available columns in table 'repeat_interval':
 * @property integer $id
 * @property string $description
 * @property integer $methodId
 * @property integer $numberSeriesId
 *
 * The followings are the available model relations:
 * @property Method $method
 */
class RepeatInterval extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'repeat_interval';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('methodId', 'numerical', 'integerOnly'=>true),
            array('description,methodId', 'required'),
            array('description', 'length', 'max'=>255),
            array('description','match','allowEmpty'=>true,'pattern'=>"/^(\d{1,2}-){1,11}\d{1,2}$|^(\d{1,2},){1,11}\d{1,2}$|^(?'f1'\d{1,2}\*\(\d{1,2}(,\d{1,2}){0,11}\))(,(?&f1)){0,11}$|^(?'f2'\d{1,2}\*\(\d{1,2}(-\d{1,2}){0,1}\))(,(?&f2)){0,11}$|^(\d{1,2}\/){1,11}\d{1,2}$|^(?'f3'\d{1,2}\*\(\d{1,2}(\/\d{1,2}){0,11}\))$/",'message'=>'The {attribute} must have a valid format'),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]]),
            array('description,methodId', 'ECompositeUniqueValidator','allowEmpty'=>false,'attributeNames'=>'description,methodId','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'insert','attributesToAddError'=>'description','message'=>'The description "{value_description}" already exists for this conbination {attr_numberSeriesId} - {value_methodId} .'),
            array('description,methodId', 'ECompositeUniqueValidator','allowEmpty'=>false,'attributeNames'=>'description,methodId','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','attributesToAddError'=>'description','message'=>'The description "{value_description}" already exists for this conbination {attr_numberSeriesId} - {value_methodId} .','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]]),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, description, methodId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'method' => array(self::BELONGS_TO, 'Method', 'methodId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
			'methodId' => 'Strength Quality',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('methodId',$this->methodId);
		$criteria->compare('numberSeriesId',$this->numberSeriesId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RepeatInterval the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
