<?php

/**
 * This is the model class for table "supplement".
 *
 * The followings are the available columns in table 'supplement':
 * @property integer $id
 * @property string $name
 * @property integer $independent
 * @property string $linkToBuy
 * @property integer $idCountry
 *
 * The followings are the available model relations:
 * @property ChooseSuplement[] $chooseSuplements
 * @property Country $idCountry0
 */
class Supplement extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supplement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('independent, idCountry', 'numerical', 'integerOnly'=>true),
			array('name, linkToBuy', 'length', 'max'=>255),
			array('name','validateUniqueCreate','on'=>array('create')),
			array('name','validateUniqueUpdate','on'=>array('update')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, independent, linkToBuy, idCountry', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'chooseSuplements' => array(self::HAS_MANY, 'ChooseSuplement', 'idSuplement'),
			'idCountry0' => array(self::BELONGS_TO, 'Country', 'idCountry'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'independent' => 'Independent',
			'linkToBuy' => 'Link to Buy',
			'idCountry' => 'Zone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('independent',$this->independent);
		$criteria->compare('linkToBuy',$this->linkToBuy,true);
		$criteria->compare('idCountry',$this->idCountry);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Supplement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function validateUniqueCreate()
	{
		$temp = Supplement::model()->findAllByAttributes(['name'=>$this->name, 'idCountry'=>$this->idCountry, 'independent'=>$this->independent]);
		if(count($temp)>0)
		{
			$this->addError('name','The combination of name, zone and independent is duplicate');
		}
	}
	public function validateUniqueUpdate()
	{
		$temp = Supplement::model()->findAllByAttributes(['name'=>$this->name, 'idCountry'=>$this->idCountry, 'independent'=>$this->independent],'id<>:id',['id'=>$this->id]);
		if(count($temp)>0)
		{
			$this->addError('name','The combination of name, zone and independent is duplicate');
		}
	}
}
