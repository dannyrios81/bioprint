<?php

/**
 * This is the model class for table "measurement_types".
 *
 * The followings are the available columns in table 'measurement_types':
 * @property integer $id
 * @property string $name
 * @property integer $idAttachment
 * @property integer $calculable
 * @property string $youtubeId
 *
 * The followings are the available model relations:
 * @property HormoneFamily[] $hormoneFamilies
 * @property HormoneFamily[] $hormoneFamilies1
 * @property HormoneFamily[] $hormoneFamilies2
 * @property HormoneUserDate[] $hormoneUserDates
 * @property HormoneUserDate[] $hormoneUserDates1
 * @property HormoneUserDate[] $hormoneUserDates2
 * @property IdealMeasurement[] $idealMeasurements
 * @property Measurement[] $measurements
 * @property Attachment $idAttachment0
 * @property Recipes[] $recipes
 */
class MeasurementTypes extends CActiveRecord
{
    public $conteo;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'measurement_types';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idAttachment, calculable', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255,'allowEmpty'=>false),
			array('name','required'),
			array('name','unique','className'=>'MeasurementTypes','on'=>array('create')),
			array('name','unique','className'=>'MeasurementTypes','on'=>array('update'),'criteria' => array('condition' => 'id <> :id','params' => array('id' => $this->id))),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, idAttachment, calculable,youtubeId', 'safe', 'on'=>'search'),
			array('youtubeId', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hormoneFamilies' => array(self::HAS_MANY, 'HormoneFamily', 'idAssociatedMeasurementType1'),
			'hormoneFamilies1' => array(self::HAS_MANY, 'HormoneFamily', 'idAssociatedMeasurementType2'),
			'hormoneFamilies2' => array(self::HAS_MANY, 'HormoneFamily', 'idAssociatedMeasurementType3'),
			'hormoneUserDates' => array(self::HAS_MANY, 'HormoneUserDate', 'idMeasurementType1'),
			'hormoneUserDates1' => array(self::HAS_MANY, 'HormoneUserDate', 'idMeasurementType2'),
			'hormoneUserDates2' => array(self::HAS_MANY, 'HormoneUserDate', 'idMeasurementType3'),
			'idealMeasurements' => array(self::HAS_MANY, 'IdealMeasurement', 'idMeasuremetType'),
			'measurements' => array(self::HAS_MANY, 'Measurement', 'idMeasurementType'),
			'idAttachment0' => array(self::BELONGS_TO, 'Attachment', 'idAttachment'),
			'recipes' => array(self::HAS_MANY, 'Recipes', 'idMeasurmenetTypes'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'idAttachment' => 'Attachment',
			'calculable' => 'Calculable',
			'youtubeId' => 'Youtube Id',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('idAttachment',$this->idAttachment);
		$criteria->compare('calculable',$this->calculable);
		$criteria->compare('youtubeId',$this->youtubeId);
//		$criteria->order='name';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function is_deletable()
    {
        $sql='SELECT 
                (select COUNT(*) from measurement m WHERE m.idMeasurementType=mt.id GROUP BY m.idMeasurementType) +
                (select COUNT(*) from hormone_family hf WHERE hf.idAssociatedMeasurementType1=mt.id or hf.idAssociatedMeasurementType2=mt.id or hf.idAssociatedMeasurementType3=mt.id) +
                (select  COUNT(*) from hormone_user_date hud WHERE hud.idMeasurementType1=mt.id OR hud.idMeasurementType2=mt.id OR hud.idMeasurementType3=mt.id) +
                (select COUNT(*) from ideal_measurement im WHERE im.idMeasuremetType=mt.id) +
                (select count(*) from recipes r where r.idMeasurmenetTypes=mt.id) conteo
                FROM
                measurement_types mt
                where mt.id = :id
                GROUP BY mt.id;';

        $this->conteo=$this::model()->findBySql($sql,[':id'=>$this->id]);

        if(!empty($this->conteo))
        {
            $this->conteo=$this->conteo->conteo;
        }

//        CVarDumper::dump($this->conteo,10,true);exit;

        if($this->conteo > 0)
            return false;
        else
            return true;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MeasurementTypes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
