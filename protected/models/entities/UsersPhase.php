<?php

/**
 * This is the model class for table "users_phase".
 *
 * The followings are the available columns in table 'users_phase':
 * @property integer $id
 * @property string $date
 * @property integer $numberPhase
 * @property integer $numberOfTimes
 * @property integer $idUser
 * @property string $Fase
 * @property string $BodyPart
 * @property integer $numberOfTimesDone
 *
 * The followings are the available model relations:
 * @property Execution[] $executions
 * @property UserPhaseExercise[] $userPhaseExercises
 * @property Users $idUsers0
 */
class UsersPhase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users_phase';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numberPhase, numberOfTimes, idUser, numberOfTimesDone', 'numerical', 'integerOnly'=>true),
			array('Fase, BodyPart', 'length', 'max'=>255),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date, numberPhase, numberOfTimes, idUser, Fase, BodyPart, numberOfTimesDone', 'safe'),
			array('id, date, numberPhase, numberOfTimes, idUser, Fase, BodyPart, numberOfTimesDone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'executions' => array(self::HAS_MANY, 'Execution', 'idUserPhase'),
			'userPhaseExercises' => array(self::HAS_MANY, 'UserPhaseExercise', 'idUserPhase'),
            'idUsers0' => array(self::BELONGS_TO, 'Users', 'idUsers'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'numberPhase' => 'Number Phase',
			'numberOfTimes' => 'Number Of Times',
			'idUser' => 'Id User',
			'Fase' => 'Fase',
			'BodyPart' => 'Body Part',
			'numberOfTimesDone' => 'Number Of Times Done',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('numberPhase',$this->numberPhase);
		$criteria->compare('numberOfTimes',$this->numberOfTimes);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('Fase',$this->Fase,true);
		$criteria->compare('BodyPart',$this->BodyPart,true);
		$criteria->compare('numberOfTimesDone',$this->numberOfTimesDone);

		$criteria->order='date DESC,CONVERT(numberPhase,UNSIGNED INTEGER) ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersPhase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
