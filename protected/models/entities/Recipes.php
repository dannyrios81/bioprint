<?php

/**
 * This is the model class for table "recipes".
 *
 * The followings are the available columns in table 'recipes':
 * @property integer $id
 * @property integer $idHormoneFamily
 * @property integer $idMeasurmenetTypes
 * @property integer $idCountry
 * @property string $notes
 *
 * The followings are the available model relations:
 * @property Choose[] $chooses
 * @property Country $idCountry0
 * @property HormoneFamily $idHormoneFamily0
 * @property MeasurementTypes $idMeasurmenetTypes0
 */
class Recipes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'recipes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idHormoneFamily, idMeasurmenetTypes, idCountry', 'numerical', 'integerOnly'=>true),
			array('notes', 'safe'),
			array('idHormoneFamily','validateUniqueCreate','on'=>array('create')),
			array('idHormoneFamily','validateUniqueUpdate','on'=>array('update')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idHormoneFamily, idMeasurmenetTypes, idCountry, notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'chooses' => array(self::HAS_MANY, 'Choose', 'idRecipe'),
			'idCountry0' => array(self::BELONGS_TO, 'Country', 'idCountry'),
			'idHormoneFamily0' => array(self::BELONGS_TO, 'HormoneFamily', 'idHormoneFamily'),
			'idMeasurmenetTypes0' => array(self::BELONGS_TO, 'MeasurementTypes', 'idMeasurmenetTypes'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idHormoneFamily' => 'Hormone Family',
			'idMeasurmenetTypes' => 'Site',
			'idCountry' => 'Zone',
			'notes' => 'Note',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idHormoneFamily',$this->idHormoneFamily);
		$criteria->compare('idMeasurmenetTypes',$this->idMeasurmenetTypes);
		$criteria->compare('idCountry',$this->idCountry);
		$criteria->compare('notes',$this->notes,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Recipes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/*
	 * custom validate
	 */
	public function validateUniqueCreate()
	{
		$temp = Recipes::model()->findAllByAttributes(['idHormoneFamily'=>$this->idHormoneFamily, 'idMeasurmenetTypes'=>$this->idMeasurmenetTypes, 'idCountry'=>$this->idCountry]);
		if(count($temp)>0)
		{
			$this->addError('idHormoneFamily','The combination of hormonal family, zone and supplement this duplicate');
		}
	}
	public function validateUniqueUpdate()
	{
		$temp = Recipes::model()->findAllByAttributes(['idHormoneFamily'=>$this->idHormoneFamily, 'idMeasurmenetTypes'=>$this->idMeasurmenetTypes, 'idCountry'=>$this->idCountry],'id<>:id',['id'=>$this->id]);
		if(count($temp)>0)
		{
			$this->addError('idHormoneFamily','The combination of hormonal family, zone and supplement this duplicate');
		}
	}
}
