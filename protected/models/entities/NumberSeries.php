<?php

/**
 * This is the model class for table "number_series".
 *
 * The followings are the available columns in table 'number_series':
 * @property integer $id
 * @property string $description
 * @property integer $methodId
 *
 * The followings are the available model relations:
 * @property Method $method
 * @property RepeatInterval[] $repeatIntervals
 */
class NumberSeries extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'number_series';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('methodId', 'numerical', 'integerOnly'=>true),
            array('description,methodId', 'required'),
            array('description', 'length', 'max'=>255),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]]),
            array('description,methodId', 'ECompositeUniqueValidator','allowEmpty'=>false,'attributeNames'=>'description,methodId','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'insert','attributesToAddError'=>'description','message'=>'The description "{value_description}" already exists for this {attr_familyExerciseId}.'),
            array('description,methodId', 'ECompositeUniqueValidator','allowEmpty'=>false,'attributeNames'=>'description,methodId','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','attributesToAddError'=>'description','message'=>'The description "{value_description}" already exists for this {attr_familyExerciseId}.','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]]),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, description, methodId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'method' => array(self::BELONGS_TO, 'Method', 'methodId'),
			'repeatIntervals' => array(self::HAS_MANY, 'RepeatInterval', 'numberSeriesId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
			'methodId' => 'Strength Quality',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('methodId',$this->methodId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NumberSeries the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    function beforeDelete(){
        if( $this->repeatIntervals !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The number of sets cannot be deleted because it\'s linked to a rep interval');
            return false;
        }
        return parent::beforeDelete();
    }
}
