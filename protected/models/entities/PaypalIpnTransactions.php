<?php

/**
 * This is the model class for table "paypal_ipn_transactions".
 *
 * The followings are the available columns in table 'paypal_ipn_transactions':
 * @property string $payment_type
 * @property string $payment_date
 * @property string $payment_status
 * @property string $pending_reason
 * @property string $address_status
 * @property string $payer_status
 * @property string $first_name
 * @property string $last_name
 * @property string $payer_email
 * @property string $payer_id
 * @property string $address_name
 * @property string $address_country
 * @property string $address_country_code
 * @property string $address_zip
 * @property string $address_state
 * @property string $address_city
 * @property string $address_street
 * @property string $business
 * @property string $receiver_email
 * @property string $receiver_id
 * @property string $residence_country
 * @property string $item_name
 * @property string $item_name1
 * @property string $item_number
 * @property string $item_number1
 * @property string $quantity
 * @property string $shipping
 * @property string $tax
 * @property string $mc_currency
 * @property string $mc_fee
 * @property string $mc_gross
 * @property string $mc_gross_1
 * @property string $mc_handling
 * @property string $mc_handling1
 * @property string $mc_shipping
 * @property string $mc_shipping1
 * @property string $txn_type
 * @property string $txn_id
 * @property string $notify_version
 * @property string $parent_txn_id
 * @property string $reason_code_tx
 * @property string $receipt_ID
 * @property string $auction_buyer_id
 * @property string $auction_closing_date
 * @property string $for_auction
 * @property string $reason_code_refund
 * @property string $receipt_id_refund
 * @property string $custom
 * @property string $invoice
 */
class PaypalIpnTransactions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paypal_ipn_transactions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('payment_type, payment_date, payment_status, pending_reason, address_status, payer_status, first_name, last_name, payer_email, payer_id, address_name, address_country, address_country_code, address_zip, address_state, address_city, address_street, business, receiver_email, receiver_id, residence_country, item_name, item_name1, item_number, item_number1, quantity, shipping, tax, mc_currency, mc_fee, mc_gross, mc_gross_1, mc_handling, mc_handling1, mc_shipping, mc_shipping1, txn_type, txn_id, notify_version, parent_txn_id, reason_code_tx, receipt_ID, auction_buyer_id, auction_closing_date, for_auction, reason_code_refund, receipt_id_refund, custom, invoice', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('payment_type, payment_date, payment_status, pending_reason, address_status, payer_status, first_name, last_name, payer_email, payer_id, address_name, address_country, address_country_code, address_zip, address_state, address_city, address_street, business, receiver_email, receiver_id, residence_country, item_name, item_name1, item_number, item_number1, quantity, shipping, tax, mc_currency, mc_fee, mc_gross, mc_gross_1, mc_handling, mc_handling1, mc_shipping, mc_shipping1, txn_type, txn_id, notify_version, parent_txn_id, reason_code_tx, receipt_ID, auction_buyer_id, auction_closing_date, for_auction, reason_code_refund, receipt_id_refund, custom, invoice', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'payment_type' => 'Payment Type',
			'payment_date' => 'Payment Date',
			'payment_status' => 'Payment Status',
			'pending_reason' => 'Pending Reason',
			'address_status' => 'Address Status',
			'payer_status' => 'Payer Status',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'payer_email' => 'Payer Email',
			'payer_id' => 'Payer',
			'address_name' => 'Address Name',
			'address_country' => 'Address Country',
			'address_country_code' => 'Address Country Code',
			'address_zip' => 'Address Zip',
			'address_state' => 'Address State',
			'address_city' => 'Address City',
			'address_street' => 'Address Street',
			'business' => 'Business',
			'receiver_email' => 'Receiver Email',
			'receiver_id' => 'Receiver',
			'residence_country' => 'Residence Country',
			'item_name' => 'Item Name',
			'item_name1' => 'Item Name1',
			'item_number' => 'Item Number',
			'item_number1' => 'Item Number1',
			'quantity' => 'Quantity',
			'shipping' => 'Shipping',
			'tax' => 'Tax',
			'mc_currency' => 'Mc Currency',
			'mc_fee' => 'Mc Fee',
			'mc_gross' => 'Mc Gross',
			'mc_gross_1' => 'Mc Gross 1',
			'mc_handling' => 'Mc Handling',
			'mc_handling1' => 'Mc Handling1',
			'mc_shipping' => 'Mc Shipping',
			'mc_shipping1' => 'Mc Shipping1',
			'txn_type' => 'Txn Type',
			'txn_id' => 'Txn',
			'notify_version' => 'Notify Version',
			'parent_txn_id' => 'Parent Txn',
			'reason_code_tx' => 'Reason Code Tx',
			'receipt_ID' => 'Receipt',
			'auction_buyer_id' => 'Auction Buyer',
			'auction_closing_date' => 'Auction Closing Date',
			'for_auction' => 'For Auction',
			'reason_code_refund' => 'Reason Code Refund',
			'receipt_id_refund' => 'Receipt Id Refund',
			'custom' => 'Custom',
			'invoice' => 'Invoice',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('payment_type',$this->payment_type,true);
		$criteria->compare('payment_date',$this->payment_date,true);
		$criteria->compare('payment_status',$this->payment_status,true);
		$criteria->compare('pending_reason',$this->pending_reason,true);
		$criteria->compare('address_status',$this->address_status,true);
		$criteria->compare('payer_status',$this->payer_status,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('payer_email',$this->payer_email,true);
		$criteria->compare('payer_id',$this->payer_id,true);
		$criteria->compare('address_name',$this->address_name,true);
		$criteria->compare('address_country',$this->address_country,true);
		$criteria->compare('address_country_code',$this->address_country_code,true);
		$criteria->compare('address_zip',$this->address_zip,true);
		$criteria->compare('address_state',$this->address_state,true);
		$criteria->compare('address_city',$this->address_city,true);
		$criteria->compare('address_street',$this->address_street,true);
		$criteria->compare('business',$this->business,true);
		$criteria->compare('receiver_email',$this->receiver_email,true);
		$criteria->compare('receiver_id',$this->receiver_id,true);
		$criteria->compare('residence_country',$this->residence_country,true);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('item_name1',$this->item_name1,true);
		$criteria->compare('item_number',$this->item_number,true);
		$criteria->compare('item_number1',$this->item_number1,true);
		$criteria->compare('quantity',$this->quantity,true);
		$criteria->compare('shipping',$this->shipping,true);
		$criteria->compare('tax',$this->tax,true);
		$criteria->compare('mc_currency',$this->mc_currency,true);
		$criteria->compare('mc_fee',$this->mc_fee,true);
		$criteria->compare('mc_gross',$this->mc_gross,true);
		$criteria->compare('mc_gross_1',$this->mc_gross_1,true);
		$criteria->compare('mc_handling',$this->mc_handling,true);
		$criteria->compare('mc_handling1',$this->mc_handling1,true);
		$criteria->compare('mc_shipping',$this->mc_shipping,true);
		$criteria->compare('mc_shipping1',$this->mc_shipping1,true);
		$criteria->compare('txn_type',$this->txn_type,true);
		$criteria->compare('txn_id',$this->txn_id,true);
		$criteria->compare('notify_version',$this->notify_version,true);
		$criteria->compare('parent_txn_id',$this->parent_txn_id,true);
		$criteria->compare('reason_code_tx',$this->reason_code_tx,true);
		$criteria->compare('receipt_ID',$this->receipt_ID,true);
		$criteria->compare('auction_buyer_id',$this->auction_buyer_id,true);
		$criteria->compare('auction_closing_date',$this->auction_closing_date,true);
		$criteria->compare('for_auction',$this->for_auction,true);
		$criteria->compare('reason_code_refund',$this->reason_code_refund,true);
		$criteria->compare('receipt_id_refund',$this->receipt_id_refund,true);
		$criteria->compare('custom',$this->custom,true);
		$criteria->compare('invoice',$this->invoice,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaypalIpnTransactions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
