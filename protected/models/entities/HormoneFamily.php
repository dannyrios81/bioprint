<?php

/**
 * This is the model class for table "hormone_family".
 *
 * The followings are the available columns in table 'hormone_family':
 * @property integer $id
 * @property string $name
 * @property integer $idAssociatedMeasurementType1
 * @property integer $idAssociatedMeasurementType2
 * @property integer $idAssociatedMeasurementType3
 * @property integer $priority
 *
 * The followings are the available model relations:
 * @property MeasurementTypes $idAssociatedMeasurementType10
 * @property MeasurementTypes $idAssociatedMeasurementType20
 * @property MeasurementTypes $idAssociatedMeasurementType30
 * @property HormoneUserDate[] $hormoneUserDates
 * @property Recipes[] $recipes
 */
class HormoneFamily extends CActiveRecord
{
    public $conteo;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hormone_family';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idAssociatedMeasurementType1, idAssociatedMeasurementType2, idAssociatedMeasurementType3', 'numerical', 'integerOnly'=>true),
			array('name','unique','className'=>'HormoneFamily','on'=>array('create')),
			array('name','unique','className'=>'HormoneFamily','on'=>array('update'),'criteria' => array('condition' => 'id <> :id','params' => array('id' => $this->id))),
			array('name', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, idAssociatedMeasurementType1, idAssociatedMeasurementType2, idAssociatedMeasurementType3', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idAssociatedMeasurementType10' => array(self::BELONGS_TO, 'MeasurementTypes', 'idAssociatedMeasurementType1'),
			'idAssociatedMeasurementType20' => array(self::BELONGS_TO, 'MeasurementTypes', 'idAssociatedMeasurementType2'),
			'idAssociatedMeasurementType30' => array(self::BELONGS_TO, 'MeasurementTypes', 'idAssociatedMeasurementType3'),
			'hormoneUserDates' => array(self::HAS_MANY, 'HormoneUserDate', 'idHormoneFamily'),
			'recipes' => array(self::HAS_MANY, 'Recipes', 'idHormoneFamily'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'idAssociatedMeasurementType1' => 'Associated Site 1',
			'idAssociatedMeasurementType2' => 'Associated Site 2',
			'idAssociatedMeasurementType3' => 'Associated Site 3',
            'priority' => 'Priority',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('idAssociatedMeasurementType1',$this->idAssociatedMeasurementType1);
		$criteria->compare('idAssociatedMeasurementType2',$this->idAssociatedMeasurementType2);
		$criteria->compare('idAssociatedMeasurementType3',$this->idAssociatedMeasurementType3);
        $criteria->compare('priority',$this->priority);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HormoneFamily the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function is_deletable()
    {
//        (count($data->hormoneUserDates)==0 && count($data->recipes)==0
        $sql='select 
                (select count(*) from hormone_user_date hud where hud.idHormoneFamily=hf.id)+
                (select count(*) from recipes r where r.idHormoneFamily=hf.id) conteo
                FROM hormone_family hf
                where hf.id = :id
                GROUP BY hf.id;';

        $this->conteo=$this::model()->findBySql($sql,[':id'=>$this->id]);

        if(!empty($this->conteo))
        {
            $this->conteo=$this->conteo->conteo;
        }

//        CVarDumper::dump($this->conteo,10,true);exit;

        if($this->conteo > 0)
            return false;
        else
            return true;

    }
}
