<?php

/**
 * This is the model class for table "course".
 *
 * The followings are the available columns in table 'course':
 * @property integer $id
 * @property string $name
 * @property string $startDate
 * @property string $location
 * @property string $courseIdentification
 *
 * The followings are the available model relations:
 * @property CourseHasProducts[] $courseHasProducts
 * @property UsersCourses[] $usersCourses
 */
class Course extends CActiveRecord
{
    public $courseHasProductsCheckBoxSelect=[];
    public $courseHasProductsCheckBoxStored=[];

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'course';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('startDate', 'required'),
			array('name, location, courseIdentification', 'length', 'max'=>255),
            array('courseHasProductsCheckBoxSelect', 'type', 'type' => 'array', 'allowEmpty' => false,'message'=>'You must select at least one product'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, startDate, location, courseIdentification', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'courseHasProducts' => array(self::HAS_MANY, 'CourseHasProducts', 'idcourse'),
			'usersCourses' => array(self::HAS_MANY, 'UsersCourses', 'idCourse'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'startDate' => 'Start Date',
			'location' => 'Location',
			'courseIdentification' => 'Seminar ID',
			'courseHasProducts' => 'Products',
			'courseHasProductsCheckBoxSelect' => 'Products',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('startDate',$this->startDate,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('courseIdentification',$this->courseIdentification,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Course the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    function beforeDelete(){
        if( $this->courseHasProducts !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The seminar cannot be deleted because it\'s linked to a product');
            return false;
        }
        if( $this->usersCourses !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The seminar cannot be deleted because it\'s linked to a client');
            return false;
        }
        return parent::beforeDelete();
    }
    public function afterFind() {
        $this->courseHasProductsCheckBoxSelect = [];
        foreach ($this->courseHasProducts as $r) { //productsToCategories is relation with table product_to_category
            $this->courseHasProductsCheckBoxSelect[] = $r->idproduct;
        }
        $this->courseHasProductsCheckBoxStored = $this->courseHasProductsCheckBoxSelect;
        parent::afterFind();
    }
    protected function afterSave() {

        if (!$this->courseHasProductsCheckBoxSelect) //if nothing selected set it as an empty array
            $this->courseHasProductsCheckBoxSelect = array();

        //save the new selected ids that are not exist in the stored ids
        $ids_to_insert = array_diff($this->courseHasProductsCheckBoxSelect , $this->courseHasProductsCheckBoxStored);

        foreach ($ids_to_insert as $nid) {
            $m = new CourseHasProducts();
            $m->idcourse = $this->id;
            $m->idproduct = $nid;
            $m->save();
        }

        //remove the stored ids that are not exist in the selected ids
        $ids_to_delete = array_diff($this->courseHasProductsCheckBoxStored,$this->courseHasProductsCheckBoxSelect);


        foreach ($ids_to_delete as $did) {
            if ($d = CourseHasProducts::model()->findByAttributes(['idcourse' => $this->id, 'idproduct' => $did])) {
                $d->delete();
            }
        }

        parent::afterSave();
    }

}
