<?php

/**
 * This is the model class for table "hormone_user_date".
 *
 * The followings are the available columns in table 'hormone_user_date':
 * @property integer $idUsers
 * @property string $date
 * @property string $scoreMeasure1
 * @property string $scoreMeasure2
 * @property string $scoreMeasure3
 * @property string $totalScore
 * @property integer $idHormoneFamily
 * @property integer $idMeasurementType1
 * @property integer $idMeasurementType2
 * @property integer $idMeasurementType3
 * @property integer $idMeasurementTypeWorse
 * @property integer $scroreWorseMeasure
 * @property string $deviation
 * @property integer $id
 * @property integer $applyPriority
 *
 * The followings are the available model relations:
 * @property MeasurementTypes $idMeasurementTypeWorse0
 * @property HormoneFamily $idHormoneFamily0
 * @property MeasurementTypes $idMeasurementType10
 * @property MeasurementTypes $idMeasurementType20
 * @property MeasurementTypes $idMeasurementType30
 * @property Users $idUsers0
 */
class HormoneUserDate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hormone_user_date';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date', 'required'),
			array('idUsers, idHormoneFamily, idMeasurementType1, idMeasurementType2, idMeasurementType3, idMeasurementTypeWorse', 'numerical', 'integerOnly'=>true),
			array('scoreMeasure1, scoreMeasure2, scoreMeasure3, totalScore, deviation,scroreWorseMeasure', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idUsers, date, scoreMeasure1, scoreMeasure2, scoreMeasure3, totalScore, idHormoneFamily, idMeasurementType1, idMeasurementType2, idMeasurementType3, idMeasurementTypeWorse, scroreWorseMeasure, deviation, id,applyPriority', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMeasurementTypeWorse0' => array(self::BELONGS_TO, 'MeasurementTypes', 'idMeasurementTypeWorse'),
			'idHormoneFamily0' => array(self::BELONGS_TO, 'HormoneFamily', 'idHormoneFamily'),
			'idMeasurementType10' => array(self::BELONGS_TO, 'MeasurementTypes', 'idMeasurementType1'),
			'idMeasurementType20' => array(self::BELONGS_TO, 'MeasurementTypes', 'idMeasurementType2'),
			'idMeasurementType30' => array(self::BELONGS_TO, 'MeasurementTypes', 'idMeasurementType3'),
			'idUsers0' => array(self::BELONGS_TO, 'Users', 'idUsers'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idUsers' => 'Id Users',
			'date' => 'Date',
			'scoreMeasure1' => 'Score Measure1',
			'scoreMeasure2' => 'Score Measure2',
			'scoreMeasure3' => 'Score Measure3',
			'totalScore' => 'Total Score',
			'idHormoneFamily' => 'Id Hormone Family',
			'idMeasurementType1' => 'Id Measurement Type1',
			'idMeasurementType2' => 'Id Measurement Type2',
			'idMeasurementType3' => 'Id Measurement Type3',
			'idMeasurementTypeWorse' => 'Id Measurement Type Worse',
			'scroreWorseMeasure' => 'Scrore Worse Measure',
			'deviation' => 'Deviation',
			'id' => 'ID',
            'applyPriority' => 'Apply Priority',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUsers',$this->idUsers);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('scoreMeasure1',$this->scoreMeasure1,true);
		$criteria->compare('scoreMeasure2',$this->scoreMeasure2,true);
		$criteria->compare('scoreMeasure3',$this->scoreMeasure3,true);
		$criteria->compare('totalScore',$this->totalScore,true);
		$criteria->compare('idHormoneFamily',$this->idHormoneFamily);
		$criteria->compare('idMeasurementType1',$this->idMeasurementType1);
		$criteria->compare('idMeasurementType2',$this->idMeasurementType2);
		$criteria->compare('idMeasurementType3',$this->idMeasurementType3);
		$criteria->compare('idMeasurementTypeWorse',$this->idMeasurementTypeWorse);
		$criteria->compare('scroreWorseMeasure',$this->scroreWorseMeasure);
		$criteria->compare('deviation',$this->deviation,true);
		$criteria->compare('id',$this->id);
        $criteria->compare('applyPriority',$this->applyPriority);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HormoneUserDate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
