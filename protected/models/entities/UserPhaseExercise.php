<?php

/**
 * This is the model class for table "user_phase_exercise".
 *
 * The followings are the available columns in table 'user_phase_exercise':
 * @property integer $id
 * @property integer $idUserPhase
 * @property integer $idExercise
 * @property string $numberSeries
 * @property string $repeatInterval
 * @property string $tempo
 * @property string $rest
 * @property string $tut
 * @property string $tutTotal
 * @property string $order
 * @property string $notes
 * @property string $unitType
 * @property string $rangeMotion
 *
 * The followings are the available model relations:
 * @property ExecutionExercise[] $executionExercises
 * @property ExecutionExerciseCalc[] $executionExerciseCalcs
 * @property Exercise $idExercise0
 * @property UsersPhase $idUserPhase0
 * @property RangeMotion $idRangeMotion0
 */
class UserPhaseExercise extends CActiveRecord
{
    public $methodDescription;
    public $exerciseDescription;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_phase_exercise';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUserPhase, idExercise', 'numerical', 'integerOnly'=>true),
			array('numberSeries, repeatInterval, tempo, rest, tut, tutTotal, order, rangeMotion', 'length', 'max'=>255),
            array('unitType', 'length', 'max'=>50),
            array('notes', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idUserPhase, idExercise, numberSeries, repeatInterval, tempo, rest, tut, tutTotal,methodDescription,exerciseDescription', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'executionExercises' => array(self::HAS_MANY, 'ExecutionExercise', 'idUserPhaseExercise'),
            'executionExerciseCalcs' => array(self::HAS_MANY, 'ExecutionExerciseCalc', 'idUserPhaseExercice'),
			'idExercise0' => array(self::BELONGS_TO, 'Exercise', 'idExercise'),
			'idUserPhase0' => array(self::BELONGS_TO, 'UsersPhase', 'idUserPhase'),
            'idRangeMotion0' => array(self::BELONGS_TO,'RangeMotion', 'rangeMotion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idUserPhase' => 'Id User Phase',
			'idExercise' => 'Id Exercise',
			'numberSeries' => 'Number Series',
			'repeatInterval' => 'Repeat Interval',
			'tempo' => 'Tempo',
			'rest' => 'Rest',
			'tut' => 'Tut',
			'tutTotal' => 'Tut Total',
            'order' => 'Order',
            'notes' => 'Notes',
            'unitType' => 'Unit Type',
            'rangeMotion' => 'Range Motion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(!empty($this->methodDescription))
        {
            $criteria->with=['idExercise0','idExercise0.idMethod0'];
            $criteria->addSearchCondition('idMethod0.description',$this->methodDescription);
        }
        if(!empty($this->exerciseDescription))
        {
//            CVarDumper::dump($criteria->with,10,true);exit;
            if(!is_array($criteria->with) or count($criteria->with)<=0)
            {
//                CVarDumper::dump($criteria->with,10,true);exit;
                $criteria->with=['idExercise0'];
            }

            $criteria->addSearchCondition('idExercise0.description',$this->exerciseDescription);
        }

		$criteria->compare('id',$this->id);
		$criteria->compare('idUserPhase',$this->idUserPhase);
		$criteria->compare('idExercise',$this->idExercise);
		$criteria->compare('numberSeries',$this->numberSeries,true);
		$criteria->compare('repeatInterval',$this->repeatInterval,true);
		$criteria->compare('tempo',$this->tempo,true);
		$criteria->compare('rest',$this->rest,true);
		$criteria->compare('tut',$this->tut,true);
		$criteria->compare('tutTotal',$this->tutTotal,true);
        $criteria->compare('t.order',$this->order,true);
        $criteria->compare('notes',$this->notes,true);
        $criteria->compare('unitType',$this->unitType,true);
        $criteria->compare('rangeMotion',$this->rangeMotion,true);

        $criteria->order='t.order';



		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserPhaseExercise the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
