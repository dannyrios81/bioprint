<?php

/**
 * This is the model class for table "weight_height".
 *
 * The followings are the available columns in table 'weight_height':
 * @property integer $id
 * @property integer $weight
 * @property string $weightUnit
 * @property integer $height
 * @property string $heightUnit
 * @property integer $idUser
 * @property string $date
 * @property integer $migrate
 *
 * The followings are the available model relations:
 * @property Users $idUser0
 */
class WeightHeight extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'weight_height';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('weight, height, weightUnit, heightUnit,date,idUser', 'required'),
			array('weight, height', 'numerical', 'integerOnly'=>false),
			array('idUser', 'numerical', 'integerOnly'=>true),
			array('weightUnit, heightUnit', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, weight, weightUnit, height, heightUnit, idUser, date, migrate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idUser0' => array(self::BELONGS_TO, 'Users', 'idUser'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'weight' => 'Weight',
			'weightUnit' => 'Weight Unit',
			'height' => 'Height',
			'heightUnit' => 'Height Unit',
			'idUser' => 'Id User',
			'date' => 'Date',
            'migrate' => 'Migrate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('weightUnit',$this->weightUnit,true);
		$criteria->compare('height',$this->height);
		$criteria->compare('heightUnit',$this->heightUnit,true);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('date',$this->date,true);
        $criteria->compare('migrate',$this->migrate);


        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WeightHeight the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
