<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $create_record
 * @property string $name
 * @property string $lastName
 * @property string $gender
 * @property string $bornDate
 * @property string $historyHealthCondition
 * @property string $familyHistory
 * @property string $currentHealthCondition
 * @property string $objectivesToBeAchieved
 * @property string $imcGoal
 * @property string $sportsPractice
 * @property string $insuranceCompany
 * @property string $prescriptionDrugs
 * @property string $allergies
 * @property string $medicalRestrictions
 * @property integer $idAttachment
 * @property string $parentEmail
 * @property integer $forgotPass
 * @property string $userType
 * @property integer $idCountry
 * @property string $contactInformation
 * @property integer $migrate
 *
 * The followings are the available model relations:
 * @property Execution[] $executions
 * @property HormoneUserDate[] $hormoneUserDates
 * @property Licences[] $licences
 * @property Measurement[] $measurements
 * @property Purchase[] $purchases
 * @property UserProduct[] $userProducts
 * @property Attachment $idAttachment0
 * @property Country $idCountry0
 * @property UsersCourses[] $usersCourses
 * @property UsersPhase[] $usersPhases
 * @property WeightHeight[] $weightHeights
 * @property Users[] $usersDeletes
 */
class Users extends CActiveRecord
{
	private $oldEmail;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, password, create_record, name, bornDate, parentEmail,gender,userType,idCountry', 'required'),
//			array('idAttachment', 'required', 'on'=>'Nuevo'),
			array('idAttachment, forgotPass, idCountry, migrate', 'numerical', 'integerOnly'=>true),
			array('email, name, lastName, insuranceCompany, parentEmail', 'length', 'max'=>255),
			array('email', 'email','allowEmpty'=>false),
			array('email','unique','className'=>'Users','on'=>array('Nuevo')),
			array('email','unique','className'=>'Users','on'=>array('update'),'criteria' => array('condition' => 'email <> :email','params' => array('email' => $this->email))),
			array('password', 'length', 'max'=>64),
			array('gender, imcGoal', 'length', 'max'=>6),
			array('userType', 'length', 'max'=>20),
			array('historyHealthCondition, familyHistory, currentHealthCondition, objectivesToBeAchieved, sportsPractice, prescriptionDrugs, allergies, medicalRestrictions, contactInformation', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, password, create_record, name, lastName, gender, bornDate, historyHealthCondition, familyHistory, currentHealthCondition, objectivesToBeAchieved, imcGoal, sportsPractice, insuranceCompany, prescriptionDrugs, allergies, medicalRestrictions, idAttachment, parentEmail, forgotPass, userType, idCountry, contactInformation', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'executions' => array(self::HAS_MANY, 'Execution', 'idUsers'),
			'hormoneUserDates' => array(self::HAS_MANY, 'HormoneUserDate', 'idUsers'),
			'licences' => array(self::HAS_MANY, 'Licences', 'idUsers'),
			'measurements' => array(self::HAS_MANY, 'Measurement', 'idUsers'),
			'purchases' => array(self::HAS_MANY, 'Purchase', 'idUsers'),
			'userProducts' => array(self::HAS_MANY, 'UserProduct', 'idUsers'),
			'idAttachment0' => array(self::BELONGS_TO, 'Attachment', 'idAttachment'),
			'idCountry0' => array(self::BELONGS_TO, 'Country', 'idCountry'),
			'usersCourses' => array(self::HAS_MANY, 'UsersCourses', 'idUsers'),
            'usersPhases' => array(self::HAS_MANY, 'UsersPhase', 'idUser'),
            'usersDeletes' => array(self::HAS_MANY, 'UsersDelete', 'id'),
			'weightHeights' => array(self::HAS_MANY, 'WeightHeight', 'idUser'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'create_record' => 'Create Record',
			'name' => 'Name',
			'lastName' => 'Last Name',
			'gender' => 'Gender',
			'bornDate' => 'Date of Birth',
			'historyHealthCondition' => 'History of Health Conditions',
			'familyHistory' => 'Family History',
			'currentHealthCondition' => 'Current Health Condition',
			'objectivesToBeAchieved' => 'Objectives To Be Achieved',
			'imcGoal' => 'Body Fat % Goal',
			'sportsPractice' => 'Sports',
			'insuranceCompany' => 'Insurance Company',
			'prescriptionDrugs' => 'Prescription Drugs',
			'allergies' => 'Allergies',
			'medicalRestrictions' => 'Medical Restrictions',
			'idAttachment' => 'Photo (Max Size 6 Mb)',
			'parentEmail' => 'Parent Email',
			'forgotPass' => 'Forgot Pass',
			'userType' => 'User Type',
			'idCountry' => 'Zone',
			'contactInformation' => 'Contact Information',
            'migrate' => 'Migrate',
		);
	}
    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'vendor.pentium10.yii-remember-filters-gridview.components.ERememberFiltersBehavior',
                'defaults'=>array(),           /* optional line */
                'defaultStickOnClear'=>false   /* optional line */
            ),
        );
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;

		if(empty($this->userType))
		{
			switch(Yii::app()->user->getState('userType'))
			{
				case 'Admin':
				case 'Trainer':
					$criteria->addInCondition('userType',['Trainer','User','Practitioner1']);
					break;
				case 'User':
					$criteria->addInCondition('userType',['User']);
					break;
				default:
					$criteria->compare('userType',$this->userType,true);
			}
		}
		else
		{
			$criteria->compare('userType', $this->userType, true);
		}

		if(Yii::app()->user->getState('userType')=='Trainer' or Yii::app()->user->getState('userType')=='Practitioner1')
		{
			$userLogeado = Yii::app()->user->getState('_user');
			$criteria->compare('parentEmail',$userLogeado->email);
		}
		else
		{
			$criteria->compare('parentEmail',$this->parentEmail);
		}

//		CVarDumper::dump($_GET,10,true);
//		CVarDumper::dump($this->userType,10,true);exit;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('create_record',$this->create_record,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('lastName',$this->lastName,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('bornDate',$this->bornDate,true);
		$criteria->compare('historyHealthCondition',$this->historyHealthCondition,true);
		$criteria->compare('familyHistory',$this->familyHistory,true);
		$criteria->compare('currentHealthCondition',$this->currentHealthCondition,true);
		$criteria->compare('objectivesToBeAchieved',$this->objectivesToBeAchieved,true);
		$criteria->compare('imcGoal',$this->imcGoal,true);
		$criteria->compare('sportsPractice',$this->sportsPractice,true);
		$criteria->compare('insuranceCompany',$this->insuranceCompany,true);
		$criteria->compare('prescriptionDrugs',$this->prescriptionDrugs,true);
		$criteria->compare('allergies',$this->allergies,true);
		$criteria->compare('medicalRestrictions',$this->medicalRestrictions,true);
		$criteria->compare('idAttachment',$this->idAttachment);
		$criteria->compare('forgotPass',$this->forgotPass);
		$criteria->compare('idCountry',$this->idCountry);
		$criteria->compare('contactInformation',$this->contactInformation,true);
        $criteria->compare('migrate',$this->migrate);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function beforeSave()
	{
	    if(empty($this->imcGoal))
	        $this->imcGoal = NULL;

		if($this->getScenario()!=='migrate')
		{
			$user =Yii::app()->user->getState('_user');

			if(isset($user->userType) and ($user->userType == "Trainer" or $user->userType == "Practitioner1"))
			{
				$this->oldEmail=$this->parentEmail;
				$this->parentEmail = $user->email;
				//CVarDumper::dump($this,10,true);exit;
			}
//			else
//				$this->parentEmail = !empty($user)?$user->email:$this->parentEmail;
		}

		return parent::beforeSave();
	}

	public function afterSave()
	{
		if(!empty($this->oldEmail) and $this->oldEmail != $this->email and ($this->userType == "Trainer" or $this->userType == "Practitioner1"))
		{
			Users::model()->updateAll(['parentEmail'=>$this->email],'parentEmail = "'.$this->oldEmail.'"');
		}

		return parent::afterSave();
	}
	function delete()
    {
        $user = Yii::app()->user->getState('_user');
        if("Admin"==$user->userType)
        {
            $trans = Yii::app()->db->beginTransaction();

            try
            {
                Licences::model()->deleteAllByAttributes(['idUsers'=>$this->id]);

                HormoneUserDate::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                Measurement::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                UserProduct::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                UsersCourses::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                Purchase::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                WeightHeight::model()->deleteAllByAttributes(['idUser'=>$this->id]);

/*                if( $this->executions !== array() )//by relationName I mean the name of the relation between the models
                {
                    foreach ($this->executions as $execution) {
                        ExecutionExercise::model()->deleteAllByAttributes(['idExecution'=>$execution->id]);
                        ExecutionExerciseCalc::model()->deleteAllByAttributes(['idExecution'=>$execution->id]);
                        if(!empty($execution->idUserPhase0) and is_a($execution->idUserPhase0 ,'UsersPhase'))
                        {
                            UserPhaseExercise::model()->deleteAllByAttributes(['idUserPhase'=>$execution->idUserPhase0->id]);
                        }
                    }
                }*/

                if($this->usersPhases!==array())
                {
                    foreach ($this->usersPhases as $usersPhase) {
//                        $usersPhase=new UsersPhase();

                        if(is_array($usersPhase->userPhaseExercises) and $usersPhase->userPhaseExercises!==array())
                        {
                            foreach ($usersPhase->userPhaseExercises as $userPhaseExercise)
                            {
//                                $userPhaseExercise = new UserPhaseExercise();
                                ExecutionExercise::model()->deleteAllByAttributes(['idUserPhaseExercise'=>$userPhaseExercise->id]);
                                ExecutionExerciseCalc::model()->deleteAllByAttributes(['idUserPhaseExercice'=>$userPhaseExercise->id]);
                            }
                        }
                        UserPhaseExercise::model()->deleteAllByAttributes(['idUserPhase'=>$usersPhase->id]);
                    }
                }
                Execution::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                UsersPhase::model()->deleteAllByAttributes(['idUser'=>$this->id]);

                /**
                 * agregar el registro a la tabla de usuarios borrados
                 */

                $this->logUser($user);

                /**
                 * si es un practitioner crear un usuario ficticio y asignarle todos los usuarios del practitioner
                 * que se va a borrar
                 */
                if($this->userType == "Trainer" or $this->userType == "Practitioner1")
                {
                    $this->practitionerTemp();
                }

                parent::delete();
                Attachment::model()->deleteByPk($this->idAttachment);

                $trans->commit();
            }
            catch (Exception $e)
            {
                echo $e->getMessage();
                $trans->rollback();
            }
        }
        else
        {
            if( $this->executions !== array() )//by relationName I mean the name of the relation between the models
            {
                throw new CDbException('The client cannot be deleted because it\'s linked to an execution');
                return false;
            }
            if( $this->usersPhases !== array() )//by relationName I mean the name of the relation between the models
            {
                throw new CDbException('The client cannot be deleted because it\'s linked to a phase');
                return false;
            }
            if( $this->licences !== array() )//by relationName I mean the name of the relation between the models
            {
                throw new CDbException('The client cannot be deleted because it\'s linked to a licence');
                return false;
            }
        }

    }

    /**
     * The followings are the available model relations:
     * @property Execution[] $executions
     * @property HormoneUserDate[] $hormoneUserDates
     * @property Licences[] $licences
     * @property Measurement[] $measurements
     * @property Purchase[] $purchases
     * @property UserProduct[] $userProducts
     * @property Attachment $idAttachment0
     * @property Country $idCountry0
     * @property UsersCourses[] $usersCourses
     * @property UsersPhase[] $usersPhases
     * @property WeightHeight[] $weightHeights
     */
    /*function beforeDelete(){
        $user = Yii::app()->user->getState('_user');
        if("Admin"==$user->userType)
        {
            $trans = Yii::app()->db->beginTransaction();

            try
            {
                Licences::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                Attachment::model()->deleteByPk($this->idAttachment);

                HormoneUserDate::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                Measurement::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                UserProduct::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                UsersCourses::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                Purchase::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                WeightHeight::model()->deleteAllByAttributes(['idUser'=>$this->id]);

                if( $this->executions !== array() )//by relationName I mean the name of the relation between the models
                {
                    foreach ($this->executions as $execution) {
                        ExecutionExercise::model()->deleteAllByAttributes(['idExecution'=>$execution->id]);
                        ExecutionExerciseCalc::model()->deleteAllByAttributes(['idExecution'=>$execution->id]);
                        if(!empty($execution->idUserPhase0) and is_a($execution->idUserPhase0 ,'UsersPhase'))
                        {
                            UserPhaseExercise::model()->deleteAllByAttributes(['idUserPhase'=>$execution->idUserPhase0->id]);
                        }
                    }
                }
                Execution::model()->deleteAllByAttributes(['idUsers'=>$this->id]);
                UsersPhase::model()->deleteAllByAttributes(['idUser'=>$this->id]);

                /**
                 * agregar el registro a la tabla de usuarios borrados
                 * /

                $this->logUser($user);

                /**
                 * si es un practitioner crear un usuario ficticio y asignarle todos los usuarios del practitioner
                 * que se va a borrar
                 * /
                if($this->userType == "Trainer")
                {
                    $this->practitionerTemp();
                }



                $trans->commit();
            }
            catch (Exception $e)
            {
                echo $e->getMessage();
                $trans->rollback();
            }
        }
        else
        {
            if( $this->executions !== array() )//by relationName I mean the name of the relation between the models
            {
                throw new CDbException('The client cannot be deleted because it\'s linked to an execution');
                return false;
            }
            if( $this->usersPhases !== array() )//by relationName I mean the name of the relation between the models
            {
                throw new CDbException('The client cannot be deleted because it\'s linked to a phase');
                return false;
            }
            if( $this->licences !== array() )//by relationName I mean the name of the relation between the models
            {
                throw new CDbException('The client cannot be deleted because it\'s linked to a licence');
                return false;
            }
        }
        return parent::beforeDelete();
    }*/

    /**
     * @return int
     */
    public function logUser(Users $user)
    {
        $userDelete = new UsersDelete();
        $userDelete->id = $this->id;
        $userDelete->email = $this->email;
        $userDelete->deleted_date = date('Y-m-d H:i:s');
        $userDelete->userType = $this->userType;
        $userDelete->idUserDelete = $user->id;
        $userDelete->emailAdminDelete = $user->email;
        $userDelete->save();
    }
    public function practitionerTemp()
    {
        $email =$this->email;

        $contador = Users::model()->findBySql('select * from users WHERE email LIKE \'%@temporal.com\' ORDER BY id DESC LIMIT 1');
        if(!empty($contador->email))
        {
            $contador=$contador->email;
            $contador = str_replace('@temporal.com','',$contador);
            $contador = str_replace('temporal','',$contador);
            $contador = $contador + 1;
        }
        else
        {
            $contador = 1;
        }


        $temp=new Users();

        $temp->email = 'temporal'.$contador.'@temporal.com';
        $temp->bornDate = '1900-01-01';
        $temp->parentEmail = $temp->email;
        $temp->password = CPasswordHelper::hashPassword(uniqid());
        $temp->create_record = date('Y-m-d H:i:s');
        $temp->userType = $this->userType;
        $temp->name = "Temporal".$contador;
        $temp->lastName = "Temporal".$contador;
        $temp->save(false);

        Users::model()->updateAll(['parentEmail'=>$temp->email],'parentEmail=:email',[':email'=>$email]);
    }

}
