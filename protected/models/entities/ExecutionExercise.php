<?php

/**
 * This is the model class for table "execution_exercise".
 *
 * The followings are the available columns in table 'execution_exercise':
 * @property integer $id
 * @property integer $idExecution
 * @property integer $idUserPhaseExercise
 * @property integer $idExecutionExerciseCalc
 * @property double $weigth
 * @property integer $repetitions
 * @property string $weigthModificate
 * @property integer $dropoff
 *
 * The followings are the available model relations:
 * @property ExecutionExerciseCalc $idExecutionExerciseCalc0
 * @property Execution $idExecution0
 * @property UserPhaseExercise $idUserPhaseExercise0
 */
class ExecutionExercise extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'execution_exercise';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idExecution, idUserPhaseExercise, idExecutionExerciseCalc, repetitions', 'numerical', 'integerOnly'=>true),
			array('weigth, weigthModificate', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idExecution, idUserPhaseExercise, idExecutionExerciseCalc, weigth, repetitions, weigthModificate,dropoff', 'safe'),
			array('id, idExecution, idUserPhaseExercise, idExecutionExerciseCalc, weigth, repetitions, weigthModificate,dropoff', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idExecutionExerciseCalc0' => array(self::BELONGS_TO, 'ExecutionExerciseCalc', 'idExecutionExerciseCalc'),
			'idExecution0' => array(self::BELONGS_TO, 'Execution', 'idExecution'),
			'idUserPhaseExercise0' => array(self::BELONGS_TO, 'UserPhaseExercise', 'idUserPhaseExercise'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idExecution' => 'Id Execution',
			'idUserPhaseExercise' => 'Id User Phase Exercise',
			'idExecutionExerciseCalc' => 'Id Execution Exercise Calc',
			'weigth' => 'Weigth',
			'repetitions' => 'Repetitions',
			'weigthModificate' => 'Weigth Modificate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idExecution',$this->idExecution);
		$criteria->compare('idUserPhaseExercise',$this->idUserPhaseExercise);
		$criteria->compare('idExecutionExerciseCalc',$this->idExecutionExerciseCalc);
		$criteria->compare('weigth',$this->weigth,true);
		$criteria->compare('repetitions',$this->repetitions);
		$criteria->compare('weigthModificate',$this->weigthModificate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ExecutionExercise the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
