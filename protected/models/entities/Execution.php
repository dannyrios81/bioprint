<?php

/**
 * This is the model class for table "execution".
 *
 * The followings are the available columns in table 'execution':
 * @property integer $id
 * @property integer $idUserPhase
 * @property string $date
 * @property string $unitType
 * @property string $weigth
 * @property integer $idUsers
 *
 * The followings are the available model relations:
 * @property Users $idUsers0
 * @property UsersPhase $idUserPhase0
 * @property ExecutionExercise[] $executionExercises
 * @property ExecutionExerciseCalc[] $executionExerciseCalcs
 */
class Execution extends CActiveRecord
{
    public $variationweigth;
    public $variationreps;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'execution';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date', 'required'),
			array('idUserPhase, idUsers', 'numerical', 'integerOnly'=>true),
			array('unitType', 'length', 'max'=>50),
			array('weigth', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
            array('id, idUserPhase, date, unitType, weigth, idUsers', 'safe'),
			array('id, idUserPhase, date, unitType, weigth, idUsers', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idUsers0' => array(self::BELONGS_TO, 'Users', 'idUsers'),
			'idUserPhase0' => array(self::BELONGS_TO, 'UsersPhase', 'idUserPhase'),
			'executionExercises' => array(self::HAS_MANY, 'ExecutionExercise', 'idExecution'),
			'executionExerciseCalcs' => array(self::HAS_MANY, 'ExecutionExerciseCalc', 'idExecution'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idUserPhase' => 'Id User Phase',
			'date' => 'Date',
			'unitType' => 'Units',
			'weigth' => 'Weight',
			'idUsers' => 'Id Users',
            'variationweigth' => '&#916; Weigth',
            'variationreps' => '&#916; Reps',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idUserPhase',$this->idUserPhase);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('unitType',$this->unitType,true);
		$criteria->compare('weigth',$this->weigth,true);
		$criteria->compare('idUsers',$this->idUsers);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Execution the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


}
