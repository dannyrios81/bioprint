<?php

/**
 * This is the model class for table "family_exercise".
 *
 * The followings are the available columns in table 'family_exercise':
 * @property integer $id
 * @property string $description
 * @property integer $bodyAreaId
 *
 * The followings are the available model relations:
 * @property Exercise[] $exercises
 * @property BodyArea $bodyArea
 * @property Orientation[] $orientations
 */
class FamilyExercise extends CActiveRecord
{
    public $idMethod;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'family_exercise';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('description', 'filter', 'filter'=>'trim'),
//            array('description','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('bodyAreaId, idMethod', 'numerical', 'integerOnly'=>true),
            array('description,bodyAreaId, idMethod', 'required'),
            array('description', 'length', 'max'=>255),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]]),
            array('bodyAreaId, description', 'ECompositeUniqueValidator','allowEmpty'=>false,'attributeNames'=>'bodyAreaId, description','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'insert','attributesToAddError'=>'description','message'=>'The description "{value_description}" already exists for this {attr_bodyAreaId}.'),
            array('bodyAreaId, description', 'ECompositeUniqueValidator','allowEmpty'=>false,'attributeNames'=>'bodyAreaId, description','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]],'attributesToAddError'=>'description','message'=>'The description "{value_description}" already exists for this {attr_bodyAreaId}.'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, description, bodyAreaId, idMethod', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'exercises' => array(self::HAS_MANY, 'Exercise', 'idFamilyExercise'),
			'bodyArea' => array(self::BELONGS_TO, 'BodyArea', 'bodyAreaId'),
			'orientations' => array(self::HAS_MANY, 'Orientation', 'idFamily_exercise'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
			'bodyAreaId' => 'Body Area',
            'idMethod' => 'Strength Quality',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $with =[];

        if(!empty($this->idMethod))
        {

            $with[]='bodyArea.idMethod0';
            $criteria->compare('idMethod0.id',$this->idMethod);
        }

        if(count($with)>0)
            $criteria->with=$with;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('bodyAreaId',$this->bodyAreaId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FamilyExercise the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    function beforeDelete(){
        /**
         * @property Grip[] $grips
         * @property Implement[] $implements
         * @property Orientation[] $orientations
         */
        if( $this->exercises !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The exercise family cannot be deleted because it\'s linked to an exercise');
            return false;
        }
        if( $this->orientations !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The exercise family cannot be deleted because it\'s linked to an orientation');
            return false;
        }
        return parent::beforeDelete();
    }
    function afterFind(){

        $this->idMethod=$this->bodyArea->idMethod0->id;

        return parent::afterFind();
    }
}
