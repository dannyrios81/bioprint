<?php

/**
 * This is the model class for table "implement".
 *
 * The followings are the available columns in table 'implement':
 * @property integer $id
 * @property string $description
 * @property integer $idOrientation
 *
 * The followings are the available model relations:
 * @property Exercise[] $exercises
 * @property Grip[] $grips
 * @property Orientation $idOrientation0
 */
class Implement extends CActiveRecord
{
    public $idMethod;
    public $idBodyArea;
    public $idFamilyExercise;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'implement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('description', 'filter', 'filter'=>'trim'),
//            array('description','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('idOrientation, idMethod, idBodyArea, idFamilyExercise', 'numerical', 'integerOnly'=>true),
            array('description,idOrientation, idMethod, idBodyArea, idFamilyExercise', 'required'),
            array('description', 'length', 'max'=>255),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]]),
            array('description,idOrientation', 'ECompositeUniqueValidator','allowEmpty'=>false,'attributeNames'=>'description,idOrientation','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'insert','attributesToAddError'=>'description','message'=>'The description "{value_description}" already exists for this {attr_idOrientation}.'),
            array('description,idOrientation', 'ECompositeUniqueValidator','allowEmpty'=>false,'attributeNames'=>'description,idOrientation','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]],'attributesToAddError'=>'description','message'=>'The description "{value_description}" already exists for this {attr_idOrientation}.'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, description, idOrientation, idMethod, idBodyArea, idFamilyExercise', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'exercises' => array(self::HAS_MANY, 'Exercise', 'idImplement'),
            'grips' => array(self::HAS_MANY, 'Grip', 'idImplement'),
            'idOrientation0' => array(self::BELONGS_TO, 'Orientation', 'idOrientation'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
            'idOrientation' => 'Orientation',
            'idMethod' => 'Strength Quality',
            'idBodyArea' => 'Body Area',
            'idFamilyExercise' => 'Exercise Family',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $with =[];

        if(!empty($this->idMethod))
        {

            $with[]='idOrientation0.idFamilyExercise.bodyArea.idMethod0';
            $criteria->compare('idMethod0.id',$this->idMethod);
        }
        if(!empty($this->idBodyArea))
        {
            $with[]='idOrientation0.idFamilyExercise.bodyArea';
            $criteria->compare('bodyArea.id',$this->idBodyArea);
        }
        if(!empty($this->idFamilyExercise))
        {
            $with[]='idOrientation0.idFamilyExercise';
            $criteria->compare('idFamilyExercise.id',$this->idFamilyExercise);
        }

        if(count($with)>0)
            $criteria->with=$with;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);
        $criteria->compare('idOrientation',$this->idOrientation);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Implement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    function beforeDelete(){
        if( $this->exercises !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The implement cannot be deleted because it\'s linked to an exercise');
            return false;
        }
        if( $this->grips !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The implement cannot be deleted because it\'s linked to a grip');
            return false;
        }
        return parent::beforeDelete();
    }
    function afterFind(){

        $this->idMethod=$this->idOrientation0->idFamilyExercise->bodyArea->idMethod0->id;
        $this->idBodyArea=$this->idOrientation0->idFamilyExercise->bodyArea->id;
        $this->idFamilyExercise=$this->idOrientation0->idFamilyExercise->id;

        return parent::afterFind();
    }
}
