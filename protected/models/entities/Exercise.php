<?php

/**
 * This is the model class for table "exercise".
 *
 * The followings are the available columns in table 'exercise':
 * @property integer $id
 * @property string $description
 * @property integer $idMethod
 * @property integer $idBodyArea
 * @property integer $idFamilyExercise
 * @property integer $idOrientation
 * @property integer $idImplement
 * @property integer $idGrip
 * @property integer $idLaterality
 * @property string $notes
 * @property string $video
 * @property integer $idRangeMotion
 *
 * The followings are the available model relations:
 * @property BodyArea $idBodyArea0
 * @property FamilyExercise $idFamilyExercise0
 * @property Grip $idGrip0
 * @property Implement $idImplement0
 * @property Laterality $idLaterality0
 * @property Method $idMethod0
 * @property Orientation $idOrientation0
 * @property UserPhaseExercise[] $userPhaseExercises
 * @property RangeMotion $idRangeMotion0
 */
class Exercise extends CActiveRecord
{
    public $BodyAreadescription;
    public $FamilyExercisedescription;
    public $Gripdescription;
    public $Implementdescription;
    public $Lateralitydescription;
    public $Methoddescription;
    public $Orientationdescription;
    public $RangeMotiondescription;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'exercise';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idMethod, idBodyArea, idFamilyExercise, idOrientation, idImplement, idGrip, idLaterality, idRangeMotion', 'numerical', 'integerOnly'=>true),
			array('video', 'length', 'max'=>255),
            array('description', 'required'),
            array('description', 'length', 'max'=>255),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]]),
			array('notes', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, description, idMethod, idBodyArea, idFamilyExercise, idOrientation, idImplement, idGrip, idLaterality, notes, video, idRangeMotion', 'safe', 'on'=>'search'),
			array('id, description, BodyAreadescription, FamilyExercisedescription, Gripdescription, Implementdescription, Lateralitydescription, Methoddescription, Orientationdescription, notes, video, RangeMotiondescription', 'safe', 'on'=>'searchSql'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idBodyArea0' => array(self::BELONGS_TO, 'BodyArea', 'idBodyArea'),
			'idFamilyExercise0' => array(self::BELONGS_TO, 'FamilyExercise', 'idFamilyExercise'),
			'idGrip0' => array(self::BELONGS_TO, 'Grip', 'idGrip'),
			'idImplement0' => array(self::BELONGS_TO, 'Implement', 'idImplement'),
			'idLaterality0' => array(self::BELONGS_TO, 'Laterality', 'idLaterality'),
			'idMethod0' => array(self::BELONGS_TO, 'Method', 'idMethod'),
			'idOrientation0' => array(self::BELONGS_TO, 'Orientation', 'idOrientation'),
			'userPhaseExercises' => array(self::HAS_MANY, 'UserPhaseExercise', 'idExercise'),
            'idRangeMotion0' => array(self::BELONGS_TO,'RangeMotion', 'idRangeMotion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Exercise Name',
			'idMethod' => 'Strength Quality',
			'idBodyArea' => 'Body Area',
			'idFamilyExercise' => 'Exercise Family',
			'idOrientation' => 'Orientation',
			'idImplement' => 'Implement',
			'idGrip' => 'Grip/Stance',
			'idLaterality' => 'Laterality',
			'notes' => 'Notes',
			'video' => 'Video',
            'idRangeMotion' => 'Range',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;
//        $criteria->together=true;
        $criteria->with=[
            'idBodyArea0',
            'idFamilyExercise0',
            'idGrip0',
            'idImplement0',
            'idLaterality0',
            'idMethod0',
            'idOrientation0',
            'userPhaseExercises',
            'idRangeMotion0'
        ];

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.description',$this->description,true);
		$criteria->compare('t.idMethod',$this->idMethod);
		$criteria->compare('t.idBodyArea',$this->idBodyArea);
		$criteria->compare('t.idFamilyExercise',$this->idFamilyExercise);
		$criteria->compare('t.idOrientation',$this->idOrientation);
		$criteria->compare('t.idImplement',$this->idImplement);
		$criteria->compare('t.idGrip',$this->idGrip);
		$criteria->compare('t.idLaterality',$this->idLaterality);
		$criteria->compare('t.notes',$this->notes,true);
		$criteria->compare('t.video',$this->video,true);
        $criteria->compare('t.idRangeMotion',$this->idRangeMotion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function searchSql()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.description',$this->description,true);
        $criteria->compare('idMethod0.description',$this->Methoddescription);
        $criteria->compare('idBodyArea0.description',$this->BodyAreadescription);
        $criteria->compare('idFamilyExercise0.description',$this->FamilyExercisedescription);
        $criteria->compare('idOrientation0.description',$this->Orientationdescription);
        $criteria->compare('idImplement0.description',$this->Implementdescription);
        $criteria->compare('idGrip0.description',$this->Gripdescription);
        $criteria->compare('idLaterality0.description',$this->Lateralitydescription);
        $criteria->compare('t.notes',$this->notes,true);
        $criteria->compare('t.video',$this->video,true);
        $criteria->compare('idRangeMotion0.description',$this->RangeMotiondescription);

        $count=Yii::app()->db->createCommand()
            ->select("count(DISTINCT t.id)")
            ->from("exercise t")
            ->join("family_exercise idFamilyExercise0"," t.idFamilyExercise = idFamilyExercise0.id")
            ->join("body_area idBodyArea0","idFamilyExercise0.bodyAreaId = idBodyArea0.id")
            ->join("method idMethod0"," idBodyArea0.idMethod = idMethod0.id")
            ->join("grip idGrip0"," t.idGrip = idGrip0.id")
            ->join("implement idImplement0","t.idImplement = idImplement0.id")
            ->join("laterality idLaterality0","t.idLaterality = idLaterality0.id")
            ->join("orientation idOrientation0","idImplement0.idOrientation = idOrientation0.id")
            ->join("range_motion idRangeMotion0","t.idRangeMotion = idRangeMotion0.id")
            ->where($criteria->condition,$criteria->params)->queryScalar();

        $sql=Yii::app()->db->createCommand()
            ->select("
                t.id AS idExercise,
                t.description AS exerciseDescription,
                t.idMethod AS idMethod,
                t.idBodyArea AS idBodyArea,
                t.idFamilyExercise AS idFamilyExercise,
                t.idOrientation AS idOrientation,
                t.idImplement AS idImplement,
                t.idGrip AS idGrip,
                t.idLaterality AS idLaterality,
                t.notes AS notes,
                t.video AS video,
                t.idRangeMotion AS idRangeMotion,
                idBodyArea0.description AS bodyAreaDescription,
                idFamilyExercise0.description AS familyExerciseDescription,
                idGrip0.description AS gripDescription,
                idImplement0.description AS implementDescription,
                idLaterality0.description AS lateralityDescription,
                idMethod0.description AS methodDescription,
                idOrientation0.description AS orientationDescription,
                idRangeMotion0.description AS rangeMotionDescription
            ")
            ->from("exercise t")
            ->join("family_exercise idFamilyExercise0"," t.idFamilyExercise = idFamilyExercise0.id")
            ->join("body_area idBodyArea0","idFamilyExercise0.bodyAreaId = idBodyArea0.id")
            ->join("method idMethod0"," idBodyArea0.idMethod = idMethod0.id")
            ->join("grip idGrip0"," t.idGrip = idGrip0.id")
            ->join("implement idImplement0","t.idImplement = idImplement0.id")
            ->join("laterality idLaterality0","t.idLaterality = idLaterality0.id")
            ->join("orientation idOrientation0","idImplement0.idOrientation = idOrientation0.id")
            ->join("range_motion idRangeMotion0","t.idRangeMotion = idRangeMotion0.id")
            ->where($criteria->condition,$criteria->params);


        return new CSqlDataProvider($sql, array(
            'keyField' => 'idExercise',
            'totalItemCount'=>$count,
            'sort' => array(
                'defaultOrder' => array(
                    'exerciseDescription' => CSort::SORT_ASC,
                ),
            ),
            'pagination'=>array(
                'pageSize'=>20, //Show all records
            ),
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Exercise the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    function beforeDelete(){
        if( $this->userPhaseExercises !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The exercise cannot be deleted becasue it\'s linked to an user phase');
            return false;
        }
        return parent::beforeDelete();
    }
}
