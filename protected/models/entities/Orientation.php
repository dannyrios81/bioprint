<?php

/**
 * This is the model class for table "orientation".
 *
 * The followings are the available columns in table 'orientation':
 * @property integer $id
 * @property string $description
 * @property integer $idFamily_exercise
 *
 * The followings are the available model relations:
 * @property Exercise[] $exercises
 * @property Implement[] $implements
 * @property FamilyExercise $idFamilyExercise
 */
class Orientation extends CActiveRecord
{
    public $idMethod;
    public $idBodyArea;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orientation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('description', 'filter', 'filter'=>'trim'),
//            array('description','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			array('idFamily_exercise, idMethod, idBodyArea', 'numerical', 'integerOnly'=>true),
            array('description,idFamily_exercise, idMethod, idBodyArea', 'required'),
            array('description', 'length', 'max'=>255),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__),
//            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]]),
            array('description,idFamily_exercise', 'ECompositeUniqueValidator','allowEmpty'=>false,'attributeNames'=>'description,idFamily_exercise','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'insert','attributesToAddError'=>'description','message'=>'The description "{value_description}" already exists for this {attr_idFamily_exercise}.'),
            array('description,idFamily_exercise', 'ECompositeUniqueValidator','allowEmpty'=>false,'attributeNames'=>'description,idFamily_exercise','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','attributesToAddError'=>'description','message'=>'The description "{value_description}" already exists for this {attr_idFamily_exercise}.','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]]),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, description, idFamily_exercise, idMethod, idBodyArea', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'exercises' => array(self::HAS_MANY, 'Exercise', 'idOrientation'),
            'implements' => array(self::HAS_MANY, 'Implement', 'idOrientation'),
			'idFamilyExercise' => array(self::BELONGS_TO, 'FamilyExercise', 'idFamily_exercise'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
			'idFamily_exercise' => 'Exercise Family',
            'idMethod' => 'Strength Quality',
            'idBodyArea' => 'Body Area',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $with =[];

        if(!empty($this->idMethod))
        {

            $with[]='idFamilyExercise.bodyArea.idMethod0';
            $criteria->compare('idMethod0.id',$this->idMethod);
        }
        if(!empty($this->idBodyArea))
        {
            $with[]='idFamilyExercise.bodyArea';
            $criteria->compare('bodyArea.id',$this->idBodyArea);
        }


        if(count($with)>0)
            $criteria->with=$with;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);
        $criteria->compare('idFamily_exercise',$this->idFamily_exercise);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orientation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    function beforeDelete(){
        if( $this->exercises !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The orientation cannot be deleted because it\'s linked to an exercise');
            return false;
        }
        if( $this->implements !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The orientation cannot be deleted because it\'s linked to an implement');
            return false;
        }
        return parent::beforeDelete();
    }
    function afterFind(){

        $this->idMethod=$this->idFamilyExercise->bodyArea->idMethod0->id;
        $this->idBodyArea=$this->idFamilyExercise->bodyArea->id;

        return parent::afterFind();
    }
}
