<?php

/**
 * This is the model class for table "laterality".
 *
 * The followings are the available columns in table 'laterality':
 * @property integer $id
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Exercise[] $exercises
 */
class Laterality extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laterality';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('description', 'required'),
            array('description', 'length', 'max'=>255),
            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__),
            array('description', 'unique', 'allowEmpty'=>false,'attributeName'=>'description','caseSensitive'=>false,'className'=>__CLASS__,'on'=>'update','criteria'=>['condition'=>'id <> :id','params'=>['id'=>$this->id]]),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'exercises' => array(self::HAS_MANY, 'Exercise', 'idLaterality'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Laterality the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    function beforeDelete(){
        if( $this->exercises !== array() )//by relationName I mean the name of the relation between the models
        {
            throw new CDbException('The laterality cannot be deleted because it\'s linked to an exercise');
            return false;
        }
        return parent::beforeDelete();
    }
}
