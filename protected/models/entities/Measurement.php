<?php

/**
 * This is the model class for table "measurement".
 *
 * The followings are the available columns in table 'measurement':
 * @property integer $idUsers
 * @property string $date
 * @property double $measurement
 * @property integer $id
 * @property integer $idMeasurementType
 * @property integer $migrate
 *
 * The followings are the available model relations:
 * @property MeasurementTypes $idMeasurementType0
 * @property Users $idUsers0
 */
class Measurement extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'measurement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, idMeasurementType', 'required'),
			array('idUsers, idMeasurementType,migrate', 'numerical', 'integerOnly'=>true),
			array('measurement', 'numerical', 'integerOnly'=>false),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idUsers, date, measurement, id, idMeasurementType, migrate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMeasurementType0' => array(self::BELONGS_TO, 'MeasurementTypes', 'idMeasurementType'),
			'idUsers0' => array(self::BELONGS_TO, 'Users', 'idUsers'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idUsers' => 'Id Users',
			'date' => 'Date',
			'measurement' => 'Measurement',
			'id' => 'ID',
			'idMeasurementType' => 'Id Measurement Type',
            'migrate' => 'Migrate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUsers',$this->idUsers);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('measurement',$this->measurement);
		$criteria->compare('id',$this->id);
		$criteria->compare('idMeasurementType',$this->idMeasurementType);
        $criteria->compare('migrate',$this->migrate);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Measurement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
