<?php echo '<?php'; ?>
 
class <?php echo ucfirst($this->className); ?>Form extends CFormModel
{
	<?php foreach($fields as $field): ?>
    public $<?php echo $field ?>;
    <?php endforeach; ?>
    
    public function rules()
	{
		return array(
			array('<?php echo $allFields ?>', 'required'),
		);
	}
    public function attributeLabels()
	{
		return array(
			<?php foreach($fields as $field): ?>
			'<?php echo $field ?>'=>'<?php echo $field ?>',
			<?php endforeach; ?>
		);
	}
	public function init()
    {
        parent::init();
    }
    public function __construct()
    {
        parent::__construct();
    }
	
}
