<?php
class FormModelCode extends CCodeModel
{
    public $className;
    public $fields;
    public $modelPath='application.models.formModels.';
 
    public function rules()
    {
        return array_merge(parent::rules(), array(
            array('className,fields', 'required'),
            array('className', 'match', 'pattern'=>'/^\w+$/'),
        ));
    }
 
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), array(
            'className'=>'Form Model Class Name',
            'fields'=>'Fields in the form',
        ));
    }
 
    public function prepare()
    {
		$params = array('fields'=>explode(',',$this->fields),'allFields'=>$this->fields);
		
        $path=Yii::getPathOfAlias($this->modelPath . ucfirst($this->className)) . 'Form.php';
        $code=$this->render($this->templatepath.'/formModel.php',$params);
 
        $this->files[]=new CCodeFile($path, $code);
    }
}
