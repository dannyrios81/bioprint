<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Bioprint',
//    'theme'=>'default',
    'aliases' => array(
        'vendor' =>__DIR__ . '/../vendor',
    ),

    'import'=>array(
        'application.models.entities.*',
        'application.models.formModels.*',
        'application.components.*',
        'vendor.dannyrios81.yii-mail.YiiMailMessage',
    ),

	// preloading 'log' component
	'preload'=>array('log'),
    'commandMap' => array(
        'cron' => 'vendor.yiivgeny.yii-phpdoc-crontab.PHPDocCrontab'
    ),
	// application components
	'components'=>array(

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
        'mail' => array(
            /*
             * metabolicsoftware@strengthsensei.com
             * Metsoft001
             */
            'class' => 'vendor.dannyrios81.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'smtp.gmail.com',
                'username' => 'software@strength-community.com',
                'password' => 'chcjehcqwjtbmket',
                'port' => '465',
                'encryption'=>'ssl',
            ),
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false
        ),

	),
);
