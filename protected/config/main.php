<?php
//print_r(mcrypt_module_get_algo_key_size('aes'));exit;
require(__DIR__ . '/../vendor/autoload.php');
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'S&M Software',
	'defaultController' => 'Site/Login',
    'language'=>'en',
    'sourceLanguage'=>'en',
    'aliases' => array(
			'vendor' =>__DIR__ . '/../vendor',
	),

	// preloading 'log' component
	'preload'=>array('log','UnderConstruction'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.entities.*',
        'application.models.formModels.*',
		'application.components.*',
		'vendor.dannyrios81.yii-mail.YiiMailMessage',
		'vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget',
        'vendor.pentium10.yii-remember-filters-gridview.components.ERememberFiltersBehavior',
        'vendor.dannyrios81.yii-clear-filters-gridview-fork.components.EButtonColumnWithClearFilters',
        'ext.ECompositeUniqueValidator.ECompositeUniqueValidator',
//		'vendor.miloschuman.yii-highcharts.highcharts',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123456',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','192.168.1.149','192.168.1.60','192.168.1.121','::1'),
		),
	),

	// application components
	'components'=>array(
		'clientScript' => array(

			// disable default yii scripts
			'scriptMap' => array(
				'jquery.js'     => '//code.jquery.com/jquery-1.11.3.min.js',
//				'jquery.min.js' => false,
//				'core.css'      => false,
//				'styles.css'    => false,
//				'pager.css'     => false,
//				'default.css'   => false,
			),
		),

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>false,
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'urlSuffix'=>'.html',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__) . '/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				array(
			                'class'=>'CFileLogRoute',
                    			'categories'=>'ext.yii-mail.YiiMail',
                			'logFile' => 'Mail.log',
		                ),
				// uncomment the following to show log messages on web pages

				/*array(
					'class'=>'CWebLogRoute',
				),*/
				
			),
		),
		'mail' => array(
			/*
			 * metabolicsoftware@strengthsensei.com
			 * Metsoft001
			 */
			'class' => 'vendor.dannyrios81.yii-mail.YiiMail',
			'transportType' => 'smtp',
			'transportOptions' => array(
				'host' => 'smtp.gmail.com',
				'username' => 'software@strength-community.com',
				'password' => 'chcjehcqwjtbmket',
				'port' => '465',
				'encryption'=>'ssl',
			),
			'viewPath' => 'application.views.mail',
			'logging' => true,
			'dryRun' => false
		),
        'UnderConstruction' => array(
			'class' => 'application.components.UnderConstruction',
			'allowedIPs'=>array('186.154.134.94','115.189.95.216','116.12.57.81'), //whatever IPs you want to allow
			'locked'=>false,//this is the on off switch
			'redirectURL'=>'http://www.google.com/',//put in your desired redirect page.
		),
		'securityManager'=>array(
			'cryptAlgorithm' => 'rijndael-128',
			'encryptionKey' => '0123456789ABCDEF',
		),
		'widgetFactory' => array(
			'widgets' => array(
				'CLinkPager' => array(
					'htmlOptions' => array(
						'class' => 'pagination'
					),
					'header' => false,
					'maxButtonCount' => 5,
					'cssFile' => false,
				),
				'CGridView' => array(
					'htmlOptions' => array(
						'class' => 'table-responsive'
					),
					'pagerCssClass' => 'dataTables_paginate paging_bootstrap',
					'itemsCssClass' => 'table table-striped table-hover',
					'cssFile' => false,
					'summaryCssClass' => 'dataTables_info',
					'summaryText' => 'Showing {start} to {end} of {count} entries',
					'template' => '{items}<div class="row"><div class="col-md-5 col-sm-12">{summary}</div><div class="col-md-7 col-sm-12">{pager}</div></div><br />',
				),
			),
		),
        'ePdf' => array(
            'class'         => 'vendor.borales.yii-pdf.EYiiPdf',
            'params'        => array(
                'mpdf'     => array(
                    'librarySourcePath' => 'application.vendor.mpdf.mpdf.*',
                    'constants'         => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                    ),
                    'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder.
                    /*'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
                        'mode'              => '', //  This parameter specifies the mode of the new document.
                        'format'            => 'A4', // format A4, A5, ...
                        'default_font_size' => 0, // Sets the default document font size in points (pt)
                        'default_font'      => '', // Sets the default font-family for the new document.
                        'mgl'               => 15, // margin_left. Sets the page margins for the new document.
                        'mgr'               => 15, // margin_right
                        'mgt'               => 16, // margin_top
                        'mgb'               => 16, // margin_bottom
                        'mgh'               => 9, // margin_header
                        'mgf'               => 9, // margin_footer
                        'orientation'       => 'P', // landscape or portrait orientation
                    )*/
                ),
                'HTML2PDF' => array(
                    'librarySourcePath' => 'application.vendor.ensepar.html2pdf.*',
                    'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap
                    /*'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
                        'orientation' => 'P', // landscape or portrait orientation
                        'format'      => 'A4', // format A4, A5, ...
                        'language'    => 'en', // language: fr, en, it ...
                        'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
                        'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
                        'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
                    )*/
                )
            ),
        ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'software@strength-community.com',
		'paypalPayEmail'=>'mariaferze1@hotmail.com',
		'idBiopriontProduct'=>1,
		'CodeProductBasic'=>'P0001',
		'CodeProductPhase'=>'P0002',
        'HormoneFamiliesWithPriority'=>[
            'HormoneFamilyIdCortisol'=>7,
            'HormoneFamilyIdTyroid'=>8,
        ],
	),
);
