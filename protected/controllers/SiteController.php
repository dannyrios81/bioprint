<?php

class SiteController extends Controller
{

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', array('error'=>$error));
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout= '//layouts/login';
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
//		CVarDumper::dump($_POST,10,true);exit;
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->createUrl('users/index'));
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	public function actionChangePassword($change=0)
	{
		$this->pageTitle = 'Change Password';
		// si el usuario no esta logueado se muestra el layout de login,
		// si esta logueado se muestra el que este por default
		if(Yii::app()->user->isGuest) {
			$this->layout= '//layouts/login';
		}
		$model = new ChangePasswordForm();
		$model->email =!empty($change)?Utilities::desencriptar($change):Yii::app()->user->getState('_user')->email;
        if(isset($_POST['ChangePasswordForm']))
        {
            $model->attributes=$_POST['ChangePasswordForm'];
            if($model->validate())
            {
				if(!Yii::app()->user->isGuest)
				{
					$this->redirect(Yii::app()->createUrl('users/profile'));
				}
				else
				{
					$this->redirect(Yii::app()->createUrl('site/login'));
				}
            }
        }

		$this->render('ChangePassword',['model'=>$model]);
	}
	public function actionForgotPassword()
	{
		$this->layout="//layouts/login";

//		CVarDumper::dump($this->layout,10,true);exit;
		$model=new ForgotPasswordForm();
		if(isset($_POST['ForgotPasswordForm']))
		{
			$model->attributes = $_POST['ForgotPasswordForm'];
			if($model->validate())
			{
				$params['renderParams']['url'] = Yii::app()->createAbsoluteUrl('site/ChangePassword',array('change'=>Utilities::encriptar($model->email)));
				$params['renderParams']['clave'] = $model->pass;
				$params['to'] = $model->email;

				Utilities::forgotMail($params);
				$model->email = '';
			}
		}
		$this->render('ForgotPassword',['model'=>$model]);
	}
}