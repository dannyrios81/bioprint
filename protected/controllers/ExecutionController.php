<?php

class ExecutionController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index','createExecution','update','lastExecution','validate','deleteExecution','validateCreateExecution'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
	public function actionIndex($id)
	{
        if(!empty($id))
        {
            $executions = $this->getExecution($id);
        }
        else
        {
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
		$this->render('index',['executions'=>$executions]);
	}
    /**
     * funcion para crear todos los registros necesarios para la ejecion de la rutina
     *
     * @param $id identificador de la userphase
     */
    public function actionCreateExecution($id)
    {
        /**
         * pasos a seguir
         * 0. validar si el ultimo registro de ejecucion esta vacio, si es asi se le actualiza la fecha y se usa ese.
         * 1. cargar el userphase del id pasado
         * 2. Consultar el peso mas actual del usuario
         * 3. crear un registro en la tabla execution sacando los datos del userphase
         * 4. por medio de las relaciones extraer todos los ejercicios que tiene la phase de usuario
         * 5. crear un registro de execution_exercise_calc por cada ejercicio que tenga la userphase
         * 6. crear tantos registros para cada ejercicio, tantas veces como indique el numero de series del ejercicio
         * 7. redireccionar a la accion de index con el id del registro de la ejecucion
         *
         */

        $variationweigth = $_GET['variationweigth'];
        $variationreps = $_GET['variationreps'];

        if(!empty($id))
        {
            $userPhase = UsersPhase::model()->findByPk($id);
            $validLastExecution = Calculos::lastExecutionPhase($id);

            if($userPhase->numberOfTimes<=$validLastExecution['totalExecutions'])
            {
                Yii::app()->user->setFlash('warning', "The client has performed the total number of executions planned for this phase");
                $this->redirect(array('fase/index','id'=>$userPhase->idUser));
            }
            $validLastExecution=Calculos::executionInfo($validLastExecution['id']);

            if(is_array($validLastExecution) and !is_null($validLastExecution['id']) and $validLastExecution['id']<>0 and ($validLastExecution['sumtotalreps']==0 or is_null($validLastExecution['sumtotalreps'])))
            {
                /** recupera la ultima ejecucion, **/
                /** le actualiza la fecha y lo carga para trabajar en el **/

                Yii::app()->user->setFlash('new','To create a new execution, you must fill this one. You can use the “Delete” Button to discard this one');

                $tempo = Execution::model()->findByPk($validLastExecution['id']);
                $tempo->date = date('Y-m-d H:i:s');
                $tempo->save(false);

//                $tempo = new Execution();
                /*foreach ($tempo->executionExercises as $executionExercise)
                {
                    $executionExercise->weigth = ((($variationweigth/100))*$executionExercise->weigth)+$executionExercise->weigth;
                    $executionExercise->repetitions = ((($variationreps/100))*$executionExercise->repetitions)+$executionExercise->repetitions;
                    $executionExercise->save();
                }*/

                $this->redirect(array('index','id'=>$validLastExecution['id']));
            }
            else
            {
                /*if(!empty($variationweigth) or !empty($variationreps))
                {*/
                    $criteria = new CDbCriteria;
                    $criteria->order='id DESC';
                    $criteria->condition='idUserPhase='.$id;
                    $lastExecution = Execution::model()->find($criteria);
                //}
                /** crea una nueva ejecucion **/
                /* 1. cargar el userphase del id pasado */
                $userPhase = UsersPhase::model()->findByPk($id);
//            $userPhase = new UsersPhase();

                /* 2. Consultar el peso mas actual del usuario */
                $weightInfo = Calculos::LastWeigth($userPhase->idUser);

                /* 3. crear un registro en la tabla execution sacando los datos del userphase */
                $execution = new Execution();
                $execution->idUserPhase=$userPhase->id;
                $execution->date = date('Y-m-d H:i:s');
                $execution->weigth=$weightInfo['weight'];
                $execution->unitType = $weightInfo['unitType'];
                $execution->idUsers = $userPhase->idUser;
                $execution->save();

//                CVarDumper::dump($lastExecution->executionExercises,10,true);
//                CVarDumper::dump($userPhase->userPhaseExercises,10,true);
//                exit();

                /* 4. por medio de las relaciones extraer todos los ejercicios que tiene la phase de usuario */
                foreach ($userPhase->userPhaseExercises as $item) {
//                $item = new UserPhaseExercise();

                    $eec = new ExecutionExerciseCalc();
                    $eec->idExecution = $execution->id;
                    $eec->idUserPhaseExercice=$item->id;
                    $eec->save(false);
                    for ($i=0;$i<count($lastExecution->executionExercises);$i++)
                    {
                        if(isset($lastExecution) and isset($lastExecution->executionExercises) and  is_array($lastExecution->executionExercises) and isset($lastExecution->executionExercises[$i]) and $item->id==$lastExecution->executionExercises[$i]->idUserPhaseExercise)
                        {
                            $ee=new ExecutionExercise();
                            $ee->idExecution=$execution->id;
                            $ee->idUserPhaseExercise = $item->id;
                            $ee->idExecutionExerciseCalc=$eec->id;

                            $ee->weigth = (!empty($variationweigth))?((($variationweigth/100))*$lastExecution->executionExercises[$i]->weigth)+$lastExecution->executionExercises[$i]->weigth:null;
                            $ee->repetitions = (!empty($variationreps))?$variationreps+$lastExecution->executionExercises[$i]->repetitions:'';
                            $ee->repetitions = $ee->repetitions<0?0:$ee->repetitions;
                            $ee->save(false);
                        }
//                        CVarDumper::dump($ee,10,true);exit;

                    }
                }
                $this->redirect(array('index','id'=>$execution->id));
            }
        }
        else
        {
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }

    }
    public function acrionValidateCreateExecution($id)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $jsonResponse=[];
            $userPhase = UsersPhase::model()->findByPk($id);
            $validLastExecution = Calculos::lastExecutionPhase($id);

            if($userPhase->numberOfTimes<=$validLastExecution['totalExecutions'])
            {
                $jsonResponse['message']='The client has performed the total number of executions planned for this phase';
                $jsonResponse['type']='warning';
                $jsonResponse['url']=Yii::app()->createAbsoluteUrl("fase/index",["id"=>$userPhase->idUser]);
            }
            $validLastExecution=Calculos::executionInfo($validLastExecution['id']);

            if(is_array($validLastExecution) and !is_null($validLastExecution['id']) and $validLastExecution['id']<>0 and ($validLastExecution['sumtotalreps']==0 or is_null($validLastExecution['sumtotalreps'])))
            {
                $jsonResponse['message']='To create a new execution, you must fill this one. You can use the “Delete” Button to discard this one';
                $jsonResponse['type']='info';
                $jsonResponse['url']=Yii::app()->createAbsoluteUrl("execution/index",["id"=>$validLastExecution['id']]);
            }
            else
            {
                $jsonResponse['message']='To create a new execution, you must fill this one. You can use the “Delete” Button to discard this one';
                $jsonResponse['type']='info';
                $jsonResponse['url']=Yii::app()->createAbsoluteUrl("execution/createExecution",["id"=>$id]);
            }
            echo CJSON::encode($jsonResponse);
            Yii::app()->end();
        }
    }
    public function actionDeleteExecution($id)
    {
        $trans = Yii::app()->db->beginTransaction();
        $response = [];
        try
        {
//            CVarDumper::dump($_REQUEST['idUserPhase'],10,true);exit;
            ExecutionExercise::model()->deleteAllByAttributes(['idExecution'=>$id]);
            ExecutionExerciseCalc::model()->deleteAllByAttributes(['idExecution'=>$id]);
            Execution::model()->deleteByPk($id);

            $trans->commit();

            $info = Calculos::lastExecutionPhase($_REQUEST['idUserPhase']);

            if($info!==array() and isset($info["totalExecutions"]) and $info["totalExecutions"]<=0)
                $response['url']=Yii::app()->createAbsoluteUrl("fase/index",["id"=>$_REQUEST['idUser']]);
            else
                $response['url']=Yii::app()->createAbsoluteUrl("execution/lastExecution",["id"=>$_REQUEST['idUserPhase']]);

            $response['success'] = 1;

            echo CJSON::encode($response);
        }
        catch (Exception $e)
        {
            $trans->rollback();
            $response['success'] = 0;
            $response['url']=Yii::app()->createAbsoluteUrl("execution/lastExecution",["id"=>$_REQUEST['idUserPhase']]);
            $response['errorMessage'] = $e->getMessage();
            echo CJSON::encode($response);
        }
    }
    public function actionLastExecution($id)
    {
        if(!empty($id))
        {
            $validLastExecution = Calculos::lastExecutionPhase($id);

//          CVarDumper::dump(Calculos::lastExecutionPhase($id) ,10,false);exit;
            if(is_array($validLastExecution) and !is_null($validLastExecution['id']) and $validLastExecution['id']<>0)
            {
                $tempo = Execution::model()->findByPk($validLastExecution['id']);
                $tempo->date = date('Y-m-d H:i:s');
                $tempo->save(false);

                $this->redirect(array('index','id'=>$validLastExecution['id']));
            }
            elseif ($validLastExecution['countExercisesPhase']<=0)
            {
                $userPhase = UsersPhase::model()->findByPk($id);

                Yii::app()->user->setFlash('warning', "The phase has no associated exercises");
                $this->redirect(array('fase/index','id'=>$userPhase->idUser));
            }
            else
            {
                /* 1. cargar el userphase del id pasado */
                $userPhase = UsersPhase::model()->findByPk($id);

                /* 2. Consultar el peso mas actual del usuario */
                $weightInfo = Calculos::LastWeigth($userPhase->idUser);

                /* 3. crear un registro en la tabla execution sacando los datos del userphase */
                $execution = new Execution();
                $execution->idUserPhase=$userPhase->id;
                $execution->date = date('Y-m-d H:i:s');
                $execution->weigth=$weightInfo['weight'];
                $execution->unitType = $weightInfo['unitType'];
                $execution->idUsers = $userPhase->idUser;
                $execution->save();

                /* 4. por medio de las relaciones extraer todos los ejercicios que tiene la phase de usuario */
                foreach ($userPhase->userPhaseExercises as $item) {
//                $item = new UserPhaseExercise();

                    $eec = new ExecutionExerciseCalc();
                    $eec->idExecution = $execution->id;
                    $eec->idUserPhaseExercice=$item->id;
                    $eec->totalReps=0;
                    $eec->save(false);
                    for ($i=0;$i<$item->numberSeries;$i++)
                    {
                        $ee=new ExecutionExercise();
                        $ee->idExecution=$execution->id;
                        $ee->idUserPhaseExercise = $item->id;
                        $ee->idExecutionExerciseCalc=$eec->id;
                        $ee->save(false);
                    }
                }
                $this->redirect(array('index','id'=>$execution->id));
            }
        }
        else
        {
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
    }
    public function actionUpdate()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $class_valid=['Execution','UserPhaseExercise','ExecutionExerciseCalc','ExecutionExercise'];
            foreach ($_POST as $key => $item) {
                if(class_exists($key) and isset($item['id']))
                {
                    if($key=='ExecutionExerciseCalc' and isset($item['id']) and !empty($item['id']))
                    {
                        $executionExerciseCalc = $item['id'];
                    }
                    if($key=='ExecutionExercise' and trim($item['weigth'])==='')
                    {
                        unset($item['weigth']);
                    }
                    $element=CActiveRecord::model($key)->findByPk($item['id']);
                    $element->attributes=$item;
                    $element->save(false);
                }
                else
                {
                    foreach ($item as $value)
                    {
                        if($key=='ExecutionExercise' and trim($value['weigth'])==='')
                        {
                            unset($value['weigth']);
                        }
//                        CVarDumper::dump($key,10);
//                        CVarDumper::dump($value,10);
                        if(isset($value['id']))
                        {
                            $element=CActiveRecord::model($key)->findByPk($value['id']);
                            $element->attributes=$value;
                            $element->save(false);
                        }
                    }
                }
            }
//exit;
            $executionExerciseCalc=$this->getExecutionExerciseCalc($executionExerciseCalc);
            Calculos::calculusTable($executionExerciseCalc);

            echo CJSON::encode([
                'selectorCalculus'=>'#Calculations'.$executionExerciseCalc->id,
                'selectorDetails'=>'#Details'.$executionExerciseCalc->id,
                'calculus'=>$this->renderPartial('_calculus',['executionExerciseCalc'=>$executionExerciseCalc],true,true),
                'details'=>$this->renderPartial('_executionDetails',['executionExercises'=>$executionExerciseCalc->executionExercises],true,true)]
            );
            exit;
        }
        else
        {
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
    }
    /**
     * funciones complementarias
     */
    protected function getExecution($id)
    {
        $criteria = new CDbCriteria();
        $criteria->together=true;
        $criteria->with=[
            'idUserPhase0',
            'executionExerciseCalcs',
            'executionExerciseCalcs.executionExercises',
            'executionExerciseCalcs.idUserPhaseExercice0',
            'executionExerciseCalcs.idUserPhaseExercice0.idExercise0'
        ];
        $criteria->compare('t.id',$id);
        $criteria->order='idUserPhaseExercice0.order';

        return Execution::model()->find($criteria);

    }
    protected function getExecutionExerciseCalc($id)
    {
        $criteria = new CDbCriteria();
        $criteria->with=[
            'executionExercises',
            'idExecution0',
            'idExecution0.idUsers0',
        ];
        $criteria->compare('t.id',$id);


        return ExecutionExerciseCalc::model()->find($criteria);
    }
    public function getHistoric($idUserPhaseExercice,$idExecution)
    {
        $criteria = new CDbCriteria();
        $criteria->together=true;
        $criteria->with=[
            'executionExerciseCalcs',
            'executionExerciseCalcs.executionExercises'
        ];
        $criteria->limit=72;
        $criteria->condition="executionExerciseCalcs.idUserPhaseExercice = :idUserPhaseExercice and t.id <> :idExecution";
        $criteria->params=[':idUserPhaseExercice'=>$idUserPhaseExercice,':idExecution'=>$idExecution];
        $criteria->order="t.date DESC,executionExerciseCalcs.id ASC,executionExercises.id ASC";

        return Execution::model()->findAll($criteria);
    }
    public function actionValidate()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
//            CVarDumper::dump($_REQUEST,10,true);exit;

            $model = new VariationsForm();
            $return = [];

            $model->variationreps = $_REQUEST['Execution']['variationreps'];
            $model->variationweigth = $_REQUEST['Execution']['variationweigth'];

            if($model->validate())
            {
                $return['url'] = $urlvalidator=$this->createAbsoluteUrl('execution/createExecution',['id'=>$_REQUEST['Execution']['idUserPhase'],'variationreps' => $_REQUEST['Execution']['variationreps'],'variationweigth' => $_REQUEST['Execution']['variationweigth']]);
                $return['result']=true;
            }
            else
            {
                $return['errors'] = $model->getErrors();
                $return['result'] =false;
            }


            echo CJSON::encode($return);
        }
    }
}