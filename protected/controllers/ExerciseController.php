<?php

class ExerciseController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index','view','admin','delete','create','update','dropsBodyArea','dropsFamilyExercise','dropsOrientation','dropsImplement','dropsGrip'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Exercise;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Exercise']))
		{
			$model->attributes=$_POST['Exercise'];
			if($model->save())
				$this->redirect(array('index','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Exercise']))
		{
			$model->attributes=$_POST['Exercise'];
			if($model->save())
				$this->redirect(array('index','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        try
        {
            $this->loadModel($id)->delete();
        }
        catch (CDbException $exception)
        {
            echo $exception->getMessage();
//            CVarDumper::dump($exception->getMessage(),10,true);
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//		if(!isset($_GET['ajax']))
//			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
    public function actionIndex()
    {
        $model=new Exercise('searchSql');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Exercise']))
            $model->attributes=$_GET['Exercise'];

        $methods = Method::model()->findAll(['order'=>'description']);
        $bodyAreas = BodyArea::model()->findAll(['group'=>'description','order'=>'description']);
        $familyExercises = FamilyExercise::model()->findAll(['group'=>'description','order'=>'description']);
        $organitations = Orientation::model()->findAll(['group'=>'description','order'=>'description']);
        $implements = Implement::model()->findAll(['group'=>'description','order'=>'description']);
        $grips = Grip::model()->findAll(['group'=>'description','order'=>'description']);
        $ranges = RangeMotion::model()->findAll(['order'=>'description']);
        $laterality = Laterality::model()->findAll(['order'=>'description']);



        $selects["methodSelect"] = CHtml::listData($methods,'description','description');
        $selects["bodyAreaSelect"] = CHtml::listData($bodyAreas,'description','description');
        $selects["familyExerciseSelect"] = CHtml::listData($familyExercises,'description','description');
        $selects["orientationSelect"] = CHtml::listData($organitations,'description','description');
        $selects["implementSelect"] = CHtml::listData($implements,'description','description');
        $selects["gripSelect"] = CHtml::listData($grips,'description','description');
        $selects["rangeSelect"] = CHtml::listData($ranges,'description','description');
        $selects["lateralitySelect"] = CHtml::listData($laterality,'description','description');

        $this->render('index',array(
            'model'=>$model,
            'selects'=>$selects
        ));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Exercise the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Exercise::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Exercise $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='exercise-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    /**
     * funcion que retorna un json con las opciones de los dropdowns de Body areas
     * @param $id integer id del Metodo
     */
    public function actionDropsBodyArea()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $id = $_POST['id'];

            $htmlOptions=['empty'=>'Select.......'];
            $returnArray['BodyArea'] = CHtml::listOptions([],CHtml::listData(BodyArea::model()->findAllByAttributes(['idMethod'=>$id]),'id','description'),$htmlOptions);

            echo CJSON::encode($returnArray);
            Yii::app()->end();
        }
    }
    /**
     * funcion que retorna un json con las opciones de las familias de ejercicios
     * @param $id integer id del Body Area
     */
    public function actionDropsFamilyExercise()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $id = $_POST['id'];

            $htmlOptions=['empty'=>'Select.......'];
            $returnArray['FamilyExercise'] = CHtml::listOptions([],CHtml::listData(FamilyExercise::model()->findAllByAttributes(['bodyAreaId'=>$id]),'id','description'),$htmlOptions);

            echo CJSON::encode($returnArray);
            Yii::app()->end();
        }
    }
    /**
     * funcion que retorna un json con las opciones de la orientacion
     * @param $id integer id del family exercise
     */
    public function actionDropsOrientation()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $id = $_POST['id'];

            $htmlOptions=['empty'=>'Select.......'];
            $returnArray['Orientation'] = CHtml::listOptions([],CHtml::listData(Orientation::model()->findAllByAttributes(['idFamily_exercise'=>$id]),'id','description'),$htmlOptions);

            echo CJSON::encode($returnArray);
            Yii::app()->end();
        }
    }
    /**
     * funcion que retorna un json con las opciones de los implementos
     * @param $id integer id de la orientacion
     */
    public function actionDropsImplement()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $id = $_POST['id'];

            $htmlOptions=['empty'=>'Select.......'];
            $returnArray['Implement'] = CHtml::listOptions([],CHtml::listData(Implement::model()->findAllByAttributes(['idOrientation'=>$id]),'id','description'),$htmlOptions);

            echo CJSON::encode($returnArray);
            Yii::app()->end();
        }
    }
    /**
     * funcion que retorna un json con las opciones de el grip
     * @param $id integer id del implemento
     */
    public function actionDropsGrip()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $id = $_POST['id'];

            $htmlOptions=['empty'=>'Select.......'];
            $returnArray['Grip'] = CHtml::listOptions([],CHtml::listData(Grip::model()->findAllByAttributes(['idImplement'=>$id]),'id','description'),$htmlOptions);

            echo CJSON::encode($returnArray);
            Yii::app()->end();
        }
    }
}
