<?php

class UsersController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*array('allow', // allow authenticated user to perform 'create' and 'update' actions
					'actions'=>array('update','index'),
					'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
					'actions'=>array('create'),
					'users'=>array('Admin','Trainer'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
					'actions'=>array('delete'),
					'users'=>array('Admin'),
			),*/
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
					'actions'=>array('update','index','delete','create','profile','ProductsList','createAccessPractitioner','updateAccessPractitioner','seminars','changepractitioner'),
					'users'=>array('@'),
			),
			array('deny',  // deny all users
					'users'=>array('*'),
			),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{


		$model=new Users('Nuevo');
		$userLogeado = Yii::app()->user->getState('_user');

//		$userLogeado = new Users();

//		$userLogeado->idCountry0->exclusive
//      $userLogeado->idCountry0->id;

//        if($userLogeado->idCountry0->exclusive)
//        {
//            $criteria = new CDbCriteria();
//            $criteria->compare('id',$userLogeado->idCountry);
//
//        }

		if(!empty($model->idAttachment))
			$photo = $model->idAttachment0->path;
		else
			$photo = '/img/defaultProfileImg.jpg';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$noError = true;

			$model->attributes=$_POST['Users'];
			if(Yii::app()->user->getState('userType')=='Trainer' or Yii::app()->user->getState('userType')=='Practitioner1' )
			{
				$model->parentEmail = $userLogeado->email;
				//$model->userType = 'User';
			}
			if(Yii::app()->user->getState('userType')=='Admin' )
			{
				$model->parentEmail = $model->email;
				//$model->userType = 'User';
			}
			if(empty($model->userType))
				$model->userType = 'User';
			
			$pass = Yii::app()->getSecurityManager()->generateRandomString(10);
			$model->password = CPasswordHelper::hashPassword($pass);
			$model->forgotPass = 1;
			$model->create_record = date('Y-m-d H:i:s',time());

			if(!empty(CUploadedFile::getInstance($model,'idAttachment')))
			{
				$att = new ImageForm('Crear');
				$att->image=CUploadedFile::getInstance($model,'idAttachment');

				$model->idAttachment = $att->save();

				if(count($att->getErrors('image'))>0)
				{
					foreach ($att->getErrors('image') as $error) {
						$model->addError('idAttachment', $error);
					}
					$noError = false;
				}
			}
//			else
//			{
//				$model->addError('idAttachment','Photo cannot be empty');
//				$noError=false;
//			}

			if($noError and $model->save())
			{
				Yii::app()->user->setFlash('success', "The user has successfully created and sent an email to {$model->email} with startup data");
				$params['renderParams']['url'] = Yii::app()->createAbsoluteUrl('');
				$params['renderParams']['email'] = $model->email;
				$params['renderParams']['password'] = $pass;
				$params['to'] = $model->email;

				Utilities::newUser($params);
				$this->redirect(array('index'));
			}

		}

		$this->render('create',array(
			'model'=>$model,
			'photo'=>$photo,
			'user'=>$userLogeado,
		));
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		$model->setScenario('update');
		$userLogeado = Yii::app()->user->getState('_user');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(!empty($model->idAttachment))
			$photo = $model->idAttachment0->path;
		else
			$photo = '/img/defaultProfileImg.jpg';

		if(isset($_POST['Users']))
		{
			$orgIdAttachment = $model->idAttachment;
			$model->attributes=$_POST['Users'];
			if(!empty(CUploadedFile::getInstance($model,'idAttachment')))
			{
				$att = new ImageForm('Actualizar');
				$att->image=CUploadedFile::getInstance($model,'idAttachment');

				$model->idAttachment = $att->save();
			}
			else
			{
				$model->idAttachment = $orgIdAttachment;
			}

			if($model->save()){
				if($model->id === Yii::app()->user->id)
				{
					Yii::app()->user->setState('_user',$model);
				}
				$this->redirect(array('index','id'=>$model->id));

			}
		}

		$this->render('update',array(
			'model'=>$model,
			'photo'=>$photo,
			'user'=>$userLogeado,
		));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        try
        {
            $this->loadModel($id)->delete();
        }
        catch (CDbException $exception)
        {
            echo $exception->getMessage();
//            CVarDumper::dump($exception->getMessage(),10,true);
        }
	}
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
//		CVarDumper::dump(Yii::app()->db,10,true);exit;
		if(Yii::app()->user->getState('userType')=='User')
		{
			$this->redirect(Yii::app()->createUrl('users/profile'));
		}
		$this->pageTitle = 'Manage Profile';
		$user = Yii::app()->user->getState('_user');
//		CVarDumper::dump($user,10,true);exit;

		if(!empty($user->idAttachment))
			$photo = $user->idAttachment0->path;
		else
			$photo = '/img/defaultProfileImg.jpg';

		if(Yii::app()->user->getState('userType')=='Admin' or Yii::app()->user->getState('userType')=='Trainer' or Yii::app()->user->getState('userType')=='Practitioner1')
		{
			$buttons=[
				['label'=>'<i class="fa fa-key"></i>','name'=>'Password'],
//				['label'=>'Products','name'=>'Products'],
				['label'=>'Update','name'=>'Update'],
				['label'=>'Measures','name'=>'Measures']
			];
		}
		else
		{
			$buttons=[
				['label'=>'<i class="fa fa-key">&nbsp;</i>','name'=>'Password'],
				['label'=>'Update','name'=>'Update'],
				['label'=>'Measures','name'=>'Measures']
			];
		}

		$model=new Users('search');
//		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Users']))
		{
			$model->attributes=$_GET['Users'];
		}

		$this->render('index',array(
			'model'=>$model,
			'buttons'=>$buttons,
			'user' =>$user,
			'photo'=>$photo
		));
	}
	public function actionProfile()
	{
		$this->pageTitle = 'Client Profile';
		$user = Yii::app()->user->getState('_user');

		if(!empty($user->idAttachment))
			$photo = $user->idAttachment0->path;
		else
			$photo = '/img/defaultProfileImg.jpg';

//		CVarDumper::dump(count($user->measurements),10,true);exit;

		if(Yii::app()->user->getState('userType')=='Admin' or Yii::app()->user->getState('userType')=='Trainer' or Yii::app()->user->getState('userType')=='Practitioner1')
		{
			$buttons=[
				['label'=>'<i class="fa fa-key"></i>','name'=>'Password'],
				//['label'=>'Products','name'=>'Products'],
				//['label'=>'Update','name'=>'Update'],
                ['label'=>'Workout','name'=>'Workout'],
				['label'=>'Measurements','name'=>'Measures']
			];
		}
		else
		{
			$buttons=[
				['label'=>'<i class="fa fa-key">&nbsp;</i>','name'=>'Password'],
				['label'=>'Update','name'=>'Update'],
				['label'=>'Workout','name'=>'Workout']
			];
			if(count($user->measurements)>0)
			{
				$buttons[]=['label'=>'Measurements','name'=>'Measures'];
			}
		}



		$this->render('profile',array(
			'buttons'=>$buttons,
			'user' =>$user,
			'photo'=>$photo
		));
	}
	public function actionProductsList($id)
	{
		$criteria = new CDbCriteria();
		$criteria->together = true;
		$criteria->with=['idProduct0','idUsers0'];
		$criteria->compare('t.idUsers',$id);

		$user = Users::model()->findByPk($id);
//		$user=new Users();


		$model = new CActiveDataProvider('UserProduct',array(
			'criteria'=>$criteria,
		));
		$this->render('productsList',['user'=>$user,'model'=>$model]);
	}
	public function actionCreateAccessPractitioner($id)
	{
		$model=new AccessPractitionerForm();
		$model->idUsers = $id;

		if(isset($_POST['AccessPractitionerForm']))
		{
			//CVarDumper::dump($_POST['AccessPractitionerForm'],10,true);exit;
			$model->attributes = $_POST['AccessPractitionerForm'];
//			CVarDumper::dump($model,10,true);exit;
			if($model->validate())
			{
				if($model->save())
				{
					Yii::app()->user->setFlash('success', "The practitioner access has successfully created");
					$this->redirect(array('productsList','id'=>$model->idUsers));
				}
			}

		}

		$this->render('createAccessPractitioner',['model'=>$model]);
	}
	public function actionUpdateAccessPractitioner($id)
	{
		$model=new AccessPractitionerForm($id);
		if(isset($_POST['AccessPractitionerForm']))
		{
			//CVarDumper::dump($_POST['AccessPractitionerForm'],10,true);exit;
			$model->attributes = $_POST['AccessPractitionerForm'];
//			CVarDumper::dump($model,10,true);exit;
			if($model->validate())
			{
				if($model->save())
				{
					Yii::app()->user->setFlash('success', "The practitioner access has successfully created");
					$this->redirect(array('productsList','id'=>$model->idUsers));
				}
			}

		}

		$this->render('updateAccessPractitioner',['model'=>$model]);
	}

	public function actionSeminars($id)
	{
		$model = new UsersSeminarsForm($id);
		if(isset($_REQUEST['UsersSeminarsForm']))
		{
			$model->attributes = $_REQUEST['UsersSeminarsForm'];
			if($model->validate())
			{
				if($model->save())
				{
					Yii::app()->user->setFlash('success','The course is successfully assigned the client');
					//$this->redirect(array('index'));
				}
			}
		}
		$this->render('usersSeminars',['model'=>$model]);
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function actionChangepractitioner($id)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $response=[];

            $model = new ChangePractitionerForm();
            $model->idPractitionerFrom = $id;
            if(isset($_POST['ChangePractitionerForm']))
            {
                $model->attributes=$_POST['ChangePractitionerForm'];
                if ($model->validate() && $model->save())
                {
                    $response['html']= '';
                    $response['id']=1;
//                    CVarDumper::dump($response,10,true);exit;
                    Yii::app()->user->setFlash('success', "All users were changed to the new practitioner");
                    echo CJavaScript::jsonEncode($response);
                    Yii::app()->end();

                }
            }
            $response=[];
            $response['id']=0;
            $response['html']=$this->renderPartial('changepractitioner',['model'=>$model],true,true);
            echo CJavaScript::jsonEncode($response);
            Yii::app()->end();
        }
    }
	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function validateFolderDestination($path)
	{
		if(!is_dir($path))
		{
			return false;
		}
		else
		{
			if(!is_writable($path))
			{
				return false;
			}
		}
		return true;
	}
	public function templateButtonsGrilla()
    {
        $template=['{xupdate}'];
        $templateBase=['{xmeasure}','{xfase}','{xprofile}','{xupdate}','{xproducts}','{xuserProduct}','{xseminar}','{xdelete}','{xchangepractitioner}'];

        if(Yii::app()->user->getState('userType')=='Trainer' or Yii::app()->user->getState('userType')=='Practitioner1')
        {
            $permisos=Yii::app()->user->getState('permisosCodes');

            if(is_array($permisos) and $permisos!==array())
            {
                foreach ($permisos as $permiso) {
                    switch ($permiso)
                    {
                        case Yii::app()->params['CodeProductBasic']:
                            $template=array_merge($template,['{xmeasure}','{xprofile}','{xproducts}','{xuserProduct}','{xseminar}','{xdelete}']);
                            break;
                        case Yii::app()->params['CodeProductPhase']:
                            $template=array_merge($template,['{xfase}']);
                            break;
                    }
                }
                $template = array_intersect($templateBase,$template);
                $template = implode("",$template);
                return $template;
            }
        }
        else
        {
            return implode("",$templateBase);
        }

    }
}
