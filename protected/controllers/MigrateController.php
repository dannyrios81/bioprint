<?php

class MigrateController extends Controller
{
    private $usuariosMigrados=0;
    private $medidasMigradas=0;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            /*array('allow', // allow authenticated user to perform 'create' and 'update' actions
                    'actions'=>array('update','index'),
                    'users'=>array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                    'actions'=>array('create'),
                    'users'=>array('Admin','Trainer'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                    'actions'=>array('delete'),
                    'users'=>array('Admin'),
            ),*/
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('update','index','delete','create','update','index'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
	public function actionIndex()
	{
        /* habilita la deteccion del fin de linea del archivo subido*/
        ini_set('auto_detect_line_endings',TRUE);
        /* poner limite de tiempo infinito en el procesamiento de las migraciones */
        set_time_limit(0);

        $this->pageTitle = 'Migration';
        $model = new MigrateForm();

        /* carga el archivo al server */
        if(isset($_POST['MigrateForm']))
        {
            $model->attributes = $_POST['MigrateForm'];
            $model->filecsv=CUploadedFile::getInstance($model,'filecsv');
            if($model->save())
            {
                /* inicia el proceso de migracion */
                $this->startMigration($model->realRoute);
            }
        }
        /* renderiza la vista para subir el archivo */
		$this->render('index',['model'=>$model]);
	}
	/*
	 * funciones complementarias migracion
	 */
    public function startMigration($route)
    {
        $iniciado = 0;
        /* abre el archivo cargado */
        if (($gestor = fopen($route, "r")) !== FALSE) {
            /* extrae una linea completa el archivo abierto */
            while (($datos = fgetcsv($gestor,0,",",'"')) !== FALSE) {
                /* valida que el registro leido tenga mas de dos datos */
                if(count($datos)>2)
                {
                    /* vallida que la codificcion de los datos sea valida */
                    if(mb_detect_encoding($datos[0])!='ASCII' and mb_detect_encoding($datos[0])!='UTF-8')
                    {
                        Yii::app()->user->setFlash('error','File encoding invalid, it must be UTF-8 or ASCII');
                        return;
                    }
                    if($iniciado == 0)
                    {
                        $practitioner = $this->validateExistPractitioner($datos,$iniciado);
                    }
                    if (!empty($practitioner))
                    {
                        $this->createUser($datos,$practitioner);
                    }
                    else
                    {
                        brake;
                    }

                    $iniciado++;
                }
            }
            fclose($gestor);
        }
        if($iniciado>0 and !empty($practitioner))
        {
            Yii::app()->user->setFlash('success',$this->usuariosMigrados.' users migrated and '.$this->medidasMigradas.' Measurement set');
        }
	}
    public function validateExistPractitioner($data,$iniciado)
    {
        /*
         * validar que el campo de email del practitioner este lleno
         */

        $model = new ValidatorForm();
        $model->email = $data[2];
        if($model->validate(['email']))
        {

            $practitioner = Users::model()->findByAttributes(['email'=> $data[2],'userType'=>['Trainer'.'Practitioner1']]);
            if(!empty($practitioner))
            {
                if($iniciado<1)
                {
                    $this->deleteOldData($practitioner);
                }
                //$this->createUser($data,$practitioner);
                return $practitioner;
            }
        }
        Yii::app()->user->setFlash('error','The practitioner not exist');
        return null;
	}
    public function createUser($data,$practitioner)
    {
        /* validaciones adicionales del nuevo requerimiento */

        /**
         * si un set de medidas no tiene fecha no se sube
         * si un set de medidas tiene tres o mas medidas en cero no se sube
         * si un set de medidas no tiene peso no se sube
         * si el usurio esta creado en otro practitioner, se crea el usuario con un correo temporal
         * si el set de medidas no tiene altura pero otro set si se le pone la misma altura de otro set de medidas
         **/

        if(!$this->validates($data))
        {
            return false;
        }
        /*
         * 1. buscar si exise el correo electronico
         * 2. si existe validar si es el mismo practitioner
         * 2.1. si es el mismo practitioner usar ese usuario
         * 2.2. si no es el mismo practitioner vaciar el campo de correo de los datos leidos del archivo
         * 3. si no se encuentra con el correo buscarlo por nombre apellido y email de practitioner
         */
        $userOtherParent = false;

        if(!empty($data[6]))
        {
            $user=Users::model()->findByAttributes(['email'=>$data[6]]);
            if(!empty($user))
            {
                if($user->parentEmail != $data[2])
                {
                    $user = Users::model()->findByAttributes(['name'=>$data[4],'lastName'=>$data[5],'parentEmail'=>$data[2]]);
                    $data[6]='';
                    $userOtherParent = true;
                }
            }
            else
            {
                $user = Users::model()->findByAttributes(['name'=>$data[4],'lastName'=>$data[5],'parentEmail'=>$data[2]]);
            }
        }
        else
        {
            $user = Users::model()->findByAttributes(['name'=>$data[4],'lastName'=>$data[5],'parentEmail'=>$data[2]]);
        }
        if (!$this->validateHeight($data,$user))
        {
            return false;
        }
        //validar correo para el usuario.
        if(empty($user))
        {
            $user = new Users('migrate');

//            if($userOtherParent)
//            {
//                $user->email = $data[2].uniqid();
//            }
//            else
//            {
//                $user->email = (!empty(trim($data[6])))?trim($data[6]):$data[2].uniqid();
//            }
            $user->email = uniqid().'@temporal.com';
            try
            {
                $user->gender = (strtolower(trim($data[3]))=='mr')?'Male':'Female';
                $user->name = $data[4];
                $user->lastName = $data[5];
                $user->bornDate = !empty($data[7])?$data[7]:'0000-00-00';
                $user->password = $practitioner->password;
                $user->create_record = !empty($data[8])?$data[8]:'0000-00-00';
                $user->idCountry = $practitioner->idCountry;
                $user->parentEmail = $practitioner->email;
                $user->migrate = 1;
                $user->userType = 'User';
                //CVarDumper::dump($user,10,true);exit;
                if($user->save(false))
                {
                    $this->usuariosMigrados++;
                    $this->createWeigthHeigth($data,$user);
                }
            }
            catch (Exception $e)
            {
//                echo $e->getMessage();
//                CVarDumper::dump($user,10,true);exit;
            }

        }
        else
        {
            $this->createWeigthHeigth($data,$user);
        }
    }
    public function createWeigthHeigth($data,$user)
    {
        $wh = new WeightHeight();

        $wh->date = $data[8];
        if(!empty($data[9]))
        {
            $wh->height=floatval(str_replace(',','.',$data[9]));
            $wh->heightUnit='Cm';
        }
        if(!empty($data[10]))
        {
            $wh->height=floatval(str_replace(',','.',$data[10]));
            $wh->heightUnit='In';
        }
        if(!empty($data[11]))
        {
            $wh->weight=floatval(str_replace(',','.',$data[11]));
            $wh->weightUnit='Kg';
        }
        if(!empty($data[12]))
        {
            $wh->weight=floatval(str_replace(',','.',$data[12]));
            $wh->weightUnit='Lbs';
        }
        $wh->idUser=$user->id;
        $wh->migrate = 1;
        if(!empty($wh->height) and !empty($wh->weight))
        {
            if($wh->save(false))
            {
                $this->createMeasure($data,$user,$wh);
            }
        }
	}
    public function createMeasure($data,$user,$wh)
    {
        $idMeasurementType=[13=>1,14=>2,15=>3,16=>4,17=>5,18=>6,19=>8,20=>9,21=>7,22=>10,23=>11,24=>12,25=>13,26=>14];
        $measures=[];
        $sum=0;

        for ($i=13;$i<27;$i++)
        {
            $measurememtType = new MeasurementTypes();
            $measurememtType->id = $idMeasurementType[$i];

            $measures[$idMeasurementType[$i]]=new MeasurementUserForm();
            $measures[$idMeasurementType[$i]]->date = $data[8];
            $measures[$idMeasurementType[$i]]->val = floatval(str_replace(',','.',!empty($data[$i])?$data[$i]:"0"));
            $measures[$idMeasurementType[$i]]->MeasurementType= $measurememtType;
            $measures[$idMeasurementType[$i]]->idUser = $user->id;
            $measures[$idMeasurementType[$i]]->migration = 1;
            $measures[$idMeasurementType[$i]]->save(false);

            $sum=$sum+floatval(str_replace(',','.',$data[$i]));
        }
        $this->medidasMigradas++;
        $this->calculateMeasures($user,$data,$sum,$wh,$measures);
    }
    public function calculateMeasures($user,$data,$sum,$modelWH,$measures)
    {
        Utilities::calculateMeasurements($user->id,$data[8],$sum,$modelWH);
        Utilities::calculateHormoneFamilyScore($user->id,$data[8],$measures);
    }
    public function deleteOldData($practitioner)
    {
        $trans = Yii::app()->db->beginTransaction();
        try
        {
            Yii::app()->db->createCommand('delete from weight_height where weight_height.idUser in (select id from users where parentEmail="'.$practitioner->email.'" and migrate=1);')
                ->execute();
            Yii::app()->db->createCommand('delete from measurement where measurement.idUsers in (select id from users where parentEmail="'.$practitioner->email.'" and migrate=1);')
                ->execute();
            Yii::app()->db->createCommand('delete from hormone_user_date where hormone_user_date.idUsers in (select id from users where parentEmail="'.$practitioner->email.'" and migrate=1);')
                ->execute();
            Yii::app()->db->createCommand('delete from users where parentEmail="'.$practitioner->email.'" and migrate=1;')
                ->execute();

            $trans->commit();
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
            $trans->rollback();
        }
        /*'delete from weight_height where weight_height.idUser in (select id from users where parentEmail=\'dannyrios81@gmail.com\' and id>23);
delete from measurement where measurement.idUsers in (select id from users where parentEmail=\'dannyrios81@gmail.com\' and id>23);
delete from hormone_user_date where hormone_user_date.idUsers in (select id from users where parentEmail=\'dannyrios81@gmail.com\' and id>23);
delete from users where parentEmail=\'dannyrios81@gmail.com\' and id>23;'*/
    }
    public function validates(&$data)
    {
        /* validaciones adicionales del nuevo requerimiento */

        /**
         * si un set de medidas no tiene fecha no se sube
         * si un set de medidas tiene tres o mas medidas en cero no se sube
         * si un set de medidas no tiene peso no se sube
         **/


        /*
         * si un set de medidas no tiene fecha no se sube
         */
        if(empty($data[8]))
        {
            return false;
        }

        if(!strtotime($data[8]))
        {
            return false;
        }

        /*
         * si un set de medidas tiene tres o mas medidas en cero no se sube
         */
        $count=0;
        for ($i=13;$i<27;$i++)
        {
            if(empty(floatval(str_replace(',','.',$data[$i]))))
                $count = $count+1;

        }
        if($count>2)
            return false;

        /*
         * si un set de medidas no tiene peso no se sube
         */

        if(empty(floatval(str_replace(',','.',$data[11]))) and empty(floatval(str_replace(',','.',$data[12]))))
            return false;


        return true;
    }

    public function validateHeight(&$data, $user)
    {
        $idUser=0;

        if(!empty($user) and ($user instanceof Users) )
            $idUser = $user->id;

        if(empty($data[10]) and empty($data[9]))
        {
            if(!empty($idUser))
            {
                $criteria = new CDbCriteria();
                $criteria->addCondition('idUser=:idUser');
                $criteria->order = 'date DESC';
                $criteria->params=[':idUser'=>$idUser];

                $query = WeightHeight::model()->find($criteria);

                if(!empty($query))
                {
//                    $query = new WeightHeight;

                    if($query->heightUnit == 'Cm')
                        $data[9] = $query->height;
                    else
                        $data[10] = $query->height;

                    return true;
                }
            }
            return false;
        }
        return true;
    }
}
