<?php

class RecipesController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),*/
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update','formChoose','FormChooseSuplement'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Recipes('create');

		$info = isset($_POST['Choose'])?$this->loadInfo($model,$_POST):[];

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Recipes']))
		{
			//genero estructura de informacion para poder empezar con el gurdado de la informacion
			if(!isset($_POST['Choose']) or !isset($_POST['ChooseSuplement']))
				$model->addError('id','The recipe must be at least a moment and a supplement');

			// asignando la informacion de la receta al objeto
			$model->attributes=$_POST['Recipes'];
			try{
				$trans = Yii::app()->db->beginTransaction();
				//inicio de la transaccion
				$NoerrorMoments=true;
				$NoerrorSuplements=true;
				if(!$model->hasErrors()) {
					if ($model->save()) {
						if (count($info['Choose']) > 0 and $info['ChooseSuplement']>0) {
							foreach ($info['Choose'] as $key => $item) {
								$item->idRecipe = $model->id;
								if ($item->save() and $NoerrorMoments) {
									foreach ($info['ChooseSuplement'][$key] as $val) {
										$val->idChoose = $item->id;
										$NoerrorSuplements = !$val->save()?false:$NoerrorSuplements;
									}
								} else {
									$NoerrorMoments = false;
								}
							}
						} else {
							$model->addError('id', 'The recipe must be at least a moment and a supplement');
						}
					}
				}
				if($NoerrorMoments and $NoerrorSuplements and !$model->hasErrors()) {
					$trans->commit();
					$this->redirect(array('index', 'id' => $model->id));
				}
				else
					$trans->rollback();
			}
			catch(CDbException $e)
			{
				$trans->rollback();
				$model->addError('id', 'Check the supplements in all moments, there is a duplicate');
			}
			catch(ErrorException $e)
			{
				$trans->rollback();
			}
			catch(Exception $e)
			{
				$trans->rollback();
			}

		}
		$this->render('create',array(
			'model'=>$model,
			'info'=>$info,
		));
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$info=$this->loadInfoUpdate($model,$_POST);
//		CVarDumper::dump($info,3,true);exit;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Recipes']))
		{
			//genero estructura de informacion para poder empezar con el gurdado de la informacion
			if(!isset($_POST['Choose']) or !isset($_POST['ChooseSuplement']))
				$model->addError('id','The recipe must be at least a moment and a supplement');

			// asignando la informacion de la receta al objeto
			$model->attributes=$_POST['Recipes'];
			try{
				$trans = Yii::app()->db->beginTransaction();
				//inicio de la transaccion
				$NoerrorMoments=true;
				$NoerrorSuplements=true;
				if(!$model->hasErrors()) {
					if ($model->save()) {
						if (count($info['Choose']) > 0 and $info['ChooseSuplement']>0) {
							foreach ($info['Choose'] as $key => $item) {
								$item->idRecipe = $model->id;
								$idSuplements = [];
								if ($item->save() and $NoerrorMoments) {
									foreach ($info['ChooseSuplement'][$key] as $val) {
										//$val=new ChooseSuplement();
										$val->idChoose = $item->id;
										if(!in_array($val->idSuplement,$idSuplements))
										{
											$NoerrorSuplements = !$val->save()?false:$NoerrorSuplements;
										}
										else
										{
											$val->addError('idSuplement','The supplements is duplicate in this moment');
											$model->addError('id','The supplements is duplicate in this moment');
											$NoerrorSuplements = false;
										}
										$idSuplements[] = $val->idSuplement;
									}
								} else {
									$NoerrorMoments = false;
								}
							}
						} else {
							$model->addError('id', 'The recipe must be at least a moment and a supplement');
						}
					}
				}
				if($NoerrorMoments and $NoerrorSuplements and !$model->hasErrors()) {
//					CVarDumper::dump($model->hasErrors(),10,true);exit;
					$trans->commit();
					$this->redirect(array('index', 'id' => $model->id));
				}
				else
				{
					$trans->rollback();

				}

			}
			catch(CDbException $e)
			{
				$trans->rollback();
				$model->addError('id', 'Check the supplements in all moments, there is a duplicate');
//				CVarDumper::dump($e,10,true);exit;
			}
			catch(ErrorException $e)
			{
				$trans->rollback();
//				CVarDumper::dump($e,10,true);exit;
			}
			catch(Exception $e)
			{
				$trans->rollback();
//				CVarDumper::dump($e,10,true);exit;
			}

		}

		$this->render('update',array(
			'model'=>$model,
			'info'=>$info,
		));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{

		//inicio de la transaccio para el borrado de los datos
		try
		{
			$trans = Yii::app()->db->beginTransaction();

			$recipe = $this->loadModel($id);
//		$recipe = new Recipes();
			foreach ($recipe->chooses as $choose)
			{
//			$choose = new Choose();
				foreach ($choose->chooseSuplements as $item) {
//				$item = new ChooseSuplement();
					$item->delete();
				}
				$choose->delete();
			}
			$recipe->delete();
			$trans->commit();
		}
		catch(CDbException $e)
		{
			$trans->rollback();
			CVarDumper::dump($e,10,true);exit;
		}
		catch(ErrorException $e)
		{
			$trans->rollback();
			CVarDumper::dump($e,10,true);exit;
		}
		catch(Exception $e)
		{
			$trans->rollback();
			CVarDumper::dump($e,10,true);exit;
		}

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Recipes('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Recipes']))
			$model->attributes=$_GET['Recipes'];

		$this->render('index',array(
			'model'=>$model,
		));
	}
	/**
	 * retorna el formulario de un choose
	 */
	public function actionFormChoose()
	{
		$model = new Choose();
		if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
			$response['body']=$this->renderPartial('_formChoose',['model'=>$model],true);
			echo CJSON::encode($response);
		}
	}
	/**
	 * retorna el formulario de un choose
	 */
	public function actionFormChooseSuplement()
	{
		$model = new ChooseSuplement();
		if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
			$id = $_POST['id'];
			$idZone = $_POST['idZone'];
			$response['body']=$this->renderPartial('_formChooseSuplement',['model'=>$model,'id'=>$id,'idZone'=>$idZone],true);
			echo CJSON::encode($response);
		}
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Recipes the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$criteria = new CDbCriteria();
		$criteria->together = true;
		$criteria->with = [
			'chooses',
			'chooses.idMoment0',
			'chooses.chooseSuplements'
		];
		$criteria->addCondition('t.id = '.$id);
		$criteria->order='idMoment0.priority';

		$model=Recipes::model()->find($criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	/**
	 * Performs the AJAX validation.
	 * @param Recipes $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='recipes-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	protected function loadInfo(&$model,$post)
	{
		$return=[];
		$error=false;
//		CVarDumper::dump($model,10,true);exit;
		foreach ($post['Choose'] as $key => $choose) {
			$tempChoose = new Choose();
			$tempChoose->attributes = $choose;
			$tempChoose->validate();
			if($tempChoose->hasErrors() and !$error)
			{
				$model->addError('id', 'Please fill out the fields in red to continue');
				$error=true;
			}
			$return['Choose'][$key] = $tempChoose;

			if(isset($post['ChooseSuplement'][$key]))
			{
				foreach ($post['ChooseSuplement'][$key] as $index => $ChooseSuplement) {
					$tempChooseSuplement = new ChooseSuplement();
					$tempChooseSuplement->attributes = $ChooseSuplement;
					$tempChooseSuplement->validate();
					if($tempChooseSuplement->hasErrors() and !$error)
					{
						$model->addError('id', 'Please fill out the fields in red to continue');
						$error=true;
					}
					$return['ChooseSuplement'][$key][$index] = $tempChooseSuplement;
				}
			}
		}
//		CVarDumper::dump($return,10,true);exit;
		return $return;
	}
	protected function loadInfoUpdate(&$model,$post)
	{
		$return=[];
		$error=false;
		if(isset($post['Choose']) or isset($post['ChooseSuplement']))
		{

			/*
			 * actualizar y borrar registros existentes
			 */
			foreach ($model->chooses as $choose)
			{
				if(isset($post['Choose'][$choose->id])){
					$choose->attributes = $post['Choose'][$choose->id];
					$choose->validate();
					if($choose->hasErrors() and !$error)
					{
						$model->addError('id', 'Please fill out the fields in red to continue');
						$error=true;
					}
					$return['Choose'][$choose->id]=	$choose;
				}
				else
				{
					foreach ($choose->chooseSuplements as $chooseSuplement) {
						$chooseSuplement->delete();
					}
					$choose->delete();
				}
				if(isset($post['ChooseSuplement'][$choose->id]))
				{
					foreach ($choose->chooseSuplements as $chooseSuplement) {
					//$chooseSuplement = new ChooseSuplement();
					if(isset($post['ChooseSuplement'][$choose->id][$chooseSuplement->id]))
					{
						$chooseSuplement->attributes = $post['ChooseSuplement'][$choose->id][$chooseSuplement->id];
						$chooseSuplement->validate();
						if($chooseSuplement->hasErrors() and !$error)
						{
							$model->addError('id', 'Please fill out the fields in red to continue');
							$error=true;
						}
						$return['ChooseSuplement'][$choose->id][$chooseSuplement->id] =	$chooseSuplement;
					}
					else
					{
						$chooseSuplement->delete();
					}
					unset($post['ChooseSuplement'][$choose->id][$chooseSuplement->id]);
					if(isset($post['ChooseSuplement'][$choose->id]) and count($post['ChooseSuplement'][$choose->id])<=0)
						unset($post['ChooseSuplement'][$choose->id]);
					}
				}
				//if(isset($post['ChooseSuplement'][$choose->id]) and count($post['ChooseSuplement'][$choose->id])<=0)
					unset($post['Choose'][$choose->id]);
			}
			//CVarDumper::dump($post,10,true);exit;
			$this->loadInfoAditional($model,$post,$return);
		}
		else
		{
			/*
			 * carga los registros iniciales
			 */
			foreach ($model->chooses as $choose)
			{
//				$choose = new Choose();
				$return['Choose'][$choose->id]=	$choose;
				foreach ($choose->chooseSuplements as $chooseSuplement) {
//					$chooseSuplement = new ChooseSuplement();
					$return['ChooseSuplement'][$choose->id][$chooseSuplement->id]=$chooseSuplement;
				}
			}
		}

		return $return;
	}
	protected function loadInfoAditional(&$model,$post,&$return)
	{

//		$return=[];
		$error=false;
//		CVarDumper::dump($model,10,true);exit;
		foreach ($post['Choose'] as $key => $choose) {
			$tempChoose = new Choose();
			$tempChoose->attributes = $choose;
			$tempChoose->validate();
			if($tempChoose->hasErrors() and !$error)
			{
				$model->addError('id', 'Please fill out the fields in red to continue');
				$error=true;
			}
			else
				$return['Choose'][$key] = $tempChoose;
		}
		if(count($post['ChooseSuplement'])>0)
		{
			foreach ($post['ChooseSuplement'] as $key=>$item) {
				if(count($post['ChooseSuplement'][$key])>0)
				{
					foreach ($post['ChooseSuplement'][$key] as $index => $ChooseSuplement) {
						$tempChooseSuplement = new ChooseSuplement();
						$tempChooseSuplement->attributes = $ChooseSuplement;
						$tempChooseSuplement->validate();
						if($tempChooseSuplement->hasErrors() and !$error)
						{
							$model->addError('id', 'Please fill out the fields in red to continue');
							$error=true;
						}
						$return['ChooseSuplement'][$key][$index] = $tempChooseSuplement;
					}
				}
			}
		}
//		CVarDumper::dump($return,10,true);exit;

	}
}
