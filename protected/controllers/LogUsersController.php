<?php

class LogUsersController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
		);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model=new UsersDelete('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['UsersDelete']))
            $model->attributes=$_GET['UsersDelete'];

        $this->render('index',array(
            'model'=>$model,
        ));
	}


}
