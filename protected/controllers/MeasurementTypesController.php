<?php

class MeasurementTypesController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),*/
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','index','view','create','update'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MeasurementTypes('create');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MeasurementTypes']))
		{
			$model->attributes=$_POST['MeasurementTypes'];
			$model->youtubeId = trim($model->youtubeId);

			if(!empty(CUploadedFile::getInstance($model,'idAttachment')) and $model->validate() )
			{
				$att = new ImageForm('Crear');
				$att->image=CUploadedFile::getInstance($model,'idAttachment');
				$att->idAttachmentType = 2;
				$att->path = '/img/imgMeasurementTypes/';
				$model->idAttachment = $att->save();

				if(count($att->getErrors('image'))>0)
				{
					foreach ($att->getErrors('image') as $error) {
						$model->addError('idAttachment', $error);
					}
				}
			}
			else
			{
				$model->addError('idAttachment','Photo is required');
			}
			if(!count($model->getErrors())>0 and $model->save())
				$this->redirect(array('index','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MeasurementTypes']))
		{
			$orgIdAttachment = $model->idAttachment;
			$model->attributes=$_POST['MeasurementTypes'];
			$model->youtubeId = trim($model->youtubeId);

			if(!empty(CUploadedFile::getInstance($model,'idAttachment')))
			{
				$att = new ImageForm('Actualizar');
				$att->image=CUploadedFile::getInstance($model,'idAttachment');

				$model->idAttachment = $att->save();
			}
			else
			{
				$model->idAttachment = $orgIdAttachment;
			}
			if($model->save())
				$this->redirect(array('index','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model=$this->loadModel($id);
		if($model->is_deletable())
        {
            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }

	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{


//	    $model = MeasurementTypes::model()->findByPk(1);
//	    CVarDumper::dump($model->is_deletable(),10,true);exit;

		$model=new MeasurementTypes('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MeasurementTypes']))
			$model->attributes=$_GET['MeasurementTypes'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MeasurementTypes the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MeasurementTypes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MeasurementTypes $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='measurement-types-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function validateFolderDestination($path)
	{
		if(!is_dir($path))
		{
			return false;
		}
		else
		{
			if(!is_writable($path))
			{
				return false;
			}
		}
		return true;
	}
}
