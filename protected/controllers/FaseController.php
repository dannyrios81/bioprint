<?php

class FaseController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }
    public function accessRules()
    {
        return array(
            /*array('allow', // allow authenticated user to perform 'create' and 'update' actions
                    'actions'=>array('update','index'),
                    'users'=>array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                    'actions'=>array('create'),
                    'users'=>array('Admin','Trainer'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                    'actions'=>array('delete'),
                    'users'=>array('Admin'),
            ),*/
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('edit','index','delete','create','listBodyPart','listFase','exercise','AddExercise',
                    'NewAddExercise','dropsMethod','dropsBodyArea','dropsFamilyExercise','dropsExercise','SelectLaterality',
                    'CloneExercise','DeleteExercise','editFaseExercise','copyPhase','pastePhase','DropsRange','Pdf','NewEditFaseExercise',
                    'validateFirstExecution','SearchExercise'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
	public function actionIndex($id=0)
	{
	    if (empty($id))
            throw new CHttpException(404,'The requested page does not exist.');
	    else
        {
            $user = Users::model()->findByPk($id);
            $param=Params::model()->findByAttributes(['codParam'=>'WORKOUTVIDEO']);

            $model=new UsersPhase();
            $model->idUser=$id;

            $model->setScenario('search');

            if(isset($_GET['UsersPhase']))
            {
                $model->attributes=$_GET['UsersPhase'];
            }
            $this->render('index',['user'=>$user,'model'=>$model,'param'=>$param]);
        }
	}
	public function actionCreate($id=0)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $response=[];

            $model = new FaseCreateForm();
            if(isset($_POST['FaseCreateForm']))
            {
                $model->idUser=$id;
                $model->attributes=$_POST['FaseCreateForm'];
                if ($model->validate() && $model->save())
                {
                    $response['html']= '';
                    $response['id']=(integer)$model->id;
                    echo CJavaScript::jsonEncode($response);
                    Yii::app()->end();

                }
            }
            $response=[];
            $response['id']=0;
            $response['html']=$this->renderPartial('create',['model'=>$model],true,true);
            echo CJavaScript::jsonEncode($response);
            Yii::app()->end();
        }
    }
    public function actionEdit($id=0)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $response=[];

            $model = new FaseCreateForm($id);
            if(isset($_POST['FaseCreateForm']))
            {
                $model->attributes=$_POST['FaseCreateForm'];
                if ($model->validate() && $model->save())
                {
                    $response['html']= '';
                    $response['id']=(integer)$model->id;
                    echo CJavaScript::jsonEncode($response);
                    Yii::app()->end();

                }
            }
            $response=[];
            $response['id']=0;
            $response['html']=$this->renderPartial('create',['model'=>$model],true,true);
            echo CJavaScript::jsonEncode($response);
            Yii::app()->end();
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $model = UsersPhase::model()->findByPk($id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');

            /**
             * eliminar todos los registros relacionados de las siguientes tablas en el siguiente orden
             * 1. execution_exercise
             * 2. execution_exercise_calc
             * 3. execution
             * 4. user_phase_exercise
             * 5. user_phase
             */
            foreach ($model->executions as $execution) {
                foreach ($execution->executionExercises as $executionExercise) {
                    $executionExercise->delete();
                }
                foreach ($execution->executionExerciseCalcs as $executionExerciseCalc) {
                    $executionExerciseCalc->delete();
                }
                $execution->delete();
            }
            foreach ($model->userPhaseExercises as $userPhaseExercise) {
                $userPhaseExercise->delete();
            }
            $model->delete();
        }
    }
    public function actionListBodyPart()
    {

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $model = new FaseCreateForm();
            $model->idBodyPart = $_POST['id'];

            echo CHtml::activeDropDownList($model,'idBodyPart',FaseCreateForm::bodyParts(),['empty'=>'Select ...','class'=>"form-control"]);
        }

    }
    public function actionListFase()
    {

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $model = new FaseCreateForm();
            $model->idFase = $_POST['id'];

            echo CHtml::activeDropDownList($model,'idFase',FaseCreateForm::fases(),['empty'=>'Select ...','class'=>"form-control"]);
        }

    }
    /**
     * accion que permite agregarle a una userphase sus ejercicios
     * @param $id identificador de la userphase
     */
    public function actionExercise($id)
    {
        if (empty($id))
            throw new CHttpException(404,'The requested page does not exist.');
        else
        {
            $UsersPhase = UsersPhase::model()->findByPk($id);
            $model = new UserPhaseExercise('search');
            $model->idUserPhase = $id;

            if(isset($_GET['UserPhaseExercise']))
            {
                $model->attributes=$_GET['UserPhaseExercise'];
            }
            $this->render('exerciseView',['model'=>$model,'idUserPhase'=>$id,'UsersPhase'=>$UsersPhase]);
        }
    }
    public function actionAddExercise($id)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $response=[];

            $model = new UserPhaseExerciseForm();
            if(isset($_POST['UserPhaseExerciseForm']))
            {
                try
                {
                    $model->idUserPhase=$id;
                    $model->attributes=$_POST['UserPhaseExerciseForm'];
                    //CVarDumper::dump($model->validate(),10,false);exit;
                    if ($model->validate() && $model->save())
                    {
                        $response['html']= '';
                        $response['id']=(integer)$model->id;
                        echo CJavaScript::jsonEncode($response);
                        Yii::app()->end();
                    }
                }
                catch (Exception $e)
                {
//                    CVarDumper::dump($e,10,false);exit;
                }

            }
            $response=[];
            $response['id']=0;
            $response['html']=$this->renderPartial('addExercise',['model'=>$model],true,true);
            echo CJavaScript::jsonEncode($response);
            Yii::app()->end();
        }

    }
    public function actionNewAddExercise($id)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $response=[];

            $model = new NewUserPhaseExerciseForm();
            if(isset($_POST['NewUserPhaseExerciseForm']))
            {
                try
                {
                    $model->idUserPhase=$id;
                    $model->attributes=$_POST['NewUserPhaseExerciseForm'];

                    if(empty($model->idExercise))
                    {
                        $temp = Exercise::model()->findByPk($model->idExercise);
//                        $temp = new Exercise();
                        $model->idMethod = $temp->idMethod;
                        $model->idBodyArea = $temp->idBodyArea;
                        $model->idFamilyExercise = $temp->idFamilyExercise;
                        $model->idOrientation = $temp->idOrientation;
                        $model->idImplement = $temp->idImplement;
                        $model->idGrip = $temp->idGrip;
                        $model->idLaterality = $temp->idLaterality;
                    }

                    if ($model->validate() && $model->save())
                    {
                        $response['html']= '';
                        $response['id']=(integer)$model->id;
                        echo CJavaScript::jsonEncode($response);
                        Yii::app()->end();
                    }
                }
                catch (Exception $e)
                {
//                    CVarDumper::dump($e,10,false);exit;
                }

            }
            $response=[];
            $response['id']=0;
            $response['html']=$this->renderPartial('newAddExercise',['model'=>$model],true,true);
            echo CJavaScript::jsonEncode($response);
            Yii::app()->end();
        }

    }
    /**
     * funcion que retorna un json con las opciones de los dropdowns de intervalo de repeticion,
     * rest y rango de movimiento los cuales son dependientes del metodo
     * @param $id integer id del metodo seleccionado
     */
    public function actionDropsMethod()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $id = $_POST['id'];

//            $returnArray['RepeatInterval'] = BHtml::listOptionsTextFielDrop(CHtml::listData(RepeatInterval::model()->findAllByAttributes(['methodId'=>$id]),'id','description'));
//            $returnArray['Rest'] = BHtml::listOptionsTextFielDrop(CHtml::listData(Rest::model()->findAllByAttributes(['methodId'=>$id]),'id','description'));
//            $returnArray['NumberSeries'] = BHtml::listOptionsTextFielDrop(CHtml::listData(NumberSeries::model()->findAllByAttributes(['methodId'=>$id]),'id','description'));

            $htmlOptions=['empty'=>'Select.......'];

            $returnArray['RepeatInterval'] = CHtml::listOptions('',CHtml::listData(RepeatInterval::model()->findAllByAttributes(['methodId'=>$id]),'id','description'),$htmloptions);
            $returnArray['Rest'] = CHtml::listOptions('',CHtml::listData(Rest::model()->findAllByAttributes(['methodId'=>$id]),'id','description'),$htmloptions);
            $returnArray['NumberSeries'] = CHtml::listOptions('',CHtml::listData(NumberSeries::model()->findAllByAttributes(['methodId'=>$id]),'id','description'),$htmloptions);
            $returnArray['BodyArea'] = CHtml::listOptions([],CHtml::listData(BodyArea::model()->findAllByAttributes(['idMethod'=>$id]),'id','description'),$htmlOptions);

            echo CJSON::encode($returnArray);
            Yii::app()->end();
        }
    }
    /**
     * funcion que retorna un json con las opciones de los dropdowns de familia de ejercicios
     * @param $id integer id del area de cuerpo seleccionada
     */
    public function actionDropsBodyArea()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $id = $_POST['id'];

            $htmlOptions=['empty'=>'Select.......'];
            $returnArray['FamilyExercise'] = CHtml::listOptions([],CHtml::listData(FamilyExercise::model()->findAllByAttributes(['bodyAreaId'=>$id]),'id','description'),$htmlOptions);

            echo CJSON::encode($returnArray);
            Yii::app()->end();
        }
    }
    /**
     * funcion que retorna un json con las opciones de los dropdowns de grip
     * implemento y orientacion los cuales son dependientes de la familia de ejercicios seleccionados
     * @param $id integer id de la familia de ejercicios seleccionados
     */
    public function actionDropsFamilyExercise()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $id = $_POST['id'];

            $htmlOptions=['empty'=>'Select.......'];
            $returnArray['FamilyExercise'] = CHtml::listOptions([],CHtml::listData(FamilyExercise::model()->findAllByAttributes(['bodyAreaId'=>$id]),'id','description'),$htmlOptions);

            echo CJSON::encode($returnArray);
            Yii::app()->end();
        }
    }
    /**
     * funcion que retorna un json con las opciones de los dropdowns de lateralidad dependiente del ejercicio seleccionado
     * @param $id integer id del ejercicio seleccionado
     */
    public function actionDropsExercise()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $valid = true;
            $dataValid = array('idMethod' => '','idBodyArea' => '','idFamilyExercise' => '','idOrientation' => '','idImplement' => '','idGrip' => '');
            $data = $_POST['UserPhaseExerciseForm'];

            $data = array_intersect_key($data,$dataValid);
//            foreach ($data as $datum) {
//                if(empty($datum))
//                {
//                    $valid=false;
//                    continue;
//                }
//            }
            /*if($valid)
            {*/
                $htmlOptions=['empty'=>'Select.......'];
                $returnArray['Exercise'] = CHtml::listOptions([],CHtml::listData(Exercise::model()->findAllByAttributes($data,array('group'=>'idBodyArea,idFamilyExercise,idOrientation,idImplement,idGrip,description')),'id','description'),$htmlOptions);
                $returnArray['valid']=1;
//            }
            /*else
                $returnArray['valid']=0;*/

            echo CJSON::encode($returnArray);
            Yii::app()->end();
        }
    }
    /**
     * funcion que retorna un json con las opciones de los dropdowns de range dependiente del ejercicio seleccionado
     * @param $id integer id del ejercicio seleccionado
     */
    public function actionDropsRange()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $id = $_POST['id'];

            $htmlOptions=['empty'=>'Select.......'];

            $exercise = Exercise::model()->findByPk($id);
            $idMethod = $exercise->idMethod;
            if(!empty($exercise ))
            {
                $exercise = Exercise::model()->findAllByAttributes(['description'=>$exercise->description]);
                $RangesIds=[];
                foreach ($exercise as $item) {
                    $RangesIds[] = $item->idRangeMotion;
                }

                $criteria = new CDbCriteria();
                $criteria->compare('id',$RangesIds);

                $criteriaOrderRest = new CDbCriteria(array('order'=>'CONVERT(description, SIGNED INTEGER) ASC'));

                $returnArray['Range'] = CHtml::listOptions([],CHtml::listData(RangeMotion::model()->findAll($criteria),'id','description'),$htmlOptions);
                $returnArray['RepeatInterval'] = CHtml::listOptions('',CHtml::listData(RepeatInterval::model()->findAllByAttributes(['methodId'=>$idMethod]),'id','description'),$htmloptions);
                $returnArray['Rest'] = CHtml::listOptions('',CHtml::listData(Rest::model()->findAllByAttributes(['methodId'=>$idMethod],$criteriaOrderRest),'id','description'),$htmloptions);
                $returnArray['NumberSeries'] = CHtml::listOptions('',CHtml::listData(NumberSeries::model()->findAllByAttributes(['methodId'=>$idMethod]),'id','description'),$htmloptions);
            }
            else
            {
                $returnArray['Range'] = CHtml::listOptions([],[],$htmlOptions);
            }

            echo CJSON::encode($returnArray);
            Yii::app()->end();
        }
    }
    /**
     * funcion que retorna el id de la lateralidad del ejercicio seleccionado
     */
    public function actionSelectLaterality()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $id = $_POST['id'];

            $temp = Exercise::model()->findByPk($id);

            $returnArray['idLaterality'] = $temp->idLaterality;

            echo CJSON::encode($returnArray);
            Yii::app()->end();
        }
    }
    /**
     * ruta para clonar uno o varios ejercicios dentro de la fase
     */
    public function actionCloneExercise()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $retorno['valid'] = 0;
            try
            {
                if(array_key_exists('ids',$_POST) and count($_POST['ids'])>0)
                {

                    foreach ($_POST['ids'] as $id) {
                        $temp =UserPhaseExercise::model()->findByPk($id);
//                        $temp = new UserPhaseExercise();
                        $temp->id = null;
                        $temp->setIsNewRecord(true);
                        $temp->save(false);
                    }
                    $retorno['valid']=1;
                    $retorno['message'] = "Exercises were duplicated satisfactorily";
                }

            }
            catch (Exception $e)
            {
                $retorno['valid']=1;
                $retorno['message'] = "Error deleting data. Please try again";
            }
            echo CJSON::encode($retorno);
            Yii::app()->end();
        }
    }
    /**
     * Ruta para eliminar ejercicio de la fase, incluyendo las ejecusiones que tenga relacionadas ese ejercicio
     */
    public function actionDeleteExercise($id)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $temp = UserPhaseExercise::model()->findByPk($id);
            $transaction = Yii::app()->db->beginTransaction();
            try
            {
                /**
                 * eliminar las siguientes tablas en el siguiente orden
                 * 1. execution_exercise
                 * 2. execution_exercice_calc
                 * 3. user_phase_exercise
                 */
//                foreach ($_POST['ids'] as $id) {
//                    $temp->delete();
//                }
                foreach ($temp->executionExercises as $executionExercise) {
                    $executionExercise->delete();
                }
                foreach ($temp->executionExerciseCalcs as $executionExerciseCalc) {
                    $executionExerciseCalc->delete();
                }


                $temp->delete();
                $transaction->commit();
            }
            catch (Exception $e)
            {
                CVarDumper::dump($e,10,true);
                $transaction->rollBack();
            }

        }
    }
    public function actionEditFaseExercise($id)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $response=[];

            $model = new UserPhaseExerciseForm($id);
//            CVarDumper::dump($model,10,true);exit;
            if(isset($_POST['UserPhaseExerciseForm']))
            {
//                CVarDumper::dump($model->getScenario());exit;
                $model->attributes=$_POST['UserPhaseExerciseForm'];
                if ($model->validate() && $model->save())
                {
                    $response['html']= '';
                    $response['id']=(integer)$model->id;
                    echo CJavaScript::jsonEncode($response);
                    Yii::app()->end();
                }
            }
            $response=[];
            $response['id']=0;
            $response['html']=$this->renderPartial('editExercise',['model'=>$model],true,true);
            echo CJavaScript::jsonEncode($response);
            Yii::app()->end();
        }
    }
    public function actionNewEditFaseExercise($id)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $response=[];

            $model = new NewUserPhaseExerciseForm($id);
            if(isset($_POST['NewUserPhaseExerciseForm']))
            {
                try
                {

                    $model->attributes=$_POST['NewUserPhaseExerciseForm'];

                    if(empty($model->idExercise))
                    {
                        $temp = Exercise::model()->findByPk($model->idExercise);
//                        $temp = new Exercise();
                        $model->idMethod = $temp->idMethod;
                        $model->idBodyArea = $temp->idBodyArea;
                        $model->idFamilyExercise = $temp->idFamilyExercise;
                        $model->idOrientation = $temp->idOrientation;
                        $model->idImplement = $temp->idImplement;
                        $model->idGrip = $temp->idGrip;
                        $model->idLaterality = $temp->idLaterality;
                    }

                    if ($model->validate() && $model->save())
                    {
                        $response['html']= '';
                        $response['id']=(integer)$model->id;
                        echo CJavaScript::jsonEncode($response);
                        Yii::app()->end();
                    }
                }
                catch (Exception $e)
                {
//                    CVarDumper::dump($e,10,false);exit;
                }

            }
            $response=[];
            $response['id']=0;
            $response['html']=$this->renderPartial('newEditFaseExercise',['model'=>$model],true,true);
            echo CJavaScript::jsonEncode($response);
            Yii::app()->end();
        }
    }
    /**
     * accion para guardar en sesion de usuario, las phases seleccionadas para ser pegadas a otro usuario
     */
    public function actionCopyPhase()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $retorno['valid'] = 0;
            try
            {
                if(array_key_exists('ids',$_POST) and count($_POST['ids'])>0)
                {
                    Yii::app()->user->setState('PhasesCopied', $_POST['ids']);

                    $retorno['valid']=1;
                    $retorno['message'] = "Successful phase copy";
                }

            }
            catch (Exception $e)
            {
                $retorno['valid']=1;
                $retorno['message'] = "Error deleting data. Please try again";
            }
            echo CJSON::encode($retorno);
            Yii::app()->end();
        }
    }
    /**
     * accion para crear (pegar) las phases previamente seleccionadas que estan guardadas en una variable de sesion del usuario
     * @var int $id id de usuario a quien se le van a pegar las fases
     */
    public function actionPastePhase($id)
    {
        /*
         * para borrar un state de yii, se setea la variable a null
         * Yii::app()->user->setState('foo',null);
         */

        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $retorno['valid'] = 0;
            try {
                $phasesCopiadas = Yii::app()->user->getState('PhasesCopied');

                if(is_array($phasesCopiadas) and $phasesCopiadas!==array()) {
                    $criteria = new CDbCriteria();
                    $criteria->addInCondition('id', $phasesCopiadas);
                    $phases = UsersPhase::model()->findAll($criteria);

                    foreach ($phases as $phase) {
//                        $phase = new UsersPhase();

                        $exercices = $phase->userPhaseExercises;

                        $phase->id = null;
                        $phase->date = date('Y-m-d');
                        $phase->idUser = $id;
                        $phase->setIsNewRecord(true);
                        $phase->save(false);

                        foreach ($exercices as $exercice) {
//                            $exercice = new UserPhaseExercise();

                            $exercice->id = null;
                            $exercice->idUserPhase = $phase->id;
                            $exercice->setIsNewRecord(true);
                            $exercice->save(false);

                        }

                    }
                    $retorno['valid'] = 1;
                    $retorno['message'] = "Successfully pasted phases";

                    Yii::app()->user->setState('PhasesCopied',null);
                }
                else
                {
                    $retorno['valid'] = 1;
                    $retorno['message'] = "there are no copied phases";
                }

            }
            catch (Exception $e)
            {
                $retorno['valid']=1;
                $retorno['message'] = "Error paste data. Please try again";
                $retorno['error'] = $e->getMessage();
            }
            echo CJSON::encode($retorno);
            Yii::app()->end();
        }
    }
    public function actionPdf($id)
    {
//        $this->layout='pdflayout';

        $iduserphase = $id;

        $user = new Users();
        $userPhaseExercises = new UserPhaseExercise();
        $userPhase = new UsersPhase();

        $userPhaseExercises = UserPhaseExercise::model()->findAllByAttributes(['idUserPhase'=>$iduserphase],['order'=>'`order`']);
        $userPhase = UsersPhase::model()->findByPk($iduserphase);

        if(!empty($userPhase))
        {
            $user = Users::model()->findByPk($userPhase->idUser);
            $user = (!empty($user))?$user:new Users();
        }


        if(!empty($user))
        {
            $practitioner = Users::model()->find('email=:email',[':email' => $user->parentEmail]);
            $practitioner = (!empty($practitioner))?$practitioner:new Users();
        }

//        $this->render('pdf', array('maxSets'=>Calculos::maxSetsPhase($iduserphase),'userPhaseExercises'=>$userPhaseExercises,'userPhase'=>$userPhase,'user'=>$user,'practitioner'=>$practitioner));

        $html2pdf = Yii::app()->ePdf->HTML2PDF('L', 'A4', 'en');


        $html2pdf->WriteHTML($this->renderPartial('pdf', array('maxSets'=>Calculos::maxSetsPhase($iduserphase),'userPhaseExercises'=>$userPhaseExercises,'userPhase'=>$userPhase,'user'=>$user,'practitioner'=>$practitioner), true));
        $html2pdf->Output('Phase.pdf','D');
    }
    public function actionValidateFirstExecution($id)
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $return=[];
            $return['show'] = 0;
            $return['url'] =  Yii::app()->createAbsoluteUrl("execution/lastExecution",["id"=>$id]);

            $info = Calculos::lastExecutionPhase($id);

//            CVarDumper::dump($info,10,true);exit;
            if($info!==array() and isset($info["totalExecutions"]) and $info["totalExecutions"]<=0)
            {
                $return['show'] = 1;
            }

            echo CJSON::encode($return);
            yii::app()->end();

        }
    }
    public function actionSearchExercise()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            //CVarDumper::dump($_REQUEST,10);exit;

            /* partir la cadena de busqueda dependiendo la cantidad de espacios que tenga*/
            $criteria = new CDbCriteria;
            $params=$this->splitString((string)$_GET['search']);

            if(count($params)>0)
            {
                foreach ($params as $param) {
                    $criteria->addSearchCondition('description',(string)$param,true,'AND');
                }
                $criteria->group = 'description';
                $return=[];
                $result = CHtml::listData(Exercise::model()->findAll($criteria),'id','description');
                foreach ($result as $index => $item) {
                    $return['results'][]=['id'=>$index,'text'=>$item];
                }
            }
            else
            {
                $return['results'][]=[];
            }
            //header('Content-type: application/json');
            echo CJSON::encode( $return );
            Yii::app()->end();
        }
    }
    protected function splitString($splitString)
    {
        $splitString = trim($splitString);
        $arrayString = explode(' ',$splitString);

        $result=[];

        foreach ($arrayString as $baseString) {
            if(strlen($baseString)>=3)
            {
                $result[]=$baseString;
            }
        }
        return $result;
    }
}
