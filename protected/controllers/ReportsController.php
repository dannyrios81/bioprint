<?php

class ReportsController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),*/
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('adminReports','index','trainerReports','userReports','treport1','treport2','treport3',
                    'treport4','treport5','treport6','treport7','treport7a','treport7b','treport7c','treport7d','ureport1',
                    'ureport2','ureport3','ureport4','ureport4a','Areport1','Areport1a','Areport2','Areport3','Areport4',
                    'Areport5'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionIndex()
	{
		switch(Yii::app()->user->getState('userType'))
		{
			case 'Admin':
				$this->redirect(Yii::app()->createUrl('reports/adminReports'));
			break;
			case 'Trainer':
				$this->redirect(Yii::app()->createUrl('reports/trainerReports'));
//				$this->redirect(Yii::app()->createUrl('reports/adminReports'));

				break;
			case 'User':
				$this->redirect(Yii::app()->createUrl('reports/userReports'));
			break;

		}
		$this->render('index');
	}
	/*
	 * Administrator reports
	 */
	public function actionAdminReports()
	{
		$this->render('adminReports');
	}
	public function actionAreport1()
	{
		$this->pageTitle = $title = "Seminars Attendees";

		$model=new Course('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Course']))
			$model->attributes=$_GET['Course'];

		$this->render('adminReports1',array(
			'model'=>$model,'title'=>$title
		));
		
	}
	public function actionAreport1a($id)
	{
		$this->pageTitle = $title = "Seminars Attendees";

		$users = UsersCourses::model()->findAllByAttributes(['idCourse'=>$id]);
		$seminar = Course::model()->findByPk($id);

		$this->render('adminReports1a',['title'=>$title,'users'=>$users,'seminar'=>$seminar]);

	}
	public function actionAreport2()
	{
		$this->pageTitle = $title = "Licenses Report";

		/*
		 * quyery para la primera tabla faltaria sacar los subtotales
		 *
		 * SELECT Count(l.id) AS count,p.`name`,l.renewal,p.cost*Count(l.id) total FROM licences AS l INNER JOIN product AS p ON l.idProduct = p.id GROUP BY l.idProduct, l.renewal
		 */

		/*
		 * query para sacar la informacion por meses graficas
		 * SELECT Count(l.id) AS count,p.`name`,l.renewal,p.cost*Count(l.id) total,m.month FROM licences AS l INNER JOIN product AS p ON l.idProduct = p.id inner join months m on m.id = MONTH(l.purchaseDate) GROUP BY l.idProduct, l.renewal,m.month		 
		 */

		$condition = '';

		if(!empty($_POST['beginDate']) or !empty($_POST['endDate']))
		{
			$condition = 'where ';
			$flag = false;
			if(!empty($_POST['beginDate']))
			{
				$condition = $condition . "l.purchaseDate >= '". $_POST['beginDate']."'";
				$flag = true;
			}
			if(!empty($_POST['endDate']))
			{
				if($flag)
					$condition = $condition . " and l.purchaseDate <= '". $_POST['endDate']."'";
				else
					$condition = $condition . "l.purchaseDate <= '". $_POST['endDate']."''";

			}
		}

		$tabla = Yii::app()->db->createCommand('
			SELECT 
			Count(l.id) AS count,
			p.`name`,
			l.renewal,
			format(p.cost*Count(l.id),2) total 
			FROM 
			licences AS l 
			INNER JOIN product AS p ON l.idProduct = p.id 
			'.$condition.'
			GROUP BY l.idProduct, l.renewal
			ORDER BY p.name,l.renewal')
			->queryAll();

		$graficas = Yii::app()->db->createCommand('
			SELECT 
			Count(l.id) AS count,
			p.`name`,
			p.cost*Count(l.id) total,
			m.month,
			m.id
			FROM 
			licences AS l 
			INNER JOIN product AS p ON l.idProduct = p.id 
			inner join months m on m.id = MONTH(l.purchaseDate) 
			'.$condition.'
			GROUP BY l.idProduct,m.month
			ORDER BY p.name,m.id')
			->queryAll();




		/*crear la serie de la grafica y los datos de la grafica*/

		$serie = [];
		$dataLicencias = [];
		$dataFacturado = [];
		foreach ($graficas as $grafica) {
			$serie[$grafica['id']-1] = $grafica['month'];
			$dataLicencias[$grafica['name']]['data'][] = (integer)$grafica['count'];
			$dataFacturado[$grafica['name']]['data'][] = (integer)$grafica['total'];
			$dataLicencias[$grafica['name']]['name'] = $dataFacturado[$grafica['name']]['name'] = $grafica['name'];
		}


//		CVarDumper::dump($condition,10,true);exit;

		if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
//			CVarDumper::dump($_POST,10,true);exit;
			echo CJSON::encode(['tabla'=>$tabla,'dataLicencia'=>array_values($dataLicencias),'dataFacturado'=>array_values($dataFacturado),'serie'=>array_values($serie)]);
		}
		else
		{
			$this->render('adminReports2',['title'=>$title,'tabla'=>$tabla,'dataLicencia'=>array_values($dataLicencias),'dataFacturado'=>array_values($dataFacturado),'serie'=>array_values($serie)]);
		}
	}
	public function actionAreport3()
	{
		$this->pageTitle = $title = "Active Practitioners";

//		and ((UP.lastDatePurchase >= '2016-01-01' and  UP.lastDatePurchase <= '2016-05-28' ) or
//		(UP.dueDate>= '2016-01-01' and  UP.dueDate<= '2016-05-28') or
//		(UP.lastDatePurchase >= '2016-01-01' and  UP.dueDate<= '2016-05-28'))

//		and (m.date >= '2016-01-01' and m.date <= '2016-05-28' )


		$conditionUP = '';
		$conditionM = '';


		if(!empty($_POST['beginDate']) or !empty($_POST['endDate']))
		{
			$flag = false;
			if(!empty($_POST['beginDate']))
			{
				$conditionM = "m.date >= '". $_POST['beginDate']."'";
				$conditionUP[0]="UP.lastDatePurchase >= '". $_POST['beginDate']."'";
				$conditionUP[1]="UP.dueDate >= '". $_POST['beginDate']."'";
				$conditionUP[2]="UP.lastDatePurchase >= '". $_POST['beginDate']."'";
				$flag=true;
			}
			if(!empty($_POST['endDate']))
			{
				if($flag)
				{
					$conditionM = $conditionM . " and m.date <= '". $_POST['endDate']."'";
					$conditionUP[0] = $conditionUP[0] . " and UP.lastDatePurchase <= '". $_POST['endDate']."'";
					$conditionUP[1] = $conditionUP[1] . " and UP.dueDate <= '". $_POST['endDate']."'";
					$conditionUP[2] = $conditionUP[2] . " and UP.dueDate <= '". $_POST['endDate']."'";
				}
				else
				{
					$conditionM = $conditionM . "m.date <= '". $_POST['endDate']."''";
					$conditionUP[0] = $conditionUP[0] . " UP.lastDatePurchase <= '". $_POST['endDate']."''";
					$conditionUP[1] = $conditionUP[1] . " UP.dueDate <= '". $_POST['endDate']."''";
					$conditionUP[2] = $conditionUP[2] . " UP.dueDate <= '". $_POST['endDate']."''";
				}

			}
		}
		if(!empty($conditionM))
			$conditionM=' and ('.$conditionM.')';

		if(!empty($conditionUP))
			$conditionUP=' and (('.implode(') or (',$conditionUP).'))';

		$tablas = Yii::app()->db->createCommand('
			select 
			a.id,
			a.email,
			a.`name`,
			a.lastName,
			a.withOutMeasure,
			b.withMeasure
			FROM
			(
			select 
			T.id,
			T.email,
			T.`name`,
			T.lastName,
			\'1\' withOutMeasure
			FROM
			users T
			INNER JOIN user_product UP on UP.idUsers=T.id '.$conditionUP .'
			) a
			left JOIN 
			(SELECT 
			T.id,
			T.email,
			T.`name`,
			T.lastName,
			\'1\' withMeasure
			FROM
			users T
			INNER JOIN users C on C.parentEmail = T.email
			INNER JOIN measurement m on m.idUsers=C.id '.$conditionM.'
			GROUP BY
			T.id) b on b.id=a.id
			GROUP BY a.id, a.email,a.`name`,a.lastName
			;')
			->queryAll();


//		CVarDumper::dump($tablas,10,true);exit;

		$inuseUsers =[];
		$withoutuseUsers =[];
		$withuse = 0;
		$total = 0;

		foreach ($tablas as $fila) {
			if($fila['withOutMeasure']==$fila['withMeasure'])
			{
				$inuseUsers[]=$fila;
				$withuse++;
			}
			else
			{
				$withoutuseUsers[]=$fila;
			}
			$total++;
		}



		$dataGraph=[];

		$dataGraph[]=['name'=>'In Use','y'=>$withuse,'id'=>'inuse'];
		$dataGraph[]=['name'=>'Without Use','y'=>$total-$withuse,'id'=>'withoutuse'];
		
				
//		CVarDumper::dump($condition,10,true);exit;

		$renderWhitoutTable = $this->renderPartial('tablewithoutusers',['withoutusers'=>$withoutuseUsers],true);
		$renderInuseTable = $this->renderPartial('tableinuseUsers',['inuseUsers'=>$inuseUsers],true);

		if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
			echo CJSON::encode(['data'=>$dataGraph,'inusetable'=>$renderInuseTable,'withoutusetable'=>$renderWhitoutTable]);
		}
		else
		{
			$this->render('adminReports3',['title'=>$title,'dataGraph'=>$dataGraph,'inusetable'=>$renderInuseTable,'withoutusetable'=>$renderWhitoutTable]);
		}

		
	}
	public function actionAreport4()
	{
		$this->pageTitle = $title = "Products";

		$tablas = Yii::app()->db->createCommand('
			select 
			p.name,
			COUNT(u.id) totalUsers,
			COUNT(up.lastDatePurchase) usersActives
			from 
			users u
			LEFT JOIN user_product up on up.idUsers=u.id and now() >=up.lastDatePurchase and now()<=up.dueDate
			LEFT JOIN product p on p.id = up.idProduct
			where 
			u.userType in (\'Trainer\',\'Practitioner1\');
			;')
			->queryAll();


//		CVarDumper::dump($tablas,10,true);exit;

//		$renderWhitoutTable = $this->renderPartial('tablewithoutusers',['withoutusers'=>$withoutuseUsers],true);


		$this->render('adminReports4',['title'=>$title,'data'=>$tablas]);

	}
	public function actionAreport5()
	{
		$this->pageTitle = $title = "Measure Sets By Pratitioner";

		$conditionM = '';


		if(!empty($_POST['beginDate']) or !empty($_POST['endDate']))
		{
			$flag = false;
			if(!empty($_POST['beginDate']))
			{
				$conditionM = "m.date >= '". $_POST['beginDate']."'";
				$flag=true;
			}
			if(!empty($_POST['endDate']))
			{
				if($flag)
					$conditionM = $conditionM . " and m.date <= '". $_POST['endDate']."'";
				else
					$conditionM = $conditionM . "m.date <= '". $_POST['endDate']."''";
			}
		}
		if(!empty($conditionM))
			$conditionM=' and ('.$conditionM.')';

		$tabla = Yii::app()->db->createCommand('
			select
			T.name,
			T.lastName,
			T.email,
			SUM(a.qty_measures) measures
			FROM
			users T
			INNER JOIN (
			select
			u.name,
			u.lastName,
			u.parentEmail,
			count( DISTINCT m.date) qty_measures
			from
			users u
			left JOIn measurement m on u.id=m.idUsers '.$conditionM.'
			GROUP BY
			u.parentEmail,u.name,u.lastName) a on T.email=a.parentEmail
			where T.userType in ("Trainer","Practitioner1")
			GROUP BY T.email')
			->queryAll();

//		CVarDumper::dump($tabla,10,true);exit;
		
		$renderTabla = $this->renderPartial('tableCountMeasuresPratitioner',['tabla'=>$tabla],true);

		if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
			echo CJSON::encode(['tabla'=>$renderTabla]);
		}
		else
		{
			$this->render('adminReports5',['title'=>$title,'tabla'=>$renderTabla]);
		}
	}
	/*
	 * Trainer Reports
	 */
	public function actionTrainerReports()
	{
		$this->render('trainerReports');
	}
	public function actionTreport1()
	{
		$this->pageTitle = $title = "Gender Distribution";
		$user = Yii::app()->user->getState('_user');

		$data = Yii::app()->db->createCommand()
			->select('IFNULL(gender,"Profile Pending") AS gender,COUNT(*) qty')
			->from('users')
			->where('parentEmail = "'.$user->email.'"')
			->group('gender')
			->queryAll();

		$dataGraph=[];
		foreach ($data as $item) {
			$dataGraph[]=[$item['gender'],(double)$item['qty']];
		}

		$this->render('trainerReport1',['title'=>$title,'dataGraph'=>$dataGraph]);
	}
	public function actionTreport2()
	{
		$this->pageTitle = $title = "Age Distribution";
		$user = Yii::app()->user->getState('_user');

		$data = Yii::app()->db->createCommand()
			->select("CONCAT(a.minAge,' - ',a.maxAge) ageGroup, count(a.id) qty")
			->from('age_group a')
			->join('users u','TIMESTAMPDIFF(YEAR, u.bornDate, CURDATE()) >= a.minage  and  TIMESTAMPDIFF(YEAR, u.bornDate, CURDATE())  < a.maxage and parentEmail = "'.$user->email.'"')
			->group('a.id')
			->queryAll();

		//CVarDumper::dump($data,10,true);exit;

		$dataGraph=[];
		foreach ($data as $item) {
			$dataGraph[]=[$item['ageGroup'],(double)$item['qty']];
		}

		$this->render('trainerReport2',['title'=>$title,'dataGraph'=>$dataGraph]);
	}
	public function actionTreport3()
	{
		$this->pageTitle = $title = "Body Fat Percentage Distribution";
		$user = Yii::app()->user->getState('_user');

		$data = Yii::app()->db->createCommand('
		select 
		a.minData,a.maxData,  count(a.id) qty  
		from 
		fatandlean_group a 
		inner JOIN measurement m1 on m1.measurement>=a.minData and m1.measurement<a.maxData 
		inner join (
		select 
		max(m.id) maxid
		FROM
		users u
		inner JOIN measurement m on m.idUsers=u.id
		inner join measurement_types mt on mt.id=m.idMeasurementType and mt.`name` like \'\% fat\'
		where u.parentEmail = "'.$user->email.'"
		GROUP BY u.id) sq on sq.maxid=m1.id
		inner join measurement_types mt on mt.id=m1.idMeasurementType and mt.`name` like \'\% fat\'
		GROUP BY a.id;
		')
		->queryAll();

//		CVarDumper::dump($data,10,true);exit;

		$dataGraph=[];
		foreach ($data as $item) {
			$dataGraph[]=[$item['minData'].' - '.$item['maxData'],(double)$item['qty']];
		}
//		CVarDumper::dump($dataGraph,10,true);exit;
		$this->render('trainerReport3',['title'=>$title,'dataGraph'=>$dataGraph]);
	}
	public function actionTreport4()
	{
		$this->pageTitle = $title = "Fat Mass Distribution";
		$user = Yii::app()->user->getState('_user');

		$data = Yii::app()->db->createCommand('
			select
			a.minData,a.maxData, count(a.id) qty  
			from 
			fatandlean_group a 
			INNER JOIN (
			select
			u.id isuser,
			m.id measureid,
			m.measurement,
			wh.weightUnit,
			case 
			WHEN wh.weightUnit = \'Lbs\' THEN m.measurement /2.2
			ELSE m.measurement
			END as measurement_calc
			from 
			measurement m 
			inner join (select 
			max(m1.id) maxid
			FROM
			users u1
			inner JOIN measurement m1 on m1.idUsers=u1.id
			inner join measurement_types mt1 on mt1.id=m1.idMeasurementType and mt1.`name` like "Fat Mass"
			where u1.parentEmail = "'.$user->email.'"
			GROUP BY u1.id) sq on sq.maxid=m.id
			inner join users u on u.id=m.idUsers and u.parentEmail = "'.$user->email.'"
			inner join measurement_types mt on mt.id=m.idMeasurementType and mt.`name` like "Fat Mass"
			inner JOIN weight_height wh on wh.date = m.date
			GROUP BY u.id
			) u on u.measurement_calc>=a.minData and u.measurement_calc <a.maxData
			GROUP BY a.id;
			')
			->queryAll();

//		CVarDumper::dump($data,10,true);exit;

		$dataGraph=[];
		foreach ($data as $item) {
			$dataGraph[]=[$item['minData'].' - '.$item['maxData'],(double)$item['qty']];
		}
//		CVarDumper::dump($dataGraph,10,true);exit;
		$this->render('trainerReport4',['title'=>$title,'dataGraph'=>$dataGraph]);
	}
	public function actionTreport5()
	{
		$this->pageTitle = $title = "Lean Mass Distribution";
		$user = Yii::app()->user->getState('_user');

		$data = Yii::app()->db->createCommand('
			select
			a.minData,a.maxData, count(a.id) qty  
			from 
			fatandlean_group a 
			INNER JOIN (
			select
			u.id isuser,
			m.id measureid,
			m.measurement,
			wh.weightUnit,
			case 
			WHEN wh.weightUnit = \'Lbs\' THEN m.measurement /2.2
			ELSE m.measurement
			END as measurement_calc
			from 
			measurement m 
			inner join (select 
			max(m1.id) maxid
			FROM
			users u1
			inner JOIN measurement m1 on m1.idUsers=u1.id
			inner join measurement_types mt1 on mt1.id=m1.idMeasurementType and mt1.`name` like "Lean Mass"
			where u1.parentEmail = "'.$user->email.'"
			GROUP BY u1.id) sq on sq.maxid=m.id
			inner join users u on u.id=m.idUsers and u.parentEmail = "'.$user->email.'"
			inner join measurement_types mt on mt.id=m.idMeasurementType and mt.`name` like "Lean Mass"
			inner JOIN weight_height wh on wh.date = m.date
			GROUP BY u.id
			) u on u.measurement_calc>=a.minData and u.measurement_calc <a.maxData
			GROUP BY a.id;')
			->queryAll();

//		CVarDumper::dump($data,10,true);exit;

		$dataGraph=[];
		foreach ($data as $item) {
			$dataGraph[]=[$item['minData'].' - '.$item['maxData'],(double)$item['qty']];
		}
//		CVarDumper::dump($dataGraph,10,true);exit;
		$this->render('trainerReport5',['title'=>$title,'dataGraph'=>$dataGraph]);
	}
	public function actionTreport6()
	{
		$this->pageTitle = $title = "Clients Measurements";
		$user = Yii::app()->user->getState('_user');

		$data = Yii::app()->db->createCommand('
			select
			u.name,u.lastName,count( DISTINCT m.date) qty_measures
			from
			users u
			left JOIn measurement m on u.id=m.idUsers
			where
			u.parentEmail = "'.$user->email.'"
			GROUP BY
			u.name,u.lastName')
			->queryAll();


		$this->render('trainerReport6',['title'=>$title,'dataGraph'=>$data]);
	}
	public function actionTreport7()
	{
		$this->pageTitle = 'Clients';

		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Users']))
		{
			$model->attributes=$_GET['Users'];
		}

		$this->render('trainerReport7',array(
			'model'=>$model
		));
	}
	public function actionTreport7a($id)
	{
		$this->pageTitle = $title = "Measurement History";
		$dateFilter='';
		if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
			if(isset($_POST['beginDate']) and !empty($_POST['beginDate']))
				$dateFilter .= ' and m.date >="'.$_POST['beginDate'] .'"';
			if(isset($_POST['endDate']) and !empty($_POST['endDate']))
				$dateFilter .= ' and m.date <= "'.$_POST['endDate'].'" ';

			//CVarDumper::dump($dateFilter,10,true);exit;
		}

		$serie = Yii::app()->db->createCommand('
			select 
			m.date
			from 
			measurement m
			where 
			m.idUsers = '.$id.$dateFilter.'
			GROUP BY
			m.date')
			->queryAll();

		$data = Yii::app()->db->createCommand('
			select 
			m.date,
			mt.name,
			m.measurement,
			wh.weightUnit
			from 
			measurement m
			INNER JOIN measurement_types mt on mt.id=m.idMeasurementType and mt.calculable=0
			INNER JOIN weight_height wh on wh.date = m.date and wh.idUser=m.idUsers
			where 
			m.idUsers = '.$id.$dateFilter.'
			ORDER BY m.date, m.idMeasurementType')
			->queryAll();

		$serieGraph=[];
		$dataGraph=[];
		$dataGraphJson=[];
		foreach ($serie as $item) {
			$serieGraph[]=Yii::app()->dateFormatter->format('yyyy-MM-dd',$item['date']);
		}
//		CVarDumper::dump($data,10,true);exit();
		foreach ($data as $item) {
			if($item['name']=='Lean Mass' or $item['name']=='Fat Mass')
				$dataGraph[$item['name']]['data'][]=round((double)($item['weightUnit']=='Kg'?$item['measurement']:$item['measurement']/2.2),1);
			else
				$dataGraph[$item['name']]['data'][]=round((double)$item['measurement'],1);

			$dataGraph[$item['name']]['name']=$item['name'];
		}
		foreach ($dataGraph as $item) {
			$dataGraphJson[]=$item;
		}
		//CVarDumper::dump(CJSON::encode($dataGraphJson),10,true);exit();
		if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
//			CVarDumper::dump($_POST,10,true);exit;
			echo CJSON::encode(['dataGraph'=>$dataGraphJson,'serieGraph'=>$serieGraph]);
		}
		else
		{
			$this->render('trainerReport7a',['title'=>$title,'dataGraph'=>$dataGraphJson,'serieGraph'=>$serieGraph,'id'=>$id]);
		}
	}
	public function actionTreport7b($id)
	{
		$this->pageTitle = $title = "Body Fat Percent History";
		$dateFilter='';
		if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
			if(isset($_POST['beginDate']) and !empty($_POST['beginDate']))
				$dateFilter .= ' and m.date >="'.$_POST['beginDate'] .'"';
			if(isset($_POST['endDate']) and !empty($_POST['endDate']))
				$dateFilter .= ' and m.date <= "'.$_POST['endDate'].'" ';

			//CVarDumper::dump($dateFilter,10,true);exit;
		}

		$serie = Yii::app()->db->createCommand('
			select 
			m.date
			from 
			measurement m
			where 
			m.idUsers = '.$id.$dateFilter.'
			GROUP BY
			m.date')
			->queryAll();

		$data = Yii::app()->db->createCommand('
			select 
			m.date,
			mt.name,
			m.measurement,
			wh.weightUnit
			from 
			measurement m
			INNER JOIN measurement_types mt on mt.id=m.idMeasurementType and mt.`name` like \'\% fat\'
			INNER JOIN weight_height wh on wh.date = m.date and wh.idUser=m.idUsers
			where 
			m.idUsers = '.$id.$dateFilter.'
			ORDER BY m.date, m.idMeasurementType')
			->queryAll();

		$serieGraph=[];
		$dataGraph=[];
		$dataGraphJson=[];
		foreach ($serie as $item) {
			$serieGraph[]=Yii::app()->dateFormatter->format('yyyy-MM-dd',$item['date']);
		}
//		CVarDumper::dump($data,10,true);exit();
		foreach ($data as $item) {
			if($item['name']=='Lean Mass' or $item['name']=='Fat Mass')
				$dataGraph[$item['name']]['data'][]=round((double)($item['weightUnit']=='Kg'?$item['measurement']:$item['measurement']/2.2),1);
			else
				$dataGraph[$item['name']]['data'][]=round((double)$item['measurement'],1);

			$dataGraph[$item['name']]['name']=$item['name'];
		}
		foreach ($dataGraph as $item) {
			$dataGraphJson[]=$item;
		}
		//CVarDumper::dump(CJSON::encode($dataGraphJson),10,true);exit();
		if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
//			CVarDumper::dump($_POST,10,true);exit;
			echo CJSON::encode(['dataGraph'=>$dataGraphJson,'serieGraph'=>$serieGraph]);
		}
		else
		{
			$this->render('trainerReport7a',['title'=>$title,'dataGraph'=>$dataGraphJson,'serieGraph'=>$serieGraph,'id'=>$id]);
		}
	}
	public function actionTreport7c($id)
	{
		$this->pageTitle = $title = "Lean Mass and Fat Mass History";
		$dateFilter='';
		if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
			if(isset($_POST['beginDate']) and !empty($_POST['beginDate']))
				$dateFilter .= ' and m.date >="'.$_POST['beginDate'] .'"';
			if(isset($_POST['endDate']) and !empty($_POST['endDate']))
				$dateFilter .= ' and m.date <= "'.$_POST['endDate'].'" ';

			//CVarDumper::dump($dateFilter,10,true);exit;
		}

		$serie = Yii::app()->db->createCommand('
			select 
			m.date
			from 
			measurement m
			where 
			m.idUsers = '.$id.$dateFilter.'
			GROUP BY
			m.date')
			->queryAll();

		$data = Yii::app()->db->createCommand('
			select 
			m.date,
			mt.name,
			m.measurement,
			wh.weightUnit
			from 
			measurement m
			INNER JOIN measurement_types mt on mt.id=m.idMeasurementType and mt.`name` like \'%mass\'
			INNER JOIN weight_height wh on wh.date = m.date  and wh.idUser=m.idUsers
			where 
			m.idUsers = '.$id.$dateFilter.'
			ORDER BY m.date, m.idMeasurementType')
			->queryAll();

		$serieGraph=[];
		$dataGraph=[];
		$dataGraphJson=[];
		foreach ($serie as $item) {
			$serieGraph[]=Yii::app()->dateFormatter->format('yyyy-MM-dd',$item['date']);
		}
//		CVarDumper::dump($data,10,true);exit();
		foreach ($data as $item) {
			if($item['name']=='Lean Mass' or $item['name']=='Fat Mass')
				$dataGraph[$item['name']]['data'][]=round((double)($item['weightUnit']=='Kg'?$item['measurement']:$item['measurement']/2.2),1);
			else
				$dataGraph[$item['name']]['data'][]=round((double)$item['measurement'],1);

			$dataGraph[$item['name']]['name']=$item['name'];
        }
//        foreach ($dataGraph as $item) {
//            $dataGraphJson[]=$item;
//        }
//        CVarDumper::dump(CJSON::encode(array_values($dataGraph)),10,true);
//		echo '<br><br><br>';
//        CVarDumper::dump(CJSON::encode($dataGraphJson),10,true);
//		exit();
        if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
//			CVarDumper::dump($_POST,10,true);exit;
			echo CJSON::encode(['dataGraph'=>array_values($dataGraph),'serieGraph'=>$serieGraph]);
		}
		else
		{
			$this->render('trainerReport7c',['title'=>$title,'dataGraph'=>array_values($dataGraph),'serieGraph'=>$serieGraph,'id'=>$id]);
		}
	}
    public function actionTreport7d($id)
    {
        $this->pageTitle = $title = "Hormonal Family History";
        $data = Yii::app()->db->createCommand("select 
                hf.name,
                hud.date,
                GREATEST(CONCAT_WS(',',hud.scoreMeasure1,mt1.name),CONCAT_WS(',',hud.scoreMeasure2,mt2.name),CONCAT_WS(',',hud.scoreMeasure3,mt3.name)) maximo,
                hud.scoreMeasure1,hud.scoreMeasure2,hud.scoreMeasure3
                from 
                hormone_user_date hud
                inner join hormone_family hf on hf.id=hud.idHormoneFamily
				INNER JOIN measurement_types mt1 on mt1.id = hud.idMeasurementType1
				INNER JOIN measurement_types mt2 on mt2.id = hud.idMeasurementType2
				INNER JOIN measurement_types mt3 on mt3.id = hud.idMeasurementType3
                where hud.idUsers=$id
                ORDER BY hud.date,hf.id")
            ->queryAll();

        $dataGrafic=['serie'=>[],'dataJson'=>[]];

        foreach ($data as $index => $datum) {
			$datum['maximo'] = array_combine(['y','name'],explode(',',$datum['maximo']));
			$datum['maximo']['y'] = round((double)$datum['maximo']['y'],2);

            $dataGrafic['serie'][]=Yii::app()->dateFormatter->format('yyyy-MM-dd H:m:s',$datum['date']);
            $dataGrafic['dataJson'][$datum['name']]['name']=CHtml::encode($datum['name']);
            $dataGrafic['dataJson'][$datum['name']]['data'][]=$datum['maximo'];
        }
        $dataGrafic['serie']=array_values(array_unique($dataGrafic['serie'],SORT_REGULAR));
        foreach ($dataGrafic['serie'] as $i => $item) {
            $dataGrafic['serie'][$i]=substr($item,0,10);
        }

        $dataGrafic['dataJson']=array_values($dataGrafic['dataJson']);

        $this->render('trainerReport7d',array_merge(['title'=>$title,'id'=>$id],$dataGrafic));
    }

	/*
	 * User Reports
	 */
	public function actionUserReports()
	{
		$this->render('userReports');
	}
	public function actionUreport1()
	{
		$this->pageTitle = $title = "Body Fat Percentage History";
		$user = Yii::app()->user->getState('_user');

		$data = Yii::app()->db->createCommand()
			->select('m.date,ROUND(m.measurement,1) AS measurement,wh.weightUnit,mt.name')
			->from('measurement m')
			->join('weight_height wh','wh.date=m.date and m.idUsers ='.$user->id.' and wh.idUser='.$user->id)
			->join('measurement_types mt',"mt.id=m.idMeasurementType and mt.name like '\\% fat'")
			->queryAll();

//		CVarDumper::dump($data,10,true);exit;

		$dataGraph=[];
		$serieGraph=[];
		foreach ($data as $item) {
			if($item['name']=='Lean Mass' or $item['name']=='Fat Mass')
				$dataGraph[]=(double)($item['weightUnit']=='Kg'?$item['measurement']:$item['measurement']/2.2);
			else
				$dataGraph[]=(double)$item['measurement'];
			$serieGraph[]=Yii::app()->dateFormatter->format('yyyy-MM-dd',$item['date']);;
		}
//		CVarDumper::dump($serieGraph,10,true);exit;
		$this->render('userReport1',['title'=>$title,'dataGraph'=>$dataGraph,'serieGraph'=>$serieGraph]);
	}
	public function actionUreport2()
	{
		$this->pageTitle = $title = "Fat Mass History";
		$user = Yii::app()->user->getState('_user');

		$data = Yii::app()->db->createCommand()
			->select('m.date,ROUND(m.measurement,1) AS measurement,wh.weightUnit,mt.name')
			->from('measurement m')
			->join('weight_height wh','wh.date=m.date and m.idUsers ='.$user->id.' and wh.idUser='.$user->id)
			->join('measurement_types mt',"mt.id=m.idMeasurementType and mt.name like 'Fat Mass'")
			->queryAll();

//		CVarDumper::dump($data,10,true);exit;

		$dataGraph=[];
		$serieGraph=[];
		foreach ($data as $item) {
			if($item['name']=='Lean Mass' or $item['name']=='Fat Mass')
				$dataGraph[]=round((double)($item['weightUnit']=='Kg'?$item['measurement']:$item['measurement']/2.2),1);
			else
				$dataGraph[]=round((double)$item['measurement'],1);
			$serieGraph[]=Yii::app()->dateFormatter->format('yyyy-MM-dd',$item['date']);;
		}
//		CVarDumper::dump($serieGraph,10,true);exit;
		$this->render('userReport2',['title'=>$title,'dataGraph'=>$dataGraph,'serieGraph'=>$serieGraph]);
	}
	public function actionUreport3()
	{
		$this->pageTitle = $title = "Lean Mass History";
		$user = Yii::app()->user->getState('_user');

		$data = Yii::app()->db->createCommand()
			->select('m.date,ROUND(m.measurement,1) AS measurement,wh.weightUnit,mt.name')
			->from('measurement m')
			->join('weight_height wh','wh.date=m.date and m.idUsers ='.$user->id.' and wh.idUser='.$user->id)
			->join('measurement_types mt',"mt.id=m.idMeasurementType and mt.name like 'Lean Mass'")
			->queryAll();

//		CVarDumper::dump($data,10,true);exit;

		$dataGraph=[];
		$serieGraph=[];
		foreach ($data as $item) {
			if($item['name']=='Lean Mass' or $item['name']=='Fat Mass')
				$dataGraph[]=round((double)($item['weightUnit']=='Kg'?$item['measurement']:$item['measurement']/2.2),1);
			else
				$dataGraph[]=round((double)$item['measurement'],1);
			$serieGraph[]=Yii::app()->dateFormatter->format('yyyy-MM-dd',$item['date']);;
		}
//		CVarDumper::dump($serieGraph,10,true);exit;
		$this->render('userReport3',['title'=>$title,'dataGraph'=>$dataGraph,'serieGraph'=>$serieGraph]);
	}
	public function actionUreport4()
	{
		$this->pageTitle = $title = "Sent Protocols";
		$user =Yii::app()->user->getState('_user');
//		$user = new Users();
		$model=new SentProtocols('search');
		//CVarDumper::dump($model,10,true);exit;
		$model->unsetAttributes();  // clear any default values
		$model->idUsers=$user->id;

		if(isset($_GET['SentProtocols']))
			$model->attributes=$_GET['SentProtocols'];

		$this->render('userReport4',array(
			'model'=>$model,
			'title'=>$title
		));
	}
	public function actionUreport4a($id)
	{
		$this->pageTitle = "Metabolic Oracle - Protocol";
		$model=SentProtocols::model()->findByPk($id);

		$this->render('userReport4a',array(
			'content'=>$model->content,
		));
	}
}
