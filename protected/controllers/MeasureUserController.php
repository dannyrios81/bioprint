<?php

class MeasureUserController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),*/
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('DelMeasurementsSet','index','Addmeasurement','AnalisisMeasurement','Protocol','Editmeasurement'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionIndex($id)
	{

		$criteria = new CDbCriteria;
		$criteria->together = true;
		$criteria->with = ['idMeasurementType0'];
		$criteria->addSearchCondition('idUsers',$id, false);
		$criteria->order = 'date DESC,idMeasurementType ASC';

		$criteriaWH = new CDbCriteria;
		$criteriaWH->addSearchCondition('idUser',$id, false);
		$criteriaWH->order = 'date ASC';

		$array = [];
		$valores = Measurement::model()->findAll($criteria);

		if(count($valores)<=0)
			$this->redirect(array('Addmeasurement','id'=>$id));

		$valoresWH = WeightHeight::model()->findAll($criteriaWH);

		$array['user']=Users::model()->findByPk($id);
		$array['idUser']=$id;
		

		if(!empty($array['user']->idAttachment))
			$array['photo'] = $array['user']->idAttachment0->path;
		else
			$array['photo'] = '/img/defaultProfileImg.jpg';

		$previous = [];

		if(count($valoresWH))
		{
			foreach ($valoresWH as $value) {
				$array['WH'][$value->date]=$value;
			}
		}

		foreach ($valores as $valor)
		{
			$array['mobile'][$valor->date][$valor->idMeasurementType0->name]=number_format($valor->measurement,1);
			$array['desktop'][$valor->idMeasurementType0->name][$valor->date]=number_format($valor->measurement,1);
			$array['mobileDiff'][$valor->date][$valor->idMeasurementType0->name]=number_format((!isset($previous[$valor->idMeasurementType0->name])?0:$previous[$valor->idMeasurementType0->name]-$valor->measurement),1);
			$array['desktopDiff'][$valor->idMeasurementType0->name][$valor->date]=number_format((!isset($previous[$valor->idMeasurementType0->name])?0:$previous[$valor->idMeasurementType0->name]-$valor->measurement),1);
			$array['mobilePercent'][$valor->date][$valor->idMeasurementType0->name]=($valor->measurement>0)?Yii::app()->NumberFormatter->formatPercentage((!isset($previous[$valor->idMeasurementType0->name])?0:($previous[$valor->idMeasurementType0->name]-$valor->measurement)/$valor->measurement),1):'';
			$array['desktopPercent'][$valor->idMeasurementType0->name][$valor->date]=($valor->measurement>0)?Yii::app()->NumberFormatter->formatPercentage((!isset($previous[$valor->idMeasurementType0->name])?0:($previous[$valor->idMeasurementType0->name]-$valor->measurement)/$valor->measurement),1):'';
			//$array['desktopPercent'][$valor->idMeasurementType0->name][$valor->date]
			$previous[$valor->idMeasurementType0->name] = $valor->measurement;
		}
//		CVarDumper::dump($array['mobilePercent'],10,true);exit;

		$this->render('index',$array);
	}
	public function actionAddmeasurement($id)
	{
		$measurementTypes = MeasurementTypes::model()->findAll(['condition'=>'calculable=0','order'=>'id']);
		$model = new CFormModel();
		$modelWH = new WeightHeight();
		$modelWHPrev = WeightHeight::model()->findByAttributes(['idUser'=>$id]);

		if(!empty($modelWHPrev))
		{
			$modelWH->height = $modelWHPrev->height;
			$modelWH->heightUnit = $modelWHPrev->heightUnit;
		}

		$measurementDate = date('Y-m-d H:i:s',time());
//		CVarDumper::dump($_POST,10,true);exit;

		foreach($measurementTypes as $measure)
		{
//			$measure = new MeasurementTypes();
			$measures[$measure->id]=new MeasurementUserForm();
			$measures[$measure->id]->name = $measure->name;
			$measures[$measure->id]->MeasurementType = $measure;
			$measures[$measure->id]->date = $measurementDate;
			$measures[$measure->id]->idUser = $id;
			$measures[$measure->id]->val=isset($_POST['MeasurementUserForm'][$measure->id])?$_POST['MeasurementUserForm'][$measure->id]['val']:NULL;
		}

		if(isset($_POST['MeasurementUserForm']))
		{
//			CVarDumper::dump($_POST,10,true);
			$saveOk = false;
			foreach ($measures as $measure) {
//				$measure=new MeasurementUserForm();
				$measure->validate();
				$model->addErrors($measure->getErrors());
			}
			try{
				$trans = Yii::app()->db->beginTransaction();
//				$trans = new CDbTransaction();

//				CVarDumper::dump($trans,10,true);exit;

				if(count($model->getErrors())<=0)
				{
					$sum = 0;
					$saveOk = true;
					foreach ($measures as $measureForm) {
						//$measure=new MeasurementUserForm();
						$save = $measureForm->save();
						$sum = $sum + $measureForm->val;
						if(!$save and $saveOk)
						{
							$saveOk=false;
							break;
						}
					}
				}
				if (isset($_POST['WeightHeight']))
				{
					$modelWH->attributes = $_POST['WeightHeight'];
					$modelWH->date = $measurementDate;
					$modelWH->idUser = $id;
					if(!$modelWH->save())
					{
						$saveOk=false;
						$temp = array_merge($modelWH->getErrors(),$model->getErrors());
						$model->clearErrors();
						$model->addErrors($temp);
					}

				}
				if($saveOk)
				{
//					CVarDumper::dump($measures,10,true);exit;
					Utilities::calculateMeasurements($id,$measurementDate,$sum,$modelWH);
					Utilities::calculateHormoneFamilyScore($id,$measurementDate,$measures);
					$trans->commit();
					$this->redirect(array('index','id'=>$id));

				}
				else
					$trans->rollback();
			}
			catch(CDbException $e)
			{
				$trans->rollback();
			}
			catch(ErrorException $e)
			{
				$trans->rollback();
			}
			catch(Exception $e)
			{
				$trans->rollback();
			}
		}

		$this->render('formulario',['measures'=>$measures,'model'=>$model,"modelWH"=>$modelWH]);
//		CVarDumper::dump($measures,10,true);exit;
	}
	public function actionDelMeasurementsSet($id)
	{
		if (Yii::app()->getRequest()->getIsAjaxRequest())
			WeightHeight::model()->deleteAllByAttributes(['idUser'=>$id,'date'=>$_POST['date']]);
			Measurement::model()->deleteAllByAttributes(['idUsers'=>$id,'date'=>$_POST['date']]);

	}
	public function actionAnalisisMeasurement($id,$idUser)
	{
		$date = date('Y-m-d H:i:s',$id);

		$criteria = new CDbCriteria();
		$criteria->addSearchCondition('date',$date,false);
		$criteria->addSearchCondition('idUsers',$idUser,false);
		$criteria->order = 'applyPriority DESC, deviation DESC';

		$homoneUserDate = HormoneUserDate::model()->findAll($criteria);
//		CVarDumper::dump($homoneUserDate,10,true);exit;
		//$idUser=0;
		$arrayOrden=[];
		$arrayDesorden=[];

		foreach ($homoneUserDate as $item) {
//			$item = new HormoneUserDate();
//			$idUser=$item->idUsers;
			$arrayDesorden[(string)$item->id][]=[
				'score'=>$item->scoreMeasure1,
				'priority'=>1,
//                'visible' => true,
                'visible' => (!in_array($item->idHormoneFamily0->id,Yii::app()->params['HormoneFamiliesWithPriority']))?true:false,
                'hormoneFamily'=> $item->idHormoneFamily0->name,
                'hormoneFamilyId' => $item->idHormoneFamily0->id,
				'measurementType'=>$item->idMeasurementType10,
				'measure'=>Measurement::model()->findByAttributes(['date'=>$date,'idMeasurementType'=>$item->idMeasurementType1,'idUsers'=>$item->idUsers])
			];
            $arrayDesorden[(string)$item->id][] = [
                'score' => (!in_array($item->idHormoneFamily0->id,Yii::app()->params['HormoneFamiliesWithPriority']))?$item->scoreMeasure2:$item->scoreMeasure1,
//                'score' => $item->scoreMeasure2,
                'priority' => 2,
//                'visible' => (!in_array($item->idHormoneFamily0->id,Yii::app()->params['HormoneFamiliesWithPriority']))?true:false,
                'visible' => true,
//                'rojoVisible'=>(!in_array ($item->idHormoneFamily0->id,Yii::app()->params['HormoneFamiliesWithPriority']))?false:true,
                'measurementType' => (!in_array($item->idHormoneFamily0->id,Yii::app()->params['HormoneFamiliesWithPriority']))?$item->idMeasurementType20:$item->idMeasurementType10,
                'measure' => (!in_array($item->idHormoneFamily0->id,Yii::app()->params['HormoneFamiliesWithPriority']))?
                    Measurement::model()->findByAttributes(['date' => $date, 'idMeasurementType' => $item->idMeasurementType2, 'idUsers' => $item->idUsers]):
                    Measurement::model()->findByAttributes(['date' => $date, 'idMeasurementType' => $item->idMeasurementType1, 'idUsers' => $item->idUsers])

            ];
            $arrayDesorden[(string)$item->id][] = [
                'score' => $item->scoreMeasure3,
                'priority' => 3,
                'visible' => (!in_array($item->idHormoneFamily0->id,Yii::app()->params['HormoneFamiliesWithPriority']))?true:false,
                'measurementType' => $item->idMeasurementType30,
                'measure' => Measurement::model()->findByAttributes(['date' => $date, 'idMeasurementType' => $item->idMeasurementType3, 'idUsers' => $item->idUsers])
            ];
		}
		foreach ($arrayDesorden as $key=>$item) {
//			if($item[0]['hormoneFamily'] != 'Cortisol')
			if(!in_array($item[0]['hormoneFamilyId'],Yii::app()->params['HormoneFamiliesWithPriority']))
		        uasort($item, [$this,'cmp']);
			$arrayOrden[$key]=($item);
		}

        $key = count($arrayOrden) ? array_keys($arrayOrden)[0] : null;
		if(!is_null($key))
        {
            if(in_array($arrayOrden[$key][0]['hormoneFamilyId'],Yii::app()->params['HormoneFamiliesWithPriority']))
            {
                $count = 0;
                foreach ($arrayOrden[$key] as $key1 => $item) {
                    if ($count == 1)
                    {
                        $arrayOrden[$key][$key1]['color'] = 'red';
                        break;
                    }
                    $count++;
                }
            }
            else {
                $count = 0;
                foreach ($arrayOrden[$key] as $key1 => $item) {
                    if ($count == 0)
                    {
                        $arrayOrden[$key][$key1]['color'] = 'red';
                        break;
                    }
                    $count++;
                }
            }
        }

		$this->render('analisis',[
			'homoneUserDate'=> $homoneUserDate,
			'measurementsOrder'=>$arrayOrden,
			'idUser'=>$idUser
		]);
	}
	public function actionProtocol($id)
	{
		$model=new ProtocoloForm();

		$hormoneUserDate= $this->findHormuneUserDate($id);;

		$criteria = new CDbCriteria();
		$criteria->together = true;
		$criteria->with = [
			'chooses',
			'chooses.idMoment0',
			'chooses.chooseSuplements'
		];

		$criteria->compare('t.idHormoneFamily',$hormoneUserDate->idHormoneFamily);
		$criteria->compare('t.idMeasurmenetTypes',$hormoneUserDate->idMeasurementTypeWorse);
		$criteria->compare('t.idCountry',$hormoneUserDate->idUsers0->idCountry);

		$criteria->order='idMoment0.priority';

		$recipe=Recipes::model()->find($criteria);

//		$recipe = Recipes::model()->findByAttributes(['idHormoneFamily'=>$hormoneUserDate->idHormoneFamily,'idMeasurmenetTypes'=>$hormoneUserDate->idMeasurementTypeWorse,'idCountry'=>$hormoneUserDate->idUsers0->idCountry]);
//		CVarDumper::dump($recipe->chooses,10,true);
//		$recipe = new Recipes();
		$model->description = $hormoneUserDate->idUsers0->idCountry0->notesRecipes;
		$model->description = $this->renderPartial('//mailTempletes/sendProtocol',['model'=>$model,'recipe'=>$recipe],true);
		//CVarDumper::dump($_POST['ProtocoloForm'],10,true);exit;

		if (isset($_POST['ProtocoloForm']))
		{
			$model->attributes = $_POST['ProtocoloForm'];
			if($model->validate())
			{
				$params['to']=$hormoneUserDate->idUsers0->email;
				$params['subject']='S&M Software Protocol';
				$params['userId']=$hormoneUserDate->idUsers;
				$params['content']=$model->description;
				$params['renderParams']['model'] = $model;
				$params['renderParams']['hormoneUserDate']=$hormoneUserDate;
				$params['renderParams']['recipe'] = $recipe;


				Utilities::sendProtocol($params);
                Yii::app()->user->setFlash('success', "The protocol has been successfully sent to email {$hormoneUserDate->idUsers0->email} ");
                $this->redirect(array('users/index'));
			}
		}
		$this->render('protocolSend',['model'=>$model,'recipe'=>$recipe]);
	}
	public function actionEditmeasurement($id,$idUser)
	{
		$measurementDate = date('Y-m-d H:i:s',$id);

		$measurementTypes = MeasurementTypes::model()->findAll(['condition'=>'calculable=0','order'=>'id']);
		$model = new CFormModel();
		$modelWH = WeightHeight::model()->findByAttributes(['idUser'=>$idUser,'date'=>$measurementDate]);

		foreach($measurementTypes as $measure)
		{
			$measures[$measure->id]=new MeasurementUserForm();
			$measures[$measure->id]->name = $measure->name;
			$measures[$measure->id]->MeasurementType = $measure;
			$measures[$measure->id]->date = $measurementDate;
			$measures[$measure->id]->idUser = $idUser;

			$objMeasurement = Measurement::model()->findByAttributes(['idUsers'=>$idUser,'date'=>$measurementDate,'idMeasurementType'=>$measure->id]);

			$measures[$measure->id]->val=isset($_POST['MeasurementUserForm'][$measure->id])?number_format($_POST['MeasurementUserForm'][$measure->id]['val'],1):number_format($objMeasurement->measurement,1);
			$measures[$measure->id]->Measurement=$objMeasurement;
		}

		if(isset($_POST['MeasurementUserForm']))
		{
			$saveOk = false;
			foreach ($measures as $measure) {
				$measure->validate();
				$model->addErrors($measure->getErrors());

			}
			try{
				$trans = Yii::app()->db->beginTransaction();

				if(count($model->getErrors())<=0)
				{
					$sum = 0;
					$saveOk = true;
					foreach ($measures as $measureForm) {
						//$measure=new MeasurementUserForm();
						$save = $measureForm->edit();
						$sum = $sum + $measureForm->val;
						if(!$save and $saveOk)
						{
							$saveOk=false;
							break;
						}
					}
				}
				if (isset($_POST['WeightHeight']))
				{
					$modelWH->attributes = $_POST['WeightHeight'];
//					$modelWH->date = $measurementDate;
//					$modelWH->idUser = $idUser;
					if(!$modelWH->save())
					{
						$saveOk=false;
						$temp = array_merge($modelWH->getErrors(),$model->getErrors());
						$model->clearErrors();
						$model->addErrors($temp);
					}

				}
				if($saveOk)
				{
					Utilities::calculateMeasurements($idUser,$measurementDate,$sum,$modelWH,1);
					Utilities::calculateHormoneFamilyScore($idUser,$measurementDate,$measures,1);
					$trans->commit();
					$this->redirect(array('index','id'=>$idUser));

				}
				else
					$trans->rollback();
			}
			catch(CDbException $e)
			{
				$trans->rollback();
			}
			catch(ErrorException $e)
			{
				$trans->rollback();
			}
			catch(Exception $e)
			{
				$trans->rollback();
			}
		}

		$this->render('formulario',['measures'=>$measures,'model'=>$model,"modelWH"=>$modelWH]);
		//enviar una variable que me indique que es desde la edicion, para poder mostrar la alerta de eliminacion de los protocolos.
//		CVarDumper::dump($measures,10,true);exit;
	}
	/*
	 * funciones complementarias
	 */
	function cmp($a, $b) {
		if ($a['score'] == $b['score']) {
			return ($a['priority'] < $b['priority']) ? -1 : 1;
		}
		return ($a['score'] > $b['score']) ? -1 : 1;
	}
	protected function findMeasure($idUser,$date,$idMeasurementType)
	{


//		CVarDumper::dump($measurement,10,true);exit;
		if(!empty($measurement))
		{
//			$measurement = new Measurement();
			$measurement = $measurement->measurement;
		}
		else
		{
			$measurement = 0;
		}


		return $measurement;
	}

    protected function findHormuneUserDate($id)
    {
        $hormoneUserDate=HormoneUserDate::model()->findByPk($id);
//        $hormoneUserDate = new HormoneUserDate();
        if($hormoneUserDate->idHormoneFamily == Yii::app()->params['HormoneFamiliesWithPriority']['HormoneFamilyIdTyroid'])
        {
            $hormoneUserDate1=HormoneUserDate::model()->findAllByAttributes(['date'=>$hormoneUserDate->date],['order'=>'applyPriority DESC, deviation DESC']);
            if (is_array($hormoneUserDate1) and count($hormoneUserDate1)>0)
                $hormoneUserDate=$hormoneUserDate1[1];

            $hormoneUserDate->idHormoneFamily = Yii::app()->params['HormoneFamiliesWithPriority']['HormoneFamilyIdTyroid'];
        }
        return $hormoneUserDate;
	}
}