<?php

class PruebaController extends Controller
{
	public function actionIndex($id)
	{
		Utilities::userAgeSpecificDate('1981-07-15','now');
		$criteria = new CDbCriteria;
		$criteria->together = true;
		$criteria->with = ['idMeasurementType0'];
		$criteria->addSearchCondition('idUsers',$id, false);
		$criteria->order = 'date ASC,idMeasurementType ASC';

		$array = [];
		$valores = Measurement::model()->findAll($criteria);
		$array['user']=Users::model()->findByPk($id);
		$previous = [];

		if(count($valores))
		{
			foreach ($valores as $valor) {

				$array['mobile'][$valor->date][$valor->idMeasurementType0->name]=$valor->measurement;
				$array['desktop'][$valor->idMeasurementType0->name][$valor->date]=$valor->measurement;
				$array['mobileDiff'][$valor->date][$valor->idMeasurementType0->name]=(!isset($previous[$valor->idMeasurementType0->name])?0:$valor->measurement-$previous[$valor->idMeasurementType0->name]);
				$array['desktopDiff'][$valor->idMeasurementType0->name][$valor->date]=(!isset($previous[$valor->idMeasurementType0->name])?0:$valor->measurement-$previous[$valor->idMeasurementType0->name]);
				$array['mobilePercent'][$valor->date][$valor->idMeasurementType0->name]=(!isset($previous[$valor->idMeasurementType0->name])?0:($valor->measurement-$previous[$valor->idMeasurementType0->name])/$previous[$valor->idMeasurementType0->name]);
				$array['desktopPercent'][$valor->idMeasurementType0->name][$valor->date]=(!isset($previous[$valor->idMeasurementType0->name])?0:($valor->measurement-$previous[$valor->idMeasurementType0->name])/$previous[$valor->idMeasurementType0->name]);
				$previous[$valor->idMeasurementType0->name] = $valor->measurement;
			}
		}
		else
		{
			$array['mobile']=[];
			$array['desktop']=[];
			$array['mobileDiff']=[];
			$array['desktopDiff']=[];
			$array['mobilePercent']=[];
			$array['desktopPercent']=[];
		}

		$this->render('index',$array);
	}
	public function actionFormulario($id)
	{
		$measurementTypes = MeasurementTypes::model()->findAll(['condition'=>'calculable=0','order'=>'name']);
		$model = new CFormModel();
		$measurementDate = date('Y-m-d H:i:s',time());
//		CVarDumper::dump($_POST,10,true);exit;

		foreach($measurementTypes as $measure)
		{
//			$measure = new MeasurementTypes();
			$measures[$measure->id]=new MeasurementUserForm();
			$measures[$measure->id]->name = $measure->name;
			$measures[$measure->id]->MeasurementType = $measure;
			$measures[$measure->id]->date = $measurementDate;
			$measures[$measure->id]->idUser = $id;
			$measures[$measure->id]->val=isset($_POST['MeasurementUserForm'][$measure->id])?$_POST['MeasurementUserForm'][$measure->id]['val']:NULL;
		}

		if(isset($_POST['MeasurementUserForm']))
		{
//			CVarDumper::dump($_POST,10,true);
			$saveOk = false;
			foreach ($measures as $measure) {
//				$measure=new MeasurementUserForm();
				$measure->validate();
				$model->addErrors($measure->getErrors());

			}
			try{
				$trans = Yii::app()->db->beginTransaction();
//				$trans = new CDbTransaction();

//				CVarDumper::dump($trans,10,true);exit;

				if(count($model->getErrors())<=0)
				{
					$sum = 0;
					$saveOk = true;
					foreach ($measures as $measureForm) {
						//$measure=new MeasurementUserForm();
						$save = $measureForm->save();
						$sum = $sum + $measureForm->val;
						if(!$save and $saveOk)
						{
							$saveOk=false;
							break;
						}
					}
				}
				if($saveOk)
				{
					Utilities::calculateMeasurements($id,$measurementDate,$sum);
					$trans->commit();
					$this->redirect(array('index','id'=>$id));

				}
				else
					$trans->rollback();
			}
			catch(CDbException $e)
			{
				$trans->rollback();
			}
			catch(ErrorException $e)
			{
				$trans->rollback();
			}
			catch(Exception $e)
			{
				$trans->rollback();
			}
		}

		$this->render('formulario',['measures'=>$measures,'model'=>$model]);
//		CVarDumper::dump($measures,10,true);exit;
	}
	public function actionNewItem()
	{
		$model = new Choose();
		$model->idMoment = 1;
		$model->note = "asdasdasdas";

		$model->save();
		echo $model->id;
	}
	public function actionCount()
	{
		$data = Yii::app()->db->createCommand()
			->select('gender,COUNT(*) qty')
			->from('users')
			->group('gender')
			->queryAll();

//		CVarDumper::dump($data,10,true);

		$this->render('count');
	}
	public function actionDate()
	{
		$course1 = Course::model()->findByAttributes(['id'=>222]);
		CVarDumper::dump($course1,10,true);exit;
		$duedate= new DateTime($course1->startDate);
		$duedate->add(new DateInterval('P1Y'));

		echo $duedate->format('Y-m-d H:i:s');
	}
    public function actionForm()
    {
        $this->render('form');
	}
	public function actionForm1()
    {
        $this->render('form1');
	}
    public function actionUpdate()
    {
      CJSON::encode(CVarDumper::dump($_REQUEST,10,true));
      exit;
    }
    public function actionPeso()
    {
        Calculos::LastWeigth(650);
    }
    public function actionTotalReps()
    {
        CVarDumper::dump(Calculos::lastExecutionPhase(10),10,true);
    }
    public function actionDiff()
    {
        $seleccionados=[1,3];
        $almacenados=[1,2];

        CVarDumper::dump(array_diff($seleccionados,$almacenados),10,true);
        echo PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
        CVarDumper::dump(array_diff($almacenados,$seleccionados),10,true);
    }
    public function actionMails()
    {
        $message1 = new YiiMailMessage;
        $message2 = new YiiMailMessage;

        $message1->subject = 'S&M Software - Your software will expire in 7 days.';
        $message1->view = 'notificationSupervisor';

        $message2->subject = 'S&M Software - Your software will expire in 2 weeks.';
        $message2->view = 'notificationSupervisor2Weeks';

        $message1->setBody(array('model' => '', 'topic' => ""),'text/html');
        $message2->setBody(array('model' => '', 'topic' => ""),'text/html');

        $message1->addTo('dannyrios81@gmail.com');
        $message2->addTo('dannyrios81@gmail.com');

//        $message1->from = Yii::app()->mail->transportOptions['username'];
        $message1->from = 'metabolicanalytics@gmail.com';
//        $message2->from = Yii::app()->mail->transportOptions['username'];
        $message2->from = 'metabolicanalytics@gmail.com';

        Yii::app()->mail->send($message1);
        Yii::app()->mail->send($message2);
    }
    public function actionIcons(){
	    $this->render('iconos');
    }
    public function actionPdf($id)
    {
//        $this->layout='pdflayout';

        $iduserphase = $id;

        $user = new Users();
        $userPhaseExercises = new UserPhaseExercise();
        $userPhase = new UsersPhase();

        $userPhaseExercises = UserPhaseExercise::model()->findAllByAttributes(['idUserPhase'=>$iduserphase],['order'=>'`order`']);
        $userPhase = UsersPhase::model()->findByPk($iduserphase);

        if(!empty($userPhase))
        {
            $user = Users::model()->findByPk($userPhase->idUser);
            $user = (!empty($user))?$user:new Users();
        }


        if(!empty($user))
        {
            $practitioner = Users::model()->find('email=:email',[':email' => $user->parentEmail]);
            $practitioner = (!empty($practitioner))?$practitioner:new Users();
        }

//        $this->render('pdf', array('maxSets'=>Calculos::maxSetsPhase($iduserphase),'userPhaseExercises'=>$userPhaseExercises,'userPhase'=>$userPhase,'user'=>$user,'practitioner'=>$practitioner));

        $this->renderPartial('pdf', array('maxSets'=>Calculos::maxSetsPhase($iduserphase),'userPhaseExercises'=>$userPhaseExercises,'userPhase'=>$userPhase,'user'=>$user,'practitioner'=>$practitioner));
//        $html2pdf = Yii::app()->ePdf->HTML2PDF('L', 'A4', 'en');
//
//        $html2pdf->WriteHTML($this->renderPartial('pdf', array('maxSets'=>Calculos::maxSetsPhase($iduserphase),'userPhaseExercises'=>$userPhaseExercises,'userPhase'=>$userPhase,'user'=>$user,'practitioner'=>$practitioner), true));
//        $html2pdf->Output('Phase.pdf','D');
    }
    public function actionDepurate()
    {
        $arrayData=UserPhaseExercise::model()->findAll("rest = '0' AND tempo <> '' AND tempo IS NOT NULL");

        foreach ($arrayData as $index => $arrayDatum)
        {
            //$arrayDatum=new UserPhaseExercise();

            $formulario = new UserPhaseExerciseForm($arrayDatum->id);
            if($formulario->validate())
            {
                $formulario->save();
            }
        }
    }
    public function actionNewuser()
    {
        $model = Users::model()->findByPk('1');
//        $model = User::model()->findByPk('48855');
        /*$pass = Yii::app()->getSecurityManager()->generateRandomString(10);
        $model->password = CPasswordHelper::hashPassword($pass);
        $model->save(false);*/

        $params['renderParams']['url'] = Yii::app()->createAbsoluteUrl('');
        $params['renderParams']['email'] = $model->email;
//        $params['renderParams']['password'] = $pass;
        $params['renderParams']['password'] = 'sdadaasdsa';
        $params['to'] = $model->email;

//        Utilities::newUser($params);

        $MailParams = $params;
        $user =Yii::app()->user->getState('_user');

        $controller=new Controller('temp');
        $MailParams['content']=$controller->renderPartial('//mailTempletes/newUser',isset($params['renderParams'])?$params['renderParams']:[],true);

        $MailParams['to'] = isset($params['to'])?$params['to']:$user->email;

        if(isset($params['cco']) and !empty($params['cco']))
            $MailParams['cco'] = $params['cco'];

        $MailParams['subject'] = isset($params['subject'])?$params['subject']:'New Account';



        //$controller=new Controller('temp');
        $params = $MailParams;
        $render=$controller->renderPartial('//mailTempletes/main',['content'=>isset($params['content'])?$params['content']:''],true);

        $message = new YiiMailMessage;
        $message->setBody($render,'text/html');
        $message->subject = isset($params['subject'])?$params['subject']:'';
        $message->addTo($params['to']);
        if(isset($params['cco']) and !empty($params['cco']))
            $message->addBcc($params['cco']);

        $message->from = isset($params['from'])?$params['from']:Yii::app()->params['adminEmail'];

        CVarDumper::dump(Yii::app()->mail->send($message),10,true);

    }

    public function actionInter()
    {
        $inter[]='1-6';
        $inter[]='1,6,1,6';
        $inter[]='6,7,8,9';
        $inter[]='6/4/4';
        $inter[]='6/6/6';
        $inter[]='7/5/5';
        $inter[]='8,8,6,6';
        $inter[]='8/6/6';
        $inter[]='9,8,7,6';
        $inter[]='10,8,6,6,6';
        $inter[]='4,4,8,8,8';
        $inter[]='4,5,6,7,8';
        $inter[]='4*(4-6),1*(10-12)';
        $inter[]='5,3,3,3,7';
        $inter[]='5,5,5,9,9';
        $inter[]='8,6,6,4,4';
        $inter[]='2*(2),2*(4-6)';
        $inter[]='3*(3-5),1*(6-8)';
        $inter[]='6,6,6,10,10';
        $inter[]='6,6,6,8,8';
        $inter[]='2*(7,5,3)';
        $inter[]='2*(8,6,4)';
        $inter[]='2*(9,7,5)';
        $inter[]='3,3,5,5,7,7';
        $inter[]='4,4,6,6,8,8';
        $inter[]='5,5,7,7,9,9';
        $inter[]='7,5,3,3,5,7';
        $inter[]='7,7,5,5,3,3';
        $inter[]='4,4,4,4,6,6,8,8';
        $inter[]='5*(4-6),3*(8)';
        $inter[]='8,8,6,6,4,4,4,4';
        $inter[]='12,10,8';
        $inter[]='3*(4/4/4)';
        $inter[]='8,10,12';
        $inter[]='10/8/6';
        $inter[]='10/8/8';
        $inter[]='11,9,7,5';
        $inter[]='12,10,8,8';
        $inter[]='12/10/8';
        $inter[]='4*(4/4/4)';
        $inter[]='6,8,10,12';
        $inter[]='10,10,8,8,8';
        $inter[]='12,10,8,6,15';
        $inter[]='12,10,8,8,8';
        $inter[]='12,12,10,10,8';
        $inter[]='4*(4-6),1*(10-12)';
        $inter[]='6,6,6,10,10';
        $inter[]='6,6,8,8,10';
        $inter[]='8,6,4,4,12';
        $inter[]='1,2,3,4,5';
        $inter[]='4/2/2';
        $inter[]='5,3,3,2,2';
        $inter[]='5,3,5,3,5';
        $inter[]='5,4,3,2,1';
        $inter[]='5*(1/1/1/1/1)';
        $inter[]='5*(3/1/1)';
        $inter[]='5/3/3';
        $inter[]='1,1,3,3,5,5';
        $inter[]='1,6,1,6,1,6';
        $inter[]='2,2,2,3,3,3';
        $inter[]='5,3,5,3,5,3';
        $inter[]='2*(3,2,1)';
        $inter[]='2*(5,3,1)';
        $inter[]='2*(5,3,2)';
        $inter[]='2*(5,4,3)';
        $inter[]='2*(7,5,3)';
        $inter[]='4*(3,2)';
        $inter[]='3,2,1,1,2,3';
        $inter[]='4,3,2,2,3,4';
        $inter[]='5,4,3,3,4,5';
        $inter[]='8,8,4,4,2,2,1,1';
        $inter[]='3*(3,2,1)';
        $inter[]='8,3,3,2,2,2,2,2,2,2';
        $inter[]='4*(3,2,1)';
        $inter[]='12,12,20';
        $inter[]='15,15,30';
        $inter[]='15/15/15';
        $inter[]='20,20,50';
        $inter[]='20,30,50';
        $inter[]='20/10/10';
        $inter[]='20/20/20';
        $inter[]='10,12,15,20';
        $inter[]='10,15,20,30';
        $inter[]='20,15,12,10';
        $inter[]='30,20,15,10';
        $inter[]='4*(10/10/10)';
        $inter[]='20,15,12,10,8,20';

        foreach ($inter as $index => $item) {
            $validacion=RepeatIntervalProcessor::proccess($item);
            echo '<pre>texto : '.$item.' index : '.$index.' index parent : '.$validacion['funcion'].'</pre>';
        }

    }
}