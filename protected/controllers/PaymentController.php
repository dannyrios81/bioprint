<?php
class PaymentController extends Controller{
    public function actionListen(){
        // intantiate the IPN listener
        Yii::import("ext.PHP-PayPal-IPN.src.*", true);
        

        // try to process the IPN POST
        try {
            $listener = new IpnListener();

            // tell the IPN listener to use the PayPal test sandbox
            $listener->use_sandbox = false;
            $listener->use_curl = true;
            $listener->follow_location = false;
            $listener->timeout = 30;
            $listener->verify_ssl = true;

            $listener->requirePostMethod();
            $verified = $listener->processIpn();

            $model = new PaypalIpnTransactions();
            $model->attributes=$_POST;
            $model->save();

//          file_put_contents('ipn.txt',str_pad(str_pad(date('Y-m-d H:i:s'),30,'*',STR_PAD_BOTH),3,PHP_EOL,STR_PAD_BOTH),FILE_APPEND);
//          file_put_contents('ipn.txt',print_r($_POST,true),FILE_APPEND);

        } catch (Exception $e) {
            // error_log($e->getMessage());
            Yii::log("IPN FAILED! returned an error: $e",CLogger::LEVEL_ERROR);
            Yii::app()->end();
        }

        // TODO: Handle IPN Response here
        if ($verified and $_POST['txn_type']==='web_accept' and $_POST['payment_status']==='Completed') {
//            $model = new PaypalIpnTransactions();
//            $model->attributes=$_POST;
//            $model->save();

            $course1 = Course::model()->findByAttributes(['courseIdentification'=>$model->item_number]);
            $course1 = new Course();
            $user = Users::model()->findByAttributes(['email'=>$model->payer_email]);

            if(!empty($course1))//si el seminario existe
            {
                Yii::log("encontro curso ",CLogger::LEVEL_INFO);
                if(empty($user))// se crea usuario
                {
                    Yii::log("el usuario no estaba creado, lo va a crear",CLogger::LEVEL_INFO);
                    $user = new Users();

                    $user->email = $model->payer_email;
                    $user->parentEmail = $model->payer_email;
                    $user->bornDate = date('Y-m-d',time());
                    $user->create_record = date('Y-m-d H:i:s',time());
                    $pass = Yii::app()->getSecurityManager()->generateRandomString(10);
                    $user->password=CPasswordHelper::hashPassword($pass);
                    $user->name = $model->first_name;
                    $user->lastName = $model->last_name;
                    $user->userType = 'Trainer';

                    $user->save(false);

                    $params['renderParams']['url'] = Yii::app()->createAbsoluteUrl('');
                    $params['renderParams']['user'] = $user->name .' '.$user->lastName;
                    $params['renderParams']['email'] = $user->email;
                    $params['renderParams']['password'] = $pass;
                    $params['cco'] = Yii::app()->params['paypalPayEmail'];
                    $params['to'] = $user->email;

                    Utilities::newUser($params);
                }
                else
                {
                    Yii::log("el usuario existe y lo convierte en trainer, lo va a crear",CLogger::LEVEL_INFO);

                    $user->parentEmail = $user->email;
                    if($user->userType!="Admin")
                    {
                        $user->userType = 'Trainer';
                    }
                    $user->save(false);
                }
                Yii::log("se crea la relacion entre el usuario y el curso",CLogger::LEVEL_INFO);
                // se crea la relacion entre el usuario y el curso
                $usercourse = new UsersCourses();
                $usercourse->idUsers = $user->id;
                $usercourse->idCourse=$course1->id;
                $usercourse->save(false);

                Yii::log("se busca si el usuario tiene relacion con el producto comprado",CLogger::LEVEL_INFO);
                foreach ($course1->courseHasProducts as $courseHasProduct) {
                    // se busca si el usuario tiene relacion con el producto comprado
                    $userProduct = UserProduct::model()->findByAttributes(['idUsers'=>$user->id,'idProduct'=>$courseHasProduct->idproduct]);

                    if(!empty($userProduct))// si el usuario tiene relacion con el producto se actualiza fecha de finalizacion y se guardda su nueva licencia
                    {
                        Yii::log("si el usuario tiene relacion con el producto se actualiza fecha de finalizacion y se guardda su nueva licencia",CLogger::LEVEL_INFO);
                        $duedate= new DateTime($course1->startDate);
                        $duedate->add(new DateInterval('P1Y'));

//                    $userProduct = new UserProduct();

                        $userProduct->lastDatePurchase = date('Y-m-d H:i:s',time());
                        $userProduct->dueDate = $duedate->format('Y-m-d H:i:s');
                        $userProduct->save(false);

                        Yii::log("fecha de duedate ".$duedate->format('Y-m-d H:i:s'),CLogger::LEVEL_INFO);

                        $licencias = new Licences();
                        $licencias->idProduct = $userProduct->idProduct;
                        $licencias->idUsers = $userProduct->idUsers;
                        $licencias->purchaseDate = date('Y-m-d H:i:s',time());
                        $licencias->renewal = 1;
                        $licencias->save();
                    }
                    else// si el usuario no tiene relacion con el producto se crea y se guarda su nueva licencia
                    {
                        Yii::log("fsi el usuario no tiene relacion con el producto se crea y se guarda su nueva licencia ",CLogger::LEVEL_INFO);

                        $duedate= new DateTime($course1->startDate);
                        $duedate->add(new DateInterval('P1Y'));

                        $userProduct = new UserProduct();
                        $userProduct->idUsers = $user->id;
                        $userProduct->idProduct = $courseHasProduct->idproduct;
                        $userProduct->lastDatePurchase = date('Y-m-d H:i:s',time());
                        $userProduct->activateDate = $course1->startDate;
                        $userProduct->dueDate = $duedate->format('Y-m-d H:i:s');
                        $userProduct->save(false);

                        $licencias = new Licences();
                        $licencias->idProduct = $userProduct->idProduct;
                        $licencias->idUsers = $userProduct->idUsers;
                        $licencias->purchaseDate = date('Y-m-d H:i:s',time());
                        $licencias->renewal = 0;
                        $licencias->save();
                    }

                }

            }
            else //si el seminario no existe
            {
                if(empty($user))
                {
                    $user = new Users();

                    $user->parentEmail = $model->payer_email;
                    $user->email = $model->payer_email;
                    $user->bornDate = date('Y-m-d',time());
                    $user->create_record = date('Y-m-d H:i:s',time());
                    $pass = Yii::app()->getSecurityManager()->generateRandomString(10);
                    $user->password=CPasswordHelper::hashPassword($pass);
                    $user->name = $model->first_name;
                    $user->lastName = $model->last_name;
                    $user->userType = 'Trainer';
                    $user->save(false);

                    $params['renderParams']['url'] = Yii::app()->createAbsoluteUrl('');
                    $params['renderParams']['user'] = $user->name .' '.$user->lastName;
                    $params['renderParams']['email'] = $user->email;
                    $params['renderParams']['password'] = $pass;
                    $params['to'] = $user->email;
                    $params['cco'] = Yii::app()->params['paypalPayEmail'];

                    Utilities::newUser($params);
                }
                else
                {
                    $user->parentEmail = $user->email;
                    $user->userType = 'Trainer';
                    $user->save(false);
                }
                if(!empty($userProduct))
                {
                    $duedate= new DateTime($course1->startDate);
                    $duedate->add(new DateInterval('P1Y'));

                    $userProduct->lastDatePurchase = date('Y-m-d H:i:s',time());
                    $userProduct->dueDate = $duedate->format('Y-m-d H:i:s');
                    $userProduct->save(false);

                    $licencias = new Licences();
                    $licencias->idProduct = $userProduct->idProduct;
                    $licencias->idUsers = $userProduct->idUsers;
                    $licencias->purchaseDate = date('Y-m-d H:i:s',time());
                    $licencias->renewal = 1;
                    $licencias->save();
                }
                else
                {
                    $duedate= new DateTime($course1->startDate);
                    $duedate->add(new DateInterval('P1Y'));

                    $userProduct = new UserProduct();
                    $userProduct->idUsers = $user->id;
                    $userProduct->idProduct = Yii::app()->params['idBiopriontProduct'];
                    $userProduct->lastDatePurchase = date('Y-m-d H:i:s',time());
                    $userProduct->activateDate = $course1->startDate;
                    $userProduct->dueDate = $duedate->format('Y-m-d H:i:s');
                    $userProduct->save(false);

                    $licencias = new Licences();
                    $licencias->idProduct = $userProduct->idProduct;
                    $licencias->idUsers = $userProduct->idUsers;
                    $licencias->purchaseDate = date('Y-m-d H:i:s',time());
                    $licencias->renewal = 0;
                    $licencias->save();
                }

                //enviar correo de que el curso no existe al usuario administrador.
                $params['renderParams']['user'] = $user->name .' '.$user->lastName;
                $params['renderParams']['course'] = $model->item_name;
                $params['renderParams']['courseCode'] = $model->item_number;
                $params['to'] = Yii::app()->params['adminEmail'];

                Utilities::courseNotFound($params);
            }
        } else {
            // manually investigate the invalid IPN
            //mail('myname@example.com', 'Invalid IPN', $listener->getTextReport());
        }
    }

}