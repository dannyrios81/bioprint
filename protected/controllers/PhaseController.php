<?php

class PhaseController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('update','index','delete','create','listBodyPart',),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$model=new Fase;

		if(isset($_POST['Fase']))
		{
			$model->attributes=$_POST['Fase'];
			if($model->validate() && $model->save())
            {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                {
                    $response=[];
                    $response['html']= '';
                    $response['id']=(integer)$model->id;
                    echo CJavaScript::jsonEncode($response);
                    Yii::app()->end();
                }
                else
                {
                    $this->redirect(array('index','id'=>$model->id));
                }
            }

		}
        if (Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $response=[];
            $response['id']=0;
            $response['html']=$this->renderPartial('_formPopup',['model'=>$model,],true,true);
            echo CJavaScript::jsonEncode($response);
            Yii::app()->end();
        }
        else
        {
            $this->render('create',['model'=>$model,]);
        }
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Fase']))
		{
			$model->attributes=$_POST['Fase'];
			if($model->save())
				$this->redirect(array('index','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex()
	{
		$model=new Fase('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Fase']))
			$model->attributes=$_GET['Fase'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function loadModel($id)
	{
		$model=Fase::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fase-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
