<?php
class RepeatIntervalProcessor
{
    /**
     * funcion statica que permite procesar los diferentes formatos del
     * repeatInterval retornando el formato requerido para
     * calcular el Tut y el TutTotal, dicho formato es de la forma
     * #-# donde # es un numero de maximo 2 digitos
     *
     * @param string $repeatInterval
     */
    public static function proccess($repeatInterval)
    {
        /*
         * 1. identificar cual de los 6 tipos de formato es
         * 2. crear una funcion especifica por cada tipo
         * 3. retornar el resultado de la forma #-# teniendo en cuenta que # es un numero de dos digitos
         *
         * regex para validacion
         * 1. /^(\d{1,2}-){1,11}\d{1,2}$/ix
         * 2. /^(\d{1,2},){1,11}\d{1,2}$/ix
         * 3. /^(?\'f1\'\d{1,2}\*\(\d{1,2}(,\d{1,2}){0,11}\))(,(?&f1)){0,11}$/ix
         * 4. /^(?\'f2\'\d{1,2}\*\(\d{1,2}(-\d{1,2}){0,1}\))(,(?&f2)){0,11}$/ix
         * 5. /^(\d{1,2}\/){1,11}\d{1,2}$/ix
         * 6. /^(?\'f3\'\d{1,2}\*\(\d{1,2}(\/\d{1,2}){0,11}\))$/ix
         * 7. /^(\d{1,2}\/){1,11}M$/ix
         */

        $parents[] = '/^(\d{1,3}-){1,11}\d{1,3}$/ix';
        $parents[] = '/^(\d{1,3},){1,11}\d{1,3}$/ix';
        $parents[] = '/^(?\'f1\'\d{1,3}\*\(\d{1,3}(,\d{1,3}){0,11}\))(,(?&f1)){0,11}$/ix';
        $parents[] = '/^(?\'f2\'\d{1,3}\*\(\d{1,3}(-\d{1,3}){0,1}\))(,(?&f2)){0,11}$/ix';
        $parents[] = '/^(\d{1,3}\/){1,11}\d{1,3}$/ix';
        $parents[] = '/^(?\'f3\'\d{1,3}\*\(\d{1,3}(\/\d{1,3}){0,11}\))$/ix';
        $parents[] = '/^(\d){1,11}[M]$/ix';

        $return=[];

        foreach ($parents as $index => $parent)
        {
            $matches = [];
            if(preg_match($parent,$repeatInterval,$matches)===1)
            {
                $function='formato'.$index;

                $return['patron']=$parent;
                $return['index'] = $index;
                $return['funcion'] = $function;
                $return=array_merge($return,self::$function($repeatInterval));
            }
        }
        return $return;

    }

    protected static function formato0($cadena)
    {
        /*
         * extrae la informacion de acuerdo a el formato legacy de la aplicacion
         * para este caso el valor enviado es el retornado
         * Formato : #-#
         * retorna formato #-#
         */
        return ['value'=>$cadena];
    }
    protected static function formato1($cadena)
    {
        /*
         * Siempre que sean de la forma #,#,#
         * Pueden haber entre 2 y 12 números, no solo 3.
         * Se toma el mínimo y el máximo.
         * Formato : #,#,#
         * retorna formato #-#
         */

        $data = explode(',',$cadena);
        $return = min($data).'-'.max($data);
        return ['value'=>$return];
    }
    protected static function formato2($cadena)
    {
        /*
         * Siempre que sean de la forma #*(#,#,#),#*(#,#,#)
         * Entre paréntesis puede haber entre 1 y 12 números. Puede haber un conjunto o varios separados por coma
         * Se toma el mínimo y el máximo de todos los números que estén entre paréntesis.
         * Formato : #*(#,#,#),#*(#,#,#)
         * retorna formato #-#
         */

        $cadena = explode('),',$cadena);
        foreach ($cadena as $key=>$item) {
            $positionIni = strpos($item,'(');
            if($positionIni!==false)
                $item=substr($item,$positionIni+1);

            $positionFin = strpos($item,')');
            if($positionFin!==false)
                $item=substr($item,0,-1);

            $cadena[$key]=$item;
        }
        $cadena = explode(',',implode(',',$cadena));
        $return = min($cadena).'-'.max($cadena);
        return ['value'=>$return];
    }
    protected static function formato3($cadena)
    {
        /*
         * Siempre que sean de la forma #*(#-#),#*(#-#)
         * Entre paréntesis puede haber un rango o un sólo número. Puede haber un conjunto o varios separados por comas
         * Se toma el mínimo y el máximo de todos los números que estén entre paréntesis.
         * Formato : #*(#-#),#*(#-#)
         * retorna formato #-#
         */
        $cadena = explode('),',$cadena);
        foreach ($cadena as $key=>$item) {
            $positionIni = strpos($item,'(');
            if($positionIni!==false)
                $item=substr($item,$positionIni+1);

            $positionFin = strpos($item,')');
            if($positionFin!==false)
                $item=substr($item,0,-1);

            $cadena[$key]=$item;
        }
        $cadena = explode('-',implode('-',$cadena));
        $return = min($cadena).'-'.max($cadena);
        return ['value'=>$return];
    }
    protected static function formato4($cadena)
    {
        /*
         * Siempre que sean de la forma #/#/#
         * Pueden haber entre 2 y 12 números, no solo 3
         * Se toma la suma de los números
         * Formato : #/#/#
         * retorna formato #-#
         */
        $cantidadSlash=substr_count($cadena, '/');
        $data = explode('/',$cadena);
        $return = array_sum($data).'-'.array_sum($data);

        return ['value'=>$return,'cantidadSlash'=>$cantidadSlash];
    }
    protected static function formato5($cadena)
    {
        /*
         * Siempre que sean de la forma #*(#/#/#)
         * Pueden haber entre 2 y 12 números en el paréntesis, no solo 3
         * Se toma la suma de los números entre paréntesis
         * Formato : #*(#/#/#)
         * retorna formato #-#
         */
//        CVarDumper::dump($cadena,10,true);exit;
        $cantidadSlash=substr_count($cadena, '/');
        $cadena = explode('),',$cadena);
        foreach ($cadena as $key=>$item) {
            $positionIni = strpos($item,'(');
            if($positionIni!==false)
                $item=substr($item,$positionIni+1);

            $positionFin = strpos($item,')');
            if($positionFin!==false)
                $item=substr($item,0,-1);

            $cadena[$key]=$item;
        }
        $cadena = explode('/',implode('/',$cadena));
        $return = array_sum($cadena).'-'.array_sum($cadena);

        return ['value'=>$return,'cantidadSlash'=>$cantidadSlash];
    }
    protected static function formato6($cadena)
    {
        /*
         * Siempre que sean de la forma #M
         * Se aplica la formula de #-((#)*0.5)
         * Formato : #M
         * retorna formato #-#
         */
//        CVarDumper::dump($cadena,10,true);exit;

        $value = str_replace('M','',$cadena);
        $valor_inicial = $value = (integer)$value;
        $valor_final = (($value) * 0.5);
        $cadena = $valor_inicial. '-' . $valor_final;

        return ['value'=>$cadena];
    }
}
