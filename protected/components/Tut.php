<?php

class Tut
{
    public static function CalcTut($laterality, $tempo, $repeatInterval, $rest, $numberSeries)
    {
        $tempo=str_replace('x','0',$tempo);
        $tempo=str_replace('X','0',$tempo);
        $tempo=array_sum(explode('-',$tempo));

        /**
         * realizar funcion para poder procesar el repeatinterval esperando que el retorno sea de la forma
         * #-#
         */

        $repeatIntervalProsessed = RepeatIntervalProcessor::proccess($repeatInterval);
        $repeatInterval=isset($repeatIntervalProsessed['value'])?$repeatIntervalProsessed['value']:'0-0';


        $repeatInterval=str_replace('x','0',$repeatInterval);
        $repeatInterval=str_replace('X','0',$repeatInterval);
        $repeatInterval=explode('-',$repeatInterval);

        $retorno=[];
        if(is_array($repeatInterval) and count($repeatInterval)>1 and is_numeric($rest) and is_numeric($numberSeries))
        {
            $tut1=$tempo*$repeatInterval[0];
            $tut2=$tempo*$repeatInterval[1];

            switch ($laterality)
            {
                case 1:

                    if($repeatIntervalProsessed['index']==4 or  $repeatIntervalProsessed['index']==5)
                    {
                        $tutTotal1=(($tut1*2)+($rest+(10*$repeatIntervalProsessed['cantidadSlash'])))*$numberSeries;
                        $tutTotal2=(($tut2*2)+($rest+(10*$repeatIntervalProsessed['cantidadSlash'])))*$numberSeries;
                    }
                    else
                    {
                        $tutTotal1=(($tut1*2)+$rest)*$numberSeries;
                        $tutTotal2=(($tut2*2)+$rest)*$numberSeries;
                    }
                    break;
                case 2:
                    if($repeatIntervalProsessed['index']==4 or  $repeatIntervalProsessed['index']==5) {
                        $tutTotal1=($tut1+($rest+(10*$repeatIntervalProsessed['cantidadSlash'])))*$numberSeries;
                        $tutTotal2=($tut2+($rest+(10*$repeatIntervalProsessed['cantidadSlash'])))*$numberSeries;
                    }
                    else
                    {
                        $tutTotal1=($tut1+$rest)*$numberSeries;
                        $tutTotal2=($tut2+$rest)*$numberSeries;
                    }
                    break;
            }

            $retorno['tut']=$tut1.'-'.$tut2;
            $retorno['tutTotal']=$tutTotal1.'-'.$tutTotal2;
        }
        else
        {
            $retorno['tut']=0;
            $retorno['tutTotal']=0;
        }


        return $retorno;
    }

    /**
     * funcion que retorna el calculo del limite maximo y minimo del TUT
     *
     * @param integer $id identificador del userPhase
     * @return string
     */
    public static function CalcTutTotalFooter($id)
    {
        $tutTotalFooter = UserPhaseExercise::model()->findAllByAttributes(['idUserPhase'=>$id]);

        $tutTotalMinVar = 0;
        $tutTotalMaxVar=0;
        foreach ($tutTotalFooter as $item)
        {
            $temp = explode('-',$item->tutTotal);
            if(count($temp)>1 and is_numeric($temp[0]) and is_numeric($temp[1]))
            {
                $tutTotalMinVar+=$temp[0];
                $tutTotalMaxVar+=$temp[1];
            }

        }
        $tutTimeMinumunMin=(int)($tutTotalMinVar/60);
        $tutTimeMinumunSec=(int)($tutTotalMinVar%60);

        $tutTimeMaxMin=(int)($tutTotalMaxVar/60);
        $tutTimeMaxSec=(int)($tutTotalMaxVar%60);

        return "Min $tutTimeMinumunMin':$tutTimeMinumunSec\"<br/> Max $tutTimeMaxMin':$tutTimeMaxSec\"";
    }
    /**
     * funcion que retorna el calculo del limite maximo y minimo del TUT
     *
     * @param integer $id identificador del userPhase
     * @return string
     */
    public static function CalcTutTotalPdf($idUserPhase)
    {
        $tutTotalFooter = UserPhaseExercise::model()->findAllByAttributes(['idUserPhase'=>$idUserPhase]);

        $tutTotalMinVar = 0;
        $tutTotalMaxVar=0;
        foreach ($tutTotalFooter as $item)
        {
            $temp = explode('-',$item->tutTotal);
            if(count($temp)>1 and is_numeric($temp[0]) and is_numeric($temp[1]))
            {
                $tutTotalMinVar+=$temp[0];
                $tutTotalMaxVar+=$temp[1];
            }

        }
        $tutTimeMinumunMin=(int)($tutTotalMinVar/60);
        $tutTimeMinumunSec=(int)($tutTotalMinVar%60);

        $tutTimeMaxMin=(int)($tutTotalMaxVar/60);
        $tutTimeMaxSec=(int)($tutTotalMaxVar%60);

        return "{$tutTimeMinumunMin}m:{$tutTimeMinumunSec}s - {$tutTimeMaxMin}m:{$tutTimeMaxSec}s";
    }
    public static function CalcTutTotalMaxFooter($id)
    {
        $tutTotalFooter = UserPhaseExercise::model()->findAllByAttributes(['idUserPhase'=>$id]);

        $tutTotalMinVar1 = 0;
        foreach ($tutTotalFooter as $item)
        {
            $temp = explode('-',$item->tutTotal);
            if(!empty($temp[1]) and is_numeric($temp[1]))
                $tutTotalMinVar1+=$temp[1];
        }
        $tutTimeMin=(int)($tutTotalMinVar1/60);
        $tutTimeSec=(int)($tutTotalMinVar1%60);
        return "Max $tutTimeMin':$tutTimeSec\"";
    }
}
