<?php
/**
 * Created by PhpStorm.
 * User: iguazo
 * Date: 26/09/17
 * Time: 12:24 AM
 */

class Calculos
{
    /**
     * permite conseguir el ultimo peso registrado de un usuario
     * @param $id integer identificador de usuario
     * @return array contiene dos posiciones, una con el peso y otra con la unidad de peso
     */
    public static function LastWeigth($id)
    {
        /**
         * 1. traer el ultimo registro de la tabla weigth_heigth de ese usuario
         * 2. traer el ultimo registro del join entre las tablas users_phase y execution
         * 3. comparar los dos resultados y devolver el valor mas nuevo
         */
        $weight1 = WeightHeight::model()->findByAttributes(['idUser'=>$id],['order'=>'date DESC']);
        $weight2 = Execution::model()->findByAttributes(['idUsers'=>$id],['order'=>'date DESC']);

        if(!empty($weight1) and !empty($weight2))
        {
            if($weight1->date > $weight2->date)
                return ['weight'=>$weight1->weight,'unitType'=>$weight1->weightUnit];
            else
                return ['weight'=>$weight2->weigth,'unitType'=>$weight2->unitType];
        }
        else
        {
            if(!empty($weight1))
                return ['weight'=>$weight1->weight,'unitType'=>$weight1->weightUnit];
            elseif (!empty($weight2))
                return ['weight'=>$weight2->weigth,'unitType'=>$weight2->unitType];
            else
                return ['weight'=>0,'unitType'=>'Kg'];
        }
    }
    /**
     * peso modificado          = OK      =  array de valores, con la repeticion (R) se busca una constante y se multiplica por el peso (peso atleta)
     * peso promedio            = OK      = suma de todos los pesos del ejercicio dividido por el numero de series
     * willks                   = OK      = (maximo peso modificado * 500) / a + bx + cx² + dx³ + ex⁴ + fx⁵ ; x=peso del atleta ; a,b,c,d,e,f constantes que verian segun el sexo
     * promedio repeticiones    = OK      = promedio de las repeticiones (R)
     * average lift             = OK      = promedio de los pesos calculados
     * rel strenght             = OK      = average lift/peso atleta
     * total sets               = OK      = total de series que tengan W y R > 0
     * total tonnage            = OK      = sumatoria(peso * R)
     * total reps               = OK      = sumatoria(R)
     * maximun lift             = OK      = Maximo peso modificado
     * porcentaje               = OK      = average lift/GOAL
     * Delta                    = OK      = porcentaje de diferencia entre average lift actual - la anterior / average lift anterior
     * Average intensity        = OK      = Average weight/average lift
     */
    public static function calculusTable(ExecutionExerciseCalc &$executionExerciseCalc)
    {
        self::modWeight($executionExerciseCalc);
        self::calcDropOff($executionExerciseCalc);
        self::allCalculus($executionExerciseCalc);
    }

    public static function modWeight(ExecutionExerciseCalc &$executionExerciseCalc)
    {
        $constasWeightMod = [
            '0'  => 0,
            '1'  => 1.0000,
            '2'  => 1.0526,
            '3'  => 1.1111,
            '4'  => 1.1429,
            '5'  => 1.1765,
            '6'  => 1.2121,
            '7'  => 1.2690,
            '8'  => 1.2755,
            '9'  => 1.3089,
            '10' => 1.3514,
            '11' => 1.3793,
            '12' => 1.4286,
            '13' => 1.4556,
            '14' => 1.4859,
            '15' => 1.5152,
            '16' => 1.5385,
            '17' => 1.5686,
            '18' => 1.6000,
            '19' => 1.6327,
            '20' => 1.6667,
            '21' => 1.6950,
            '22' => 1.7240,
            '23' => 1.7540,
            '24' => 1.7860,
            '25' => 1.8180,
            '26' => 1.8520,
            '27' => 1.8870,
            '28' => 1.9230,
            '29' => 1.9610,
            '30' => 2.0000
        ];
        foreach ($executionExerciseCalc->executionExercises as $index => $executionExercise) {
//            $executionExercise=new ExecutionExercise();
            $eraser = false;
            if(empty($executionExercise->repetitions))
            {
                $eraser = true;
                $executionExercise->repetitions=0;
            }


            if (isset($constasWeightMod[$executionExercise->repetitions]))
            {
                $executionExercise->weigthModificate = (double)$executionExercise->weigth * (double)$constasWeightMod[$executionExercise->repetitions];
//                CVarDumper::dump($executionExercise->weigthModificate,10,true);exit;
                $executionExercise->save(false);
            }

            if($eraser)
                $executionExercise->repetitions='';

        }
//        return $executionExercises;
    }
    public static function allCalculus(&$executionExerciseCalc)
    {

//        CVarDumper::dump($executionExerciseCalc,10,true);exit;
            $countRepeats = 0;
            $countWeight = 0;
            $countLift = 0;

            $avgWeight = 0;
            $avgRepeats = 0;
            $avgLift = 0;

            $maxWeightMod = 0;
            $totalSets = 0;
            $totalReps = 0;
            $totalTonnage = 0;

            $weightWithCalcSameUnitType = 0;

            if ($executionExerciseCalc->idUserPhaseExercice0->unitType =='Lbs' and $executionExerciseCalc->idExecution0->unitType == 'Kg' )
                $weightWithCalcSameUnitType = (double)$executionExerciseCalc->idExecution0->weigth*2.20462262;
            elseif ($executionExerciseCalc->idUserPhaseExercice0->unitType =='Kg' and $executionExerciseCalc->idExecution0->unitType == 'Lbs' )
                $weightWithCalcSameUnitType = (double)$executionExerciseCalc->idExecution0->weigth/2.20462262;
            else
                $weightWithCalcSameUnitType = $executionExerciseCalc->idExecution0->weigth;


            foreach ($executionExerciseCalc->executionExercises as $executionExercise) {
//                $executionExercise=new ExecutionExercise();
                if(!empty($executionExercise->weigth) and $executionExercise->weigth>0)
                {
                    $avgWeight = $avgWeight + (double)$executionExercise->weigth;
                    $countWeight = $countWeight + 1;
                }
                if(!empty($executionExercise->weigthModificate) and $executionExercise->weigthModificate>0 and $maxWeightMod<round($executionExercise->weigthModificate,2))
                {
                    $maxWeightMod = (double)round($executionExercise->weigthModificate,2);
                }
                if(!empty($executionExercise->repetitions) and $executionExercise->repetitions>0)
                {
                    $avgRepeats = $avgRepeats + (double)$executionExercise->repetitions;
                    $totalReps = $totalReps + (double)$executionExercise->repetitions;
                    $countRepeats = $countRepeats + 1;
                }
                if(!empty($executionExercise->weigthModificate) and $executionExercise->weigthModificate>0)
                {
                    $avgLift = $avgLift + (double)$executionExercise->weigthModificate;
                    $countLift = $countLift + 1;
                    $executionExerciseCalc->dropoff = $executionExercise->dropoff;
//                    $lastweigthModificate=(double)$executionExercise->weigthModificate;
                }
                if($executionExercise->repetitions>0 and $executionExercise->weigth>0)
                {
                    $totalTonnage = $totalTonnage + ((double)$executionExercise->repetitions * (double)$executionExercise->weigth );
                }
                if(!is_null($executionExercise->repetitions) and !is_null($executionExercise->weigth) and $executionExercise->repetitions!=0 and $executionExercise->weigth>0)
                {
                    $totalSets = $totalSets + 1;
                }
            }
            if($countWeight>0)
                $avgWeight = $avgWeight/$countWeight;

            if($countRepeats>0)
                $avgRepeats = $avgRepeats/$countRepeats;

            if($countLift>0)
                $avgLift = $avgLift/$countLift;

//        CVarDumper::dump($avgLift);exit;
            $executionExerciseCalc->averageWeight=$avgWeight;
            $executionExerciseCalc->maximunLift = $maxWeightMod;
            $executionExerciseCalc->averageRepetitions = $avgRepeats;
            $executionExerciseCalc->averageLift = $avgLift;
            $executionExerciseCalc->totalSets = $totalSets;
            $executionExerciseCalc->totalReps = $totalReps;
            $executionExerciseCalc->totalTonnage = $totalTonnage;
//            CVarDumper::dump(self::willks($executionExerciseCalc->idExecution0->idUsers0->gender,(double)$executionExerciseCalc->idExecution0->weigth,$maxWeightMod),10,true);

            $calcWillks = $maxWeightMod*self::willks($executionExerciseCalc->idExecution0->idUsers0->gender,(double)$executionExerciseCalc->idExecution0->weigth,$maxWeightMod);
            if($calcWillks<0)
                $calcWillks=0;

            $executionExerciseCalc->willks = round($calcWillks,2);

            if(!empty($executionExerciseCalc->idExecution0->weigth) and $executionExerciseCalc->idExecution0->weigth>0)
            {
                //Formula antigua
                //$calcRelStrength = $avgLift/$weightWithCalcSameUnitType;
                $calcRelStrength = $avgWeight/$weightWithCalcSameUnitType;

                if($calcRelStrength<0)
                    $calcRelStrength=0;

//                $executionExerciseCalc->relStrength = $avgLift/(double)$executionExerciseCalc->idExecution0->weigth;
                $executionExerciseCalc->relStrength = round($calcRelStrength,2);;
            }



            if(!empty($executionExerciseCalc->goal) and $executionExerciseCalc->goal>0)
            {
                // formula antigua
                //$executionExerciseCalc->percent = round(($avgLift/(double)$executionExerciseCalc->goal)*100,0);

                $executionExerciseCalc->percent = round(($maxWeightMod/(double)$executionExerciseCalc->goal)*100,2);
            }

            if($executionExerciseCalc->averageLift>0)
            {
                $executionExerciseCalc->averageIntensity =$executionExerciseCalc->averageWeight/$executionExerciseCalc->averageLift;
            }
            else
            {
                $executionExerciseCalc->averageIntensity = 0;
            }

            self::calcDelta($executionExerciseCalc);
//        CVarDumper::dump($executionExerciseCalc,10,true);exit;

            $executionExerciseCalc->save(false);

//            return $executionExerciseCalc;
    }
    public static function willks($sex,$weightUser,$maxWeightMod)
    {
        $constants = ['C1'=>500,
            'Male'=>['a'=>-216.0475144,'b'=>16.2606339,'c'=>-0.002388645,'d'=>-0.00113732,'e'=>0.00000701863,'f'=>-0.00000001291],
            'Female'=>['a'=>594.3174778,'b'=>-27.23842536,'c'=>0.8211222687,'d'=>-0.00930733913,'e'=>0.00004731582,'f'=>-0.00000009054],
        ];
        $divisor = 0;
        $dividendo = 0;

        $divisor = (double)$constants['C1'];
        $dividendo = (double)$constants[$sex]['a'];
        $dividendo = (double)$dividendo + $constants[$sex]['b'] * ((double)$weightUser);
        $dividendo = (double)$dividendo + $constants[$sex]['c'] * pow((double)$weightUser,2);
        $dividendo = (double)$dividendo + $constants[$sex]['d'] * pow((double)$weightUser,3);
        $dividendo = (double)$dividendo + $constants[$sex]['e'] * pow((double)$weightUser,4);
        $dividendo = (double)$dividendo + $constants[$sex]['f'] * pow((double)$weightUser,5);

        return (double)($divisor/$dividendo);


    }
    public static function lastExecutionPhase($idUserPhase)
    {
        if($idUserPhase>0)
        {
            $countUserPhaseExercises=UserPhaseExercise::model()->countByAttributes(['idUserPhase'=>$idUserPhase]);
//            CVarDumper::dump($countUserPhaseExercises,10,true);exit;



            $execution = Yii::app()->db->createCommand()
                ->join('execution_exercise_calc','execution_exercise_calc.idExecution = execution.id  and execution_exercise_calc.totalReps is not null and execution_exercise_calc.totalReps>0')
                ->select('max(execution.id) as id, Sum(execution_exercise_calc.totalReps) AS sumtotalreps,count(distinct execution.id) totalExecutions')
                ->from('execution')
                ->where('execution.idUserPhase = :idUserPhase', array(':idUserPhase'=>$idUserPhase)) //where($condition, $params)
//                ->group('execution.id')
                ->order('execution.id DESC')
                ->queryRow();


            $execution2 = Yii::app()->db->createCommand()
                ->select('max(execution.id) as id')
                ->from('execution')
                ->where('execution.idUserPhase = :idUserPhase', array(':idUserPhase'=>$idUserPhase)) //where($condition, $params);
                ->queryRow();

            $execution['id']=$execution2['id'];
            $execution['countExercisesPhase']=$countUserPhaseExercises;

//            CVarDumper::dump($execution,10,true);exit;
            return $execution;
        }
    }

    /**
     * consulta la infromacion de una ejecucion en especifico para hacer validaciones
     * @param $id numero de id de la ejecucion
     */
    public static function executionInfo($id)
    {
        $execution = Yii::app()->db->createCommand()
            ->join('execution_exercise_calc','execution_exercise_calc.idExecution = execution.id')
            ->select('max(execution.id) as id, Sum(execution_exercise_calc.totalReps) AS sumtotalreps,count(distinct execution.id) totalExecutions')
            ->from('execution')
            ->where('execution.id = :id', array(':id'=>$id)) //where($condition, $params)
//                ->group('execution.id')
            ->order('execution.id DESC')
            ->queryRow();

        return $execution;
    }
    /** funcion para calcular el delta  */
    public static function calcDelta(&$executionExerciseCalc)
    {
        /**
         * @var ExecutionExerciseCalc $executionExerciseCalc
         * @var ExecutionExerciseCalc $beforeExecutionExerciseCalc
         */
        $criteria = new CDbCriteria();
        $criteria->compare('idUserPhaseExercice',$executionExerciseCalc->idUserPhaseExercice);
        $criteria->addCondition('id<'.$executionExerciseCalc->id);
        $criteria->order='id DESC';

        $beforeExecutionExerciseCalc=ExecutionExerciseCalc::model()->find($criteria);


        if(!empty($beforeExecutionExerciseCalc) and !empty((double)$beforeExecutionExerciseCalc->averageLift) and !empty((double)$executionExerciseCalc->averageLift))
        {

            $calctemp = ((double)$executionExerciseCalc->averageLift-(double)$beforeExecutionExerciseCalc->averageLift)/(double)$beforeExecutionExerciseCalc->averageLift;
            $executionExerciseCalc->delta = round($calctemp,4)*100;
        }
        else
        {
            $executionExerciseCalc->delta = null;
        }

    }
    public static function calcDropOff(&$executionExerciseCalc)
    {
        $maxPM = [];
        $beforeDropOff = null;

        foreach ($executionExerciseCalc->executionExercises as $index => $executionExercise) {
            if (is_null($beforeDropOff))
            {
                $executionExercise->dropoff = null;
                $beforeDropOff = 0;
            }
            else
            {
                $executionExercise->dropoff = ($executionExercise->weigthModificate>max($maxPM))?0:((($executionExercise->weigthModificate - max($maxPM))/max($maxPM))+$beforeDropOff);
                $beforeDropOff = $executionExercise->dropoff;
                $executionExercise->dropoff*=100;
            }

            $maxPM[] = $executionExercise->weigthModificate;
            $executionExercise->save(false);

        }
    }
    public static function maxSetsPhase($idUserPhase)
    {
        try
        {
	    $sql = "SELECT MAX(CAST(numberSeries AS SIGNED)) FROM user_phase_exercise WHERE idUserPhase = ". $idUserPhase;
            $max = Yii::app()->db->createCommand($sql)->queryScalar();
        }
        catch (CDbException $e)
        {
            $max = 12;
        }
        return $max;
    }
}
