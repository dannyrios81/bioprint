<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;
    private $_name;
    const ERROR_LICENCE_OUTDATE=3;
    const ERROR_USER_WITHOUT_PROFILE=4;
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $users = Users::model()->findByAttributes(['email'=>$this->username]);
//		$users=new Users();
        if(empty($users))
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        elseif(!CPasswordHelper::verifyPassword($this->password, $users->password))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        elseif (empty($users->userType))
            $this->errorCode=self::ERROR_USER_WITHOUT_PROFILE;
        else
        {
            if($users->userType =='Trainer' or $users->userType =='Practitioner1')
            {
                $criteria=new CDbCriteria();
                $criteria->select='idUsers,`idProduct`,`lastDatePurchase`,MIN(`activateDate`) activateDate,MAX(`dueDate`) dueDate,id';
                $criteria->group='idProduct, idUsers';
                $criteria->addCondition('idUsers='.$users->id);

                $permisosCodes=[];

                $userProducts = UserProduct::model()->findAll($criteria);

                if(empty($userProducts) or $userProducts===array())
                {
                    $this->errorCode=self::ERROR_LICENCE_OUTDATE;
                }
                else
                {
                    foreach ($userProducts as $userProduct) {
//                        $userProduct = new UserProduct();
                        if(!empty($userProduct))
                        {
                            if(strtotime($userProduct->activateDate)<=time() and strtotime($userProduct->dueDate)>=time())
                            {
                                $permisosCodes[$userProduct->idProduct0->productCode]=$userProduct->idProduct0->productCode;
                            }
                        }
                    }
                }
                if($permisosCodes===array())
                {
                    $this->errorCode=self::ERROR_LICENCE_OUTDATE;
                }
            }
            if($this->errorCode==self::ERROR_UNKNOWN_IDENTITY)
            {
                $this->errorCode=self::ERROR_NONE;
                $this->_id=$users->id;
                $this->_name=$users->name.' '.$users->lastName;
                $this->setState('_user',$users);
                $this->setState('userType',$users->userType);

                if($users->userType =='Trainer' or $users->userType =='Practitioner1')
                    $this->setState('permisosCodes',$permisosCodes);

                //para usar el dato del tipo de usuario se debe usar la siguiente instruccion
                //Yii::app()->user->getState('userType');
                //Yii::app()->user->getState('permisosCodes');
            }
        }
//var_dump($this->errorCode);exit;
        return !$this->errorCode;
    }
    public function getId()
    {
        return $this->_id;
    }
    public function getName()
    {
        return $this->_name;
    }

}
