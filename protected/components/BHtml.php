<?php
/**
 * Created by PhpStorm.
 * User: iguazo
 * Date: 22/07/17
 * Time: 01:54 PM
 */

class BHtml extends CHtml
{
    /**
     * Generates a textbix with drop down list for a model attribute.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $data data for generating the list options (value=>display)
     * @param array $htmlAtrributesLabel additional HTML attributes. Besides normal HTML attributes
     * @param array $htmlAtrributesTextField additional HTML attributes. Besides normal HTML attributes
     */
    public static function activeTextFieldDrop($model,$attribute,$data,$htmlAtrributesTextField=[])
    {
        $element = '';

        $element.=self::activeTextField($model,$attribute,array_merge($htmlAtrributesTextField,['list'=>self::activeId($model,$attribute).'list']));
        $element.=self::openTag('datalist',['id'=>self::activeId($model,$attribute).'list']);
        foreach ($data as $datum) {
            $element.=self::tag('option',['value'=>$datum],false,false);
        }
        $element.=self::closeTag('datalist');

        return $element;
    }

    /**
     * Generates a options for text dropdowns
     * @param $data array opciones
     */
    public static function listOptionsTextFielDrop($data)
    {
        $element = '';
        foreach ($data as $datum) {
            $element.=self::tag('option',['value'=>$datum],false,false);
        }

        return $element;
    }
}