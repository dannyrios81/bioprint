<?php

class Utilities
{
    public static function sendMail($params)
    {
        $controller=new Controller('temp');
        $render=$controller->renderPartial('//mailTempletes/main',['content'=>isset($params['content'])?$params['content']:''],true);

        $message = new YiiMailMessage;
        $message->setBody($render,'text/html');
        $message->subject = isset($params['subject'])?$params['subject']:'';
        $message->addTo($params['to']);
        if(isset($params['cco']) and !empty($params['cco']))
            $message->addBcc($params['cco']);

        $message->from = isset($params['from'])?$params['from']:Yii::app()->params['adminEmail'];

        Yii::app()->mail->send($message);
    }
    public static function forgotMail($params)
    {
        $MailParams = [];
        $user =Yii::app()->user->getState('_user');

        $controller=new Controller('temp');
        $MailParams['content']=$controller->renderPartial('//mailTempletes/forgotPassword',isset($params['renderParams'])?$params['renderParams']:[],true);

        $MailParams['to'] = isset($params['to'])?$params['to']:$user->email;
        $MailParams['subject'] = isset($params['subject'])?$params['subject']:'Forgot Password';

        self::sendMail($MailParams);
    }
    public static function notificationSupervisor($params,$user)
    {
        $MailParams = [];

        $controller=new Controller('temp');
        $MailParams['content']=$controller->renderPartial('//mailTempletes/notificationSupervisor',isset($params['renderParams'])?$params['renderParams']:[],true);

        $MailParams['to'] = isset($params['to'])?$params['to']:$user->email;
        $MailParams['subject'] = isset($params['subject'])?$params['subject']:'Notificacion Finalizacion de Suscripcion';

        self::sendMail($MailParams);
    }
    public static function newUser($params)
    {
        $MailParams = [];
        $user =Yii::app()->user->getState('_user');

        $controller=new Controller('temp');
        $MailParams['content']=$controller->renderPartial('//mailTempletes/newUser',isset($params['renderParams'])?$params['renderParams']:[],true);

        $MailParams['to'] = isset($params['to'])?$params['to']:$user->email;

        if(isset($params['cco']) and !empty($params['cco']))
            $MailParams['cco'] = $params['cco'];

        $MailParams['subject'] = isset($params['subject'])?$params['subject']:'New Account';

        self::sendMail($MailParams);
    }
    public static function courseNotFound($params)
    {
        $MailParams = [];
        $user =Yii::app()->user->getState('_user');

        $controller=new Controller('temp');
        $MailParams['content']=$controller->renderPartial('//mailTempletes/courseNotFound',isset($params['renderParams'])?$params['renderParams']:[],true);

        $MailParams['to'] = isset($params['to'])?$params['to']:$user->email;
        $MailParams['subject'] = isset($params['subject'])?$params['subject']:'Course Not Found';

        self::sendMail($MailParams);
    }
    public static function sendProtocol($params)
    {
        $MailParams = [];
        $user =Yii::app()->user->getState('_user');

//        $controller=new Controller('temp');
//        $MailParams['content']=$controller->renderPartial('//mailTempletes/sendProtocol',isset($params['renderParams'])?$params['renderParams']:[],true);
        $MailParams['content']=$params['content'];

        $MailParams['to'] = isset($params['to'])?$params['to']:$user->email;
        $MailParams['subject'] = isset($params['subject'])?$params['subject']:'S&M Software';

        $sentProtocol = new SentProtocols();
        $recipe = $params['renderParams']['recipe'];
        //$recipe = new Recipes();

        $sentProtocol->hormoneFamily=$recipe->idHormoneFamily0->name;
        $sentProtocol->measurementType=$recipe->idMeasurmenetTypes0->name;
        $sentProtocol->content = $MailParams['content'];
        $sentProtocol->idUsers = $params['userId'];
        $sentProtocol->sentDate = date('Y-m-d H:i:s');
        $sentProtocol->save(false);



        self::sendMail($MailParams);
    }
    public static function encriptar($texto)
    {
        return base64_encode(Yii::app()->getSecurityManager()->encrypt($texto));
    }
    public static function desencriptar($texto)
    {
        return Yii::app()->getSecurityManager()->decrypt(base64_decode($texto));
    }
    public static function ActiveClass($requestUri)
    {
        if (is_array($requestUri))
        {
            foreach ($requestUri as $item) {
                if (strpos($_SERVER['REQUEST_URI'],$item)!==false)
                    echo 'class="active"';
            }
        }
        else
        {
            if (strpos($_SERVER['REQUEST_URI'],$requestUri)!==false)
                echo 'class="active"';
        }

    }
    public static function userAgeSpecificDate($dateBorn,$specificDate)
    {
        $born = new DateTime($dateBorn);
        $specific= new DateTime($specificDate);

        return $born->diff($specific)->y;
    }
    public static function calculationsPercentBodyFat($userId,$dateMeasurementSet,$modelWH)
    {
        $user = Users::model()->findByPk($userId);

//        $modelWH = new WeightHeight();

        // 1. peso
        if($modelWH->weightUnit == 'Kg')
            $weightCalculation = pow($modelWH->weight,0.425);
        else
            $weightCalculation = pow(($modelWH->weight/2.2),0.425);


        // 2. estatura
        if($modelWH->heightUnit == 'Cm')
            $heightCalculation = pow($modelWH->height,0.725);
        else
            $heightCalculation = pow(($modelWH->height*2.54),0.725);

        // 3. variable relacion peso estatura
        $weightHeightRelation = $weightCalculation * $heightCalculation * (71.84/10000) * 0.739 ;

        // 4.  calcular Summ_mm_10, Summ_mm_12 y expr1
        $MS = self::buildMeasurementSet($userId,$dateMeasurementSet);

        $Summ_mm_10 = $MS['Chin']+$MS['Cheek']+$MS['Pectoral']+(($MS['Triceps']+$MS['Biceps'])*0.4)+$MS['Subscapular']+$MS['Midaxilary']+(($MS['Australian']+$MS['Suprailiac'])*0.55)+$MS['Umbilical']+$MS['Knee']+$MS['Calf'];
        $Summ_mm_12 = $Summ_mm_10 + $MS['Hamstrings']+$MS['Quadriceps'];


        $expr1 = (($Summ_mm_10)-30)/20;

        // 5. calcular Summ_mm_12_tmp
        if($user->gender=='Male')
        {
            if(($Summ_mm_12-$Summ_mm_10)>12)
                $Summ_mm_12_tmp=(($Summ_mm_12-$Summ_mm_10)-12)/5;
            else
                $Summ_mm_12_tmp = 0;
        }
        else
        {
            if(($Summ_mm_12-$Summ_mm_10)>30)
                $Summ_mm_12_tmp=(($Summ_mm_12-$Summ_mm_10)-30)/8;
            else
                $Summ_mm_12_tmp = 0;
        }

        // 6. calculo del porcentaje de grasa corporal

        if($modelWH->weightUnit == 'Kg')
            $percentBodyFat = (pow(abs($expr1 * ($weightHeightRelation / $modelWH->weight) - 0.003),0.5) * 100 * 0.7) + $Summ_mm_12_tmp;
        else
            $percentBodyFat = (pow(abs($expr1 * ($weightHeightRelation / ($modelWH->weight/2.2)) - 0.003),0.5) * 100 * 0.7) + $Summ_mm_12_tmp;

        /*
         * debug para ver las medidas cuando halla error
         */
       /*
        echo 'modelHW : ';
        CVarDumper::dump($modelWH->attributes,10,true);
        echo '<br>';
        echo 'weightCalculation = ';
        CVarDumper::dump($weightCalculation,10,true);
        echo '<br>';
        echo 'heightCalculation = ';
        CVarDumper::dump($heightCalculation,10,true);
        echo '<br>';
        echo 'weightHeightRelation = ';
        CVarDumper::dump($weightHeightRelation,10,true);
        echo '<br>';
        echo 'summ_mm_10 =';
        CVarDumper::dump($Summ_mm_10,10,true);
        echo '<br>';
        echo 'summ_mm_12 =';
        CVarDumper::dump($Summ_mm_12,10,true);
        echo '<br>';
        echo 'Summ_mm_12_tmp =';
        CVarDumper::dump($Summ_mm_12_tmp,10,true);
        echo '<br>';
        echo '$expr1 =';
        CVarDumper::dump($expr1,10,true);
        echo '<br>';
        echo 'percentBodyFat =';
        CVarDumper::dump($percentBodyFat,10,true);//exit;
       */

        return $percentBodyFat;

    }
    public static function buildMeasurementSet($userId, $dateMeasurementSet)
    {
        $criteria = new CDbCriteria;
        $criteria->together = true;
        $criteria->with = ['idMeasurementType0'];
        $criteria->addSearchCondition('idUsers',$userId, false);
        $criteria->compare('date',$dateMeasurementSet,false);
        $criteria->order = 'idMeasurementType ASC';

        $measurementSet = Measurement::model()->findAll($criteria);
//        CVarDumper::dump($measurementSet,10,true);exit;
//
        $measurementSetReturn = [];
        foreach ($measurementSet as $measurement) {
//            $measurement = new Measurement();
            $measurementSetReturn[$measurement->idMeasurementType0->name]=$measurement->measurement;
        }
        return $measurementSetReturn;
    }
    public static function calculateMeasurements($userId, $dateMeasurementSet,$sum,$modelWH,$edit=0)
    {
        $percentBodyFat = self::calculationsPercentBodyFat($userId,$dateMeasurementSet,$modelWH);

        $idPercentFatMeasurementType = MeasurementTypes::model()->findByAttributes(['name'=>'% Fat']);
        $idFatMassMeasurementType = MeasurementTypes::model()->findByAttributes(['name'=>'Fat Mass']);
        $idLeanMassMeasurementType = MeasurementTypes::model()->findByAttributes(['name'=>'Lean Mass']);
        $idSumMeasurementType = MeasurementTypes::model()->findByAttributes(['name'=>'Sum']);

        if(!$edit)
        {
            $measurementPercentFat = new Measurement();
            $measurementPercentFat->idUsers = $userId;
            $measurementPercentFat->date = $dateMeasurementSet;
            $measurementPercentFat->idMeasurementType = $idPercentFatMeasurementType->id;
        }
        else
        {
            $measurementPercentFat = Measurement::model()->findByAttributes(['idUsers'=>$userId,'date'=>$dateMeasurementSet,'idMeasurementType'=>$idPercentFatMeasurementType->id]);
        }
        $measurementPercentFat->measurement = round($percentBodyFat,1);
        $measurementPercentFat->save();

        if(!$edit)
        {
            $measurementFatMass = new Measurement();
            $measurementFatMass->idUsers = $userId;
            $measurementFatMass->date = $dateMeasurementSet;
            $measurementFatMass->idMeasurementType = $idFatMassMeasurementType->id;
        }
        else
        {
            $measurementFatMass = Measurement::model()->findByAttributes(['idUsers'=>$userId,'date'=>$dateMeasurementSet,'idMeasurementType'=>$idFatMassMeasurementType->id]);
        }
        $measurementFatMass->measurement = ($modelWH->weight*$percentBodyFat)/100;
        $measurementFatMass->save();

        if(!$edit)
        {
            $measurementLeanMass = new Measurement();
            $measurementLeanMass->idUsers = $userId;
            $measurementLeanMass->date = $dateMeasurementSet;
            $measurementLeanMass->idMeasurementType = $idLeanMassMeasurementType->id;
        }
        else
        {
            $measurementLeanMass = Measurement::model()->findByAttributes(['idUsers'=>$userId,'date'=>$dateMeasurementSet,'idMeasurementType'=>$idLeanMassMeasurementType->id]);
        }

        $measurementLeanMass->measurement = $modelWH->weight-$measurementFatMass->measurement;
        $measurementLeanMass->save();

        if(!$edit)
        {
            $measurementSum = new Measurement();
            $measurementSum->idUsers = $userId;
            $measurementSum->date = $dateMeasurementSet;
            $measurementSum->idMeasurementType = $idSumMeasurementType->id;
        }
        else
        {
            $measurementSum = Measurement::model()->findByAttributes(['idUsers'=>$userId,'date'=>$dateMeasurementSet,'idMeasurementType'=>$idSumMeasurementType->id]);
        }

        $measurementSum->measurement = $sum;
        $measurementSum->save();
    }
    public static function calculateHormoneFamilyScore($idUser,$measurementDate,$measures,$edit=0)
    {
        $hormoneFamilies = HormoneFamily::model()->findAll();
        $user = Users::model()->findByPk($idUser);

        foreach ($hormoneFamilies as $hormoneFamily) {

            if(!$edit)
            {
                $hormoneUserDate = new HormoneUserDate();
                $hormoneUserDate->date = $measurementDate;
                $hormoneUserDate->idUsers = $idUser;
                $hormoneUserDate->idHormoneFamily = $hormoneFamily->id;
                $hormoneUserDate->idMeasurementType1 = $hormoneFamily->idAssociatedMeasurementType1;
                $hormoneUserDate->idMeasurementType2 = $hormoneFamily->idAssociatedMeasurementType2;
                $hormoneUserDate->idMeasurementType3 = $hormoneFamily->idAssociatedMeasurementType3;
            }
            else
            {
                $hormoneUserDate = HormoneUserDate::model()->findByAttributes(['idUsers'=>$idUser,'date'=>$measurementDate,'idHormoneFamily'=>$hormoneFamily->id]);
                $hormoneUserDate->totalScore = 0;
            }

            if(!empty($hormoneUserDate))
            {
                foreach ($hormoneFamily->idAssociatedMeasurementType10->idealMeasurements as $idealMeasurement) {
                    if($idealMeasurement->gender == $user->gender)
                    {
                        $hormoneUserDate->scoreMeasure1 = round(($measures[$hormoneFamily->idAssociatedMeasurementType1]->val/$idealMeasurement->idealMeasure),2);
                        $hormoneUserDate->totalScore += $hormoneUserDate->scoreMeasure1;

                        $hormoneUserDate->scroreWorseMeasure=$hormoneUserDate->scoreMeasure1;
                        $hormoneUserDate->idMeasurementTypeWorse = $hormoneFamily->idAssociatedMeasurementType1;

                    }
                }
                foreach ($hormoneFamily->idAssociatedMeasurementType20->idealMeasurements as $idealMeasurement) {
                    if($idealMeasurement->gender == $user->gender)
                    {
                        $hormoneUserDate->scoreMeasure2 = round(($measures[$hormoneFamily->idAssociatedMeasurementType2]->val/$idealMeasurement->idealMeasure),2);
                        $hormoneUserDate->totalScore += $hormoneUserDate->scoreMeasure2;

                        if($hormoneUserDate->scroreWorseMeasure<$hormoneUserDate->scoreMeasure2)
                        {
                            $hormoneUserDate->scroreWorseMeasure=$hormoneUserDate->scoreMeasure2;
                            $hormoneUserDate->idMeasurementTypeWorse = $hormoneFamily->idAssociatedMeasurementType2;
                        }
                    }
                }
                foreach ($hormoneFamily->idAssociatedMeasurementType30->idealMeasurements as $idealMeasurement) {
                    if($idealMeasurement->gender == $user->gender)
                    {
                        $hormoneUserDate->scoreMeasure3 = round(($measures[$hormoneFamily->idAssociatedMeasurementType3]->val/$idealMeasurement->idealMeasure),2);
                        $hormoneUserDate->totalScore += $hormoneUserDate->scoreMeasure3;

                        if($hormoneUserDate->scroreWorseMeasure<$hormoneUserDate->scoreMeasure3)
                        {
                            $hormoneUserDate->scroreWorseMeasure=$hormoneUserDate->scoreMeasure3;
                            $hormoneUserDate->idMeasurementTypeWorse = $hormoneFamily->idAssociatedMeasurementType3;
                        }
                    }
                }

                $hormoneUserDate->deviation = round($hormoneUserDate->totalScore - 3,2);

                if(in_array($hormoneFamily->id,Yii::app()->params['HormoneFamiliesWithPriority'])  and self::addHormoneFamilyPriority($hormoneFamily,$idUser,$measurementDate,$user->gender))
                {
                    $hormoneUserDate->applyPriority = $hormoneFamily->priority;
                    $hormoneUserDate->scroreWorseMeasure=$hormoneUserDate->scoreMeasure1;
                    $hormoneUserDate->idMeasurementTypeWorse =$hormoneUserDate->idMeasurementType1;
                }
                else
                    $hormoneUserDate->applyPriority = $hormoneFamily->priority*-1;

                $hormoneUserDate->save();
            }
        }
    }
    public static function countdim($array)
    {
        if (is_array(reset($array)))
            $return = self::countdim(reset($array)) + 1;
        else
            $return = 1;

        return $return;
    }
    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
    public static function bodyAreaWithFamilyExerciceListData()
    {
        $opt=Yii::app()->db->createCommand()
            ->select("t.id, CONCAT_WS(' - ',ba.description,t.description) description")
            ->from("family_exercise t")
            ->join("body_area ba","t.bodyAreaId=ba.id")
            ->order('description')
            ->queryAll();

//        CVarDumper::dump($opt,10,true);
//        exit;
        return CHtml::listData($opt, 'id', 'description');
    }
    public static function addHormoneFamilyPriority($hormoneFamily,$idUser,$measurementDate,$gender)
    {
        switch ($hormoneFamily->id)
        {
            case Yii::app()->params['HormoneFamiliesWithPriority']['HormoneFamilyIdCortisol']:
                return self::calcPriorityCortisol($hormoneFamily,$idUser,$measurementDate);
                break;
            case Yii::app()->params['HormoneFamiliesWithPriority']['HormoneFamilyIdTyroid']:
                return self::calcPriorityThyroid($hormoneFamily,$idUser,$measurementDate,$gender);
                break;
            default:
                return 0;
        }
    }
    public static function compareMeasurements($measurements)
    {
        if((is_array($measurements) and count($measurements)>1))
        {
            if($measurements[0]->measurement > $measurements[1]->measurement)
                return 1;
            else
                return 0;
        }
        else
            return 0;
    }
    public static function calcPriorityCortisol($hormoneFamily,$idUser,$measurementDate)
    {
        $measures1 = Measurement::model()->findAllByAttributes(
            ['idUsers'=>$idUser,'idMeasurementType'=>$hormoneFamily->idAssociatedMeasurementType1],
            ['condition'=>'date <= :date','params'=>[':date'=>$measurementDate],'limit'=>2,'order'=>'date DESC']
        );
        $measures2 = Measurement::model()->findAllByAttributes(
            ['idUsers'=>$idUser,'idMeasurementType'=>$hormoneFamily->idAssociatedMeasurementType2],
            ['condition'=>'date <= :date','params'=>[':date'=>$measurementDate],'limit'=>2,'order'=>'date DESC']
        );
        $measures3 = Measurement::model()->findAllByAttributes(
            ['idUsers'=>$idUser,'idMeasurementType'=>$hormoneFamily->idAssociatedMeasurementType3],
            ['condition'=>'date <= :date','params'=>[':date'=>$measurementDate],'limit'=>2,'order'=>'date DESC']
        );

        if(self::compareMeasurements($measures1) and self::compareMeasurements($measures2) and self::compareMeasurements($measures3))
        {
            return 1;
        }
        else
            return 0;
    }
    public static function calcPriorityThyroid($hormoneFamily,$idUser,$measurementDate,$gender)
    {
        /*
         * 1. revisar si hay por lo menos 3 set de medidas
         * 2. validar que los 3 set de medidas tengan entre si una semana
         * 3. validar que cada medida de maxilari sea mayor o igual entre semanas
         * 4. devolver valor de la prioridad si se cumplieron con las condiciones
         */

        $measures = Measurement::model()->findAllByAttributes(
            ['idUsers'=>$idUser,'idMeasurementType'=>$hormoneFamily->idAssociatedMeasurementType1],
            ['condition'=>'date <= :date','params'=>[':date'=>$measurementDate],'limit'=>3,'order'=>'date DESC']
        );

        if(!is_array($measures) or count($measures)<3)
            return 0;

        return self::compareThreeWeeksAndMeasure($measures,$gender);
    }
    public static function compareThreeWeeksAndMeasure($measures,$gender)
    {
        $date = '';
        $measurement = '';
        $score=0;
        if(is_array($measures))
        {
            foreach ($measures as $key=>$measure)
            {
                if(empty($date))
                {
                    $date=$measure->date;
                    $measurement = $measure->measurement;
                }

                if(is_array($measure->idMeasurementType0->idealMeasurements) and count($measure->idMeasurementType0->idealMeasurements)>0)
                {
                    $idealMeasurement = IdealMeasurement::model()->findByAttributes(['gender'=>$gender,'idMeasuremetType'=>$measure->idMeasurementType]);
                    if(self::numWeeks($date,$measure->date) == 1 or $key==0)
                    {
                        if(($measurement>=$measure->measurement) and ($measure->measurement>$idealMeasurement->idealMeasure))
                            $score+=1;
                    }
                }
                $date=$measure->date;
                $measurement = $measure->measurement;
            }
            if($score==3)
                return 1;

            return 0;
        }
        return 0;
    }
    /**
     * A custom function that calculates how many weeks occur
     * between two given dates.
     *
     * @param string $dateOne Y-m-d format.
     * @param string $dateTwo Y-m-d format.
     * @return int
     */
    public static function numWeeks($dateOne, $dateTwo)
    {
        //Create a DateTime object for the first date.
        $firstDate = new DateTime($dateOne);
        //Create a DateTime object for the second date.
        $secondDate = new DateTime($dateTwo);
        //Get the difference between the two dates in days.
        $differenceInDays = $firstDate->diff($secondDate)->days;
        //Divide the days by 7
        $differenceInWeeks = $differenceInDays / 7;
        //Round down with floor and return the difference in weeks.
        return abs(floor($differenceInWeeks));
    }

}