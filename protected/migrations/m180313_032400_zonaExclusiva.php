<?php

class m180313_032400_zonaExclusiva extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('country','exclusive','tinyint(4) NULL DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('country','exclusive');
	}
}