<?php

class m170719_012512_user_phase_exercise extends CDbMigration
{
	public function up()
	{
	    $this->dropTable('exercise');
	    $this->createTable('laterality',
            [
                'id'=>'pk',
                'description'=>'string'
            ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');

	    $this->createTable('exercise',
            [
                'id'=>'pk',
                'description'=>'string',
                'order'=>'string',
                'idMethod'=>'INT(11)',
                'idBodyArea'=>'INT(11)',
                'idFamilyExercise'=>'INT(11)',
                'idOrientation'=>'INT(11)',
                'idImplement'=>'INT(11)',
                'idGrip'=>'INT(11)',
                'idRangeMotion'=>'INT(11)',
                'idLaterality'=>'INT(11)',
                'notes'=>'text',
                'video'=>'string'
            ],'ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );


	    $this->createTable('user_phase_exercise',[
            'id'=>'pk',
            'idUserPhase'=>'INT(11)',
            'idExercise'=>'INT(11)',
            'numberSeries'=>'string',
            'repeatInterval'=>'string',
            'tempo'=>'string',
            'rest'=>'string',
            'tut'=>'string',
            'tutTotal'=>'string'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');

	    $this->addForeignKey('fk_exercise_idMethod_method','exercise','idMethod','method','id');
	    $this->addForeignKey('fk_exercise_idBodyArea_bodyArea','exercise','idBodyArea','body_area','id');
	    $this->addForeignKey('fk_exercise_idFamilyExercise_familyExercise','exercise','idFamilyExercise','family_exercise','id');
	    $this->addForeignKey('fk_exercise_idOrientation_orientation','exercise','idOrientation','orientation','id');
	    $this->addForeignKey('fk_exercise_idImplement_implement','exercise','idImplement','implement','id');
	    $this->addForeignKey('fk_exercise_idGrip_grip','exercise','idGrip','grip','id');
	    $this->addForeignKey('fk_exercise_idRangeMotion_rangeMotion','exercise','idRangeMotion','range_motion','id');
	    $this->addForeignKey('fk_exercise_idMethod_laterality','exercise','idLaterality','laterality','id');


	    $this->addForeignKey('fk_userPhaseExercise_idUserPhase_usersPhase','user_phase_exercise','idUserPhase','users_phase','id');
	    $this->addForeignKey('fk_userPhaseExercise_idExercise_exercise','user_phase_exercise','idExercise','exercise','id');


	}

	public function down()
	{
        $this->dropForeignKey('fk_exercise_idMethod_method','exercise');
        $this->dropForeignKey('fk_exercise_idBodyArea_bodyArea','exercise');
        $this->dropForeignKey('fk_exercise_idFamilyExercise_familyExercise','exercise');
        $this->dropForeignKey('fk_exercise_idOrientation_orientation','exercise');
        $this->dropForeignKey('fk_exercise_idImplement_implement','exercise');
        $this->dropForeignKey('fk_exercise_idGrip_grip','exercise');
        $this->dropForeignKey('fk_exercise_idRangeMotion_rangeMotion','exercise');
        $this->dropForeignKey('fk_exercise_idMethod_laterality','exercise');

        $this->dropForeignKey('fk_userPhaseExercise_idUserPhase_usersPhase','user_phase_exercise');

        $this->dropTable('user_phase_exercise');
        $this->dropTable('laterality');

        $this->createTable('exercise',[
            'id'=>'pk',
            'description'=>'string',
            'method'=>'string',
            'bodyArea'=>'string',
            'familyExercise'=>'string',
            'orientation'=>'string',
            'implement'=>'string',
            'grip'=>'string',
            'rangeMotion'=>'string',
            'laterality'=>'string',
            'notes'=>'text',
            'video'=>'string'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');


	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}