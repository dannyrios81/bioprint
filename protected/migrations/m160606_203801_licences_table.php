<?php

class m160606_203801_licences_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('licences',[
			'id'=>'pk',
			'purchaseDate'=>'timestamp default CURRENT_TIMESTAMP',
			'idUsers'=>'integer',
			'idProduct'=>'integer',
			'renewal'=>'boolean'
			],'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->addForeignKey('fk_licences_idUsers','licences','idUsers','users','id');
		$this->addForeignKey('fk_licences_idProduct','licences','idProduct','product','id');
		
	}

	public function down()
	{
		$this->dropTable('licences');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}