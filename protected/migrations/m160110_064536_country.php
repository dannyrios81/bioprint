<?php

class m160110_064536_country extends CDbMigration
{
	public function up()
	{
		$this->createTable('country',array(
            'id'=>'pk',
            'name'=>'string'
        ),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		echo "m160110_064536_country does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}