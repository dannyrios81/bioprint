<?php

class m170612_062227_tablesSecodFase extends CDbMigration
{
	public function up()
	{
	    $this->createTable('fase',[
	        'id'=>'pk',
            'description'=>'string'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('body_part',[
            'id'=>'pk',
            'description'=>'string'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('method',[
            'id'=>'pk',
            'description'=>'string'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('body_area',[
            'id'=>'pk',
            'description'=>'string'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('family_exercise',[
            'id'=>'pk',
            'description'=>'string',
            'bodyAreaId'=>'INT(11)'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('orientation',[
            'id'=>'pk',
            'description'=>'string',
            'familyExerciseId'=>'INT(11)'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('implement',[
            'id'=>'pk',
            'description'=>'string',
            'familyExerciseId'=>'INT(11)'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('grip',[
            'id'=>'pk',
            'description'=>'string',
            'familyExerciseId'=>'INT(11)'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('number_series',[
            'id'=>'pk',
            'description'=>'string',
            'methodId'=>'INT(11)'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('repeat_interval',[
            'id'=>'pk',
            'description'=>'string',
            'methodId'=>'INT(11)',
            'numberSeriesId'=>'INT(11)'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('rest',[
            'id'=>'pk',
            'description'=>'string',
            'methodId'=>'INT(11)'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('range_motion',[
            'id'=>'pk',
            'description'=>'string',
            'methodId'=>'INT(11)'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createTable('exercise',[
            'id'=>'pk',
            'description'=>'string',
            'description'=>'string',
            'method'=>'string',
            'bodyArea'=>'string',
            'familyExercise'=>'string',
            'orientation'=>'string',
            'implement'=>'string',
            'grip'=>'string',
            'rangeMotion'=>'string',
            'laterality'=>'string',
            'notes'=>'text',
            'video'=>'string'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey(
            'fammilyExercise_bodyArea_bodyAreaId',
            'family_exercise',
            'bodyAreaId',
            'body_area',
            'id');
        $this->addForeignKey(
            'orientation_familyExercise_familyExerciseId',
            'orientation',
            'familyExerciseId',
            'family_exercise',
            'id'
        );
        $this->addForeignKey(
            'implement_familyExercise_familyExerciseId',
            'implement',
            'familyExerciseId',
            'family_exercise',
            'id'
        );
        $this->addForeignKey(
            'grip_familyExercise_familyExerciseId',
            'grip',
            'familyExerciseId',
            'family_exercise',
            'id'
        );
        $this->addForeignKey(
            'numberSeries_method_methodId',
            'number_series',
            'methodId',
            'method',
            'id'
        );
        $this->addForeignKey(
            'repeatInterval_method_methodId',
            'repeat_interval',
            'methodId',
            'method',
            'id'
        );
        $this->addForeignKey(
            'repeatInterval_numberSeries_numberSeriesId',
            'repeat_interval',
            'numberSeriesId',
            'number_series',
            'id'
        );
        $this->addForeignKey(
            'rest_method_methodId',
            'rest',
            'methodId',
            'method',
            'id'
        );



//        $this->dropForeignKey('fammilyExercise_bodyArea_bodyAreaId','family_exercise');
//        $this->dropForeignKey('orientation_familyExercise_familyExerciseId','orientation');
//        $this->dropForeignKey('implement_familyExercise_familyExerciseId','implement');
////        $this->dropForeignKey('grip_familyExercise_familyExerciseId','grip');
////        $this->dropForeignKey('numberSeries_method_methodId','number_series');
////        $this->dropForeignKey('repeatInterval_method_methodId','repeat_interval');
////        $this->dropForeignKey('repeatInterval_numberSeries_numberSeriesId','repeat_interval');
////        $this->dropForeignKey('rest_method_methodId','rest');
//
//        $this->dropTable('fase');
//        $this->dropTable('body_part');
//        $this->dropTable('method');
//        $this->dropTable('body_area');
//        $this->dropTable('family_exercise');
//        $this->dropTable('orientation');
//        $this->dropTable('implement');
//        $this->dropTable('grip');
//        $this->dropTable('number_series');
//        $this->dropTable('repeat_interval');
//        $this->dropTable('rest');
//        $this->dropTable('range_motion');
//        $this->dropTable('exercise');
//
//        return false;
    }

	public function down()
	{
        $this->dropForeignKey('fammilyExercise_bodyArea_bodyAreaId','family_exercise');
        $this->dropForeignKey('orientation_familyExercise_familyExerciseId','orientation');
        $this->dropForeignKey('implement_familyExercise_familyExerciseId','implement');
        $this->dropForeignKey('grip_familyExercise_familyExerciseId','grip');
        $this->dropForeignKey('numberSeries_method_methodId','number_series');
        $this->dropForeignKey('repeatInterval_method_methodId','repeat_interval');
        $this->dropForeignKey('repeatInterval_numberSeries_numberSeriesId','repeat_interval');
        $this->dropForeignKey('rest_method_methodId','rest');

        $this->dropTable('fase');
        $this->dropTable('body_part');
        $this->dropTable('method');
        $this->dropTable('body_area');
        $this->dropTable('family_exercise');
        $this->dropTable('orientation');
        $this->dropTable('implement');
        $this->dropTable('grip');
        $this->dropTable('number_series');
        $this->dropTable('repeat_interval');
        $this->dropTable('rest');
        $this->dropTable('range_motion');
        $this->dropTable('exercise');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}