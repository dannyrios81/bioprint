<?php

class m160330_215427_mod_hormone_user_date extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('hormone_user_date','date','timestamp');

		$this->addColumn('hormone_user_date','idMeasurementType1','integer');
		$this->addColumn('hormone_user_date','idMeasurementType2','integer');
		$this->addColumn('hormone_user_date','idMeasurementType3','integer');
		$this->addColumn('hormone_user_date','idMeasurementTypeWorse','integer');
		$this->addColumn('hormone_user_date','scroreWorseMeasure','decimal(6,2)');
		$this->addColumn('hormone_user_date','deviation','decimal(6,2)');
		$this->addColumn('hormone_user_date','id','pk');

		$this->addForeignKey('fk_hormoneUserDate_idMeasurementType1','hormone_user_date','idMeasurementType1','measurement_types','id');
		$this->addForeignKey('fk_hormoneUserDate_idMeasurementType2','hormone_user_date','idMeasurementType2','measurement_types','id');
		$this->addForeignKey('fk_hormoneUserDate_idMeasurementType3','hormone_user_date','idMeasurementType3','measurement_types','id');
		$this->addForeignKey('fk_hormoneUserDate_idMeasurementTypeWorse','hormone_user_date','idMeasurementTypeWorse','measurement_types','id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_hormoneUserDate_idMeasurementType1','hormone_user_date');
		$this->dropForeignKey('fk_hormoneUserDate_idMeasurementType2','hormone_user_date');
		$this->dropForeignKey('fk_hormoneUserDate_idMeasurementType3','hormone_user_date');
		$this->dropForeignKey('fk_hormoneUserDate_idMeasurementTypeWorse','hormone_user_date');

		$this->alterColumn('hormone_user_date','date','date');
		$this->dropColumn('hormone_user_date','idMeasurementType1');
		$this->dropColumn('hormone_user_date','idMeasurementType2');
		$this->dropColumn('hormone_user_date','idMeasurementType3');
		$this->dropColumn('hormone_user_date','idMeasurementTypeWorse');
		$this->dropColumn('hormone_user_date','scroreWorseMeasure');
		$this->dropColumn('hormone_user_date','deviation');
		$this->dropColumn('hormone_user_date','id');


	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}