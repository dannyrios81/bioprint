<?php

class m160216_065224_measurement_type extends CDbMigration
{
	public function up()
	{
		$this->createTable('measurement_types',array(
		'id'=>'pk',
		'name'=>'string'
	));
		$this->addColumn('measurement','idMeasurementType','integer NOT NULL');
		$this->addForeignKey('fk_measurement_idMeasurementType','measurement','idMeasurementType','measurement_types','id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_measurement_idMeasurementType','measurement');
		$this->dropColumn('measurement','idMeasurementType');
		$this->dropTable('measurement_types');

	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}