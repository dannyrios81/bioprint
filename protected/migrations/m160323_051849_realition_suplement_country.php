<?php

class m160323_051849_realition_suplement_country extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey('fk_supplement_idCountry','supplement','idCountry','country','id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_supplement_idCountry','supplement');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}