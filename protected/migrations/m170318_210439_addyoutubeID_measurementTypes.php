<?php

class m170318_210439_addyoutubeID_measurementTypes extends CDbMigration
{
	public function up()
	{
		$this->addColumn('measurement_types','youtubeId','varchar(20)');
	}

	public function down()
	{
		$this->dropColumn('measurement_types','youtubeId');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}