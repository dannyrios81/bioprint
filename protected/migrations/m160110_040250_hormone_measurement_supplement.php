<?php

class m160110_040250_hormone_measurement_supplement extends CDbMigration
{
	public function up()
	{
		$this->createTable('hormone_measurement_supplement',array(
			'hormone'=>'string',
			'idMeasurement'=>'integer',
			'idSupplement'=>'integer',
			'idCountry'=>'integer',
			'moment'=>'string',
			'choose'=>'string'
		),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		echo "m160110_040250_hormone_measurement_supplement does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}