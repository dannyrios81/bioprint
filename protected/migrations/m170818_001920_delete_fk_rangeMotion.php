<?php

class m170818_001920_delete_fk_rangeMotion extends CDbMigration
{
	public function up()
	{
	    $this->dropForeignKey('fk_rangeMotion_method_metodId','range_motion');
	    $this->dropColumn('range_motion','methodId');
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}