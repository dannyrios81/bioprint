<?php

class m160630_074956_change_fatandleangroupData extends CDbMigration
{
	public function up()
	{
		$this->execute("UPDATE `fatandlean_group` SET `maxData`='40' WHERE (`id`='7');
UPDATE `fatandlean_group` SET `minData`='40', `maxData`='50' WHERE (`id`='8');
UPDATE `fatandlean_group` SET `minData`='50', `maxData`='70' WHERE (`id`='9');
UPDATE `fatandlean_group` SET `minData`='70', `maxData`='90' WHERE (`id`='10');
UPDATE `fatandlean_group` SET `minData`='90', `maxData`='110' WHERE (`id`='11');
UPDATE `fatandlean_group` SET `minData`='110', `maxData`='130' WHERE (`id`='12');
UPDATE `fatandlean_group` SET `minData`='130', `maxData`='150' WHERE (`id`='13');
UPDATE `fatandlean_group` SET `minData`='150', `maxData`='170' WHERE (`id`='14');
UPDATE `fatandlean_group` SET `minData`='170' WHERE (`id`='15');");
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}