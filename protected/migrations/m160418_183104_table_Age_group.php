<?php

class m160418_183104_table_Age_group extends CDbMigration
{
	public function up()
	{
		$this->createTable('age_group',[
			'id'=>'pk',
			'minAge'=>'integer',
			'maxAge'=>'integer'
		],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
		$this->insert('age_group',['minAge'=>0,'maxAge'=>10]);
		$this->insert('age_group',['minAge'=>10,'maxAge'=>20]);
		$this->insert('age_group',['minAge'=>20,'maxAge'=>30]);
		$this->insert('age_group',['minAge'=>30,'maxAge'=>40]);
		$this->insert('age_group',['minAge'=>40,'maxAge'=>50]);
		$this->insert('age_group',['minAge'=>50,'maxAge'=>60]);
		$this->insert('age_group',['minAge'=>60,'maxAge'=>70]);
		$this->insert('age_group',['minAge'=>70,'maxAge'=>80]);
		$this->insert('age_group',['minAge'=>80,'maxAge'=>90]);
		$this->insert('age_group',['minAge'=>90,'maxAge'=>100]);
		$this->insert('age_group',['minAge'=>100,'maxAge'=>110]);
		$this->insert('age_group',['minAge'=>110,'maxAge'=>120]);
		$this->insert('age_group',['minAge'=>120,'maxAge'=>300]);

		$this->createTable('fatandlean_group',[
			'id'=>'pk',
			'minData'=>'integer',
			'maxData'=>'integer'
		],'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->insert('fatandlean_group',['minData'=>0,'maxData'=>5]);
		$this->insert('fatandlean_group',['minData'=>5,'maxData'=>10]);
		$this->insert('fatandlean_group',['minData'=>10,'maxData'=>15]);
		$this->insert('fatandlean_group',['minData'=>15,'maxData'=>20]);
		$this->insert('fatandlean_group',['minData'=>20,'maxData'=>25]);
		$this->insert('fatandlean_group',['minData'=>25,'maxData'=>30]);
		$this->insert('fatandlean_group',['minData'=>30,'maxData'=>35]);
		$this->insert('fatandlean_group',['minData'=>35,'maxData'=>40]);
		$this->insert('fatandlean_group',['minData'=>40,'maxData'=>45]);
		$this->insert('fatandlean_group',['minData'=>45,'maxData'=>50]);
		$this->insert('fatandlean_group',['minData'=>50,'maxData'=>55]);
		$this->insert('fatandlean_group',['minData'=>55,'maxData'=>60]);
		$this->insert('fatandlean_group',['minData'=>60,'maxData'=>65]);
		$this->insert('fatandlean_group',['minData'=>65,'maxData'=>70]);
		$this->insert('fatandlean_group',['minData'=>70,'maxData'=>300]);
	}

	public function down()
	{
		$this->dropTable('age_group');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}