<?php

class m160325_182421_dropTable_hormoneMeasurementSupplement extends CDbMigration
{
	public function up()
	{
		$table_to_check = Yii::app()->db->schema->getTable('hormone_measurement_supplement');
		if (is_object($table_to_check))
			$this->dropTable('hormone_measurement_supplement');


	}

	public function down()
	{
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}