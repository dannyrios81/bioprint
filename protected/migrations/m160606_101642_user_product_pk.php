<?php

class m160606_101642_user_product_pk extends CDbMigration
{
	public function up()
	{
		$this->addColumn('user_product','id','pk');
		//$this->addPrimaryKey('pk','user_product','id');
	}

	public function down()
	{
		$this->dropColumn('user_product','id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}