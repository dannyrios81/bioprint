<?php

class m160110_035722_measurement extends CDbMigration
{
	public function up()
	{
		$this->createTable('measurement',array(
			'idUsers'=>'integer',
			'date'=>'date',
			'measurement'=>'integer',
			'idAttachment'=>'integer'
		),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		echo "m160110_035722_measurement does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}