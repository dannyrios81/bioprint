<?php

class m160322_052749_hormone_user_date extends CDbMigration
{
	public function up()
	{
		$this->addColumn('hormone_user_date','scoreMeasure1','decimal(6,2)');
		$this->addColumn('hormone_user_date','scoreMeasure2','decimal(6,2)');
		$this->addColumn('hormone_user_date','scoreMeasure3','decimal(6,2)');
		$this->addColumn('hormone_user_date','totalScore','decimal(6,2)');

	}

	public function down()
	{
		$this->dropColumn('hormone_user_date','scoreMeasure1');
		$this->dropColumn('hormone_user_date','scoreMeasure2');
		$this->dropColumn('hormone_user_date','scoreMeasure3');
		$this->dropColumn('hormone_user_date','totalScore');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}