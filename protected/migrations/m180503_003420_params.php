<?php

class m180503_003420_params extends CDbMigration
{
	public function up()
	{
	    $this->createTable('params',[
	        'id'=>'pk',
            'paramDescription'=>'string',
            'codParam'=>'varchar(20)',
            'valueParam'=>'string'
        ]);

	    $this->insert('params',[
            'paramDescription'=>'YouTube identifier for the workout explanation video',
            'codParam'=>'WORKOUTVIDEO',
            'valueParam'=>'K1HZhqEXbd8'
        ]);
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}