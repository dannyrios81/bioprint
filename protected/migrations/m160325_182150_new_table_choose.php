<?php

class m160325_182150_new_table_choose extends CDbMigration
{
	public function up()
	{
		$this->createTable('choose',[
            'id'=>'pk',
            'idRecipe'=>'integer',
            'idMoment'=>'integer',
            'note'=>'text'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey('fk_choose_idRecipe','choose','idRecipe','recipes','id');
        $this->addForeignKey('fk_choose_idMoment','choose','idMoment','moments','id');
	}

	public function down()
	{
		$this->dropTable('choose');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}