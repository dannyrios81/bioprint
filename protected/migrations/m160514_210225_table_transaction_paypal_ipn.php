<?php

class m160514_210225_table_transaction_paypal_ipn extends CDbMigration
{
	public function up()
	{
		$this->createTable('paypal_ipn_transactions',[
			'payment_type'=>'string',
			'payment_date'=>'string',
			'payment_status'=>'string',
			'pending_reason'=>'string',
			'address_status'=>'string',
			'payer_status'=>'string',
			'first_name'=>'string',
			'last_name'=>'string',
			'payer_email'=>'string',
			'payer_id'=>'string',
			'address_name'=>'string',
			'address_country'=>'string',
			'address_country_code'=>'string',
			'address_zip'=>'string',
			'address_state'=>'string',
			'address_city'=>'string',
			'address_street'=>'string',
			'business'=>'string',
			'receiver_email'=>'string',
			'receiver_id'=>'string',
			'residence_country'=>'string',
			'item_name'=>'string',
			'item_name1'=>'string',
			'item_number'=>'string',
			'item_number1'=>'string',
			'quantity'=>'string',
			'shipping'=>'string',
			'tax'=>'string',
			'mc_currency'=>'string',
			'mc_fee'=>'string',
			'mc_gross'=>'string',
			'mc_gross_1'=>'string',
			'mc_handling'=>'string',
			'mc_handling1'=>'string',
			'mc_shipping'=>'string',
			'mc_shipping1'=>'string',
			'txn_type'=>'string',
			'txn_id'=>'string',
			'notify_version'=>'string',
			'parent_txn_id'=>'string',
			'reason_code_tx'=>'string',
			'receipt_ID'=>'string',
			'auction_buyer_id'=>'string',
			'auction_closing_date'=>'string',
			'for_auction'=>'string',
			'reason_code_refund'=>'string',
			'receipt_id_refund'=>'string',
			'custom'=>'string',
			'invoice'=>'string'
		],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		$this->dropTable('paypal_ipn_transactions');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}