<?php

class m170802_065130_fk_rangeMotion_method extends CDbMigration
{
	public function up()
	{
	    $this->execute('SET FOREIGN_KEY_CHECKS=0;

create table range_motion_copy SELECT * from range_motion;
truncate range_motion;
ALTER TABLE `range_motion` ADD CONSTRAINT `fk_rangeMotion_method_metodId` FOREIGN KEY (`methodId`) REFERENCES `method` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
insert into range_motion SELECT * from range_motion_copy;
drop TABLE range_motion_copy;

SET FOREIGN_KEY_CHECKS=1;');
	}

	public function down()
	{
        return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}