<?php

class m160606_203811_mounts_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('months',[
			'id'=>'pk',
			'month'=>'string'
		],'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->insertMultiple('months',[
			['month'=>'January'],
			['month'=>'February'],
			['month'=>'March'],
			['month'=>'April'],
			['month'=>'May'],
			['month'=>'June'],
			['month'=>'July'],
			['month'=>'August'],
			['month'=>'September'],
			['month'=>'October'],
			['month'=>'November'],
			['month'=>'December'],
		]);
	}
	public function down()
	{
		$this->dropTable('months');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}