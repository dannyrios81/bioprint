<?php

class m170926_055824_add_iduser_table_execution extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('execution','idUsers','int(11)');
	    $this->execute('ALTER TABLE `execution` ADD CONSTRAINT `fk_Execution_Users` FOREIGN KEY (`idUsers`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;');
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}