<?php

class m161101_041253_index_mesures_measurestype_weightheight extends CDbMigration
{
	public function up()
	{
		$this->createIndex('ix_mesurements_date','measurement','date');
		$this->createIndex('ix_weight_height_date','weight_height','date');
	}

	public function down()
	{
		$this->dropIndex('ix_mesurements_date','measurement');
		$this->dropIndex('ix_weight_height_date','weight_height');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}