<?php

class m160408_023411_add_column_country extends CDbMigration
{
	public function up()
	{
		$this->addColumn('country','notesRecipes','text');
	}

	public function down()
	{
		$this->dropColumn('country','notesRecipes');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}