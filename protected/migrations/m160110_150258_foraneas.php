<?php

class m160110_150258_foraneas extends CDbMigration
{
	public function up()
	{
        $this->addColumn('measurement','id','pk');
        //foraneas Tabla users
        $this->addForeignKey('fk_users_idAttachments','users','idAttachment','attachment','id');
        //foraneas tabla measurement
        $this->addForeignKey('fk_measurement_idAttachments','measurement','idAttachment','attachment','id');
        //foraneas tabla hormone_user_date
        $this->addForeignKey('fk_hormoneUserDate_idUsers','hormone_user_date','idUsers','users','id');
        $this->addForeignKey('fk_hormoneUserDate_idAssociatedMeasurement1','hormone_user_date','idAssociatedMeasurement1','measurement','id');
        $this->addForeignKey('fk_hormoneUserDate_idAssociatedMeasurement2','hormone_user_date','idAssociatedMeasurement2','measurement','id');
        $this->addForeignKey('fk_hormoneUserDate_idAssociatedMeasurement3','hormone_user_date','idAssociatedMeasurement3','measurement','id');
        //foraneas tabla attachment
        $this->addForeignKey('fk_attachment_idAttachmentType','attachment','idAttachmentType','attachment_types','id');
        //foraneas tabla hormone_measurement_supplement
        $this->addForeignKey('fk_hormoneMeasurementSupplement_idMeasurement','hormone_measurement_supplement','idMeasurement','measurement','id');
        $this->addForeignKey('fk_hormoneMeasurementSupplement_idSupplement','hormone_measurement_supplement','idSupplement','supplement','id');
        $this->addForeignKey('fk_hormoneMeasurementSupplement_idCountry','hormone_measurement_supplement','idCountry','country','id');
        //foraneas tabla purchase
        $this->addForeignKey('fk_purchase_idUsers','purchase','idUsers','users','id');
        //foraneas tabla user_product
        $this->addForeignKey('fk_userProduct_idUsers','user_product','idUsers','users','id');
        $this->addForeignKey('fk_userProduct_idProduct','user_product','idProduct','product','id');

    }

	public function down()
	{
		echo "m160110_150258_foraneas does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}