<?php

class m200102_221556_adicion extends CDbMigration
{
	public function up()
	{
	    $this->alterColumn('users','userType','enum("Admin","Trainer","User","Practitioner1")');
	}

	public function down()
	{
        $this->alterColumn('users','userType','enum("Admin","Trainer","User")');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}