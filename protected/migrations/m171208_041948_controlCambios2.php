<?php

class m171208_041948_controlCambios2 extends CDbMigration
{
	public function up()
	{
	    $this->dropForeignKey('fk_exercise_idMethod_method','exercise');
	    $this->dropForeignKey('fk_exercise_idOrientation_orientation','exercise');
	    $this->dropForeignKey('fk_exercise_idImplement_implement','exercise');
//	    $this->dropForeignKey('fk_exercise_idGrip_grip','exercise');
	    $this->dropForeignKey('fk_exercise_idFamilyExercise_familyExercise','exercise');
	    $this->dropForeignKey('fk_exercise_idBodyArea_bodyArea','exercise');

//      GRIP
        $this->dropForeignKey('grip_familyExercise_familyExerciseId','grip');
        $this->dropColumn('grip','familyExerciseId');
        $this->addColumn('grip','idImplement','INT(11)');
        $this->addForeignKey('fk_grip_idImplement_implement','grip','idImplement','implement','id');

//      IMPLEMENT
        $this->dropForeignKey('implement_familyExercise_familyExerciseId','implement');
        $this->dropColumn('implement','familyExerciseId');
        $this->addColumn('implement','idOrientation','INT(11)');
        $this->addForeignKey('fk_implement_idOrientation_orientation','implement','idOrientation','orientation','id');

//      ORIENTATION
        $this->dropForeignKey('orientation_familyExercise_familyExerciseId','orientation');
        $this->dropColumn('orientation','familyExerciseId');
        $this->addColumn('orientation','idFamily_exercise','INT(11)');
        $this->addForeignKey('fk_orientation_idFamilyExercise_family_exercise','orientation','idFamily_exercise','family_exercise','id');

//      BODY_AREA
        $this->addColumn('body_area','idMethod','INT(11)');
        $this->addForeignKey('fk_bodyArea_idMethod_method','body_area','idMethod','method','id');


	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}