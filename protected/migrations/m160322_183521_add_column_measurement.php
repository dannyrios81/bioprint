<?php

class m160322_183521_add_column_measurement extends CDbMigration
{
	public function up()
	{
		$this->addColumn('measurement_types','idalMeasurement','decimal(6,2)');
		$this->addColumn('measurement_types','gender','enum("Male","Female")');
	}

	public function down()
	{
		$this->dropColumn('measurement_types','idalMeasurement');
		$this->dropColumn('measurement_types','gender');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}