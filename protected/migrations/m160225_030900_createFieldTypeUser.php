<?php

class m160225_030900_createFieldTypeUser extends CDbMigration
{
	public function up()
	{
		$this->addColumn('users','userType','enum("Admin","Trainer","User")');
	}

	public function down()
	{
		$this->dropColumn('users','userType');
	}
}