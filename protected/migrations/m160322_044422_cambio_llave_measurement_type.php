<?php

class m160322_044422_cambio_llave_measurement_type extends CDbMigration
{
	public function up()
	{
		$this->dropForeignKey('fk_hormoneMeasurementSupplement_idMeasurement','hormone_measurement_supplement');
		$this->dropColumn('hormone_measurement_supplement','idMeasurement');
		$this->addColumn('hormone_measurement_supplement','idMeasurementTypes','integer');
		$this->addForeignKey('fk_hormoneMeasurementSupplement_idMeasurementTypes','hormone_measurement_supplement','idMeasurementTypes','measurement_types','id');
	}

	public function down()
	{
		$this->addColumn('hormone_measurement_supplement','idMeasurement','integer');
		$this->addForeignKey('fk_hormoneMeasurementSupplement_idMeasurement','hormone_measurement_supplement','idMeasurement','measurement','id');
		$this->dropColumn('hormone_measurement_supplement','idMeasurementTypes');
		$this->dropForeignKey('fk_hormoneMeasurementSupplement_idMeasurementTypes','hormone_measurement_supplement');

	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}