<?php

class m160225_051229_modFieldGenderUser extends CDbMigration
{
	public function up()
	{
		$this->renameColumn('users','genre','gender');
		$this->alterColumn('users','gender','enum("Male","Female")');
	}

	public function down()
	{
		$this->alterColumn('users','gender','varchar(20) NOT NULL');
		$this->renameColumn('users','gender','genre');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}