<?php

class m160110_040525_course extends CDbMigration
{
	public function up()
	{
		$this->createTable('course',array(
			'id'=>'pk',
			'name'=>'string',
			'startDate'=>'timestamp',
			'location'=>'string',
			'courseIdentification'=>'string'
		),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		echo "m160110_040525_course does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}