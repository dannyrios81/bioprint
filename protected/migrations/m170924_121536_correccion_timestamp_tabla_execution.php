<?php

class m170924_121536_correccion_timestamp_tabla_execution extends CDbMigration
{
	public function up()
	{
	    $this->execute('ALTER TABLE `execution` MODIFY COLUMN `date`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `idUserPhase`;');
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}