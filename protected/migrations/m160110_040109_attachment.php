<?php

class m160110_040109_attachment extends CDbMigration
{
	public function up()
	{
		$this->createTable('attachment',array(
			'id'=>'pk',
			'path'=>'string',
			'idAttachmentType'=>'integer'
		),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		echo "m160110_040109_attachment does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}