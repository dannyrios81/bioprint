<?php

class m160325_182303_new_table_ChooseSuplements extends CDbMigration
{
	public function up()
	{
		$this->createTable('choose_suplement',[
			'idChoose'=>'integer',
			'idSuplement'=>'integer',
			'quantity'=>'string'
		],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
		$this->addPrimaryKey('pk','choose_suplement',['idChoose','idSuplement']);
		$this->addForeignKey('fk_chooseSuplement_idChoose','choose_suplement','idChoose','choose','id');
		$this->addForeignKey('fk_chooseSuplement_idSuplement','choose_suplement','idSuplement','supplement','id');
	}

	public function down()
	{
		$this->dropTable('choose_suplement');

	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}