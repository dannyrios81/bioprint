<?php

class m160303_062341_mod_date_column_measurement extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('measurement','date','timestamp');
	}

	public function down()
	{
		$this->alterColumn('measurement','date','date');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}