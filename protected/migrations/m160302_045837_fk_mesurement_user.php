<?php

class m160302_045837_fk_mesurement_user extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey('fk_measurement_idUsers','measurement','idUsers','users','id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_measurement_idUsers','measurement');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}