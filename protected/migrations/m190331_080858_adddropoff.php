<?php

class m190331_080858_adddropoff extends CDbMigration
{
	public function up()
	{
        $this->addColumn('execution_exercise','dropoff','decimal(6,2)');
        $this->addColumn('execution_exercise_calc','dropoff','decimal(6,2)');
	}

	public function down()
	{
		$this->dropColumn('execution_exercise','dropoff');
		$this->dropColumn('execution_exercise_calc','dropoff');
	}
}