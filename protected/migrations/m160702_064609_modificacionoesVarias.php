<?php

class m160702_064609_modificacionoesVarias extends CDbMigration
{
	public function up()
	{
		$this->execute("ALTER TABLE `users` MODIFY COLUMN `create_record`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `password`;");
		$this->execute("ALTER TABLE `weight_height`
MODIFY COLUMN `weight`  decimal(6,2) NULL DEFAULT NULL AFTER `id`,
MODIFY COLUMN `height`  decimal(6,2) NULL DEFAULT NULL AFTER `weightUnit`;");
	}

	public function down()
	{
		$this->execute("ALTER TABLE `weight_height`
MODIFY COLUMN `weight`  int(11) NULL DEFAULT NULL AFTER `id`,
MODIFY COLUMN `height`  INT(11) NULL DEFAULT NULL AFTER `weightUnit`;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}