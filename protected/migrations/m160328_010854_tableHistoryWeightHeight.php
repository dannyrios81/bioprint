<?php

class m160328_010854_tableHistoryWeightHeight extends CDbMigration
{
	public function up()
	{
		$this->createTable('weight_height',[
			'id'=>'pk',
			'weight'=>'integer',
			'weightUnit'=>'string',
			'height'=>'integer',
			'heightUnit'=>'string',
			'idUser'=>'integer',
			'date'=>'timestamp'
		],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
		$this->addForeignKey('fk_weightHeight_idUser','weight_height','idUser','users','id');
	}

	public function down()
	{
		$this->dropTable('weight_height');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}