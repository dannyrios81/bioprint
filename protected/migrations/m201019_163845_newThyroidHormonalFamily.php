<?php

class m201019_163845_newThyroidHormonalFamily extends CDbMigration
{
	public function up()
	{
        $hormoneFamily = new HormoneFamily();
        $hormoneFamily->name = 'Thyroid';
        $hormoneFamily->idAssociatedMeasurementType1 = 8;
        $hormoneFamily->idAssociatedMeasurementType2 = 8;
        $hormoneFamily->idAssociatedMeasurementType3 = 8;
        $hormoneFamily->priority = 200;
        $hormoneFamily->save(false);

        $medidas = [
            ['idealMeasure' => 5.80,'idMeasuremetType' => 8,'gender' => 'Male'],
            ['idealMeasure' => 6.20,'idMeasuremetType' => 8,'gender' => 'Female'],
        ];
        foreach ($medidas as $medida) {
            $idea = new IdealMeasurement();
            $idea->setAttributes($medida,false);
            $idea->save(false);
        }

        /** contrucciuon de los protocolos */

        $moments = Moments::model()->findAll();
        foreach ($moments as $index => $moment) {
            if($moment->priority>5)
            {
                $moment->priority=$moment->priority+1;
                $moment->save(false);
            }
        }
        $nuevoMomento = new Moments();
        $nuevoMomento->priority=6;
        $nuevoMomento->name='Post-Workout';
        $nuevoMomento->save(false);


        $countries = Country::model()->findAll();
        $measurementTypes = [
            ["measurement" => "Pectoral","id" => 3],
            ["measurement" => "Biceps","id" => 4],
            ["measurement" => "Triceps","id" => 5],
            ["measurement" => "Subscapular","id" => 6],
            ["measurement" => "Suprailiac","id" => 9],
            ["measurement" => "Australian","id" => 7,'notes'=>'Berberine must be SLOWLY titrated upwards. Start with one cap for 5 days. Add a cap every 5 days. Since it kills pathogens, side effects like loose stools, bloating, and gas are common.'],
            ["measurement" => "Umbilical","id" => 10],
            ["measurement" => "Knee","id" => 11],
            ["measurement" => "Calf","id" => 12],
            ["measurement" => "Quadriceps","id" => 13],
            ["measurement" => "Hamstrings","id" => 14],
        ];

        $momentsLoad = [
            "3"=> [
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Organo-Iodine-Drops-10','DFH-Iodine-Synergy-1']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Hormone-Balance-1','ATP-Aromatek-1']],

                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],

                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Hormone-Balance-1','ATP-Aromatek-1']],

                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Magtein-4','DFH-NeuroMag-4','ATP-Mind-Mag-4',]],
            ],
            "4"=> [
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Organo-Iodine-Drops-10','DFH-Iodine-Synergy-1']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Tribulstan-3','DFH-Libido-Stim-M-3','ATP-Alpha-Jack-3']],

                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Tribulstan-3','DFH-Libido-Stim-M-3','ATP-Alpha-Jack-3']],

                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],

                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Magtein-4','DFH-NeuroMag-4','ATP-Mind-Mag-4',]],
                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-CardioSterol-14','DFH-Foresterol-7']],
            ],
            "5"=> [
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Organo-Iodine-Drops-10','DFH-Iodine-Synergy-1']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Zinc-Pro-3','DFH-Zinc-Supreme-2','ATP-SynerZinc-2']],

                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Zinc-Pro-3','DFH-Zinc-Supreme-2','ATP-SynerZinc-2']],

                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Zinc-Pro-3','DFH-Zinc-Supreme-2','ATP-SynerZinc-2']],

                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Magtein-4','DFH-NeuroMag-4','ATP-Mind-Mag-4',]],
            ],
            "6"=> [
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Organo-Iodine-Drops-10','DFH-Iodine-Synergy-1']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Gluco-IR-3','DFH-GlucoSupreme-Herbal-2','ATP-Gluco-Control-2']],

                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Gluco-IR-3','DFH-GlucoSupreme-Herbal-2','ATP-Gluco-Control-2']],

                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Gluco-IR-3','DFH-GlucoSupreme-Herbal-2','ATP-Gluco-Control-2']],

                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Magtein-4','DFH-NeuroMag-4','ATP-Mind-Mag-4',]],
            ],
            "9"=> [
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Organo-Iodine-Drops-10','DFH-Iodine-Synergy-1']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Gluco-IR-3','DFH-GlucoSupreme-Herbal-2','ATP-Gluco-Control-2']],

                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Gluco-IR-3','DFH-GlucoSupreme-Herbal-2','ATP-Gluco-Control-2']],

                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Gluco-IR-3','DFH-GlucoSupreme-Herbal-2','ATP-Gluco-Control-2']],

                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Magtein-4','DFH-NeuroMag-4','ATP-Mind-Mag-4',]],
            ],
            "7"=> [
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Organo-Iodine-Drops-10','DFH-Iodine-Synergy-1']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Berberine-Pro-2','DFH-Berberine-Synergy-2']],

                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Berberine-Pro-2','DFH-Berberine-Synergy-2']],

                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Berberine-Pro-2','DFH-Berberine-Synergy-2']],

                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Magtein-4','DFH-NeuroMag-4','ATP-Mind-Mag-4']],
            ],
            "10"=> [
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Organo-Iodine-Drops-10','DFH-Iodine-Synergy-1']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Adreno-Trophic-2','DFH-Adrenotone-2']],

                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Adreno-Trophic-2','DFH-Adrenotone-2']],

                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Cortisol-Pro-2','DFH-CatecholaCalm-3']],

                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Magtein-4','DFH-NeuroMag-4','ATP-Mind-Mag-4']],
                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Cortisol-Pro-2','DFH-CatecholaCalm-3']],
            ],
            "11"=> [
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Organo-Iodine-Drops-10','DFH-Iodine-Synergy-1']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Detox-Phase-I-&-II-2','DFH-Detoxification-Support-1']],

                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Detox-Phase-I-&-II-2','DFH-Detoxification-Support-1']],

                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Detox-Phase-I-&-II-2','DFH-Detoxification-Support-1']],

                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Magtein-4','DFH-NeuroMag-4','ATP-Mind-Mag-4']],
            ],
            "12"=> [
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Organo-Iodine-Drops-10','DFH-Iodine-Synergy-1']],

                ['moment'=>'Post-Workout','suplements'=>['DFH-Magnesium-Malate-4']],

                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],

                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],

                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Magtein-6','DFH-NeuroMag-6','ATP-Mind-Mag-6']],
            ],
            "13"=> [
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Organo-Iodine-Drops-10','DFH-Iodine-Synergy-1']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Zinc-Pro-3','DFH-Zinc-Supreme-2','ATP-SynerZinc-2']],

                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Zinc-Pro-3','DFH-Zinc-Supreme-2','ATP-SynerZinc-2']],

                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Zinc-Pro-3','DFH-Zinc-Supreme-2','ATP-SynerZinc-2']],

                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Magtein-4','DFH-NeuroMag-4','ATP-Mind-Mag-4']],
                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Curcumin-400x-3','DFH-Curcum-Evail-3']],
            ],
            "14"=> [
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Organo-Iodine-Drops-10','DFH-Iodine-Synergy-1']],
                ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn-Hormone-Balance-1','ATP-Estro-Control-1']],

                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Lunch','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],

                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2','DFH-Complete-Multi-(no-iron-for-men)-2','ATP-Total-Defense-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Omega-Pure-EPA-DHA-720-2','DFH-OmegAvail-Ultra-2','ATP-Omega-Pure-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Thyroid-Complex-2','DFH-Thyroid-Synergy-2']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Magnesium-Glycinate-5','DFH-Magnesium-Buffered-Chelate-4','ATP-SynerMag-6']],
                ['moment'=>'Dinner','suplements'=>['Nutri-Dyn-Hormone-Balance-1','ATP-Estro-Control-1']],

                ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn-Magtein-4','DFH-NeuroMag-4','ATP-Mind-Mag-4']],
            ],
        ];

        $suplementos = [
            'Nutri-Dyn-Essential-Multi-(no-iron-for-men)-2'=>['name'=>'Nutri-Dyn Essential Multi (no iron for men)','independent'=>0,'dosis'=>'2 caps'],
            'ATP-Alpha-Jack-3'=>['name'=>'ATP Alpha Jack','independent'=>0,'dosis'=>'3 caps'],
            'ATP-Aromatek-1'=>['name'=>'ATP Aromatek','independent'=>0,'dosis'=>'1 cap'],
            'ATP-Estro-Control-1'=>['name'=>'ATP Estro Control','independent'=>0,'dosis'=>'1 cap'],
            'ATP-Gluco-Control-2'=>['name'=>'ATP Gluco Control','independent'=>0,'dosis'=>'2 caps'],
            'ATP-Mind-Mag-4'=>['name'=>'ATP Mind Mag','independent'=>0,'dosis'=>'4 caps'],
            'ATP-Mind-Mag-6'=>['name'=>'ATP Mind Mag','independent'=>0,'dosis'=>'6 caps'],
            'ATP-Omega-Pure-2'=>['name'=>'ATP Omega Pure','independent'=>0,'dosis'=>'2 softgels'],
            'ATP-SynerMag-6'=>['name'=>'ATP SynerMag','independent'=>0,'dosis'=>'6 caps'],
            'ATP-SynerZinc-2'=>['name'=>'ATP SynerZinc','independent'=>0,'dosis'=>'2 caps'],
            'ATP-Total-Defense-2'=>['name'=>'ATP Total Defense','independent'=>0,'dosis'=>'2 caps'],
            'DFH-Adrenotone-2'=>['name'=>'DFH Adrenotone','independent'=>0,'dosis'=>'2 caps'],
            'DFH-Berberine-Synergy-2'=>['name'=>'DFH Berberine Synergy','independent'=>0,'dosis'=>'2 caps'],
            'DFH-CatecholaCalm-3'=>['name'=>'DFH CatecholaCalm','independent'=>0,'dosis'=>'3 caps'],
            'DFH-Complete-Multi-(no-iron-for-men)-2'=>['name'=>'DFH Complete Multi (no iron for men)','independent'=>0,'dosis'=>'2 caps'],
            'DFH-Curcum-Evail-3'=>['name'=>'DFH Curcum-Evail','independent'=>0,'dosis'=>'3 softgels'],
            'DFH-Detoxification-Support-1'=>['name'=>'DFH Detoxification Support','independent'=>0,'dosis'=>'1 packet'],
            'DFH-Foresterol-7'=>['name'=>'DFH Foresterol','independent'=>0,'dosis'=>'7 softgels'],
            'DFH-GlucoSupreme-Herbal-2'=>['name'=>'DFH GlucoSupreme Herbal','independent'=>0,'dosis'=>'2 caps'],
            'DFH-Iodine-Synergy-1'=>['name'=>'DFH Iodine Synergy','independent'=>0,'dosis'=>'1 cap'],
            'DFH-Libido-Stim-M-3'=>['name'=>'DFH Libido Stim-M','independent'=>0,'dosis'=>'3 caps'],
            'DFH-Magnesium-Buffered-Chelate-4'=>['name'=>'DFH Magnesium Buffered Chelate','independent'=>0,'dosis'=>'4 caps'],
            'DFH-Magnesium-Malate-4'=>['name'=>'DFH Magnesium Malate','independent'=>0,'dosis'=>'4 caps'],
            'DFH-NeuroMag-4'=>['name'=>'DFH NeuroMag','independent'=>0,'dosis'=>'4 caps'],
            'DFH-NeuroMag-6'=>['name'=>'DFH NeuroMag','independent'=>0,'dosis'=>'6 caps'],
            'DFH-OmegAvail-Ultra-2'=>['name'=>'DFH OmegAvail Ultra','independent'=>0,'dosis'=>'2 sofgels'],
            'DFH-Thyroid-Synergy-2'=>['name'=>'DFH Thyroid Synergy','independent'=>0,'dosis'=>'2 caps'],
            'DFH-Zinc-Supreme-2'=>['name'=>'DFH Zinc Supreme','independent'=>0,'dosis'=>'2 caps'],
            'Nutri-Dyn-Adreno-Trophic-2'=>['name'=>'Nutri-Dyn Adreno Trophic','independent'=>0,'dosis'=>'2 caps'],
            'Nutri-Dyn-Berberine-Pro-2'=>['name'=>'Nutri-Dyn Berberine Pro','independent'=>0,'dosis'=>'2 caps'],
            'Nutri-Dyn-CardioSterol-14'=>['name'=>'Nutri-Dyn CardioSterol','independent'=>0,'dosis'=>'14 caps'],
            'Nutri-Dyn-Cortisol-Pro-2'=>['name'=>'Nutri-Dyn Cortisol Pro','independent'=>0,'dosis'=>'2 caps'],
            'Nutri-Dyn-Curcumin-400x-3'=>['name'=>'Nutri-Dyn Curcumin 400x','independent'=>0,'dosis'=>'3 softgels'],
            'Nutri-Dyn-Detox-Phase-I-&-II-2'=>['name'=>'Nutri-Dyn Detox Phase I & II','independent'=>0,'dosis'=>'2 caps'],
            'Nutri-Dyn-Gluco-IR-3'=>['name'=>'Nutri-Dyn Gluco IR','independent'=>0,'dosis'=>'3 caps'],
            'Nutri-Dyn-Hormone-Balance-1'=>['name'=>'Nutri-Dyn Hormone Balance','independent'=>0,'dosis'=>'1 scoop'],
            'Nutri-Dyn-Magnesium-Glycinate-5'=>['name'=>'Nutri-Dyn Magnesium Glycinate','independent'=>0,'dosis'=>'5 caps'],
            'Nutri-Dyn-Magtein-4'=>['name'=>'Nutri-Dyn Magtein','independent'=>0,'dosis'=>'4 caps'],
            'Nutri-Dyn-Magtein-6'=>['name'=>'Nutri-Dyn Magtein','independent'=>0,'dosis'=>'6 caps'],
            'Nutri-Dyn-Omega-Pure-EPA-DHA-720-2'=>['name'=>'Nutri-Dyn Omega Pure EPA-DHA 720','independent'=>0,'dosis'=>'2 softgels'],
            'Nutri-Dyn-Organo-Iodine-Drops-10'=>['name'=>'Nutri-Dyn Organo Iodine Drops','independent'=>0,'dosis'=>'10 drops'],
            'Nutri-Dyn-Thyroid-Complex-2'=>['name'=>'Nutri-Dyn Thyroid Complex','independent'=>0,'dosis'=>'2 caps'],
            'Nutri-Dyn-Tribulstan-3'=>['name'=>'Nutri-Dyn Tribulstan','independent'=>0,'dosis'=>'3 caps'],
            'Nutri-Dyn-Zinc-Pro-3'=>['name'=>'Nutri-Dyn Zinc Pro','independent'=>0,'dosis'=>'3 caps'],

        ];
        $moments = Moments::model()->findAll();
        $momentSelected = [];

        foreach ($moments as $moment) {
            if(in_array(strtolower($moment->name),['breakfast','lunch','dinner','post-workout','before bed']))
                $momentSelected[strtolower($moment->name)] = $moment->id;
        }

        foreach ($countries as $country) {
            foreach ($measurementTypes as $measurementType) {
                $recipe = new Recipes();
                $recipe->idHormoneFamily = $hormoneFamily->id;
                $recipe->idCountry = $country->id;
                $recipe->idMeasurmenetTypes = $measurementType['id'];
                if(isset($measurementType['notes']))
                    $recipe->notes = $measurementType['notes'];
                $recipe->save();
                $suplementsObjs=[];

                foreach ($momentsLoad[$measurementType['id']] as $momentLoad) {
                    $chose = new Choose();
                    $chose->idRecipe = $recipe->id;
                    $chose->idMoment=$momentSelected[strtolower($momentLoad['moment'])];
                    $chose->save();
                    foreach ($momentLoad['suplements'] as $suplement) {
                        if(!isset($suplementsObjs[$suplement]))
                        {
                            $suplementsObjs[$suplement] = $this->findSuplement($suplementos[$suplement],$country->id);
                        }
                        $choseSuplement = new ChooseSuplement();
                        $choseSuplement->idChoose=$chose->id;
                        $choseSuplement->idSuplement=$suplementsObjs[$suplement]->id;
                        $choseSuplement->quantity=$suplementos[$suplement]['dosis'];
                        $choseSuplement->save();
                    }
                }
            }
        }
	}

	public function down()
	{
        $medidas = [
            ['idealMeasure' => 5.80,'idMeasuremetType' => 8,'gender' => 'Male'],
            ['idealMeasure' => 6.20,'idMeasuremetType' => 8,'gender' => 'Female'],
        ];

        foreach ($medidas as $medida) {
            IdealMeasurement::model()->deleteAllByAttributes($medida);
        }

        $hormoneFamily = HormoneFamily::model()->findByAttributes(['name'=>'Thyroid']);
        HormoneUserDate::model()->deleteAllByAttributes(['idHormoneFamily'=>$hormoneFamily->id]);
        $recipes = Recipes::model()->findAllByAttributes(['idHormoneFamily'=>$hormoneFamily->id]);
        foreach ($recipes as $recipe) {
//            $recipe = new Recipes();
            $choses = Choose::model()->findAllByAttributes(['idRecipe'=>$recipe->id]);
            foreach ($choses as $chose) {
//                $chose = new Choose();
                ChooseSuplement::model()->deleteAllByAttributes(['idChoose'=>$chose->id]);
                $chose->delete();
            }
            $recipe->delete();
        }
        $hormoneFamily->delete();
        Moments::model()->deleteAllByAttributes(['name'=>'Post-Workout']);
	}
    public function findSuplement($infoSuplement, $idConuntry)
    {
        unset($infoSuplement['dosis']);
        $infoSuplement['idCountry']=$idConuntry;

        $retorno = Supplement::model()->findByAttributes($infoSuplement);

        if (empty($retorno))
        {
            $retorno = new Supplement();
            $retorno->setAttributes($infoSuplement);
            $retorno->save();
        }
        return $retorno;
    }
}