<?php

class m170801_073054_mod_fase extends CDbMigration
{
	public function up()
	{
	    $this->dropForeignKey('userPhase_bodyPart_idBodyPart','users_phase');
	    $this->dropForeignKey('userPhase_fase_idFase','users_phase');
	    $this->dropForeignKey('userPhase_users_idUser','users_phase');

	    $this->dropColumn('users_phase','idFase');
	    $this->dropColumn('users_phase','idBodyPart');

	    $this->addColumn('users_phase','Fase','string');
	    $this->addColumn('users_phase','BodyPart','string');
	}

	public function down()
	{
        $this->dropColumn('users_phase','Fase');
        $this->dropColumn('users_phase','BodyPart');

        $this->addColumn('users_phase','idFase','INT(11)');
        $this->addColumn('users_phase','idBodyPart','INT(11)');

        $this->addForeignKey('userPhase_fase_idFase','users_phase','idFase','fase','id');
        $this->addForeignKey('userPhase_bodyPart_idBodyPart','users_phase','idBodyPart','body_part','id');
        $this->addForeignKey('userPhase_users_idUser','users_phase','idUser','users','id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}