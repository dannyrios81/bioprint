<?php

class m171010_064046_lateralidad_data extends CDbMigration
{
	public function up()
	{
	    $this->insertMultiple('laterality',[
	        [
	            'id'=>1,
                'description'=>'Unilateral'
            ],
            [
                'id'=>2,
                'description'=>'Bilateral'
            ]
        ]);
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}