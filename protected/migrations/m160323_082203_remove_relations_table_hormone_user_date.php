<?php

class m160323_082203_remove_relations_table_hormone_user_date extends CDbMigration
{
	public function up()
	{
		$this->dropForeignKey('fk_hormoneUserDate_idAssociatedMeasurement1','hormone_user_date');
		$this->dropForeignKey('fk_hormoneUserDate_idAssociatedMeasurement2','hormone_user_date');
		$this->dropForeignKey('fk_hormoneUserDate_idAssociatedMeasurement3','hormone_user_date');
		$this->dropColumn('hormone_user_date','idAssociatedMeasurement1');
		$this->dropColumn('hormone_user_date','idAssociatedMeasurement2');
		$this->dropColumn('hormone_user_date','idAssociatedMeasurement3');

		$this->addColumn('hormone_family','idAssociatedMeasurementType1','integer');
		$this->addColumn('hormone_family','idAssociatedMeasurementType2','integer');
		$this->addColumn('hormone_family','idAssociatedMeasurementType3','integer');
		$this->addForeignKey('fk_hormone_family_idAssociatedMeasurementType1','hormone_family','idAssociatedMeasurementType1','measurement_types','id');
		$this->addForeignKey('fk_hormone_family_idAssociatedMeasurementType2','hormone_family','idAssociatedMeasurementType2','measurement_types','id');
		$this->addForeignKey('fk_hormone_family_idAssociatedMeasurementType3','hormone_family','idAssociatedMeasurementType3','measurement_types','id');

		$this->addColumn('hormone_user_date','idHormoneFamily','integer');
		$this->addForeignKey('fk_hormoneUserDate_idHormoneFamily','hormone_user_date','idHormoneFamily','hormone_family','id');
	}

	public function down()
	{
		$this->addColumn('hormone_user_date','idAssociatedMeasurement1','integer');
		$this->addColumn('hormone_user_date','idAssociatedMeasurement2','integer');
		$this->addColumn('hormone_user_date','idAssociatedMeasurement3','integer');
		$this->addForeignKey('fk_hormoneUserDate_idAssociatedMeasurement1','hormone_user_date','idAssociatedMeasurement1','measurement','id');
		$this->addForeignKey('fk_hormoneUserDate_idAssociatedMeasurement2','hormone_user_date','idAssociatedMeasurement2','measurement','id');
		$this->addForeignKey('fk_hormoneUserDate_idAssociatedMeasurement3','hormone_user_date','idAssociatedMeasurement3','measurement','id');

		$this->dropForeignKey('fk_hormone_family_idAssociatedMeasurementType1','hormone_family');
		$this->dropForeignKey('fk_hormone_family_idAssociatedMeasurementType2','hormone_family');
		$this->dropForeignKey('fk_hormone_family_idAssociatedMeasurementType3','hormone_family');
		$this->dropColumn('hormone_family','idAssociatedMeasurementType1');
		$this->dropColumn('hormone_family','idAssociatedMeasurementType2');
		$this->dropColumn('hormone_family','idAssociatedMeasurementType3');

		$this->dropForeignKey('fk_hormoneUserDate_idHormoneFamily','hormone_user_date');
		$this->dropColumn('hormone_user_date','idHormoneFamily');

	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}