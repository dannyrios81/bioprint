<?php

class m160427_034047_table_sent_protocols extends CDbMigration
{
	public function up()
	{
		$this->createTable('sent_protocols',[
			'id'=>'pk',
			'idUsers'=>'integer',
			'content'=>'text',
			'hormoneFamily'=>'string',
			'measurementType'=>'string',
			'sentDate'=>'timestamp'
		],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		$this->dropTable('sent_protocols');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}