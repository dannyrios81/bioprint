<?php

class m171219_002128_borrarrangemotionExercise extends CDbMigration
{
	public function up()
	{
	    $this->dropForeignKey('fk_exercise_idRangeMotion_rangeMotion','exercise');
        $this->dropColumn('exercise','idRangeMotion');
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}