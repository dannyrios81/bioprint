<?php

class m160725_074623_cambios_migracion extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('users','migrate','tinyint(1) NULL DEFAULT 0');
	    $this->addColumn('measurement','migrate','tinyint(1) NULL DEFAULT 0');
	    $this->addColumn('weight_height','migrate','tinyint(1) NULL DEFAULT 0');
        $this->insert('attachment_types',['id'=>3,'type'=>'migration']);

	}

	public function down()
	{
	    $this->delete('attachment_types','id=3');
        $this->dropColumn('users','migrate');
        $this->dropColumn('measurement','migrate');
        $this->dropColumn('weight_height','migrate');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}