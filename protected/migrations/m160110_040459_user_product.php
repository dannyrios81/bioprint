<?php

class m160110_040459_user_product extends CDbMigration
{
	public function up()
	{
		$this->createTable('user_product',array(
			'idUsers'=>'integer',
			'idProduct'=>'integer',
			'lastDatePurchase'=>'timestamp',
			'activateDate'=>'timestamp',
			'dueDate'=>'timestamp'
		),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		echo "m160110_040459_user_product does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}