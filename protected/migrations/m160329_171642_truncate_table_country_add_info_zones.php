<?php

class m160329_171642_truncate_table_country_add_info_zones extends CDbMigration
{
	public function up()
	{
		$this->update('users',['idCountry'=>1]);
		$this->update('supplement',['idCountry'=>1]);
		$this->update('recipes',['idCountry'=>1]);
		$this->execute('SET FOREIGN_KEY_CHECKS=0;');
		$this->truncateTable('country');
		$this->insertMultiple('country',[
			['name'=>'Australasia','code'=>'AS'],
			['name'=>'Canada (English)','code'=>'CE'],
			['name'=>'Canada (French)','code'=>'CF'],
			['name'=>'Dominican Republic and United States','code'=>'DU'],
			['name'=>'Europe (English)','code'=>'EE'],
			['name'=>'Europe (French)','code'=>'EF'],
		]);
		$this->execute('SET FOREIGN_KEY_CHECKS=1;');

	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}