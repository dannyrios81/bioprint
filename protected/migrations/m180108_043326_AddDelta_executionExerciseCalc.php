<?php

class m180108_043326_AddDelta_executionExerciseCalc extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('execution_exercise_calc','delta','decimal(6,2)');
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}