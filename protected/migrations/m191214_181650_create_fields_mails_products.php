<?php

class m191214_181650_create_fields_mails_products extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('product','urlPayOneWeek','string');
	    $this->addColumn('product','urlPayTwoWeeks','string');
	}

	public function down()
	{
	    $this->dropColumn('product','urlPayOneWeek');
	    $this->dropColumn('product','urlPayTwoWeeks');
	}

}