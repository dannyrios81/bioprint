<?php

class m180111_024858_addExerciseRange extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('exercise','idRangeMotion','INT(11)');
        $this->addForeignKey('fk_exercise_idRangeMotion_rangeMotion','exercise','idRangeMotion','range_motion','id');
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}