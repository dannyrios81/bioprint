<?php

class m160110_053544_attachment_types extends CDbMigration
{
	public function up()
	{
		$this->createTable('attachment_types',array(
			'id'=>'pk',
			'type'=>'string'
		),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		echo "m160110_053544_attachment_types does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}