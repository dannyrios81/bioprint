<?php

class m160110_040421_purchase extends CDbMigration
{
	public function up()
	{
		$this->createTable('purchase',array(
            'id'=>'pk',
            'idUsers'=>'integer',
            'name'=>'string',
            'renewal'=>'string',
            'purchaseDate'=>'datetime',
            'cost'=>'money'
        ),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		echo "m160110_040421_purchase does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}