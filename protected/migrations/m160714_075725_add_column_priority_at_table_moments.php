<?php

class m160714_075725_add_column_priority_at_table_moments extends CDbMigration
{
	public function up()
	{
		$this->addColumn('moments','priority','integer');
	}

	public function down()
	{
		$this->dropColumn('moments','priority');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}