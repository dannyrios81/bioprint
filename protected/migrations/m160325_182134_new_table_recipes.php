<?php

class m160325_182134_new_table_recipes extends CDbMigration
{
	public function up()
	{
		$this->createTable('recipes',[
			'id'=>'pk',
			'idHormoneFamily'=>'integer',
			'idMeasurmenetTypes'=>'integer',
			'idCountry'=>'integer',
			'notes'=>'text'
		],'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->addForeignKey('fk_recipes_idHormoneFamily','recipes','idHormoneFamily','hormone_family','id');
		$this->addForeignKey('fk_recipes_idMeasurmenetTypes','recipes','idMeasurmenetTypes','measurement_types','id');
		$this->addForeignKey('fk_recipes_idCountry','recipes','idCountry','country','id');
	}

	public function down()
	{
		$this->truncateTable('recipes');
		$this->dropTable('recipes');

	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}