<?php

class m160302_003731_seed_measurement extends CDbMigration
{
	public function up()
	{
		//$this->insert('attachment_types',['id'=>'2','type'=>'measurement image']);
		$this->addColumn('measurement_types','idAttachment','integer');
		$this->addForeignKey('pk_measurementTypes_idAttachment','measurement_types','idAttachment','attachment','id');
		$this->dropForeignKey('fk_measurement_idAttachments','measurement');
		$this->dropColumn('measurement','idAttachment');

	}

	public function down()
	{
		$this->dropForeignKey('pk_measurementTypes_idAttachment','measurement_types');
		$this->dropColumn('measurement_types','idAttachment','integer');
		//$this->delete('attachment_types','type=:type',['type'=>'measurement image']);
		$this->addColumn('measurement','idAttachment','integer');
		$this->addForeignKey('fk_measurement_idAttachments','measurement','idAttachment','attachment','id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}