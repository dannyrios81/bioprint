<?php

class m160310_035134_changesMeasurementTypes extends CDbMigration
{
	public function up()
	{
		$this->addColumn('measurement_types','calculable','boolean');
	}

	public function down()
	{
		$this->dropColumn('measurement_types','calculable');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}