<?php

class m160323_062056_create_table_idealMeasurement extends CDbMigration
{
	public function up()
	{
		$this->createTable('ideal_measurement',[
			'id'=>'pk',
			'idealMeasure'=>'decimal(6,2)',
			'idMeasuremetType'=>'integer',
			'gender'=>'enum("Female","Male")'
		],'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->addForeignKey('fk_idealMeasurement_idMeasuremetType','ideal_measurement','idMeasuremetType','measurement_types','id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_idealMeasurement_idMeasuremetType','ideal_measurement');
		$this->dropTable('ideal_measurement');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}