<?php

class m170903_014727_drop_fk_repeatinteval extends CDbMigration
{
	public function up()
	{
        $this->dropForeignKey('repeatInterval_numberSeries_numberSeriesId','repeat_interval');
	}

	public function down()
	{
        $this->addForeignKey(
            'repeatInterval_numberSeries_numberSeriesId',
            'repeat_interval',
            'numberSeriesId',
            'number_series',
            'id'
        );
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}