<?php

class m160409_011718_drop_columns_users extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('users','weight');
		$this->dropColumn('users','weightUnit');
		$this->dropColumn('users','height');
		$this->dropColumn('users','heightUnit');
	}

	public function down()
	{
		$this->addColumn('users','weight','integer');
		$this->addColumn('users','weightUnit','string');
		$this->addColumn('users','height','integer');
		$this->addColumn('users','heightUnit','string');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}