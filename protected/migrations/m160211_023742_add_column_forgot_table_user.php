<?php

class m160211_023742_add_column_forgot_table_user extends CDbMigration
{
	public function up()
	{
		$this->addColumn('users','forgotPass','boolean DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('users','forgotPass');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}