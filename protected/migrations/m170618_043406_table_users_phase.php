<?php

class m170618_043406_table_users_phase extends CDbMigration
{
	public function up()
	{
	    $this->createTable('users_phase',
        [
            'id'=>'pk',
            'idFase'=>'INT(11)',
            'idBodyPart'=>'INT(11)',
            'date'=>'timestamp',
            'numberPhase'=>'INT(11)',
            'numberOfTimes'=>'INT(11)',
            'idUser'=>'INT(11)'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');

	    $this->addForeignKey('userPhase_fase_idFase','users_phase','idFase','fase','id');
	    $this->addForeignKey('userPhase_bodyPart_idBodyPart','users_phase','idBodyPart','body_part','id');
	    $this->addForeignKey('userPhase_users_idUser','users_phase','idUser','users','id');
	}

	public function down()
	{
        $this->dropForeignKey('userPhase_fase_idFase','users_phase');
        $this->dropForeignKey('userPhase_bodyPart_idBodyPart','users_phase');
        $this->dropForeignKey('userPhase_users_idUser','users_phase');

        $this->dropTable('users_phase');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}