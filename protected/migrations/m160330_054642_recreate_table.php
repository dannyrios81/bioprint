<?php

class m160330_054642_recreate_table extends CDbMigration
{
	public function up()
	{
		$this->execute('SET FOREIGN_KEY_CHECKS=0;');
		$this->dropTable('choose_suplement');
		$this->createTable('choose_suplement',[
			'id'=>'pk',
			'idChoose'=>'integer',
			'idSuplement'=>'integer',
			'quantity'=>'string'
		],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
		$this->addForeignKey('fk_chooseSuplement_idChoose','choose_suplement','idChoose','choose','id');
		$this->addForeignKey('fk_chooseSuplement_idSuplement','choose_suplement','idSuplement','supplement','id');
		$this->execute('SET FOREIGN_KEY_CHECKS=1;');
	}

	public function down()
	{
		$this->dropTable('choose_suplement');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}