<?php

class m160519_181025_table_course_users extends CDbMigration
{
	public function up()
	{
		$this->createTable('users_courses',array(
			'id'=>'pk',
			'idCourse'=>'integer',
			'idUsers'=>'integer',
			'date'=>'timestamp NULL DEFAULT CURRENT_TIMESTAMP'
		),'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->addForeignKey('fk_userscourses_idCourse','users_courses','idCourse','course','id');
		$this->addForeignKey('fk_userscourses_idUsers','users_courses','idUsers','users','id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_userscourses_idCourse','users_courses');
		$this->dropForeignKey('fk_userscourses_idUsers','users_courses');
		$this->dropForeignKey('fk_usersproduct_idProduct','users_product');
		$this->dropForeignKey('fk_usersproduct_idUsers','users_product');

		$this->dropTable('users_courses');
		$this->dropTable('users_product');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}