<?php

class m171101_075406_mod_seminars_products extends CDbMigration
{
	public function up()
	{
	    //product
        $this->addColumn('product','productCode','string');
        $this->createTable('courseHasProducts',[
            'id'=>'pk',
            'idcourse'=>'INT(11)',
            'idproduct'=>'INT(11)'
        ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->addForeignKey('courseHasProducts_id_idcourse','courseHasProducts','idcourse','course','id');
        $this->addForeignKey('courseHasProducts_id_idproduct','courseHasProducts','idproduct','product','id');

        $this->insert('product',[
            'id'=>2,
            'name'=>'Phase Application',
            'renewal'=>0,
            'cost'=>0,
            'linkToUserManual'=>'url pending',
            'productCode'=>'P0002'
        ]);
        $update = Product::model()->findByPk(1);
//        $update = new Product();
        $update->productCode='P0001';
        $update->save();
	}

	public function down()
	{
//		$this->dropForeignKey('courseHasProducts_id_idcourse','courseHasProducts');
		//$this->dropForeignKey('courseHasProducts_id_idproduct','courseHasProducts');

        Product::model()->deleteByPk(2);

		$this->dropTable('courseHasProducts');

		$this->dropColumn('product','productCode');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}