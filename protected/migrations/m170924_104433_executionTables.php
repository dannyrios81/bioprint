<?php

class m170924_104433_executionTables extends CDbMigration
{
	public function up()
	{
	   $this->createTable('execution',
           [
               'id'=>'pk',
               'idUserPhase'=>'int(11)',
               'date'=>'timestamp',
               'unitType'=>'varchar(50)',
               'weigth'=>'decimal(6,2)'
           ],'ENGINE=InnoDB DEFAULT CHARSET=utf8'
           );
	   $this->createTable('execution_exercise',
           [
               'id'=>'pk',
               'idExecution'=>'int(11)',
               'idUserPhaseExercise'=>'int(11)',
               'idExecutionExerciseCalc'=>'int(11)',
               'weigth'=>'decimal(6,2)',
               'repetitions'=>'int(11)',
               'weigthModificate'=>'decimal(6,2)'
           ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	   $this->createTable('execution_exercise_calc',
           [
               'id'=>'pk',
               'idExecution'=>'int(11)',
               'idUserPhaseExercice'=>'int(11)',
               'averageWeight'=>'decimal(6,2)',
               'willks'=>'decimal(6,2)',
               'averageRepetitions'=>'decimal(6,2)',
               'relStrength'=>'decimal(6,2)',
               'totalSets'=>'int(11)',
               'goal'=>'decimal(6,2)',
               'totalTonnage'=>'decimal(6,2)',
               'totalReps'=>'decimal(6,2)',
               'averageLift'=>'decimal(6,2)',
               'maximunLift'=>'decimal(6,2)',
               'percent'=>'decimal(6,2)',

           ],'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addColumn('user_phase_exercise','notes','text');
        $this->addColumn('user_phase_exercise','unitType','varchar(50)');
        $this->addColumn('user_phase_exercise','rangeMotion','varchar(255)');

        $this->addForeignKey('fk_ExecutionExercise_Execution','execution_exercise','idExecution','execution','id');
        $this->addForeignKey('fk_ExecutionExercise_UserPhaseExercise','execution_exercise','idUserPhaseExercise','user_phase_exercise','id');
        $this->addForeignKey('fk_ExecutionExercise_ExecutionExerciseCalc','execution_exercise','idExecutionExerciseCalc','execution_exercise_calc','id');

        $this->addForeignKey('fk_ExecutionExerciseCalc_Execution','execution_exercise_calc','idExecution','execution','id');
        $this->addForeignKey('fk_ExecutionExerciseCalc_UserPhaseExercice','execution_exercise_calc','idUserPhaseExercice','user_phase_exercise','id');

        $this->addForeignKey('fk_Execution_UserPhase','execution','idUserPhase','users_phase','id');






	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}