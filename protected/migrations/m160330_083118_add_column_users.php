<?php

class m160330_083118_add_column_users extends CDbMigration
{
	public function up()
	{
		$this->addColumn('users','contactInformation','text');
	}

	public function down()
	{
		$this->dropColumn('users','contactInformation');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}