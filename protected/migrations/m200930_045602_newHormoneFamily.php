<?php

class m200930_045602_newHormoneFamily extends CDbMigration
{
	public function up()
	{
        $this->addColumn('hormone_family','priority','integer DEFAULT 0');
        $this->addColumn('hormone_user_date','applyPriority','integer DEFAULT 0');

	    $hormoneFamily = new HormoneFamily();
	    $hormoneFamily->name = 'Cortisol';
	    $hormoneFamily->idAssociatedMeasurementType1 = 2;
        $hormoneFamily->idAssociatedMeasurementType2 = 10;
        $hormoneFamily->idAssociatedMeasurementType3 = 5;
        $hormoneFamily->priority = 100;
        $hormoneFamily->save(false);

        $medidas = [
            ['idealMeasure' => 5.00,'idMeasuremetType' => 10,'gender' => 'Male'],
            ['idealMeasure' => 7.60,'idMeasuremetType' => 10,'gender' => 'Female'],
            ['idealMeasure' => 7.60,'idMeasuremetType' => 2,'gender' => 'Male'],
            ['idealMeasure' => 7.70,'idMeasuremetType' => 2,'gender' => 'Female'],
            ['idealMeasure' => 5.80,'idMeasuremetType' => 8,'gender' => 'Male'],
            ['idealMeasure' => 6.20,'idMeasuremetType' => 8,'gender' => 'Female'],
        ];
        foreach ($medidas as $medida) {
            $idea = new IdealMeasurement();
            $idea->setAttributes($medida,false);
            $idea->save(false);
        }

        $countries = Country::model()->findAll();

        $momentsLoad = [
            ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn Essential Multi (no iron for men)','DFH Complete Multi (no iron for men)','ATP Total Defense']],
            ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn Omega Pure EPA-DHA 720','DFH OmegAvail Ultra','ATP Omega Pure']],
            ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn Adreno Trophic','DFH Adrenotone']],
            ['moment'=>'Breakfast','suplements'=>['Nutri-Dyn Zinc Pro','DFH Zinc Supreme','ATP SynerZinc']],

            ['moment'=>'Lunch','suplements'=>['Nutri-Dyn Omega Pure EPA-DHA 720','DFH OmegAvail Ultra','ATP Omega Pure']],
            ['moment'=>'Lunch','suplements'=>['Nutri-Dyn Adreno Trophic','DFH Adrenotone']],
            ['moment'=>'Lunch','suplements'=>['Nutri-Dyn Zinc Pro','DFH Zinc Supreme','ATP SynerZinc']],
            ['moment'=>'Lunch','suplements'=>['Nutri-Dyn Magnesium Glycinate','DFH Magnesium Buffered Chelate','ATP SynerMag']],

            ['moment'=>'Dinner','suplements'=>['Nutri-Dyn Essential Multi (no iron for men)','DFH Complete Multi (no iron for men)','ATP Total Defense']],
            ['moment'=>'Dinner','suplements'=>['Nutri-Dyn Omega Pure EPA-DHA 720','DFH OmegAvail Ultra','ATP Omega Pure']],
            ['moment'=>'Dinner','suplements'=>['Nutri-Dyn Cortisol Pro','DFH CatecholaCalm']],
            ['moment'=>'Dinner','suplements'=>['Nutri-Dyn Zinc Pro','DFH Zinc Supreme','ATP SynerZinc']],
            ['moment'=>'Dinner','suplements'=>['Nutri-Dyn Magnesium Glycinate','DFH Magnesium Buffered Chelate','ATP SynerMag']],

            ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn Magtein','DFH NeuroMag','ATP Mind Mag',]],
            ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn Cortisol Pro','DFH CatecholaCalm']],
            ['moment'=>'Before Bed','suplements'=>['Nutri-Dyn Pure Glycine','DFH Glycine Powder']],
        ];

        $suplementos = [

            'Nutri-Dyn Essential Multi (no iron for men)'=>['name'=>'Nutri-Dyn Essential Multi (no iron for men)','independent'=>0,'dosis'=>'2 Caps'],
            'DFH Complete Multi (no iron for men)'=>['name'=>'DFH Complete Multi (no iron for men)','independent'=>0,'dosis'=>'2 Caps'],
            'ATP Total Defense'=>['name'=>'ATP Total Defense','independent'=>0,'dosis'=>'2 Caps'],
            'Nutri-Dyn Omega Pure EPA-DHA 720'=>['name'=>'Nutri-Dyn Omega Pure EPA-DHA 720','independent'=>0,'dosis'=>'2 softgels'],
            'DFH OmegAvail Ultra'=>['name'=>'DFH OmegAvail Ultra','independent'=>0,'dosis'=>'2 sofgels'],
            'ATP Omega Pure'=>['name'=>'ATP Omega Pure','independent'=>0,'dosis'=>'2 sofgels'],
            'Nutri-Dyn Adreno Trophic'=>['name'=>'Nutri-Dyn Adreno Trophic','independent'=>0,'dosis'=>'2 Caps'],
            'DFH Adrenotone'=>['name'=>'DFH Adrenotone','independent'=>0,'dosis'=>'2 Caps'],
            'Nutri-Dyn Zinc Pro'=>['name'=>'Nutri-Dyn Zinc Pro','independent'=>0,'dosis'=>'3 Caps'],
            'DFH Zinc Supreme'=>['name'=>'DFH Zinc Supreme','independent'=>0,'dosis'=>'2 Caps'],
            'ATP SynerZinc'=>['name'=>'ATP SynerZinc','independent'=>0,'dosis'=>'2 Caps'],
            'Nutri-Dyn Magnesium Glycinate'=>['name'=>'Nutri-Dyn Magnesium Glycinate','independent'=>0,'dosis'=>'5 Caps'],
            'DFH Magnesium Buffered Chelate'=>['name'=>'DFH Magnesium Buffered Chelate','independent'=>0,'dosis'=>'4 Caps'],
            'ATP SynerMag'=>['name'=>'ATP SynerMag','independent'=>0,'dosis'=>'6 Caps'],
            'Nutri-Dyn Cortisol Pro'=>['name'=>'Nutri-Dyn Cortisol Pro','independent'=>0,'dosis'=>'2 Caps'],
            'DFH CatecholaCalm'=>['name'=>'DFH CatecholaCalm','independent'=>0,'dosis'=>'3 Caps'],
            'Nutri-Dyn Magtein'=>['name'=>'Nutri-Dyn Magtein','independent'=>0,'dosis'=>'4 Caps'],
            'DFH NeuroMag'=>['name'=>'DFH NeuroMag','independent'=>0,'dosis'=>'4 Caps'],
            'ATP Mind Mag'=>['name'=>'ATP Mind Mag','independent'=>0,'dosis'=>'4 Caps'],
            'Nutri-Dyn Pure Glycine'=>['name'=>'Nutri-Dyn Pure Glycine','independent'=>0,'dosis'=>'4 Caps'],
            'DFH Glycine Powder'=>['name'=>'DFH Glycine Powder','independent'=>0,'dosis'=>'2 Grams'],
        ];

        $moments = Moments::model()->findAll();
        $momentSelected = [];

        foreach ($moments as $moment) {
            if(in_array(strtolower($moment->name),['breakfast','lunch','dinner','before bed']))
                $momentSelected[strtolower($moment->name)] = $moment->id;
        }

        foreach ($countries as $country) {
            $recipe = new Recipes();
            $recipe->idHormoneFamily = $hormoneFamily->id;
            $recipe->idCountry = $country->id;
            $recipe->idMeasurmenetTypes = 10;
            $recipe->save();
            $suplementsObjs=[];

            foreach ($momentsLoad as $momentLoad) {
                $chose = new Choose();
                $chose->idRecipe = $recipe->id;
                $chose->idMoment=$momentSelected[strtolower($momentLoad['moment'])];
                $chose->save();
                foreach ($momentLoad['suplements'] as $suplement) {
                    if(!isset($suplementsObjs[$suplement]))
                    {
                        $suplementsObjs[$suplement] = $this->findSuplement($suplementos[$suplement],$country->id);
                    }
                    $choseSuplement = new ChooseSuplement();
                    $choseSuplement->idChoose=$chose->id;
                    $choseSuplement->idSuplement=$suplementsObjs[$suplement]->id;
                    $choseSuplement->quantity=$suplementos[$suplement]['dosis'];
                    $choseSuplement->save();
                }
            }
        }
	}

	public function down()
	{
        $medidas = [
            ['idealMeasure' => 5.00,'idMeasuremetType' => 10,'gender' => 'Male'],
            ['idealMeasure' => 7.60,'idMeasuremetType' => 10,'gender' => 'Female'],
            ['idealMeasure' => 7.60,'idMeasuremetType' => 2,'gender' => 'Male'],
            ['idealMeasure' => 7.70,'idMeasuremetType' => 2,'gender' => 'Female'],
        ];

        foreach ($medidas as $medida) {
            IdealMeasurement::model()->deleteAllByAttributes($medida);
        }

	    $hormoneFamily = HormoneFamily::model()->findByAttributes(['name'=>'Cortisol']);
        HormoneUserDate::model()->deleteAllByAttributes(['idHormoneFamily'=>$hormoneFamily->id]);
        $recipes = Recipes::model()->findAllByAttributes(['idHormoneFamily'=>$hormoneFamily->id]);
        foreach ($recipes as $recipe) {
//            $recipe = new Recipes();
            $choses = Choose::model()->findAllByAttributes(['idRecipe'=>$recipe->id]);
            foreach ($choses as $chose) {
//                $chose = new Choose();
                ChooseSuplement::model()->deleteAllByAttributes(['idChoose'=>$chose->id]);
                $chose->delete();
            }
            $recipe->delete();
        }
        $hormoneFamily->delete();

        $this->dropColumn('hormone_family','priority');
        $this->dropColumn('hormone_user_date','applyPriority');
	}
	public function findSuplement($infoSuplement, $idConuntry)
    {
        unset($infoSuplement['dosis']);
        $infoSuplement['idCountry']=$idConuntry;

        $retorno = Supplement::model()->findByAttributes($infoSuplement);

        if (empty($retorno))
        {
            $retorno = new Supplement();
            $retorno->setAttributes($infoSuplement);
            $retorno->save();
        }
        return $retorno;
    }
}