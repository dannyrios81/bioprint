<?php

class m160310_054140_changesMeasurement extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('measurement','measurement','decimal(10,5)');
	}

	public function down()
	{
		$this->alterColumn('measurement','measurement','integer');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}