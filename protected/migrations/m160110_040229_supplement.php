<?php

class m160110_040229_supplement extends CDbMigration
{
	public function up()
	{
		$this->createTable('supplement',array(
			'id'=>'pk',
			'name'=>'string',
			'independent'=>'boolean',
			'linkToBuy'=>'string',
			'idCountry'=>'integer'
		),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		echo "m160110_040229_supplement does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}