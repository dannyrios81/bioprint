<?php

class m180825_044014_execution_exercice_calc_add_average_intensity extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('execution_exercise_calc','averageIntensity','decimal(6,2) NOT NULL DEFAULT 0 ');
	}

	public function down()
	{
		$this->dropColumn('execution_exercise_calc','averageIntensity');
	}
}