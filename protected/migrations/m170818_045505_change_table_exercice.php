<?php

class m170818_045505_change_table_exercice extends CDbMigration
{
	public function up()
	{
	    $this->dropColumn('exercise','order');
	    $this->addColumn('users_phase','numberOfTimesDone','integer');
	}

	public function down()
	{
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}