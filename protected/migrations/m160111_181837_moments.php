<?php

class m160111_181837_moments extends CDbMigration
{
	public function up()
	{
		$this->createTable('moments',array(
			'id'=>'pk',
			'name'=>'string'
		));
		$this->renameColumn('hormone_measurement_supplement','moment','idMoments');
		$this->alterColumn('hormone_measurement_supplement','idMoments','integer');
		$this->alterColumn('hormone_measurement_supplement','choose','integer');
		//foraneas tabla hormone_measurement_supplement
		$this->addForeignKey('fk_hormoneMeasurementSupplement_idMoments','hormone_measurement_supplement','idMoments','moments','id');
	}

	public function down()
	{
		echo "m160111_181837_moments does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}