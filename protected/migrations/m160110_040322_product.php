<?php

class m160110_040322_product extends CDbMigration
{
	public function up()
	{
		$this->createTable('product',array(
            'id'=>'pk',
            'name'=>'string',
            'renewal'=>'boolean',
            'cost'=>'money',
            'linkToUserManual'=>'string'
        ),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		echo "m160110_040322_product does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}