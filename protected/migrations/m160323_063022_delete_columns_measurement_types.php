<?php

class m160323_063022_delete_columns_measurement_types extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('measurement_types','idalMeasurement');
		$this->dropColumn('measurement_types','gender');
	}

	public function down()
	{
		$this->addColumn('measurement_types','idalMeasurement','decimal(6,2)');
		$this->addColumn('measurement_types','gender','enum("Male","Female")');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}