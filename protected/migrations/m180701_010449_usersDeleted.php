<?php

class m180701_010449_usersDeleted extends CDbMigration
{
	public function up()
	{
	    $this->createTable('users_delete',array(
            'id'=>'integer',
            'email'=>'string NOT NULL',
            'deleted_date'=>'timestamp',
            'idUserDelete'=>'integer',
            'emailAdminDelete'=>'string',
            'userType'=>'enum("Admin","Trainer","User")'
        ),'ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
	}

	public function down()
	{
		$this->dropTable('users_delete');
	}
}