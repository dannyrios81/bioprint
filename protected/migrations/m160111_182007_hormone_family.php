<?php

class m160111_182007_hormone_family extends CDbMigration
{
	public function up()
	{
		$this->createTable('hormone_family',array(
			'id'=>'pk',
			'name'=>'string'
		));
		$this->renameColumn('hormone_measurement_supplement','hormone','idHormoneFamily');
		$this->alterColumn('hormone_measurement_supplement','idHormoneFamily','integer');

		//foraneas tabla hormone_measurement_supplement
		$this->addForeignKey('fk_hormoneMeasurementSupplement_idHormoneFamily','hormone_measurement_supplement','idHormoneFamily','hormone_family','id');
	}

	public function down()
	{
		echo "m160111_182007_hormone_family does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}