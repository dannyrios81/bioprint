<?php

class m160110_035540_user extends CDbMigration
{
	public function up()
	{
		$this->createTable('users',array(
				'id'=>'pk',
				'email'=>'string NOT NULL',
				'password'=>'varchar(64) NOT NULL',
				'create_record'=>'timestamp',
				'name'=>'string NOT NULL',
				'lastName'=>'string',
				'genre'=>'varchar(20) NOT NULL',
				'bornDate'=>'date NOT NULL',
				'weight'=>'integer',
				'weightUnit'=>'string',
				'height'=>'integer',
				'heightUnit'=>'string',
				'historyHealthCondition'=>'text',
				'familyHistory'=>'text',
				'currentHealthCondition'=>'text',
				'objectivesToBeAchieved'=>'text',
				'imcGoal'=>'decimal(6,2)',
				'sportsPractice'=>'text',
				'insuranceCompany'=>'string',
				'prescriptionDrugs'=>'text',
				'allergies'=>'text',
				'medicalRestrictions'=>'text',
				'idAttachment'=>'integer',
				'parentEmail'=>'string NOT NULL'
			),'ENGINE=InnoDB DEFAULT CHARSET=utf8'
		);
		$this->createIndex('Uq_users_email','users',array('email'),true);
	}

	public function down()
	{
		echo "m160110_035540_user does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}