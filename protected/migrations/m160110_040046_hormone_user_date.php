<?php

class m160110_040046_hormone_user_date extends CDbMigration
{
	public function up()
	{
		$this->createTable('hormone_user_date',array(
            'idUsers'=>'integer',
            'date'=>'date',
            'idAssociatedMeasurement1'=>'integer',
            'idAssociatedMeasurement2'=>'integer',
            'idAssociatedMeasurement3'=>'integer'
        ),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function down()
	{
		echo "m160110_040046_hormone_user_date does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}