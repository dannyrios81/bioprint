$(function() {

    var navMain = $(".navbar-header");
    //var headingTableMobile = $(".table-responsive tr:first-of-type th");
    var measuresTable = $('.measures-table');
    var currentCol = 0;
    var desp = 250;
    var switchBtn = $("#switch-btn");
    var tableVisible = $(".table-responsive.visible-xs");
    var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
    //var weights = [];

    navMain.on("click", "button", null, function() {
        $(this).toggleClass("open-hamburguer");
    });

    $('.right-arrow').click(function() {

        measuresTable.animate({
            scrollLeft: '+=' + desp
        }, 500);

    });

    $('.left-arrow').click(function() {
        measuresTable.animate({
            scrollLeft: '-=' + desp
        }, 500);

    });

    var widthTableScroll = isMobile ? $(window).width() : $(".white-border-radius").width()+20;
    if (measuresTable.length > 0) {
        measuresTable.scroll(function(event) {  
            var scrollX = measuresTable.scrollLeft();
            if (scrollX > 20) {
                $(".left-arrow").addClass("open");
                $(".left-arrow").removeClass("close");
            } else {
                $(".left-arrow").addClass("close");
                $(".left-arrow").removeClass("open");
            }
            if (scrollX > measuresTable[0].scrollWidth - widthTableScroll) {
                $(".right-arrow").addClass('close');
                $(".right-arrow").removeClass('open');
            } else {
                $(".right-arrow").addClass('open');
                $(".right-arrow").removeClass('close');
            }

        });
    }

    //Tabs for switch tables in mobile
    switchBtn.on("click", "li", function() {
        $("table[id^='mobile-']").removeClass("visible-xs").addClass("hidden");
        var currentTable = $(this).data("info");
        $("#" + currentTable).addClass("visible-xs").removeClass('hidden');
        $("#switch-btn li").removeClass('selected')
        $(this).addClass('selected');
    });

    $(window).scroll(function() {

	var tableVisible = $(".table-responsive.visible-xs");
        /* Check the End of table responsive and hide the tabs for measurement*/
        if(tableVisible.length>0)
        {
            var bottom_of_object = tableVisible.offset().top + tableVisible.outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height() - $("#switch-btn").height();
            if (bottom_of_window > bottom_of_object) {
                switchBtn.addClass('hide-bar');
            } else {
                switchBtn.removeClass('hide-bar');
            }
        }
    });

    /*$('.input-group.date').datepicker({
        autoclose: true,
        todayHighlight: true
    });*/

});
